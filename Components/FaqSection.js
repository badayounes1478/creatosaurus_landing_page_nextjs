import React, { useState } from "react";
import stylesFaq from "../styles/FaqSection.module.css";

const FrequentlyAskedQuestions = ({ title, content, id }) => {
  // state for faq
  const [isActive, setIsActive] = useState(false);

  return (
    <div className={stylesFaq.accordionItem} key={id}>
      {isActive ? (
        <div
          className={stylesFaq.accordionTitle}
          onClick={() => setIsActive(!isActive)}
          style={{ background: "rgba(248, 248, 249, 0.5)" }}
        >
          <h2 style={{ fontWeight: 600, fontSize: 18 }}>{title}</h2>
          <div className="isActive" style={{ marginLeft: "10px" }}> {isActive ? "-" : "+"}</div>
        </div>
      ) : (
        <div
          className={stylesFaq.accordionTitle}
          onClick={() => setIsActive(!isActive)}
        >
          <h2 style={{ fontWeight: 600, fontSize: 18 }}>{title}</h2>
          <div className="isActive" style={{ marginLeft: "10px" }}> {isActive ? "-" : "+"}</div>
        </div>
      )}

      {isActive && (
        <div className={stylesFaq.accordionContent}>
          <p dangerouslySetInnerHTML={{ __html: content }} />
        </div>
      )}
    </div>
  );
};

export default FrequentlyAskedQuestions;

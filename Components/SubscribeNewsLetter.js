import React, { useState } from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'

const SubscribeNewsLetter = () => {
    const [email, setemail] = useState("")
    const [loading, setLoading] = useState(false)

    const addUserToNewsLetter = async () => {
        try {
            const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (regex.test(email) === false) return toast.error("The email you've provided is invalid. Please enter a valid email address.")

            setLoading(true)
            await axios.post("https://api.app.creatosaurus.io/creatosaurus/newsletter/subscribe", {
                email,
                isSubscribed: true
            })

            toast.success("Congratulations! You have successfully subscribed to our newsletter.")
            setLoading(false)
        } catch (error) {
            toast.error("Newsletter subscription failed. Please retry or check internet and form accuracy.")
            setLoading(false)
        }
    }

    return (
        <div className='mt-[100px] bg-[#FF3850] p-[40px] flex flex-col items-center justify-center text-white rounded-[10px] mb-[100px] max-[960px]:mx-[20px] min-[960px]:mx-[80px]'>
            <span className='text-[20px] font-medium'>Subscribe</span>
            <p className='mt-[3px]'>Join our weekly digest. You&apos;ll also receive some of our best posts today.</p>
            <input className='my-[20px] h-[40px] w-full max-w-[340px] px-[12px] rounded-[10px] text-black' type="email" placeholder="Enter Your Email" value={email} onChange={(e) => setemail(e.target.value)} />
            <button onClick={addUserToNewsLetter} className='border-[1px] border-white w-[200px] h-[40px] rounded-[5px] hover:bg-white hover:text-[#FF3850] flex justify-center items-center'>
                {
                    loading ? <span className='loader' /> : "Subscribe"
                }
            </button>
        </div>
    );
}

export default SubscribeNewsLetter;
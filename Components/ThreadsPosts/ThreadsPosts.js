import React from 'react'
import styles from './ThreadsPosts.module.css';
import Image from 'next/image'
import Link from 'next/link';

const ThreadsPosts = ({ templates, count, page }) => {
    return (
        <>
            <div style={{ padding: '0px 5%', marginBottom: 10, display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 30 }}>
                <h2 style={{ fontWeight: 600, fontSize: 18 }}>286+ Threads Template</h2>
                <a style={{ textDecoration: 'none', color: '#000', fontSize: 13 }} href='https://www.muse.creatosaurus.io/editor?app=muse&width=1080&height=1350&type=threads%20from%20instagram' target='_blank' rel="noreferrer">See all</a>
            </div>
            <div className={styles.threadsPosts}>
                <a target='_blank' rel="noreferrer" style={{ textDecoration: 'none' }} href='https://www.muse.creatosaurus.io/editor?width=1080&height=1350&type=threads%20from%20instagram'>
                    <div className={styles.card}>
                        <div style={{ width: 240, height: 300, backgroundColor: 'rgba(64, 87, 109, 0.07)', borderRadius: 10, display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <svg style={{ width: 24, height: 24, marginBottom: 5 }} width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                <path fill="black" d="M17 15V6.668a1 1 0 1 0-2 0V15H6.668a1 1 0 1 0 0 2H15v8.332a1 1 0 1 0 2 0V17h8.332a1 1 0 1 0 0-2zm0 0" />
                            </svg>
                            <span style={{ color: 'black', fontSize: 14 }}>Create a blank Threads Post</span>
                        </div>
                    </div>
                </a>

                {
                    templates?.map((data) => {
                        return <a target='_blank' rel="noreferrer" style={{ textDecoration: 'none' }} key={data._id} href={`https://www.muse.creatosaurus.io/editor?width=1080&height=1350&type=threads%20from%20instagram&id=${data._id}`}>
                            <div className={styles.card}>
                                <Image width={240} height={300} draggable={false} className={styles.cover} src={data.image} alt={data.description} />
                                <div className={styles.info}>
                                    <div style={{ width: 32, height: 32, borderRadius: '50%' }}>
                                        <Image width={32} height={32} layout="responsive" draggable={false} src='https://d1vxvcbndiq6tb.cloudfront.net/fit-in/150x0/9c9eb391-c7c9-4267-adf9-bbb382e71dc8' alt='Creatosaurus' />
                                    </div>
                                    <div>
                                        <p className={styles.name}>{data.projectName}</p>
                                        <p className={styles.type}>Threads Post by Muse</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    })
                }
            </div>
            <div className={styles.ThreadsPostsNumbers}>
                {count.map(number => (
                    <Link key={number} href={`/apps/muse/templates/threads-instagram-post-design?page=${number}`}>
                        <a className={styles.page} style={number === parseInt(page) ? { backgroundColor: '#FF8A25', color: '#fff' } : null}>{number}</a>
                    </Link>
                ))}
            </div>
        </>
    )
}

export default ThreadsPosts


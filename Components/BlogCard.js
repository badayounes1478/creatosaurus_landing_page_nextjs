import React from 'react';
import stylesBlog from '../styles/BlogCard.module.css';
import Link from 'next/link';
import NavigationBar from './LandingPageComponents/NavigationBar';
import Image from 'next/image';

const BlogCard = ({ post }) => {
  return (
    <div>
      <NavigationBar />
      <div className={stylesBlog.latestBlog}>
        <Image src={post.feature_image} alt='blogpic' />
        <div className={stylesBlog.cardText}>
          <span className={stylesBlog.topic}>Published On •</span>
          <span className={stylesBlog.createdAt}>
            {post.published_at.slice(0, 10)}
          </span>
          <Link href='/blog/[slug]' as={`/blog/${post.slug}`} passHref>
            <h1 className={stylesBlog.cardTitle}>{post.title}</h1>
          </Link>
          <p className={stylesBlog.cardDesc}>{post.excerpt}</p>
        </div>
      </div>
    </div>
  );
};

export default BlogCard;

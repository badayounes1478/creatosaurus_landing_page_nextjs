import React from 'react'
import style from '../styles/ArticleByCard.module.css'
import Image from 'next/image';
import profile from '../public/Assets/profile.svg'
import Link from 'next/link';

// 'https://images.unsplash.com/photo-1536300099515-6c61b290b654'
const ArticleByCard = ({ post }) => {
    return (
        <div className={style.articleByCard}>
            <div className={style.profileImage}>
                <Image
                    src={post.primary_author.profile_image === null ? profile : post.primary_author.profile_image}
                    width={100}
                    height={100}
                    layout='responsive'
                    alt=''
                />
            </div>

            <div className={style.profileInfo}>
                <span>Article by</span>
                <Link prefetch={false} href='/blog/author/[author]' as={`/blog/author/${post.primary_author.slug}`}>
                    <a className={style.h5}>{post.primary_author.name}</a>
                </Link>

                <span className={style.bio}>{post.primary_author.bio}</span>

                <div className={style.socialMedia}>
                    {
                        post.primary_author.twitter &&
                        <div className={style.twitter} style={{ cursor: "pointer" }}>
                            <a href={`https://www.twitter.com/${post.primary_author.twitter}`} target="_blank" rel="noopener noreferrer" aria-label={`Follow ${post.primary_author.name} on Twitter`}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                                    <path d="M16.3612 7.16437C16.372 7.3102 16.372 7.4552 16.372 7.6002C16.372 12.0377 12.9945 17.151 6.82199 17.151C4.92033 17.151 3.15366 16.6002 1.66699 15.6435C1.93699 15.6744 2.19699 15.6852 2.47783 15.6852C4.04699 15.6852 5.49116 15.1552 6.64533 14.251C5.16949 14.2202 3.93283 13.2535 3.50616 11.9235C3.71366 11.9544 3.92199 11.9752 4.14033 11.9752C4.44116 11.9752 4.74366 11.9335 5.02449 11.861C3.48533 11.5494 2.33283 10.1985 2.33283 8.56686V8.5252C2.78033 8.77436 3.29949 8.9302 3.84949 8.95103C2.94533 8.34936 2.35283 7.3202 2.35283 6.15603C2.35283 5.5327 2.51866 4.96103 2.80949 4.4627C4.46199 6.49853 6.94616 7.82937 9.73116 7.9752C9.67949 7.7252 9.64783 7.46603 9.64783 7.20603C9.64783 5.35603 11.1445 3.84937 13.0045 3.84937C13.9712 3.84937 14.8437 4.25437 15.457 4.90936C16.2153 4.76353 16.942 4.4827 17.587 4.09853C17.3378 4.8777 16.807 5.5327 16.1112 5.94853C16.787 5.8752 17.442 5.68853 18.0437 5.42853C17.587 6.09353 17.0162 6.68603 16.3612 7.16437Z" fill="#404040" />
                                </svg>
                            </a>
                        </div>
                    }

                    {
                        post.primary_author.linkedin &&
                        <div className={style.linkedin} style={{ cursor: "pointer" }}>
                            <a href={`https://www.linkedin.com/${post.primary_author.linkedin}`} target="_blank" rel="noopener noreferrer" aria-label={`Follow ${post.primary_author.linkedin} on Linkedin`}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                                    <path d="M4.15244 6.49768C5.15943 6.49768 5.97577 5.68135 5.97577 4.67435C5.97577 3.66735 5.15943 2.85101 4.15244 2.85101C3.14544 2.85101 2.3291 3.66735 2.3291 4.67435C2.3291 5.68135 3.14544 6.49768 4.15244 6.49768Z" fill="#404040" />
                                    <path d="M7.69744 7.87903V17.9949H10.8383V12.9924C10.8383 11.6724 11.0866 10.394 12.7233 10.394C14.3374 10.394 14.3574 11.9032 14.3574 13.0757V17.9957H17.4999V12.4482C17.4999 9.72319 16.9133 7.62903 13.7283 7.62903C12.1991 7.62903 11.1741 8.46819 10.7549 9.26236H10.7124V7.87903H7.69744ZM2.5791 7.87903H5.72493V17.9949H2.5791V7.87903Z" fill="#404040" />
                                </svg>
                            </a>
                        </div>
                    }


                    {post.primary_author.instagram &&
                        <div className={style.instagram} style={{ cursor: "pointer" }}>
                            <a href={`https://www.instagram.com/${post.primary_author.instagram}`} target="_blank" rel="noopener noreferrer" aria-label={`Follow ${post.primary_author.instagram} on Instagram`}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                                    <path d="M17.4554 7.42086C17.4462 6.79003 17.3296 6.16419 17.1062 5.57419C16.7154 4.56669 15.9196 3.77003 14.9121 3.38003C14.3296 3.16086 13.7137 3.04336 13.0904 3.03003C12.2887 2.99419 12.0346 2.98419 9.99957 2.98419C7.96457 2.98419 7.70374 2.98419 6.9079 3.03003C6.2854 3.04336 5.66957 3.16086 5.08707 3.38003C4.07957 3.77003 3.2829 4.56669 2.8929 5.57419C2.67374 6.15669 2.5554 6.77253 2.54374 7.39503C2.5079 8.19753 2.49707 8.45169 2.49707 10.4867C2.49707 12.5217 2.49707 12.7817 2.54374 13.5784C2.55624 14.2017 2.67374 14.8167 2.8929 15.4009C3.28374 16.4075 4.07957 17.2042 5.0879 17.5942C5.6679 17.8209 6.28374 17.9492 6.90874 17.9692C7.71124 18.005 7.9654 18.0159 10.0004 18.0159C12.0354 18.0159 12.2962 18.0159 13.0921 17.9692C13.7146 17.9567 14.3304 17.8392 14.9137 17.62C15.9212 17.2292 16.7171 16.4325 17.1079 15.4259C17.3271 14.8425 17.4446 14.2275 17.4571 13.6034C17.4929 12.8017 17.5037 12.5475 17.5037 10.5117C17.5021 8.47669 17.5021 8.21836 17.4554 7.42086ZM9.99457 14.335C7.86624 14.335 6.14207 12.6109 6.14207 10.4825C6.14207 8.35419 7.86624 6.63003 9.99457 6.63003C12.1212 6.63003 13.8471 8.35419 13.8471 10.4825C13.8471 12.6109 12.1212 14.335 9.99457 14.335ZM14.0004 7.38586C13.5029 7.38586 13.1021 6.98419 13.1021 6.48753C13.1021 5.99086 13.5029 5.58919 14.0004 5.58919C14.4962 5.58919 14.8979 5.99086 14.8979 6.48753C14.8979 6.98419 14.4962 7.38586 14.0004 7.38586Z" fill="#404040" />
                                    <path d="M9.99469 12.985C11.3768 12.985 12.4972 11.8646 12.4972 10.4825C12.4972 9.10039 11.3768 7.97998 9.99469 7.97998C8.6126 7.97998 7.49219 9.10039 7.49219 10.4825C7.49219 11.8646 8.6126 12.985 9.99469 12.985Z" fill="#404040" />
                                </svg>
                            </a>
                        </div>
                    }

                    {
                        post.primary_author.facebook &&
                        <div className={style.facebook} style={{ cursor: "pointer" }}>
                            <a href={`https://www.facebook.com/${post.primary_author.facebook}`} target="_blank" rel="noopener noreferrer" aria-label={`Follow ${post.primary_author.facebook} on Facebook`}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                                    <path d="M11.1645 17.9975V11.1675H13.4687L13.8112 8.49334H11.1645V6.79C11.1645 6.01834 11.3795 5.49 12.487 5.49H13.8904V3.10584C13.2079 3.0325 12.5212 2.9975 11.8345 3C9.79785 3 8.39952 4.24334 8.39952 6.52584V8.48834H6.11035V11.1625H8.40452V17.9975H11.1645Z" fill="#404040" />
                                </svg>
                            </a>
                        </div>
                    }

                    {
                        post.primary_author.youtube &&
                        <div className={style.youtube} style={{ cursor: "pointer" }}>
                            <a href={`https://www.youtube.com/${post.primary_author.youtube}`} target="_blank" rel="noopener noreferrer" aria-label={`Follow ${post.primary_author.youtube} on Youtube`}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 20 21" fill="none">
                                    <path d="M17.9944 6.50228C17.8028 5.78728 17.2403 5.22312 16.5261 5.03062C15.2211 4.67228 10.0003 4.66645 10.0003 4.66645C10.0003 4.66645 4.78027 4.66062 3.47444 5.00312C2.77444 5.19395 2.19611 5.77062 2.00277 6.48478C1.65861 7.78978 1.65527 10.4964 1.65527 10.4964C1.65527 10.4964 1.65194 13.2164 1.99361 14.5081C2.18527 15.2223 2.74777 15.7864 3.46277 15.9789C4.78111 16.3373 9.98777 16.3431 9.98777 16.3431C9.98777 16.3431 15.2086 16.3489 16.5136 16.0073C17.2269 15.8156 17.7919 15.2523 17.9861 14.5381C18.3311 13.2339 18.3336 10.5281 18.3336 10.5281C18.3336 10.5281 18.3503 7.80728 17.9944 6.50228ZM8.33027 13.0039L8.33444 8.00395L12.6736 10.5081L8.33027 13.0039Z" fill="#404040" />
                                </svg>
                            </a>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default ArticleByCard;
import React, { useState, useEffect } from 'react'
import styles from './WaitListSpotAuth.module.css'
import { useRouter } from 'next/router'
import axios from 'axios'
import { useGoogleOneTapLogin, useGoogleLogin } from '@react-oauth/google';
import constant from '../../constant';
import LoginEmailPopup from '../Auth/LoginEmailPopup';

const WaitListSpotAuth = ({ close, planName }) => {
    const router = useRouter()
    const { query, pathname, push } = router

    const [invitationCode, setinvitationCode] = useState(null)
    const [loading, setLoading] = useState(false);
    const [redirectTo, setredirectTo] = useState(null);
    const [showLoginPopup, setShowLoginPopup] = useState(false)
    const [affiliate, setAffiliate] = useState(false)

    useEffect(() => {
        setredirectTo(query.app)
        setinvitationCode(query.ref)
        setAffiliate(query.affiliate)
    }, [query]);

    function setCookie(name, value) {
        if (process.env.NODE_ENV === 'development') {
            document.cookie = `${name}=${value}`
        } else {
            document.cookie = `${name}=${value}; domain=.creatosaurus.io`
        }
    }



    const addDataToLocalStorage = async (res, email) => {
        try {
            setCookie("Xh7ERL0G", res.data.token)

            if (pathname === "/apps/muse") return push("/apps/muse/waitlist")

            if (planName === "creator" || planName === "startup") {
                try {
                    const config = { headers: { "Authorization": `Bearer ${res.data.token}` } }
                    await axios.post("https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/add/trial/plan", {
                        planName: planName
                    }, config)
                } catch (error) {

                }
            }

            setLoading(false);
            const queryString = router.asPath.split("?")

            if (email === "mayurgaikwad859@gmail.com") {
                window.open("https://www.inbox.creatosaurus.io/", "_self");
            } else if (redirectTo === null) {
                window.open("https://www.app.creatosaurus.io/", "_self");
            } else if (redirectTo === "muse") {
                if (queryString[1] !== undefined) return window.open(`https://www.muse.creatosaurus.io/editor?${queryString[1]}`, "_self");
                window.open("https://www.muse.creatosaurus.io", "_self");
            } else if (redirectTo === "apollo") {
                window.open("https://www.apollo.creatosaurus.io", "_self");
            } else if (redirectTo === "cache") {
                window.open("https://www.cache.creatosaurus.io", "_self");
            } else if (redirectTo === "captionator") {
                window.open("https://www.captionator.creatosaurus.io/", "_self");
                const template = query.template
                if (template !== undefined) return window.open(`https://www.captionator.creatosaurus.io?${queryString[1]}`, "_self");
            } else if (redirectTo === "quotes") {
                window.open("https://www.quotes.creatosaurus.io/", "_self");
            } else if (redirectTo === "hashtags") {
                window.open("https://www.hashtags.creatosaurus.io", "_self");
            } else if (redirectTo === "steno") {
                window.open("https://www.steno.creatosaurus.io/", "_self");
            } else {
                window.open("https://www.app.creatosaurus.io/", "_self");
            }
        } catch (error) {
            setLoading(false);
        }
    }

    const loginWithGoogle = async (response, type) => {
        try {
            let token = type === "oneTap" ? response.credential : response.access_token

            setLoading(true);
            const res = await axios.post(constant.url + "login", {
                token: token,
                authType: "google",
                type: type,
                invitationCodeFromUser: invitationCode,
                affiliate:affiliate,
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            })

            setLoading(false)
            addDataToLocalStorage(res)
        } catch (error) {
            setLoading(false);
        }
    }

    useGoogleOneTapLogin({
        onSuccess: res => loginWithGoogle(res, "oneTap")
    })

    const googleLogin = useGoogleLogin({
        onSuccess: res => loginWithGoogle(res, "normal")
    })


    return (
        <div className={styles.navigationAuth} onClick={close}>
            <div onClick={(e) => e.stopPropagation()}>
                {
                    showLoginPopup ?
                        <LoginEmailPopup navAuth={true} addDataToLocalStorage={addDataToLocalStorage} closeEmailLogin={() => setShowLoginPopup(false)} /> :
                        <>
                            <div className={styles.container}>
                                <div className={styles.head}>
                                    <h1>Login to check your spot 👋🏻</h1>
                                    <svg onClick={close} width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="14" cy="14" r="14" fill="#333333" fillOpacity="0.8" />
                                        <path d="M9.66885 19.1187L8.88135 18.3312L13.2126 14L8.88135 9.66873L9.66885 8.88123L14.0001 13.2125L18.3313 8.88123L19.1188 9.66873L14.7876 14L19.1188 18.3312L18.3313 19.1187L14.0001 14.7875L9.66885 19.1187Z" fill="white" />
                                    </svg>
                                </div>
                                <p className={styles.p}>Use your Google account or email to check your spot</p>

                                <div className={styles.buttonContainer}>
                                    <button onClick={googleLogin}>
                                        {
                                            loading ?
                                                <span className={styles.loader} /> :
                                                <>
                                                    <svg width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M29.5741 13.3887H28.5001V13.3334H16.5001V18.6667H24.0354C22.9361 21.7714 19.9821 24.0001 16.5001 24.0001C12.0821 24.0001 8.50008 20.4181 8.50008 16.0001C8.50008 11.5821 12.0821 8.00008 16.5001 8.00008C18.5394 8.00008 20.3947 8.76941 21.8074 10.0261L25.5787 6.25475C23.1974 4.03541 20.0121 2.66675 16.5001 2.66675C9.13675 2.66675 3.16675 8.63675 3.16675 16.0001C3.16675 23.3634 9.13675 29.3334 16.5001 29.3334C23.8634 29.3334 29.8334 23.3634 29.8334 16.0001C29.8334 15.1061 29.7414 14.2334 29.5741 13.3887Z" fill="#FFC107" />
                                                        <path d="M4.7041 9.79408L9.08477 13.0067C10.2701 10.0721 13.1408 8.00008 16.5001 8.00008C18.5394 8.00008 20.3948 8.76941 21.8074 10.0261L25.5788 6.25475C23.1974 4.03541 20.0121 2.66675 16.5001 2.66675C11.3788 2.66675 6.93743 5.55808 4.7041 9.79408Z" fill="#FF3D00" />
                                                        <path d="M16.4999 29.3333C19.9439 29.3333 23.0732 28.0153 25.4392 25.872L21.3125 22.38C19.9739 23.394 18.3099 24 16.4999 24C13.0319 24 10.0872 21.7886 8.97788 18.7026L4.62988 22.0526C6.83655 26.3706 11.3179 29.3333 16.4999 29.3333Z" fill="#4CAF50" />
                                                        <path d="M29.574 13.3886H28.5V13.3333H16.5V18.6666H24.0353C23.5073 20.1579 22.548 21.4439 21.3107 22.3806L21.3127 22.3793L25.4393 25.8713C25.1473 26.1366 29.8333 22.6666 29.8333 15.9999C29.8333 15.1059 29.7413 14.2333 29.574 13.3886Z" fill="#1976D2" />
                                                    </svg>
                                                    <span>Continue with Google</span>
                                                </>
                                        }
                                    </button>

                                    <button onClick={() => setShowLoginPopup(true)}>
                                        <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.45825 28.3334C4.89159 28.3334 4.39575 28.1209 3.97075 27.6959C3.54575 27.2709 3.33325 26.7751 3.33325 26.2084V7.79175C3.33325 7.22508 3.54575 6.72925 3.97075 6.30425C4.39575 5.87925 4.89159 5.66675 5.45825 5.66675H29.5416C30.1083 5.66675 30.6041 5.87925 31.0291 6.30425C31.4541 6.72925 31.6666 7.22508 31.6666 7.79175V26.2084C31.6666 26.7751 31.4541 27.2709 31.0291 27.6959C30.6041 28.1209 30.1083 28.3334 29.5416 28.3334H5.45825ZM17.4999 17.6376L5.45825 9.73966V26.2084H29.5416V9.73966L17.4999 17.6376ZM17.4999 15.5126L29.3999 7.79175H5.63534L17.4999 15.5126ZM5.45825 9.73966V7.79175V26.2084V9.73966Z" fill="black" />
                                        </svg>
                                        <span>Continue with Email</span>
                                    </button>
                                </div>
                                <p className={styles.p1}>By continuing, you agree to Creatosaurus&apos;s <a href='https://www.creatosaurus.io/terms' rel="noreferrer" target="_blank">Terms of Use</a>. Read our <a href="https://www.creatosaurus.io/privacy" rel="noreferrer" target="_blank">Privacy Policy</a>.</p>
                            </div>
                        </>
                }
            </div>
        </div>
    )
}

export default WaitListSpotAuth
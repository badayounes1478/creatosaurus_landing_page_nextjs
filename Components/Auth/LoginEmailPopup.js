import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import constant from '../../constant'
import { toast } from 'react-toastify'
import Poster from '../../public/NewAssets/Home/poster.webp'
import Image from 'next/image';
import jwtDecode from 'jwt-decode'
import Link from 'next/link'

const LoginEmailPopup = ({ closeEmailLogin, addDataToLocalStorage, fromPage, invitationCode, affiliate, appSumoCode, changeUserLoggedIn }) => {
    const [email, setEmail] = useState("")
    const [code, setCode] = useState("")
    const [loading, setLoading] = useState(false)
    const [resendCodeLoading, setresendCodeLoading] = useState(false)
    const [buttonActive, setButtonActive] = useState(false)
    const [emailError, setEmailError] = useState(false)

    const [emailPopup, setEmailPopup] = useState(true)
    const [isNewUser, setIsNewUser] = useState(null)
    const [timer, setTimer] = useState(59)
    const [intervalId, setIntervalId] = useState("")

    const [showOnBoarding, setShowOnBoarding] = useState(false)
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [token, setToken] = useState("")

    const router = useRouter()
    const { query, pathname } = router

    useEffect(() => {
        if (query.source === "appsumo") {
            checkEmail("appsumo")
        }
    }, [])


    const startInterval = () => {
        const id = setInterval(() => {
            setTimer((prev) => prev - 1)
        }, 1000);
        setIntervalId(id)
    }

    if (timer <= 1) {
        clearInterval(intervalId)
    }

    const checkEmail = (e) => {
        let value
        if (query.source === "appsumo") {
            value = query.email
            setEmail(value)
        } else {
            value = e.target.value
            setEmail(value)
        }

        if (new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/).test(value)) {
            setButtonActive(true)
        } else {
            setButtonActive(false)
        }
    }

    const sendEmailToUser = async () => {
        try {
            if (!buttonActive) return toast.error("Please add valid email")

            setLoading(true)
            setEmailError(false)
            const res = await axios.post(`${constant.url}send/user/email`, {
                email: email,
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            })

            setLoading(false)
            setEmailPopup(false)
            setIsNewUser(res.data.isUserNew)
            startInterval()
        } catch (error) {
            setLoading(false)
            if (error?.response?.status === 400) {
                setEmailError(true);
            }
        }
    }

    const addCode = (e) => {
        setCode(e.target.value)
    }

    const goBack = () => {
        if (emailPopup) {
            closeEmailLogin()
        } else {
            setEmailPopup(true)
            setTimer(59)
            setCode("")
            clearInterval(intervalId)
        }
    }

    const verifyCode = async () => {
        try {
            setLoading(true)
            const res = await axios.post(constant.url + "verify/user/code", {
                email: email,
                code: code,
                invitationCodeFromUser: invitationCode,
                affiliate: pathname === "/affiliate" ? "yes" : affiliate,
                appSumoCode: appSumoCode === undefined || appSumoCode === null ? "" : appSumoCode
            })

            setLoading(false)
            if (isNewUser) {
                const avoidRedirect = true
                setShowOnBoarding(true)
                setToken(res.data.token)
                addDataToLocalStorage(res, email, avoidRedirect)
            } else {
                addDataToLocalStorage(res, email)
            }
        } catch (error) {
            toast.error(error.response.data.error)
            setLoading(false)
        }
    }

    const sendCode = async () => {
        try {
            setresendCodeLoading(true)
            await axios.post(constant.url + "resend/verify/email", {
                email: email
            })
            setresendCodeLoading(false)
            setTimer(59)
            setTimeout(() => {
                startInterval()
            }, 1000);
            toast.success("A new verification code has been sent to your email address")
        } catch (error) {
            setresendCodeLoading(false)
        }
    }

    const saveFirstNameLastName = async () => {
        try {
            setLoading(true)
            const decoded = jwtDecode(token);
            await axios.put(constant.url + '/user/updateUserDetails', {
                id: decoded.id,
                firstName: firstName,
                lastName: lastName,
                onBoarding: true
            });

            setLoading(false)

            if (pathname === "/pricing") {
                changeUserLoggedIn()
            } else {
                window.open("https://www.app.creatosaurus.io/?new=true", "_self");
            }
        } catch (error) {
            setLoading(false)
            window.open("https://www.app.creatosaurus.io/?new=true", "_self");
        }
    }

    return (
        <div
            className={`flex bg-white ${fromPage ? "w-full" : "w-[80vw] max-w-[814px]"} rounded-[10px] relative overflow-hidden`}
            onClick={(e) => e.stopPropagation()}>

            {
                fromPage ? null :
                    <div className='hidden lg:flex w-[407px] min-w-[407px] h-[560px] object-contain'>
                        <Image src={Poster} alt='' />
                    </div>
            }

            <div className={`${fromPage ? "w-full" : "w-full px-[30px] py-[40px]"}`}>
                {
                    showOnBoarding ? <>
                        <span className='text-[22px] font-semibold'>Create your account</span>
                        <p className='text-[14px] text-[#333333] mt-[15px]'>You’re creating a Creatosaurus account with {email}</p>
                        <div className='mt-[30px]'>
                            <span className='text-[14px] font-medium text-[#333333]'>Enter First & Last Name</span>
                            <div className='flex justify-between gap-x-[20px]'>
                                <input
                                    className='bg-[#FAFAFA] border-[1px] border-[#DCDCDC] h-[50px] rounded-[10px] w-full px-[10px] mt-[5px] hover:border-black'
                                    value={firstName}
                                    type='text'
                                    placeholder="first name"
                                    onChange={(e) => setFirstName(e.target.value)} />

                                <input
                                    className='bg-[#FAFAFA] border-[1px] border-[#DCDCDC] h-[50px] rounded-[10px] w-full px-[10px] mt-[5px] hover:border-black'
                                    value={lastName}
                                    type='text'
                                    placeholder="last name"
                                    onChange={(e) => setLastName(e.target.value)} />
                            </div>
                        </div>

                        <button
                            onClick={saveFirstNameLastName}
                            className='flex justify-center items-center bg-black border-[1px] border-[#DCDCDC] mt-[30px] text-[16px] text-white font-semibold h-[50px] w-full rounded-[10px]'>
                            {
                                loading ?
                                    <div role="status">
                                        <svg aria-hidden="true" className="size-[24px] text-white animate-spin fill-[#ff4359]" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                        </svg>
                                        <span className="sr-only">Loading...</span>
                                    </div> : "Continue"
                            }
                        </button>
                    </> : emailPopup ?
                        <>
                            <div className='flex items-center w-full'>
                                <svg onClick={goBack} className='cursor-pointer group' width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect className='group-hover:fill-[#DCDCDC]' width="26" height="26" rx="13" fill="#F8F8F9" />
                                    <path d="M14.6956 8.47803L10.1738 12.9998L14.6956 17.5215" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <span className='text-[22px] font-semibold ml-[10px]'>Continue with email</span>
                            </div>
                            <p className='text-[14px] text-[#333333] mt-[15px]'>Use your Google account or email to continue with Creatosaurus (It&apos;s 100% free, no credit card is required!)</p>

                            {
                                emailError &&
                                <p className='text-[12px] p-[10px] rounded-[5px] mt-[15px] bg-[#FCE4E4]'>Invalid email or temporary address detected. Please check the format and use a permanent email.&nbsp;
                                    <span className='underline'>
                                        <Link href="/contact">Contact us</Link>
                                    </span>
                                    &nbsp;if the issue persists.
                                </p>
                            }

                            <div className='mt-[30px]'>
                                <span className='text-[14px] font-medium text-[#333333]'>Enter Email (personal or work)</span>
                                <input
                                    className='bg-[#FAFAFA] border-[1px] border-[#DCDCDC] h-[50px] rounded-[10px] w-full px-[10px] mt-[5px] hover:border-black'
                                    value={email}
                                    type='email'
                                    placeholder="david@example.com"
                                    onChange={checkEmail} />
                            </div>

                            <button
                                onClick={sendEmailToUser}
                                style={buttonActive ? null : { backgroundColor: '#F8F8F9', color: '#C4C4C4' }}
                                className='flex justify-center items-center bg-black border-[1px] border-[#DCDCDC] mt-[30px] text-[16px] text-white font-semibold h-[50px] w-full rounded-[10px]'>
                                {
                                    loading ?
                                        <div role="status">
                                            <svg aria-hidden="true" className="size-[24px] text-white animate-spin fill-[#ff4359]" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                            </svg>
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                        : "Continue"
                                }
                            </button>
                        </>
                        :
                        <>
                            <div className='flex items-center'>
                                <svg onClick={goBack} className='cursor-pointer group' width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect className='group-hover:fill-[#DCDCDC]' width="26" height="26" rx="13" fill="#F8F8F9" />
                                    <path d="M14.6956 8.47803L10.1738 12.9998L14.6956 17.5215" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <span className='text-[22px] font-semibold ml-[10px]'>{isNewUser ? "You're almost signed up" : "Finish logging in"}</span>
                            </div>

                            <p className='text-[14px] text-[#333333] mt-[15px]'>
                                {
                                    isNewUser ?
                                        `Once you enter the code we sent to ${email}, you'll be finish signing up` :
                                        `Once you enter the code we sent to ${email}, you'll be all logged in.`
                                }
                            </p>

                            <div className='mt-[30px]'>
                                <span className='text-[14px] font-medium text-[#333333]'>Enter Code</span>
                                <input
                                    className='bg-[#FAFAFA] border-[1px] border-[#DCDCDC] h-[50px] rounded-[10px] w-full px-[10px] mt-[5px] hover:border-black'
                                    value={code}
                                    type='text'
                                    placeholder="code"
                                    onChange={addCode} />
                            </div>

                            <button
                                onClick={verifyCode}
                                style={buttonActive ? null : { backgroundColor: '#F8F8F9', color: '#C4C4C4' }}
                                className='flex justify-center items-center bg-black border-[1px] border-[#DCDCDC] mt-[30px] text-[16px] text-white font-semibold h-[50px] w-full rounded-[10px]'>
                                {
                                    loading ?
                                        <div role="status">
                                            <svg aria-hidden="true" className="size-[24px] text-white animate-spin fill-[#ff4359]" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                            </svg>
                                            <span className="sr-only">Loading...</span>
                                        </div> : isNewUser ? "Sign up" : "Log in"
                                }
                            </button>


                            <div className='text-[14px] text-[#808080] mt-[20px]'>
                                {
                                    timer === 1 ?
                                        <>
                                            {
                                                resendCodeLoading ?
                                                    <div role="status">
                                                        <svg aria-hidden="true" className="size-[20px] text-white animate-spin fill-[#ff4359]" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                                        </svg>
                                                        <span className="sr-only">Loading...</span>
                                                    </div> :
                                                    <div>
                                                        Didn&apos;t get the code?&nbsp;
                                                        <span onClick={sendCode} className="hover:underline text-black cursor-pointer">Resend code</span>
                                                    </div>
                                            }
                                        </>
                                        :
                                        <span>Didn&apos;t get the code? Resend in {timer} seconds</span>
                                }
                            </div>
                        </>
                }
            </div>
        </div>
    )
}

export default LoginEmailPopup
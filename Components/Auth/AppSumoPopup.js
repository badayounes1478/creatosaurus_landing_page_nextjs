import React from 'react'

const AppSumoPopup = ({ setAppSumoCode, appSumoCode, goBack, setshowLogin, setIsAppSumoPopupActive }) => {
    return (
        <div className={`${styles.loginEmailPopupContainer} ${styles.loginEmailPopup}`}>
            <div className={styles.card}>
                <div className={styles.head}>
                    <svg onClick={goBack} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.5 12H6M6 12L12 6M6 12L12 18" stroke="black" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                    <h2>Continue with App Sumo Code</h2>
                </div>
                <p>
                    Upgrade to the new plan by entering your App Sumo coupon code.
                </p>

                <div className={styles.inputContainer}>
                    <label>Enter App Sumo Code</label>
                    <input
                        value={appSumoCode}
                        type="text"
                        placeholder="App Sumo Code"
                        onChange={(e) => {
                            setAppSumoCode(e.target.value)
                        }}
                    />
                </div>

                <button className={styles.active} onClick={() => {
                    setIsAppSumoPopupActive(false)
                    setshowLogin(true)
                }}>
                    Continue
                </button>
            </div>
        </div >
    );
}

export default AppSumoPopup;
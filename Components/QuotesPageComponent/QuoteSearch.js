import React from 'react'
import style from '../../styles/Quotes.module.css'
import banner from '../../public/Assets/headerimage.png';
import Image from 'next/image';

const QuoteSearch = ({ onFormSubmit, handleChange, search }) => {
    return (
        <div className={style.headerContainer}>
            <span>Get Inspired</span>
            <div className={style.inputContainer}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path d="M15.5 17.5L19 21" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M5 13C5 16.3137 7.68629 19 11 19C12.6597 19 14.1621 18.3261 15.2483 17.237C16.3308 16.1517 17 14.654 17 13C17 9.68629 14.3137 7 11 7C7.68629 7 5 9.68629 5 13Z" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
                <input type='search' placeholder='Search from 1 million quotes by keywords, authors, books and more' onChange={handleChange} onKeyDown={(event) => {
                    if (event.key === 'Enter') {
                        onFormSubmit(event);
                    }
                }} />
            </div>

            <div className={style.bannerContainer} style={search !== "" ? { display: "none" } : null}>
                <Image src={banner} />
            </div>

            <div className={style.goto} style={search !== "" ? { display: "none" } : null}>
                <a href="#author-topics">
                    See all authors and topics
                </a>
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                    <path fillRule="evenodd" clipRule="evenodd" d="M12.5307 7.71975C12.6713 7.8604 12.7503 8.05113 12.7503 8.25C12.7503 8.44887 12.6713 8.6396 12.5307 8.78025L8.03068 13.2803C7.89003 13.4209 7.6993 13.4998 7.50043 13.4998C7.30155 13.4998 7.11082 13.4209 6.97018 13.2803L2.47018 8.78025C2.39854 8.71106 2.34141 8.62831 2.3021 8.5368C2.26279 8.4453 2.2421 8.34689 2.24124 8.2473C2.24037 8.14772 2.25935 8.04896 2.29706 7.95678C2.33477 7.86461 2.39046 7.78087 2.46088 7.71045C2.5313 7.64003 2.61504 7.58434 2.70721 7.54663C2.79938 7.50892 2.89814 7.48995 2.99773 7.49081C3.09731 7.49168 3.19573 7.51237 3.28723 7.55167C3.37873 7.59098 3.46149 7.64812 3.53068 7.71975L6.75043 10.9395V2.25C6.75043 2.05109 6.82945 1.86032 6.9701 1.71967C7.11075 1.57902 7.30152 1.5 7.50043 1.5C7.69934 1.5 7.89011 1.57902 8.03076 1.71967C8.17141 1.86032 8.25043 2.05109 8.25043 2.25V10.9395L11.4702 7.71975C11.6108 7.57915 11.8016 7.50016 12.0004 7.50016C12.1993 7.50016 12.39 7.57915 12.5307 7.71975Z" fill="#0078FF" />
                </svg>
            </div>
        </div>
    );
}

export default QuoteSearch;
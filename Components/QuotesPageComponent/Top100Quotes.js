import React from 'react'
import style from '../../styles/Quotes.module.css'
import Image from 'next/image';
import Rating from '../../public/Assets/Rating.svg';

const Top100Quotes = () => {
    return (
        <div className={style.quoteOfDayContainer}>
            <div className={style.ratingContainer}>
                <Image src={Rating} />
            </div>
            <p>“Made by people who value financial well-being (like me) and care
                about citizens success and happiness.”</p>
            <div className={style.goto}>
                <a href='https://www.creatosaurus.io/apps/quotes/top100quotes' target='_blank' rel="noreferrer">Read top 100 quotes</a>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path d="M1.71387 12H20.2139M20.2139 12L14.2139 6M20.2139 12L14.2139 18" stroke="#0078FF" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </div>
        </div>
    );
}

export default Top100Quotes;
import React from 'react'
import style from '../../styles/Quotes.module.css'
import Link from 'next/link';


const QuotesCategory = ({ quotesType, slugify }) => {
    return (
        <div className={style.quotesTypeContainer} id='author-topics'>
            {quotesType.map((data, index) => {
                return <div className={style.quotesType} key={index}>
                    <div className={style.quotesHeader}>
                        <h1>{data.heading}</h1>
                        <button>See All</button>
                    </div>

                    <div className={style.typeGridContainer} >
                        {data.type.map((type, i) => {
                            const quoteSlug = slugify(type);
                            return <Link
                                href={{
                                    pathname: '/apps/quotes/authors/[slug]',
                                    query: type,
                                }}
                                as={`/apps/quotes/authors/${quoteSlug}-quotes`}
                                key={index}
                                passHref
                            >
                                <div className={style.gridItem} key={i}>
                                    <span>{type}</span>
                                </div>
                            </Link>
                        })}
                    </div>
                </div>
            })}
        </div>
    );
}

export default QuotesCategory;
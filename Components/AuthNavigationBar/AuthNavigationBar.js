import React from 'react'
import Logo from '../../public/Assets/Logo.svg';
import Image from 'next/image';
import styles from './AuthNavigationBar.module.css'
import { useRouter } from 'next/router'

const AuthNavigationBar = (props) => {
    const router = useRouter()
    
    return (
        <div className={styles.authNavigationBar}>
            <div className={styles.logoContainer} style={{ cursor: 'pointer' }}>
                <div onClick={() => router.push("/")}>
                    <Image src={Logo} alt="" />
                </div>
                <span className={styles.bold}>CREATOSAURUS</span>
                <span className={styles.thin}>&nbsp;| {props.title}</span>
            </div>
        </div>
    )
}

export default AuthNavigationBar
import React, { useState, useRef } from "react";
import stylesPills from "../LandingPageComponentCss/Pills.module.css";

const Pills = () => {
  const [redFlip, setRedFlip] = useState(false);
  const [blueFlip, setBlueFlip] = useState(false);

  const redPill = () => {
    return (
      <div>
        {redFlip ? (
          <div className={stylesPills.realityCard}>
            <h1>See the reality</h1>
            <p>
              There is a world where creators come together, collaborate and
              tell stories that matter. Join our Facebook group now.
            </p>
            <div className={stylesPills.emailContainer}>
              <input type="text" placeholder="Enter your email" />
              <button>Join now</button>
              <h2>Zero spam. Leave at any time.</h2>
            </div>
          </div>
        ) : (
          <div className={stylesPills.box} onClick={() => setRedFlip(true)}>
            <span>I’ll take the red pill.</span>
          </div>
        )}
      </div>
    );
  };

  const bluePill = () => {
    return (
      <div>
        {blueFlip ? (
          <div className={stylesPills.matrixCard}>
            <h1>Stay in the matrix</h1>
            <p>
              To help you navigate this world, we have created the best
              resources for creators to stay ahead. Subscribe newsletter now.
            </p>
            <div className={stylesPills.emailContainer}>
              <input type="text" placeholder="Enter your email" />
              <button>Sign up</button>
              <h2>Zero spam. Unsubscribe at any time.</h2>
            </div>
          </div>
        ) : (
          <div
            className={`${stylesPills.box} ${stylesPills.box1}`}
            onClick={() => setBlueFlip(true)}
          >
            <span>I’ll take the blue pill.</span>
          </div>
        )}
      </div>
    );
  };
  return (
    <div className={stylesPills.pillsContainer}>
      <h1>All we are offering is the truth, nothing more.</h1>
      <p>This is your last chance. After this, there is no turning back.</p>
      <div className={stylesPills.cardContainer}>
        {redPill()}
        {bluePill()}
      </div>
    </div>
  );
};

export default Pills;

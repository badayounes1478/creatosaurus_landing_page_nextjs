import React from 'react';
import style from '../LandingPageComponentCss/ThirdBlackSection.module.css';

const ThirdBlackSection = () => {
  return (
    <div className={style.thirdBlackSectionContainer}>
      <h1>All in One Creative & Marketing Platform</h1>
      <h5>Tired of juggling between multiple tools and integrations? Streamline & optimize your entire process.</h5>

      <div className={style.grid}>
        <div className={`${style.box} ${style.box1}`}>
          <div className={style.box2}>
            <h4>Curate</h4>
            <p>Curate ideas that are <br /> engaging for your audience</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box3}`}>
          <div className={style.box2}>
            <h4>Create</h4>
            <p>Create your content <br /> around your stories</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box4}`}>
          <div className={style.box2}>
            <h4>Distribute</h4>
            <p>Distribute your content <br /> across the Internet</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box5}`}>
          <div className={style.box2}>
            <h4>Collaborate</h4>
            <p>Collaborate and bring <br /> your ideas together</p>
          </div>
        </div>

        <div className={`${style.box} ${style.hiden}`} />

        <div className={`${style.box} ${style.box6}`}>
          <div className={style.box2}>
            <h4>Manage</h4>
            <p>Effortlessly manage your <br /> storytelling process</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box7}`}>
          <div className={style.box2}>
            <h4>Analyse</h4>
            <p>Analyze the performance <br /> of your content</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box8}`}>
          <div className={style.box2}>
            <h4>Advertise</h4>
            <p>Boost your organic reach <br /> with promotions</p>
          </div>
        </div>

        <div className={`${style.box} ${style.box9}`}>
          <div className={style.box2}>
            <h4>Monetize</h4>
            <p>Monetize your content <br /> while having fun</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ThirdBlackSection;
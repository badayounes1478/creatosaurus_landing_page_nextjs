import React from 'react';
import stylesFeatures from '../LandingPageComponentCss/Features.module.css';

const Features = () => {
  const data = [
    {
      emoji: '✍🏻',
      title: 'AI Content Writer',
      description:
        'Create marketing copies in a matter of minutes with our AI copy generator.',
    },
    {
      emoji: '🖼',
      title: 'Create Beautiful Graphics', 
      description:
        'Our easy-to-use comprehensive design editor lets you create amazing designs.',
    },
    {
      emoji: '📂',
      title: 'Upload & Organise',
      description:
        'Access your assets across the platform by uploading and organizing them.',
    },
    {
      emoji: '#️⃣',
      title: 'Hashtag Manager',
      description:
        'Find & manage your hashtags to optimize your social media workflow.',
    },
    {
      emoji: '🔍',
      title: 'Quote Finder',
      description:
        'Search from more than a million quotes and save them for later use.',
    },
    {
      emoji: '📅',
      title: 'Schedule Your Post',
      description:
        'Manage your social media accounts and save time by scheduling posts.',
    },
    {
      emoji: '🗺',
      title: 'Templates',
      description:
        'Thousands of visually appealing graphic templates for your creativity.',
    },
    {
      emoji: '🏞',
      title: 'Free Stock Assets',
      description:
        'Take advantage of millions of copyright-free stock assets to stand out.',
    },
    {
      emoji: '📊',
      title: 'Manage & Analyse',
      description:
        'Assess and improve your account data and content performance.',
    },
  ];

  return (
    <section className={stylesFeatures.features}>
      <h1>Features that you ❤️ in one place</h1>
      <div className={stylesFeatures.grid}>
        {data.map((data, index) => {
          return (
            <div className={stylesFeatures.card} key={index}>
              <span>{data.emoji}</span>
              <h3>{data.title}</h3>
              <p>{data.description}</p>
            </div>
          );
        })}
      </div>
    </section>
  );
};

export default Features;

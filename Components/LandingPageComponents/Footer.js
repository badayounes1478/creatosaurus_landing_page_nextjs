import React, { useState } from 'react';
import Logo from '../../public/Assets/Logo.svg';
import Twitter from '../../public/Assets/Twitter.svg';
import Instagram from '../../public/Assets/Instagram.svg';
import Facebook from '../../public/Assets/Facebook.svg';
import LinkedIn from '../../public/Assets/LinkedIn.svg';
import YouTube from '../../public/Assets/YouTube.svg';
import stylesFooter from '../LandingPageComponentCss/Footer.module.css';
import Image from 'next/image';
import Link from 'next/link';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const Footer = () => {
  const [showLogin, setshowLogin] = useState(false)

  return (
    <>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <div className={stylesFooter.footer}>
        <div className={stylesFooter.grid}>
          <div className={stylesFooter.list}>
            <div className={stylesFooter.row}>
              <div className={stylesFooter.logo}>
                <Image src={Logo} alt="creatosaurus" />
              </div>
              <div className={stylesFooter.titleFooter}>Creatosaurus</div>
            </div>
            <p>All in One Creative & Marketing Platform to tell stories at scale</p>
            <div className={stylesFooter.imagesContainer}>
              <div className={stylesFooter.img}>
                <Link href='https://twitter.com/creatosaurushq' passHref>
                  <a target="_blank" rel="noopener noreferrer" aria-label="follow us on twitter">
                    <Image src={Twitter} alt='twitter' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.instagram.com/creatosaurus/' passHref>
                  <a target="_blank" rel="noopener noreferrer" aria-label="follow us on instagram">
                    <Image src={Instagram} alt='instagram' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.facebook.com/creatosaurushq/' passHref>
                  <Image src={Facebook} alt='facebook' />
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.linkedin.com/company/creatosaurushq/' passHref>
                  <a target="_blank" rel="noopener noreferrer" aria-label="follow us on linkedin">
                    <Image src={LinkedIn} alt='linkedin' />
                  </a>
                </Link>
              </div>

              <div className={stylesFooter.img}>
                <Link href='https://www.youtube.com/channel/UCSUwLNb8lt8OwbnLIFlhriA' passHref>
                  <a target="_blank" rel="noopener noreferrer" aria-label="follow us on youtube">
                    <Image src={YouTube} alt='youtube' />
                  </a>
                </Link>
              </div>
            </div>
          </div>

          <div className={stylesFooter.list}>
            <div className={stylesFooter.titleFooter}>Company</div>
            <Link href='/about' passHref prefetch={false}>About </Link>
            <Link href='https://www.linkedin.com/company/creatosaurushq/' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Careers">
                Careers
              </a>
            </Link>
            <Link href='/contact' passHref prefetch={false}>Contact</Link>
          </div>

          <div className={stylesFooter.list}>
            <div className={stylesFooter.titleFooter}>Products</div>
            <Link href='/features' prefetch={false}>Features</Link>
            <Link href='/pricing' prefetch={false}>Pricing</Link>
            <Link href='https://www.youtube.com/watch?v=eS5tpAUEuzA'>
              <a target="_blank" rel="noopener noreferrer" aria-label="Video Tutorial">
                Video Tutorial
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/feature-requests'>
              <a target="_blank" rel="noopener noreferrer" aria-label="Request Feature">
                Request Feature
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/bug-board'>
              <a target="_blank" rel="noopener noreferrer" aria-label="Product Feedback">
                Product Feedback
              </a>
            </Link>
          </div>

          <div className={stylesFooter.list}>
            <div className={stylesFooter.titleFooter}>Resources</div>
            <Link href='/blog' prefetch={false}>Blog</Link>
            <Link href='https://www.facebook.com/groups/creatosaurus/' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Facebook Community">
                Facebook Community
              </a>
            </Link>
            <Link href='/blog' prefetch={false}>Blog</Link>
            <Link href="/affiliate" prefetch={false}>Affiliate</Link>
            <span style={{ cursor: 'pointer' }} onClick={() => setshowLogin(true)}>Sign up</span>
            <span style={{ cursor: 'pointer' }} onClick={() => setshowLogin(true)}>Log in</span>
          </div>

          <div className={stylesFooter.list}>
            <div className={stylesFooter.titleFooter}>Support</div>
            <Link href='https://creatosaurus.tawk.help/' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Help Center">
                Help Center
              </a>
            </Link>
            <Link href='https://wa.me/919665047289' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="WhatsApp Support">
                WhatsApp Support
              </a>
            </Link>
            <Link href='https://twitter.com/intent/tweet?text=Hey%20Creatosaurus&hashtags=StoriesMatter&via=creatosaurushq%20%F0%9F%99%8C%F0%9F%8F%BB%F0%9F%A6%96' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Tweet at us">
                Tweet at us
              </a>
            </Link>
            <Link href='https://calendly.com/malavwarke/creatosaurus' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Book Demo">
                Book Demo
              </a>
            </Link>
            <Link href='https://creatosaurus.instatus.com/' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Status">
                Status
              </a>
            </Link>
            <Link href='https://creatosaurus.canny.io/' passHref>
              <a target="_blank" rel="noopener noreferrer" aria-label="Roadmap">
                Roadmap
              </a>
            </Link>
          </div>
        </div>

        <div className={stylesFooter.footerBottom}>
          <div className={stylesFooter.subPart}>
            <div>Made with ❤️ for Creators</div>
            <Link href='/terms' passHref prefetch={false}>Terms of Service</Link>
            <Link href='/privacy' passHref prefetch={false}>Privacy Policy</Link>
            <Link href='/refund-policy' passHref prefetch={false}>Refund Policy</Link>
          </div>
          <span>© 2025 Creatosaurus. All rights reserved by Creatosaurus</span>
        </div>
      </div>
    </>
  );
};

export default Footer;
import React, { useState } from 'react';
import stylesFifthBlack from '../LandingPageComponentCss/FifthBlackSection.module.css';
import NavigationAuth from '../NavigationAuth/NavigationAuth';
import WaitlistJoinAuth from '../WaitlistJoinAuth/WaitlistJoinAuth';

const FifthBlackSection = ({ text, button }) => {
  const [showLogin, setshowLogin] = useState(false)
  return (
    <>
      {showLogin ? button === undefined ? <NavigationAuth close={() => setshowLogin(false)} /> : <WaitlistJoinAuth close={() => setshowLogin(false)} /> : null}
      <div className={stylesFifthBlack.fifthBlackSection}>
        <p>
          {
            text === undefined ?
              "You focus on telling stories,we do everything else." :
              text
          }
        </p>
        <button style={{ cursor: 'pointer' }} onClick={() => setshowLogin(true)}>
          {
            button === undefined ? "Signup for Free" : button
          }
        </button>
      </div>
    </>
  );
};

export default FifthBlackSection;

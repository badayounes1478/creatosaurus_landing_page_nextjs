import React, { useState } from 'react';
import styles from '../LandingPageComponentCss/AppsCardBlock.module.css';
import Image from "next/image";
import Cache from "../../public/Assets/Apps/cache.png";
import Captionater from "../../public/Assets/Apps/captionater.png";
import Muse from "../../public/Assets/Apps/muse.png";
import Hashtag from "../../public/Assets/Apps/hashtag.png";
import Quotes from "../../public/Assets/Apps/quotes.png";
import Noice from "../../public/Assets/Apps/noise.png";
import Appolo from "../../public/Assets/Apps/apollo.png";
import Steno from "../../public/Assets/Apps/Steno.jpg";
import Link from 'next/link';
import NavigationAuth from '../NavigationAuth/NavigationAuth';

const AppsCardBlock = () => {

    const data = [
        {
            title: "Cache",
            desc: "Manage and schedule posts",
            image: Cache,
            subTitle: "Schedule Post",
            color: "#0078FF",
            link: "https://cache.creatosaurus.io",
        },
        {
            title: "Muse",
            desc: "Design graphics with templates",
            image: Muse,
            subTitle: "Design Graphics",
            color: "#FF8A25",
            link: "https://muse.creatosaurus.io",
        },
        {
            title: "Captionator",
            desc: "Generate content with AI writer",
            image: Captionater,
            subTitle: "AI Writer",
            color: "#00ECC2",
            link: "https://captionator.creatosaurus.io",
        },
        {
            title: "Steno",
            desc: "Easy text editor & AI Blog Writer",
            image: Steno,
            subTitle: "Text Editor",
            color: "#1410C7",
            link: "https://steno.creatosaurus.io",
        },
        /*{
            title: "Apollo",
            desc: "Easy video editor (coming soon)",
            image: Appolo,
            subTitle: "Video Editor",
            color: "#4267B2",
            link: "",
        },*/
        {
            title: "Quotes",
            desc: "Search & save quotes, authors and more",
            image: Quotes,
            subTitle: "Curate Quotes",
            color: "#FFD75F",
            link: "https://quotes.creatosaurus.io",
        },
        {
            title: "#Tags",
            desc: "Create & manage hashtag groups",
            image: Hashtag,
            subTitle: "Hashtag Manager",
            color: "#FF43CA",
            link: "https://hashtags.creatosaurus.io",
        },
        {
            title: "Noice",
            desc: 'CC0 assets that will make you say "Nice!"',
            image: Noice,
            subTitle: "Free Stock Assets",
            color: "#009B77",
            link: "https://www.noice.creatosaurus.io/",
        }
    ];

    const [showLogin, setshowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
            <div className={styles.appsCardBlock}>
                <h1>Powerful apps for your stories.</h1>
                <p>The world&apos;s biggest influencers, creators, publishers, startups and brands use Creatosaurus in their marketing strategy.</p>
                <div className={styles.inputContainer}>
                    <button onClick={() => setshowLogin(true)}>Get Started For Free</button>
                </div>

                <div className={styles.head}>
                    <div>
                        <svg
                            width="20"
                            height="20"
                            viewBox="0 0 20 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M12.5 5L7.5 10L12.5 15"
                                stroke="black"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            />
                        </svg>
                        <svg
                            width="20"
                            height="20"
                            viewBox="0 0 20 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M7.5 5L12.5 10L7.5 15"
                                stroke="black"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            />
                        </svg>
                    </div>
                    <Link href='/apps' passHref>See all</Link>
                </div>
                <div className={styles.grid}>
                    {
                        data.map((data, index) => {
                            return <div className={styles.card} key={index}>
                                <div className={styles.box} style={{ backgroundColor: data.color }}>
                                    <span>{data.subTitle}</span>
                                    <div className={styles.img}>
                                        <Image src={data.image} alt="" />
                                    </div>
                                </div>
                                <h5>{data.title}</h5>
                                <span>{data.desc}</span>
                            </div>;
                        })
                    }
                </div>
            </div>
        </>
    );
};

export default AppsCardBlock;
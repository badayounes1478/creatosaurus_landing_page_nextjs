import React from 'react';
import style from '../LandingPageComponentCss/AppInfoSection.module.css';
import Image from 'next/image';

const AppInfoSection = () => {
  return (
    <div className={style.appInfoContainer}>

      <div className={style.card}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/captionator.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignRight}>
            <h5>AI powered copywriting <br />that scales</h5>
            <p>Create marketing copies at scale <br />with our AI powered content generation</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Facebook Ads</span>
              </button>

              <button>
                <span>Blog Titles</span>
              </button>

              <button>
                <span>Google Ads</span>
              </button>

              <button>
                <span>Instagram Caption</span>
              </button>

              <button>
                <span>Product Description</span>
              </button>

              <button>
                <span>Blog Ideas</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Easy to collaborate & helps to scale our <br /> social media process&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.sumeetShinde}`} />
              <div className={style.profileInfo}>
                <span>Sumeet Shinde</span>
                <span>Founder, Rise n Shine</span>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div className={`${style.card} ${style.card2}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/muse.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignLeft}>
            <h5>Create beautiful graphics <br />for your social media</h5>
            <p>Our easy-to-use comprehensive design <br />editor lets you create amazing designs</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Drag & Drop</span>
              </button>

              <button>
                <span>Free Stock Images</span>
              </button>

              <button>
                <span>Template Based</span>
              </button>

              <button>
                <span>Free Fonts</span>
              </button>

              <button>
                <span>Multi Language</span>
              </button>

              <button>
                <span>Illustrations & SVGs</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Creatosaurus combines creator workflow <br /> with design unlocking productivity for creators.&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.anshul}`} />
              <div className={style.profileInfo}>
                <span>Anshul Motwani</span>
                <span>Founder, Wittypens</span>
              </div>
            </div>

          </div>
        </div>
      </div>


      <div className={`${style.card} ${style.card3}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/cache.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignRight}>
            <h5>Time-saving tool for <br />managing social media</h5>
            <p>Manage your social media accounts <br />and save time by scheduling posts</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Facebook Page</span>
              </button>

              <button>
                <span>YouTube</span>
              </button>

              <button>
                <span>LinkedIn Profile</span>
              </button>

              <button>
                <span>Instagram</span>
              </button>

              <button>
                <span>Pinterest</span>
              </button>

              <button>
                <span>Twitter</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Creatosaurus is a cockpit for marketers <br /> with all controls in a single place&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.ishwar}`} />
              <div className={style.profileInfo}>
                <span>Ishwar Sarade</span>
                <span>Director, Bitsmith</span>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div className={`${style.card} ${style.card2} ${style.card4}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/hashtag.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignLeft}>
            <h5>Boost your visibility <br />with hashtags</h5>
            <p>Find & manage your hashtags to <br />optimize your social media workflow</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Trending Hashtags</span>
              </button>

              <button>
                <span>Search</span>
              </button>

              <button>
                <span>Twitter Trends</span>
              </button>

              <button>
                <span>Manage</span>
              </button>

              <button>
                <span>Collaborate</span>
              </button>

              <button>
                <span>Categorize</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Creatosaurus allows creators & marketers to spend  <br /> more time telling stories by optimising their complete process.&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.abhishek}`} />
              <div className={style.profileInfo}>
                <span>Abhishek Agrawal</span>
                <span>Co-Founder, NeuroTags</span>
              </div>
            </div>

          </div>
        </div>
      </div>


      <div className={`${style.card} ${style.card5}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/quotes.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignRight}>
            <h5>Engage more people <br />with your posts</h5>
            <p>Search from more than a million <br />quotes and save them for later use</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Quotes</span>
              </button>

              <button>
                <span>Captions</span>
              </button>

              <button>
                <span>Short Quotes</span>
              </button>

              <button>
                <span>Jokes</span>
              </button>

              <button>
                <span>Authors</span>
              </button>

              <button>
                <span>Novels</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Creatosaurus is a great one-stop solution for marketing teams <br /> to harness both productivity and creativity in an efficient way.&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.amogh}`} />
              <div className={style.profileInfo}>
                <span>Amogh Oak</span>
                <span>Partner, Clueless Monk Tech</span>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div className={`${style.card} ${style.card2} ${style.card7}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/steno.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignRight}>
            <h5>Write marketing collateral <br />better and faster</h5>
            <p>Easy text editor & AI Blog Writer</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Write Blogs</span>
              </button>

              <button>
                <span>AI Content</span>
              </button>

              <button>
                <span>PDFs</span>
              </button>

              <button>
                <span>Free Templates</span>
              </button>

              <button>
                <span>Documents</span>
              </button>

              <button>
                <span>Team Collaboration</span>
              </button>
            </div>

            <span className={style.info1}>&quot;With all the creator tools available at one place, <br /> team collaboration becomes super easy.&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.harsh}`} />
              <div className={style.profileInfo}>
                <span>Harsh Maheshwari</span>
                <span>Co-Founder, Lets Connect</span>
              </div>
            </div>
          </div>
        </div>
      </div>


      {/*<div className={`${style.card}  ${style.card6}`}>
        <div className={style.box}>
          <Image unoptimized={true} src="https://d3u00cz8rshep5.cloudfront.net/apollo.gif" alt="" layout="fill" />
          <div className={style.box2} />
        </div>

        <div className={style.info}>
          <div className={style.alignLeft}>
            <h5>Edit your videos easily and <br />make them look great</h5>
            <p>Video editor coming soon...</p>
            <div className={style.tagsContainer}>
              <button>
                <span>Cut & Merge</span>
              </button>

              <button>
                <span>Timeline</span>
              </button>

              <button>
                <span>Add Music</span>
              </button>

              <button>
                <span>Free Templates</span>
              </button>

              <button>
                <span>Free Stock Assets</span>
              </button>

              <button>
                <span>Drag & Drop</span>
              </button>
            </div>

            <span className={style.info1}>&quot;Creatosaurus is our go to platform to create <br /> marketing content for eChai meetups.&quot;</span>

            <div className={style.profile}>
              <div className={`${style.profileImage} ${style.jatin}`} />
              <div className={style.profileInfo}>
                <span>Jatin Chaudhary</span>
                <span>Co-Founder, eChai Ventures</span>
              </div>
            </div>
          </div>
        </div>
      </div>*/}

      <p className={style.more}>and more...</p>
    </div>
  );
};

export default AppInfoSection;
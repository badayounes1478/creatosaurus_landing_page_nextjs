import React from 'react'
import styles from './ReviewSection.module.css'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry"

const ReviewSection = () => {
    const reviews = [
        {
            title: "“Great Features and Easy to Use”",
            desc: "I love how Creatosaurus has a lot of features as tabs. Sometimes the all-in-one platforms feel like they are stuffing too many things on your plate. I love how Creatosaurus separates them to allow more in-depth focus on each feature.",
            author: "Steven A.",
            authorDesc: "Marketing Specialist",
            color: "rgba(255, 138, 37, 0.10)",
            logo: "arrow"
        },
        {
            title: "“Creatosaurus is a must have for creators that want to get things done fast & Supremely in one space”",
            desc: "Creatosaurus is an A+ company . I am happy to have their product. The support is supreme and their product which is already supreme always anounces new improvements , which is always a cool surprise. Creatosaurus is a must have for social media creators, and more. It is so easy and quick to schedule content, and the ability to have separate workspaces for teams is a HUGE plus. I highly recommend.",
            author: "Almighty Servant C.",
            authorDesc: "Founder",
            color: "rgba(255, 67, 202, 0.10)",
            logo: "arrow"
        },
        {
            title: "“All-In-One AI Tool Suite and Social Media Management Software”",
            desc: "Creatosaurus offers an invaluable set of tools for creating all types of content or telling stories as they say. It will definitely be a go-to resource that I'll be using for the foreseeable future. If you're looking for a tool like this, make sure to give this one a try. The way the tools are organized separately makes it pretty intuitive to work with, but they also integrate together nicely for smooth workflows.",
            author: "Nathan S.",
            authorDesc: "Owner",
            color: "rgba(0, 120, 255, 0.10)",
            logo: "arrow"
        },
        {
            title: "“Amazing tool for marketing, I have all I need in one space which saves me a lot of time”",
            desc: "I really like, it once I found that I was able to use the post designer effectively with a computer that has more ram I was really satisfied. I really recommend this software if you want to lighten your burden in terms of creation time, stop wasting time finding free templates that look good and spend time planning your posts and let the software do the rest, all from one tool.",
            author: "David I.",
            authorDesc: "Business Developer",
            color: "rgba(0, 236, 194, 0.10)",
            logo: "arrow"
        },
        {
            title: "“Superb tool for social media content creation”",
            desc: "As a budding social media influencer and not from a technical background, it was always a black box to figure out the world of hashtags, algorithms and engagement. Thankfully, Creatosaurus takes a lot off my plate and helps me focus on presenting rather than the logistics. The big choice of illustrations make us spoilt for choice in creativity.",
            author: "Jigna P.",
            authorDesc: "Influencer",
            color: "rgba(255, 215, 95, 0.10)",
            logo: "arrow"
        },
        {
            title: "“One site management tool for designers”",
            desc: "Not having to upload and download from other sites makes this tool very handy. I like the idea and the whole customer service from the team. I like to create all of my designs and use AI in the same environment.",
            author: "Felix R.",
            authorDesc: "Web Designer",
            color: "rgba(255, 67, 89, 0.10)",
            logo: "arrow"
        },
        {
            title: "“What’s most helpful about Creatosaurus is easy to use as a design program.”",
            desc: "It can't do everything like Adobe software but it will get the basic needs and wants done for you.",
            author: "Jason H.",
            authorDesc: "Graphic Designer",
            color: "rgba(0, 155, 119, 0.10)",
            logo: "g2rating"
        },
        {
            title: "“Just stumbled upon Creatosaurus AI, and it's a breath taking for creative and marketing teams!”",
            desc: "If you are an entrepreneur who just starts your business journey, or you run small business, this tool can really save your budget. Because, this all-in-one platform covers everything, from idea curation and collaboration to content creation, distribution, analytics, and even apps! With Creatosaurus AI, you can effortlessly handle graphics, AI-generated content, video editing, scheduling, hashtag research, article crafting, analytics management, report generation, and more—all in one place. And also, I feel like Marketing teams are going to love this tool as well!",
            author: "Oleksandr Naumov",
            authorDesc: "Marketer",
            color: "rgba(0, 78, 127, 0.10)",
            logo: "producthunt"
        },
        {
            title: "“Creatosaurus is Supreme & at an Incredible Price”",
            desc: "I have used a few other social media schedulers, and let me tell you Creatosaurus is in a supreme league of it's own at an incredible price. I love how easy it is to navigate everything, Create graphics, and schedule posts, there is a gigantic library of pre-made templates for everything from YouTube Tiles and Scripts, ad copy, Blogs, long form and short form content, Hashtag generator, Ad copy and way too many things to list here. I have the version where you can have team members to delegate task to adn that is an ultimate plus for busy entrepreneurs. It literally is a no brainier when it comes to purchasing. Not only do you get an invaluable tool for social media content creation and scheduling, you also get Ai generated text that really help you take on many tasks within your businesses flow. The support team is also very spectacular, they even get on live meets to help you with any needs you may have literally walking you through your issues via screen share. Do not sleep when it comes to getting this product. Creatosaurus is literally one of my best buys of the year thus far, and i am so grateful to have purchased this incredible find. THANKS Creatosaurus & Team. You All Truly Rock!!!",
            author: "Charon Tap Star",
            authorDesc: "Creator",
            color: "rgba(138, 67, 255, 0.10)",
            logo: "star"
        },
        {
            title: "“This product is really cool!”",
            desc: "Making social media posts can be a slow process of thinking what to say, especially with copy. This for sure expedites the process.",
            author: "Zulfi Husain",
            authorDesc: "Indie Maker",
            color: "rgba(0, 120, 255, 0.10)",
            logo: "producthunt"
        },
        {
            title: "“Super Complete Program”",
            desc: "It has everything can be necessary to create a social media campaign.",
            author: "Danilo T.",
            authorDesc: "UX Researcher",
            color: "rgba(255, 138, 37, 0.10)",
            logo: "arrow"
        }
    ]

    const scrollRightAndLeftOnArrowClicked = (direction, elementId) => {
        const width = document.getElementById(elementId).getBoundingClientRect().width
        if (direction === "right") {
            document.getElementById("scroll").scrollLeft += width + 20
        } else {
            document.getElementById("scroll").scrollLeft -= width + 20
        }
    }

    return (
        <div className={styles.reviewSectionContainer}>
            <h2>See why thousands of agencies, marketers, creators and entrepreneurs love Creatosaurus.</h2>
            <div className={styles.hide}>
                <ResponsiveMasonry columnsCountBreakPoints={{ 0: 1, 640: 2, 1024: 3 }} >
                    <Masonry gutter="40px">
                        {reviews.map((data, index) => {
                            return <div key={index} className={styles.card} style={{ backgroundColor: data.color }}>
                                <span className={styles.title}>{data.title}</span>
                                <p>{data.desc}</p>
                                <div className={styles.info}>
                                    <div className={styles.author}>
                                        <span>{data.author}</span>
                                        <span>{data.authorDesc}</span>
                                    </div>
                                    {
                                        data.logo === "arrow" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M0 7.35338L8.29429 7.35504L13.3377 7.3559V2.35938L0 7.35338Z" fill="#FF9D28" />
                                            <path d="M13.3398 2.35935V19.9996L19.639 0L13.3398 2.35935Z" fill="#68C5ED" />
                                            <path d="M13.3383 7.35632L8.29492 7.35547L13.3383 20V7.35632Z" fill="#044D80" />
                                            <path d="M0 7.35254L9.5876 10.5986L8.29429 7.3542L0 7.35254Z" fill="#E54747" />
                                        </svg> :
                                            data.logo === "producthunt" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" clipRule="evenodd" d="M20.0098 9.99996C20.0098 15.5228 15.5326 20 10.0097 20C4.48691 20 0.00976562 15.5228 0.00976562 9.99996C0.00976562 4.47715 4.48691 0 10.0097 0C15.5326 0 20.0098 4.47715 20.0098 9.99996ZM6.50966 4.99991H11.343C13.2763 4.99991 14.843 6.56657 14.843 8.49991C14.843 10.4332 13.2763 11.9999 11.343 11.9999H8.50968V14.9999H6.50966V4.99991ZM8.50968 9.99988H11.343C12.168 9.99988 12.843 9.32489 12.843 8.49991C12.843 7.6749 12.168 6.9999 11.343 6.9999H8.50968V9.99988Z" fill="#DA552F" />
                                            </svg> :
                                                data.logo === "g2rating" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <g clipPath="url(#clip0_10312_8528)">
                                                        <path d="M14.1331 2.95428C13.8485 2.67736 13.5831 2.41248 13.3081 2.15161C13.2228 2.07135 13.2827 2.01181 13.3191 1.95095C13.6338 1.41783 14.1462 1.022 14.7513 0.844602C15.118 0.726884 15.5059 0.68512 15.8901 0.721979C16.2744 0.758843 16.6464 0.873526 16.9824 1.05864C18.22 1.70948 18.1093 3.21917 17.395 3.90144C17.1241 4.15515 16.8173 4.36981 16.484 4.53889C16.1401 4.71681 15.7964 4.88471 15.4595 5.074C15.1796 5.22986 14.9617 5.44524 14.8606 5.75694C14.8194 5.88269 14.8441 5.91546 14.9823 5.91411C15.8913 5.90876 16.8009 5.91411 17.7106 5.90876C17.8481 5.90876 17.8996 5.93423 17.8941 6.07999C17.8817 6.3924 17.8866 6.7054 17.8941 7.01646C17.8941 7.11546 17.866 7.15023 17.7614 7.15023C16.2997 7.14758 14.8384 7.14758 13.3776 7.15023C13.3088 7.15023 13.2448 7.15023 13.2441 7.05323C13.2441 6.06864 13.4311 5.1516 14.2233 4.44925C14.6006 4.12421 15.0241 3.85387 15.4808 3.64658C15.7311 3.52753 15.9841 3.41381 16.2027 3.24124C16.3938 3.09007 16.5259 2.9068 16.5396 2.6593C16.5603 2.27201 16.2454 1.98439 15.7696 1.95429C15.0854 1.9068 14.5918 2.20647 14.2466 2.76298C14.2123 2.81783 14.1806 2.87402 14.1331 2.95428Z" fill="black" />
                                                        <path fillRule="evenodd" clipRule="evenodd" d="M0.496059 12.9763C1.79762 17.1736 5.78755 19.9876 10.3667 19.9996C10.8036 19.9903 11.2397 19.9584 11.6731 19.904C13.3131 19.6913 14.8755 19.0945 16.2261 18.1649C16.3169 18.1033 16.3389 18.0572 16.2777 17.9548C15.5782 16.7887 14.8847 15.6204 14.1971 14.4498C14.1318 14.3395 14.094 14.3528 14.0025 14.4197C12.2094 15.7722 10.2381 16.0873 8.11976 15.296C5.45064 14.2986 3.938 11.4846 4.56575 8.76424C4.8502 7.51965 5.55435 6.40335 6.56618 5.59295C7.57794 4.78255 8.83929 4.32464 10.1495 4.29205C10.1931 4.29656 10.237 4.28685 10.2743 4.26446C10.3116 4.24206 10.3403 4.20824 10.3557 4.16831C10.9786 2.89116 11.6046 1.61581 12.2335 0.342244C12.2891 0.22987 12.2878 0.192412 12.1448 0.166994C10.8661 -0.0655799 9.55318 -0.0551311 8.27859 0.197764C6 0.6506 4.06176 1.70879 2.55875 3.4352C0.113773 6.24453 -0.590977 9.45923 0.496059 12.9763ZM17.4008 17.0035C17.42 17.0424 17.4395 17.0816 17.4817 17.1037C17.5192 17.0935 17.5339 17.0642 17.548 17.0357C17.5525 17.0266 17.557 17.0176 17.5622 17.0094C18.3597 15.6622 19.1603 14.3138 19.9638 12.9639C19.9885 12.9282 20.0012 12.8858 19.9999 12.8428C19.9987 12.7998 19.9836 12.7581 19.9569 12.7238C19.1571 11.3838 18.3609 10.042 17.5684 8.69841C17.5478 8.65441 17.5136 8.61782 17.4705 8.59382C17.4275 8.56982 17.3778 8.55959 17.3284 8.56465C15.7401 8.56865 14.1514 8.56865 12.5622 8.56465C12.5125 8.55959 12.4625 8.56976 12.4191 8.59376C12.3757 8.61776 12.3411 8.65435 12.3202 8.69841C11.5308 10.0308 10.742 11.3633 9.95359 12.6957C9.91335 12.7386 9.8917 12.7949 9.89306 12.8529H10.0863C10.6038 12.8529 11.1212 12.8534 11.6387 12.8539C12.6735 12.8549 13.7082 12.856 14.7431 12.8529C14.8042 12.8461 14.8659 12.8581 14.9197 12.8871C14.9734 12.9161 15.0165 12.9608 15.0429 13.0148C15.8194 14.3329 16.5986 15.6498 17.3806 16.9652C17.3879 16.9775 17.3943 16.9905 17.4008 17.0035Z" fill="#EF492D" />
                                                        <path d="M14.1331 2.95428C13.8485 2.67736 13.5831 2.41248 13.3081 2.15161C13.2228 2.07135 13.2827 2.01181 13.3191 1.95095C13.6338 1.41783 14.1462 1.022 14.7513 0.844602C15.118 0.726884 15.5059 0.68512 15.8901 0.721979C16.2744 0.758843 16.6464 0.873526 16.9824 1.05864C18.22 1.70948 18.1093 3.21917 17.395 3.90144C17.1241 4.15515 16.8173 4.36981 16.484 4.53889C16.1401 4.71681 15.7964 4.88471 15.4595 5.074C15.1796 5.22986 14.9617 5.44524 14.8606 5.75694C14.8194 5.88269 14.8441 5.91546 14.9823 5.91411C15.8913 5.90876 16.8009 5.91411 17.7106 5.90876C17.8481 5.90876 17.8996 5.93423 17.8941 6.07999C17.8817 6.3924 17.8866 6.7054 17.8941 7.01646C17.8941 7.11546 17.866 7.15023 17.7614 7.15023C16.2997 7.14758 14.8384 7.14758 13.3776 7.15023C13.3088 7.15023 13.2448 7.15023 13.2441 7.05323C13.2441 6.06864 13.4311 5.1516 14.2233 4.44925C14.6006 4.12421 15.0241 3.85387 15.4808 3.64658C15.7311 3.52753 15.9841 3.41381 16.2027 3.24124C16.3938 3.09007 16.5259 2.9068 16.5396 2.6593C16.5603 2.27201 16.2454 1.98439 15.7696 1.95429C15.0854 1.9068 14.5918 2.20647 14.2466 2.76298C14.2123 2.81783 14.1806 2.87402 14.1331 2.95428Z" fill="#EF492D" />
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip0_10312_8528">
                                                            <rect width="20" height="20" fill="white" />
                                                        </clipPath>
                                                    </defs>
                                                </svg> :
                                                    <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M22.0102 7.64273H13.9721L11.4978 0L9.00875 7.64273L0.970703 7.62812L7.47142 12.3572L4.98241 20L11.4831 15.2709L17.9839 20L15.5095 12.3572L22.0102 7.64273Z" fill="#00B67A" />
                                                        <path d="M16.0661 14.0841L15.5098 12.3564L11.498 15.27L16.0661 14.0841Z" fill="#005128" />
                                                    </svg>
                                    }
                                </div>
                            </div>
                        })}
                    </Masonry>
                </ResponsiveMasonry>
            </div>

            <div>
                <div className={styles.mobile}  id='scroll'>
                    {reviews.map((data, index) => {
                        if (index === 8) return null
                        return <div id={`scroll${index}`} key={index} className={styles.card} style={{ backgroundColor: data.color }}>
                            <span className={styles.title}>{data.title}</span>
                            <p>{data.desc}</p>
                            <div className={styles.info}>
                                <div className={styles.author}>
                                    <span>{data.author}</span>
                                    <span>{data.authorDesc}</span>
                                </div>
                                {
                                    data.logo === "arrow" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 7.35338L8.29429 7.35504L13.3377 7.3559V2.35938L0 7.35338Z" fill="#FF9D28" />
                                        <path d="M13.3398 2.35935V19.9996L19.639 0L13.3398 2.35935Z" fill="#68C5ED" />
                                        <path d="M13.3383 7.35632L8.29492 7.35547L13.3383 20V7.35632Z" fill="#044D80" />
                                        <path d="M0 7.35254L9.5876 10.5986L8.29429 7.3542L0 7.35254Z" fill="#E54747" />
                                    </svg> :
                                        data.logo === "producthunt" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" clipRule="evenodd" d="M20.0098 9.99996C20.0098 15.5228 15.5326 20 10.0097 20C4.48691 20 0.00976562 15.5228 0.00976562 9.99996C0.00976562 4.47715 4.48691 0 10.0097 0C15.5326 0 20.0098 4.47715 20.0098 9.99996ZM6.50966 4.99991H11.343C13.2763 4.99991 14.843 6.56657 14.843 8.49991C14.843 10.4332 13.2763 11.9999 11.343 11.9999H8.50968V14.9999H6.50966V4.99991ZM8.50968 9.99988H11.343C12.168 9.99988 12.843 9.32489 12.843 8.49991C12.843 7.6749 12.168 6.9999 11.343 6.9999H8.50968V9.99988Z" fill="#DA552F" />
                                        </svg> :
                                            data.logo === "g2rating" ? <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clipPath="url(#clip0_10312_8528)">
                                                    <path d="M14.1331 2.95428C13.8485 2.67736 13.5831 2.41248 13.3081 2.15161C13.2228 2.07135 13.2827 2.01181 13.3191 1.95095C13.6338 1.41783 14.1462 1.022 14.7513 0.844602C15.118 0.726884 15.5059 0.68512 15.8901 0.721979C16.2744 0.758843 16.6464 0.873526 16.9824 1.05864C18.22 1.70948 18.1093 3.21917 17.395 3.90144C17.1241 4.15515 16.8173 4.36981 16.484 4.53889C16.1401 4.71681 15.7964 4.88471 15.4595 5.074C15.1796 5.22986 14.9617 5.44524 14.8606 5.75694C14.8194 5.88269 14.8441 5.91546 14.9823 5.91411C15.8913 5.90876 16.8009 5.91411 17.7106 5.90876C17.8481 5.90876 17.8996 5.93423 17.8941 6.07999C17.8817 6.3924 17.8866 6.7054 17.8941 7.01646C17.8941 7.11546 17.866 7.15023 17.7614 7.15023C16.2997 7.14758 14.8384 7.14758 13.3776 7.15023C13.3088 7.15023 13.2448 7.15023 13.2441 7.05323C13.2441 6.06864 13.4311 5.1516 14.2233 4.44925C14.6006 4.12421 15.0241 3.85387 15.4808 3.64658C15.7311 3.52753 15.9841 3.41381 16.2027 3.24124C16.3938 3.09007 16.5259 2.9068 16.5396 2.6593C16.5603 2.27201 16.2454 1.98439 15.7696 1.95429C15.0854 1.9068 14.5918 2.20647 14.2466 2.76298C14.2123 2.81783 14.1806 2.87402 14.1331 2.95428Z" fill="black" />
                                                    <path fillRule="evenodd" clipRule="evenodd" d="M0.496059 12.9763C1.79762 17.1736 5.78755 19.9876 10.3667 19.9996C10.8036 19.9903 11.2397 19.9584 11.6731 19.904C13.3131 19.6913 14.8755 19.0945 16.2261 18.1649C16.3169 18.1033 16.3389 18.0572 16.2777 17.9548C15.5782 16.7887 14.8847 15.6204 14.1971 14.4498C14.1318 14.3395 14.094 14.3528 14.0025 14.4197C12.2094 15.7722 10.2381 16.0873 8.11976 15.296C5.45064 14.2986 3.938 11.4846 4.56575 8.76424C4.8502 7.51965 5.55435 6.40335 6.56618 5.59295C7.57794 4.78255 8.83929 4.32464 10.1495 4.29205C10.1931 4.29656 10.237 4.28685 10.2743 4.26446C10.3116 4.24206 10.3403 4.20824 10.3557 4.16831C10.9786 2.89116 11.6046 1.61581 12.2335 0.342244C12.2891 0.22987 12.2878 0.192412 12.1448 0.166994C10.8661 -0.0655799 9.55318 -0.0551311 8.27859 0.197764C6 0.6506 4.06176 1.70879 2.55875 3.4352C0.113773 6.24453 -0.590977 9.45923 0.496059 12.9763ZM17.4008 17.0035C17.42 17.0424 17.4395 17.0816 17.4817 17.1037C17.5192 17.0935 17.5339 17.0642 17.548 17.0357C17.5525 17.0266 17.557 17.0176 17.5622 17.0094C18.3597 15.6622 19.1603 14.3138 19.9638 12.9639C19.9885 12.9282 20.0012 12.8858 19.9999 12.8428C19.9987 12.7998 19.9836 12.7581 19.9569 12.7238C19.1571 11.3838 18.3609 10.042 17.5684 8.69841C17.5478 8.65441 17.5136 8.61782 17.4705 8.59382C17.4275 8.56982 17.3778 8.55959 17.3284 8.56465C15.7401 8.56865 14.1514 8.56865 12.5622 8.56465C12.5125 8.55959 12.4625 8.56976 12.4191 8.59376C12.3757 8.61776 12.3411 8.65435 12.3202 8.69841C11.5308 10.0308 10.742 11.3633 9.95359 12.6957C9.91335 12.7386 9.8917 12.7949 9.89306 12.8529H10.0863C10.6038 12.8529 11.1212 12.8534 11.6387 12.8539C12.6735 12.8549 13.7082 12.856 14.7431 12.8529C14.8042 12.8461 14.8659 12.8581 14.9197 12.8871C14.9734 12.9161 15.0165 12.9608 15.0429 13.0148C15.8194 14.3329 16.5986 15.6498 17.3806 16.9652C17.3879 16.9775 17.3943 16.9905 17.4008 17.0035Z" fill="#EF492D" />
                                                    <path d="M14.1331 2.95428C13.8485 2.67736 13.5831 2.41248 13.3081 2.15161C13.2228 2.07135 13.2827 2.01181 13.3191 1.95095C13.6338 1.41783 14.1462 1.022 14.7513 0.844602C15.118 0.726884 15.5059 0.68512 15.8901 0.721979C16.2744 0.758843 16.6464 0.873526 16.9824 1.05864C18.22 1.70948 18.1093 3.21917 17.395 3.90144C17.1241 4.15515 16.8173 4.36981 16.484 4.53889C16.1401 4.71681 15.7964 4.88471 15.4595 5.074C15.1796 5.22986 14.9617 5.44524 14.8606 5.75694C14.8194 5.88269 14.8441 5.91546 14.9823 5.91411C15.8913 5.90876 16.8009 5.91411 17.7106 5.90876C17.8481 5.90876 17.8996 5.93423 17.8941 6.07999C17.8817 6.3924 17.8866 6.7054 17.8941 7.01646C17.8941 7.11546 17.866 7.15023 17.7614 7.15023C16.2997 7.14758 14.8384 7.14758 13.3776 7.15023C13.3088 7.15023 13.2448 7.15023 13.2441 7.05323C13.2441 6.06864 13.4311 5.1516 14.2233 4.44925C14.6006 4.12421 15.0241 3.85387 15.4808 3.64658C15.7311 3.52753 15.9841 3.41381 16.2027 3.24124C16.3938 3.09007 16.5259 2.9068 16.5396 2.6593C16.5603 2.27201 16.2454 1.98439 15.7696 1.95429C15.0854 1.9068 14.5918 2.20647 14.2466 2.76298C14.2123 2.81783 14.1806 2.87402 14.1331 2.95428Z" fill="#EF492D" />
                                                </g>
                                                <defs>
                                                    <clipPath id="clip0_10312_8528">
                                                        <rect width="20" height="20" fill="white" />
                                                    </clipPath>
                                                </defs>
                                            </svg> :
                                                <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M22.0102 7.64273H13.9721L11.4978 0L9.00875 7.64273L0.970703 7.62812L7.47142 12.3572L4.98241 20L11.4831 15.2709L17.9839 20L15.5095 12.3572L22.0102 7.64273Z" fill="#00B67A" />
                                                    <path d="M16.0661 14.0841L15.5098 12.3564L11.498 15.27L16.0661 14.0841Z" fill="#005128" />
                                                </svg>
                                }
                            </div>
                        </div>
                    })}
                </div>

                <div className={styles.arrowContainer}>
                    <button onClick={()=> scrollRightAndLeftOnArrowClicked("right", `scroll1`)}>{"<"}</button>
                    <button onClick={()=> scrollRightAndLeftOnArrowClicked("left", `scroll1`)}>{">"}</button>
                </div>
            </div>
        </div>
    )
}

export default ReviewSection
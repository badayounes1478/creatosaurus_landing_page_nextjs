import React from 'react'
import styles from './BlackSection.module.css'
import Link from 'next/dist/client/link'

const BlackSection = () => {
  return (
    <div className={styles.blackSectionContainer}>
      <h2>Biggest Social Media Marketing Tool Discounts For Black Friday & Cyber Monday</h2>
      <p>With our special Black Friday and Cyber Monday marketing deals, you get the same premium tool but at a much better price.</p>
      <Link href="#price">
        <button style={{ cursor: 'pointer' }}>Get Creatosaurus Now {"->"}</button>
      </Link>
    </div>
  )
}

export default BlackSection
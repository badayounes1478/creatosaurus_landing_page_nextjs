import React, { useState } from 'react'

const PricingCards = ({ isYearlyDuration, isIndian, selectPlan, plans }) => {
    const [showPlan, setShowPlan] = useState([])

    const closeIndex = (index) => {
        let data = showPlan.filter(data => data !== index)
        setShowPlan(data)
    }

    const formatPrice = (number) => {
        number = parseInt(number)
        return number.toLocaleString('en-IN');
    }

    const checkCurrency = () => {
        return isIndian ? "₹" : "$"
    }

    return (
        <div id='price' className="mt-[100px] flex flex-col max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 gap-[25px] xl:gap-[15px]">
                {
                    plans.map((data, index) => {
                        if (index === 0) return
                        return <div key={data.planName} style={{ borderColor: data.borderColor }} className="w-full border-[1px] rounded-[10px] overflow-hidden">
                            <div style={{ backgroundColor: data.color }} className="h-[334px] pt-[39px] px-[20px]"> {/* With free trial button height 374 */}
                                <div className="flex justify-between items-center">
                                    <span className="text-[10px] font-medium min-w-[93px] w-[93px] h-[25px] bg-white rounded-[50px] flex justify-center items-center">{data.tag}</span>

                                    {
                                        data.showPremium && <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clipPath="url(#clip0_3730_33054)">
                                                <circle cx="12.5" cy="12.5" r="12.5" fill="#FFBF00" />
                                                <path d="M11.8141 8.13914C11.1091 10.1132 10.3195 11.072 9.60043 11.1425C8.79672 11.2412 8.09172 10.8887 7.42902 10.0145C7.3884 9.96147 7.34099 9.91407 7.28801 9.87345C7.47859 9.61935 7.57779 9.30834 7.56954 8.99081C7.56129 8.67329 7.44607 8.36785 7.24254 8.12399C7.03902 7.88013 6.75912 7.71213 6.4482 7.6472C6.13727 7.58228 5.81353 7.62425 5.52943 7.7663C5.24534 7.90835 5.01752 8.14216 4.8829 8.42985C4.74828 8.71755 4.71474 9.04227 4.78772 9.3514C4.8607 9.66053 5.03591 9.93597 5.28498 10.1331C5.53405 10.3302 5.84238 10.4375 6.16001 10.4375L6.17411 10.6067L7.33031 15.1751C7.44338 15.6325 7.70593 16.039 8.07631 16.3302C8.44669 16.6214 8.90369 16.7806 9.37483 16.7825H15.6353C16.1064 16.7806 16.5634 16.6214 16.9338 16.3302C17.3042 16.039 17.5667 15.6325 17.6798 15.1751L18.836 10.6067C18.847 10.551 18.8517 10.4942 18.8501 10.4375C19.1677 10.4375 19.476 10.3302 19.7251 10.1331C19.9742 9.93597 20.1494 9.66053 20.2224 9.3514C20.2953 9.04227 20.2618 8.71755 20.1272 8.42985C19.9926 8.14216 19.7647 7.90835 19.4806 7.7663C19.1965 7.62425 18.8728 7.58228 18.5619 7.6472C18.251 7.71213 17.9711 7.88013 17.7675 8.12399C17.564 8.36785 17.4488 8.67329 17.4405 8.99081C17.4323 9.30834 17.5315 9.61935 17.7221 9.87345C17.67 9.90971 17.6226 9.95234 17.5811 10.0004C16.8902 10.8746 16.1711 11.2271 15.4097 11.1425C14.7047 11.072 13.9432 10.0991 13.1959 8.13914C13.4677 7.98637 13.6812 7.74782 13.803 7.46082C13.9248 7.17381 13.948 6.85453 13.8691 6.55291C13.7901 6.2513 13.6134 5.98436 13.3666 5.79385C13.1198 5.60334 12.8168 5.5 12.505 5.5C12.1933 5.5 11.8903 5.60334 11.6435 5.79385C11.3967 5.98436 11.22 6.2513 11.141 6.55291C11.062 6.85453 11.0853 7.17381 11.2071 7.46082C11.3289 7.74782 11.5424 7.98637 11.8141 8.13914ZM16.7351 17.84C16.922 17.84 17.1014 17.9143 17.2336 18.0465C17.3658 18.1787 17.4401 18.358 17.4401 18.545C17.4401 18.732 17.3658 18.9113 17.2336 19.0435C17.1014 19.1757 16.922 19.25 16.7351 19.25H8.27502C8.08804 19.25 7.90872 19.1757 7.77651 19.0435C7.64429 18.9113 7.57002 18.732 7.57002 18.545C7.57002 18.358 7.64429 18.1787 7.77651 18.0465C7.90872 17.9143 8.08804 17.84 8.27502 17.84H16.7351Z" fill="white" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0_3730_33054">
                                                    <rect width="25" height="25" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                    }
                                </div>

                                <div className="mt-[25px]">
                                    <span className="text-[20px] font-semibold">{data.planName}</span>
                                    <p className="max-w-[220px] text-[12px] font-light text-[#333333] mt-[10px]">{data.info}</p>
                                </div>

                                <div className="mt-[25px] flex flex-col">
                                    <span className="text-[24px] font-semibold"><span className='line-through'>{checkCurrency()}{formatPrice(data.year)}</span>&nbsp;&nbsp;{checkCurrency()}{formatPrice(data.discount)}</span>
                                    <span className="text-[12px] text-[#404040]">/year</span>
                                </div>

                                <button
                                    onClick={() => selectPlan(data)}
                                    style={{ backgroundColor: data.buttonColor }}
                                    className="mt-[30px] bg-[#808080] text-[14px] font-semibold text-white w-full h-[40px] rounded-[5px]">{data.buttonName}</button>
                                {data.showTrial && <button className="h-[40px] w-full mt-[10px] text-[12px] text-[#808080] rounded-[5px] hover:bg-[#0000000D]">Start 14 days free trial</button>}
                            </div>

                            <div className="py-[20px] px-[20px]">
                                <div className="flex justify-between items-center mb-[5px]">
                                    <span className="text-[14px] font-medium">{data.planHead}</span>
                                    {
                                        showPlan.includes(index) ?
                                            <svg onClick={() => closeIndex(index)} className="cursor-pointer size-[22px] xl:hidden" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.35686 16.0713L13.4997 10.9284L18.6426 16.0713" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg> :
                                            <svg onClick={() => setShowPlan(prev => [...prev, index])} className="cursor-pointer size-[22px] xl:hidden" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M18.6431 10.9282L13.5003 16.0711L8.35742 10.9282" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                    }
                                </div>

                                <div className={`${showPlan.includes(index) ? "flex" : "hidden"} xl:flex flex-col`}>
                                    {
                                        data.features.map((data, index) => {
                                            return <div key={data}>
                                                {index === 6 && <div className='h-[1px] bg-[#DCDCDCB3] mt-[10px]' />}
                                                <div className="mt-[10px]">
                                                    <div className="flex">
                                                        <svg className="mr-[5px]" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.75 9.75L6.75 12.75L14.25 5.25" stroke="black" strokeWidth="1.1" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                        <span className="text-[12px]">{data}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default PricingCards
import React from 'react'
import styles from './LimitedUserSection.module.css'
import Image from 'next/image'
import Off from '../../../public/NewAssets/Home/off.webp'
import Link from 'next/dist/client/link'

const LimitedUserSection = () => {
    return (
        <div className={styles.limitedUserSectionContainer}>
            <div className={styles.info}>
                <h2>Don&apos;t Miss Out on Creatosaurus Lifetime deal – Only 99 license available</h2>
                <p>If Creatosaurus doesn&apos;t supercharge your social media growth within 14 days, your payment is entirely waived. Our commitment is to your results, and your satisfaction is our top priority.</p>
                <Link href="#price">
                    <button style={{ cursor: "pointer" }}>Buy Now {"->"}</button>
                </Link>
            </div>
            <div className={styles.imageWrapper}>
                <Image src={Off} alt='92% off' />
            </div>
        </div>
    )
}

export default LimitedUserSection
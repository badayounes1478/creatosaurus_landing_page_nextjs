import React, { useState } from 'react'
import styles from './FaqSection.module.css'

const FaqSection = () => {
    const [activeIndex, setActiveIndex] = useState([])
    const [questions] = useState([{
        q: "How can I purchase Creatosaurus's Black Friday and Cyber Monday Marketing deals?",
        ans: "The Black Friday and Cyber Monday deals are automatically applied to your purchase. No need for coupon codes. Simply select the plan you want, proceed with payment, and enjoy the savings."
    }, {
        q: "Are there any differences between the discounted services and Creatosaurus's regular offerings?",
        ans: "No, you will receive the same high-quality services during our Black Friday and Cyber Monday deals as you would with our regular services. The only difference is the price."
    }, {
        q: "Why are your Black Friday and Cyber Monday prices so low?",
        ans: "We believe in offering value to our customers without inflated prices. Our competitive rates are the result of efficient server optimization, least marketing budget and fair pricing policies."
    }, {
        q: "How long will the Black Friday marketing deals be available?",
        ans: "Our Black Friday marketing deals run from November 1 to November 26. After this period, prices will return to their regular rates, so don't miss out."
    }, {
        q: "Will there be Cyber Monday marketing deals following Black Friday?",
        ans: "Yes, our Cyber Monday marketing deals will be available from November 27 to December 3."
    }, {
        q: "How do I upgrade my current plan to the Black Friday deal?",
        ans: "To access the Black Friday deal for an upgrade, simply log in to your Creatosaurus billing section. The process is straightforward and can be done with a few clicks."
    }, {
        q: "Do you provide customer support services during the Black Friday and Cyber Monday marketing deals?",
        ans: "Yes, our customer support team is available 24/7/365. Feel free to reach out to us with any questions or assistance you may need, even during the Black Friday and Cyber Monday marketing deals."
    }, {
        q: "Are there any additional fees during the Black Friday and Cyber Monday marketing deals?",
        ans: "No, there are no hidden fees. Your invoice will only include the chosen plans and add-ons & taxes (depending upon the location). We believe in transparent pricing."
    }, {
        q: "Do you have enterprise solutions available during Black Friday and Cyber Monday marketing deals?",
        ans: "Yes, we do have enterprise and large scale solutions. Feel free to reach us out. Contact us."
    }, {
        q: "Can I use my existing subscription or do I need to create a new account for the Black Friday marketing deals?",
        ans: "You can use your existing Creatosaurus account to access the Black Friday and Cyber Monday marketing deals. There's no need to create a new account."
    }, {
        q: "Do you offer a money-back guarantee during the Black Friday and Cyber Monday marketing deals?",
        ans: "Yes, we offer our standard 14 day money-back guarantee during the Black Friday and Cyber Monday marketing deals. If you're not satisfied with our services, you can request a refund within the specified period."
    }, {
        q: "What payment methods are accepted during the Black Friday and Cyber Monday marketing deals?",
        ans: "During the Black Friday and Cyber Monday marketing deals, we accept a variety of Stripe enabled payment methods, including credit/debit cards, PayPal, and other secure online payment options."
    }, {
        q: "Are there any educational resources or tutorials available to help users make the most of their Creatosaurus plans during the Black Friday and Cyber Monday marketing deals?",
        ans: "Yes, we provide a wealth of educational resources and knowledge base to help you maximize your Black Friday and Cyber Monday marketing deals. Plus you can book our live on-boarding call multiple times."
    }])

    const toggleQuestion = (index) => {
        if (activeIndex.includes(index)) {
            let filterData = activeIndex.filter(data => data !== index)
            setActiveIndex(filterData)
        } else {
            setActiveIndex((prev) => [...prev, index])
        }
    }

    return (
        <div className={styles.faqSectionContainer}>
            <h2>Frequently Asked Questions</h2>
            <p>We hope these frequently asked questions about Creatosaurus provide clarity. If you have any more questions, please don&apos;t hesitate to reach out to our support team for assistance.</p>
            <div className={styles.questions}>
                {
                    questions.map((data, index) => {
                        return <div key={index} className={styles.card} onClick={() => toggleQuestion(index)}>
                            <div className={styles.row}>
                                <span>{data.q}</span>
                                <div className={styles.svgWrapper}>
                                    {
                                        activeIndex.includes(index) ?
                                            <svg width="44" height="43" viewBox="0 0 44 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22.0011 21.2129L27.3044 26.5162M16.6978 26.5162L22.0011 21.2129L16.6978 26.5162ZM27.3044 15.9096L22.0011 21.2129L27.3044 15.9096ZM22.0011 21.2129L16.6978 15.9096L22.0011 21.2129Z" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                            :
                                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M15 15V22.5M7.5 15H15H7.5ZM22.5 15H15H22.5ZM15 15V7.5V15Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                    }
                                </div>
                            </div>
                            {activeIndex.includes(index) ? <div className={styles.ans}>{data.ans}</div> : null}
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default FaqSection
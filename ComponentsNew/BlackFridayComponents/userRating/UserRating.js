import React from 'react'
import styles from './UserRating.module.css'

const UserRating = () => {
    return (
        <div className={styles.userRatingContainer}>
            <svg width="261" height="40" viewBox="0 0 261 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clipPath="url(#clip0_10444_724)">
                    <path d="M20.5007 28.7834L30.8007 35L28.0673 23.2834L37.1673 15.4L25.184 14.3834L20.5007 3.33337L15.8173 14.3834L3.83398 15.4L12.934 23.2834L10.2007 35L20.5007 28.7834Z" fill="#FFD75F" />
                </g>
                <g clipPath="url(#clip1_10444_724)">
                    <path d="M75.5007 28.7834L85.8007 35L83.0673 23.2834L92.1673 15.4L80.184 14.3834L75.5007 3.33337L70.8173 14.3834L58.834 15.4L67.934 23.2834L65.2007 35L75.5007 28.7834Z" fill="#FFD75F" />
                </g>
                <g clipPath="url(#clip2_10444_724)">
                    <path d="M130.501 28.7834L140.801 35L138.067 23.2834L147.167 15.4L135.184 14.3834L130.501 3.33337L125.817 14.3834L113.834 15.4L122.934 23.2834L120.201 35L130.501 28.7834Z" fill="#FFD75F" />
                </g>
                <g clipPath="url(#clip3_10444_724)">
                    <path d="M185.501 28.7834L195.801 35L193.067 23.2834L202.167 15.4L190.184 14.3834L185.501 3.33337L180.817 14.3834L168.834 15.4L177.934 23.2834L175.201 35L185.501 28.7834Z" fill="#FFD75F" />
                </g>
                <g clipPath="url(#clip4_10444_724)">
                    <path d="M240.501 28.7834L250.801 35L248.067 23.2834L257.167 15.4L245.184 14.3834L240.501 3.33337L235.817 14.3834L223.834 15.4L232.934 23.2834L230.201 35L240.501 28.7834Z" fill="#FFD75F" />
                </g>
                <defs>
                    <clipPath id="clip0_10444_724">
                        <rect width="40" height="40" fill="white" transform="translate(0.5)" />
                    </clipPath>
                    <clipPath id="clip1_10444_724">
                        <rect width="40" height="40" fill="white" transform="translate(55.5)" />
                    </clipPath>
                    <clipPath id="clip2_10444_724">
                        <rect width="40" height="40" fill="white" transform="translate(110.5)" />
                    </clipPath>
                    <clipPath id="clip3_10444_724">
                        <rect width="40" height="40" fill="white" transform="translate(165.5)" />
                    </clipPath>
                    <clipPath id="clip4_10444_724">
                        <rect width="40" height="40" fill="white" transform="translate(220.5)" />
                    </clipPath>
                </defs>
            </svg>
            <p>“Made by people who value audience well-being (like me) and care about marketers success and happiness. Thanks to Creatosaurus, we&apos;ve replaced almost a dozen marketing tools that we no longer need.”</p>
            <span>Aditya Malhotra, Head of Growth at Deciml</span>
        </div>
    )
}

export default UserRating
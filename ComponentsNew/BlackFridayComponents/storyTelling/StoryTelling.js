import React from 'react'
import styles from './StoryTelling.module.css'

const StoryTelling = ({ hideTitle }) => {
    const info = [{
        name: "Devyani Pawar, Founder at DP House of Media",
        info: "In the face of a surge in clients, DP House of Media encountered several obstacles. These included the need for efficient team collaboration and producing high-quality consistent post designs. Creatosaurus is our go-to platform.",
        head: "50hrs",
        subHead: "Saved Manual Work",
        head1: "60%",
        subHead1: "Reduction in Backlogs",
        head2: "55%",
        subHead2: "Improved Collaboration",
        textColor: "#FFD75F",
        backColor: "#332B13",
        companyName: "DP House of Media",
        companyNameColor: "#FFF7DF"

    }, {
        name: "Amit Chavan, Co-Founder at Sobot",
        info: "The AI design editor and content writer cut our campaign creation time by almost half, giving us the edge in efficiency. Thanks, Creatosaurus, for making the complex simple and streamlining our entire social media marketing.",
        head: "",
        subHead: "",
        head1: "$1900",
        subHead1: "Dollars Saved",
        head2: "3x",
        subHead2: "Content Frequency",
        textColor: "#00ECC2",
        backColor: "#002F27",
        companyName: "Sobot",
        companyNameColor: "#ccfbf3"
    }, {
        name: "Akshata Fernando, Founder at Digiholic",
        info: "The scheduling tool lets us post at the perfect moments, boosting our reach by 25%. No more manual logins or last-minute posts. With streamlined management and boosted engagement, Creatosaurus is hassle-free and effective.",
        head: "25%",
        subHead: "Improvemed Posting",
        head1: "45%",
        subHead1: "Increased Content Creation",
        head2: "60%",
        subHead2: "Cost savings",
        textColor: "#FF8A25",
        backColor: "#331C07",
        companyName: "Digiholic",
        companyNameColor: "#FFE8D3"
    }, {
        name: "Jatin Chaudhary, Co-Founder at eChai Ventures",
        info: "With seamless poster creation, social media scheduling, and inbox management, we've enhanced our community communication and engagement. Thank you, Creatosaurus, for being our digital partner!",
        head: "",
        subHead: "",
        head1: "80%",
        subHead1: "Time Saved in Poster Design",
        head2: "60%",
        subHead2: "Better Communication",
        textColor: "#FF43CA",
        backColor: "#330D28",
        companyName: "eChai Ventures",
        companyNameColor: "#FFD9F4"
    }]

    return (
        <div className={styles.storyTellingContainer}>
            {
                hideTitle ?
                    null
                    :
                    <h2>Creatosaurus helps eliminate the busywork, so you can focus on storytelling.</h2>
            }
            <div className={styles.gridContainer}>
                {
                    info.map((data, index) => {
                        return <div key={index} className={styles.card}>
                            <div className={styles.info} style={{ backgroundColor: data.backColor, color: data.textColor }}>
                                <svg width="43" height="30" viewBox="0 0 43 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.25 30L10 20C7.25 20 4.89583 19.0208 2.9375 17.0625C0.979167 15.1042 0 12.75 0 10C0 7.25 0.979167 4.89583 2.9375 2.9375C4.89583 0.979167 7.25 0 10 0C12.75 0 15.1042 0.979167 17.0625 2.9375C19.0208 4.89583 20 7.25 20 10C20 10.9583 19.8854 11.8438 19.6562 12.6562C19.4271 13.4688 19.0833 14.25 18.625 15L10 30H4.25ZM26.75 30L32.5 20C29.75 20 27.3958 19.0208 25.4375 17.0625C23.4792 15.1042 22.5 12.75 22.5 10C22.5 7.25 23.4792 4.89583 25.4375 2.9375C27.3958 0.979167 29.75 0 32.5 0C35.25 0 37.6042 0.979167 39.5625 2.9375C41.5208 4.89583 42.5 7.25 42.5 10C42.5 10.9583 42.3854 11.8438 42.1562 12.6562C41.9271 13.4688 41.5833 14.25 41.125 15L32.5 30H26.75ZM10 13.75C11.0417 13.75 11.9271 13.3854 12.6562 12.6562C13.3854 11.9271 13.75 11.0417 13.75 10C13.75 8.95833 13.3854 8.07292 12.6562 7.34375C11.9271 6.61458 11.0417 6.25 10 6.25C8.95833 6.25 8.07292 6.61458 7.34375 7.34375C6.61458 8.07292 6.25 8.95833 6.25 10C6.25 11.0417 6.61458 11.9271 7.34375 12.6562C8.07292 13.3854 8.95833 13.75 10 13.75ZM32.5 13.75C33.5417 13.75 34.4271 13.3854 35.1562 12.6562C35.8854 11.9271 36.25 11.0417 36.25 10C36.25 8.95833 35.8854 8.07292 35.1562 7.34375C34.4271 6.61458 33.5417 6.25 32.5 6.25C31.4583 6.25 30.5729 6.61458 29.8438 7.34375C29.1146 8.07292 28.75 8.95833 28.75 10C28.75 11.0417 29.1146 11.9271 29.8438 12.6562C30.5729 13.3854 31.4583 13.75 32.5 13.75Z" fill={data.textColor} />
                                </svg>

                                <div>
                                    <span className={styles.head}>{data.head === "" ? "\u00A0" : data.head}</span>
                                    <p className={styles.subHead}>{data.subHead === "" ? "\u00A0" : data.subHead}</p>

                                    <span className={styles.head}>{data.head1}</span>
                                    <p className={styles.subHead}>{data.subHead1}</p>

                                    <span className={styles.head}>{data.head2}</span>
                                    <p className={styles.subHead}>{data.subHead2}</p>

                                    <div className={styles.agency} style={{
                                        color: data.companyNameColor
                                    }}>- {data.companyName}</div>
                                </div>
                            </div>

                            <div className={styles.userInfo} style={{ backgroundColor: data.textColor }}>
                                <svg width="43" height="30" viewBox="0 0 43 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.25 30L10 20C7.25 20 4.89583 19.0208 2.9375 17.0625C0.979167 15.1042 0 12.75 0 10C0 7.25 0.979167 4.89583 2.9375 2.9375C4.89583 0.979167 7.25 0 10 0C12.75 0 15.1042 0.979167 17.0625 2.9375C19.0208 4.89583 20 7.25 20 10C20 10.9583 19.8854 11.8438 19.6562 12.6562C19.4271 13.4688 19.0833 14.25 18.625 15L10 30H4.25ZM26.75 30L32.5 20C29.75 20 27.3958 19.0208 25.4375 17.0625C23.4792 15.1042 22.5 12.75 22.5 10C22.5 7.25 23.4792 4.89583 25.4375 2.9375C27.3958 0.979167 29.75 0 32.5 0C35.25 0 37.6042 0.979167 39.5625 2.9375C41.5208 4.89583 42.5 7.25 42.5 10C42.5 10.9583 42.3854 11.8438 42.1562 12.6562C41.9271 13.4688 41.5833 14.25 41.125 15L32.5 30H26.75ZM10 13.75C11.0417 13.75 11.9271 13.3854 12.6562 12.6562C13.3854 11.9271 13.75 11.0417 13.75 10C13.75 8.95833 13.3854 8.07292 12.6562 7.34375C11.9271 6.61458 11.0417 6.25 10 6.25C8.95833 6.25 8.07292 6.61458 7.34375 7.34375C6.61458 8.07292 6.25 8.95833 6.25 10C6.25 11.0417 6.61458 11.9271 7.34375 12.6562C8.07292 13.3854 8.95833 13.75 10 13.75ZM32.5 13.75C33.5417 13.75 34.4271 13.3854 35.1562 12.6562C35.8854 11.9271 36.25 11.0417 36.25 10C36.25 8.95833 35.8854 8.07292 35.1562 7.34375C34.4271 6.61458 33.5417 6.25 32.5 6.25C31.4583 6.25 30.5729 6.61458 29.8438 7.34375C29.1146 8.07292 28.75 8.95833 28.75 10C28.75 11.0417 29.1146 11.9271 29.8438 12.6562C30.5729 13.3854 31.4583 13.75 32.5 13.75Z" fill={data.backColor} />
                                </svg>

                                <div style={{ color: data.backColor }}>
                                    <p>{data.info}</p>
                                    <span>- {data.name}</span>
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default StoryTelling
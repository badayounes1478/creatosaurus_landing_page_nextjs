import React from 'react'
import styles from './PlanPeriod.module.css'
import Link from 'next/link'

const PlanPeriod = ({ planName, showIndianPrice, planDuration, changePlanDuration, isBlakFridayDeal }) => {

    const checkOriginalPrice = () => {
        if (isBlakFridayDeal) {
            if (showIndianPrice) {
                return {
                    originalPrice: planName === "agency" ? "₹12999" : planName === "startup" ? "₹4999" : "₹1999",
                    discountPrice: planName === "agency" ? "₹1444" : planName === "startup" ? "₹499" : "₹167",
                }
            } else {
                return {
                    originalPrice: planName === "agency" ? "$220" : planName === "startup" ? "$80" : "$30",
                    discountPrice: planName === "agency" ? "$17.67" : planName === "startup" ? "$6.28" : "$2.47",
                }
            }
        } else {
            if (showIndianPrice) {
                return {
                    originalPrice: planName === "agency" ? "₹12999" : planName === "startup" ? "₹4999" : "₹1999",
                    discountPrice: planName === "agency" ? "₹12999" : planName === "startup" ? "₹4999" : "₹1999",
                }
            } else {
                return {
                    originalPrice: planName === "agency" ? "$220" : planName === "startup" ? "$80" : "$30",
                    discountPrice: planName === "agency" ? "$220" : planName === "startup" ? "$80" : "$30",
                }
            }
        }
    }

    return (
        <div className={styles.planPeriodContainer}>
            <span className={styles.title}>1. Choose a period</span>
            <div className={styles.grid}>
                <div className={styles.card} onClick={() => changePlanDuration("monthly")}>
                    {isBlakFridayDeal ? <button>SAVE 0%</button> : null}
                    <div className={`${styles.checkBox} ${planDuration === "monthly" ? styles.checkBoxActive : null}`}>
                        {planDuration === "monthly" ? <div className={styles.check} /> : null}
                    </div>
                    <span className={styles.month}>1 Month</span>
                    <span className={styles.originalPrice}>{checkOriginalPrice().originalPrice}</span>
                    <p>{checkOriginalPrice().originalPrice}<span className={styles.sub}>/month</span></p>
                </div>

                <div className={styles.card} onClick={() => changePlanDuration("yearly")}>
                    {isBlakFridayDeal ? <button>SAVE 92%</button> : null}
                    <div className={`${styles.checkBox} ${planDuration === "yearly" ? styles.checkBoxActive : null}`}>
                        {planDuration === "yearly" ? <div className={styles.check} /> : null}
                    </div>
                    {
                        isBlakFridayDeal ?
                            <span className={styles.month}>{planName === "agency" ? "9 Months" : planName === "startup" ? "11 Months" : "12 Months"}</span>
                            :
                            <span className={styles.month}>10 Months</span>
                    }
                    <span className={styles.originalPrice}>{checkOriginalPrice().originalPrice}</span>
                    <p>{checkOriginalPrice().discountPrice}<span className={styles.sub}>/month</span></p>
                    {
                        isBlakFridayDeal ?
                            <span className={styles.free}>{planName === "agency" ? "+3 Month FREE" : planName === "startup" ? "+1 Month FREE" : null}</span>
                            :
                            <span className={styles.free}>+2 Month FREE</span>
                    }
                </div>

                {
                    /*isBlakFridayDeal ? null :
                        <div className={styles.card} onClick={() => changePlanDuration("monthly")}>
                            <button style={{ backgroundColor: '#0078FF' }}>SAVE 92%</button>
                            <span className={styles.month}>Cyber Monday Offer</span>
                            <Link href="/#price">
                                <button style={{ position: "relative", marginTop: 50, width: 140, height: 40, borderRadius: 5, backgroundColor: '#0078FF', cursor: 'pointer' }}>
                                    Claim Deal
                                </button>
                            </Link>
                        </div>*/
                }
            </div>
        </div>
    )
}

export default PlanPeriod
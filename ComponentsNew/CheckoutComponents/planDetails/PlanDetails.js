import React from 'react'
import styles from './PlanDetails.module.css'
import Image from 'next/image'
import GreenCheck from '../../../public/NewAssets/Home/greenCheck.svg'
import YellowCheck from '../../../public/NewAssets/Home/yellowCheck.svg'
import Cancel from '../../../public/NewAssets/Home/cancel.svg'

const PlanDetails = ({ planName }) => {
    return (
        <div className={styles.planDetailContainer}>
            <span className={styles.title}>You&apos;re almost there! Complete your order</span>
            <p>Selected plan: <span style={{ textTransform: 'capitalize' }}>{planName} Plan</span></p>
            <div className={styles.grid}>
                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "10 User" : planName === "startup" ? "5 User" : "1 User"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "10 Workspace" : planName === "startup" ? "5 Workspace" : "1 Workspace"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "Unlimited AI Word Credits" : planName === "startup" ? "50,000 AI Word Credits" : "10,000 AI Word Credits"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "Unlimited AI Image Credits" : planName === "startup" ? "25,000 AI Image Credits" : "5,000 AI Image Credits"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "Unlimited Social Accounts" : planName === "startup" ? "30 Social Media Accounts" : "10 Social Media Accounts"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : YellowCheck} alt="" />
                    </div>
                    <span>{planName === "agency" ? "20 GB Storage" : planName === "startup" ? "6 GB Storage" : "2 GB Storage"}</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Graphic Design Editor</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Premium Stock Asset Library</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Long Form Blog Editor</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Social Media Scheduler</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Hashtag Analytics</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={GreenCheck} alt="" />
                    </div>
                    <span>Social Media Analytics</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "creator" ? Cancel : GreenCheck} alt="" />
                    </div>
                    <span style={planName === "creator" ? { color: '#808080' } : null}>Social Inbox</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : Cancel} alt="" />
                    </div>
                    <span style={planName !== "agency" ? { color: '#808080' } : null}>Priority Support</span>
                </div>

                <div className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={planName === "agency" ? GreenCheck : Cancel} alt="" />
                    </div>
                    <span style={planName !== "agency" ? { color: '#808080' } : null}>Done for you design</span>
                </div>
            </div>
        </div>
    )
}

export default PlanDetails
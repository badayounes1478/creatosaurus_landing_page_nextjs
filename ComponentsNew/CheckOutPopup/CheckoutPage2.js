import React, { useState, useEffect, useRef } from 'react'
import Image from 'next/image'
import PopupLayout from '../PopupLayout/PopupLayout'
import axios from 'axios'
import constant from '../../constant'
import CloseButton from './CloseButton'
import Poster from '../../public/NewAssets/Home/poster.webp'
import Link from 'next/link'
import { toast } from 'react-toastify'

const CheckoutPage2 = ({ selectedPlan, close, isIndian, lifeTimeDeal, isYearlyDuration, handlePaymetComplete, back, couponCode }) => {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    const abortControllerRef = useRef(null)
    const [companyDetails, setCompanyDetails] = useState(false)

    const [name, setName] = useState("")
    const [lastName, setLastName] = useState("")
    const [zipCode, setZipCode] = useState(null)
    const [country, setCountry] = useState([])
    const [states, setStates] = useState([])
    const [selectedCountry, setSelectedCountry] = useState("")
    const [selectedState, setSelectedState] = useState("")
    const [formatedAddress, setFormatedAddress] = useState("")


    const [GSTNumber, setGSTNumber] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [companyAddress, setCompanyAddress] = useState("")
    const [zipCodeCompany, setZipCodeCompany] = useState(null)
    const [companyState, setCompanyState] = useState([])
    const [selectedCountryCompany, setSelectedCountryCompany] = useState("")
    const [selectedStateCompany, setSelectedStateCompany] = useState("")
    const [companyFormatedAddress, setCompanyFormatedAddress] = useState("")
    const [loading, setLoading] = useState(false)

    const getCookie = (name) => {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    useEffect(() => {
        getCountry()

        const token = getCookie("Xh7ERL0G")
        if (token) {
            localStorage.setItem("token", token)
        }
    }, [])

    const getCountry = async () => {
        try {
            const token = localStorage.getItem("token")
            const res = await axios.get(constant.url + "location/country", {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Accept": "application/json"
                }
            })

            setCountry(res.data)
        } catch (error) {
            console.log(error)
        }
    }

    const getStatesCompany = async (state) => {
        try {
            const token = localStorage.getItem("token")
            const res = await axios.get(`${constant.url}location/state?country=${state}`, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Accept": "application/json"
                }
            })

            setCompanyState(res.data)
        } catch (error) {
            console.log(error)
        }
    }

    const getStates = async (state) => {
        try {
            const token = localStorage.getItem("token")
            const res = await axios.get(`${constant.url}location/state?country=${state}`, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Accept": "application/json"
                }
            })

            setStates(res.data)
        } catch (error) {
            console.log(error)
        }
    }

    const getZipCodeInformation = async (value) => {
        // Abort previous request if it exists
        if (abortControllerRef.current) {
            abortControllerRef.current.abort();
        }

        // Create a new AbortController
        abortControllerRef.current = new AbortController();
        const controller = abortControllerRef.current;

        try {
            setZipCode(value);
            const token = localStorage.getItem("token")
            const res = await axios.get(`${constant.url}location/code?code=${value}`, {
                signal: controller.signal,
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            });

            setSelectedCountry(res.data.country)
            setSelectedState(res.data.state)
            setFormatedAddress(res.data.address)
            getStates(res.data.country)
        } catch (error) {
            if (error.name === 'CanceledError') {
                console.log('Request canceled:', error.message);
            } else {
                console.error('An error occurred:', error);
            }
        }
    };


    const getZipCodeInformationCompany = async (value) => {
        // Abort previous request if it exists
        if (abortControllerRef.current) {
            abortControllerRef.current.abort();
        }

        // Create a new AbortController
        abortControllerRef.current = new AbortController();
        const controller = abortControllerRef.current;

        try {
            setZipCodeCompany(value);
            const token = localStorage.getItem("token")
            const res = await axios.get(`${constant.url}location/code?code=${value}`, {
                signal: controller.signal,
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            });

            setSelectedCountryCompany(res.data.country)
            setSelectedStateCompany(res.data.state)
            setCompanyFormatedAddress(res.data.address)
            getStatesCompany(res.data.country)
        } catch (error) {
            if (error.name === 'CanceledError') {
                console.log('Request canceled:', error.message);
            } else {
                console.error('An error occurred:', error);
            }
        }
    };

    const handleZipCodeChange = (event) => {
        const value = event.target.value;
        getZipCodeInformation(value);
    }

    const handleZipCodeChangeCompany = (event) => {
        const value = event.target.value;
        getZipCodeInformationCompany(value);
    }

    const handleCountryChange = (e) => {
        const value = e.target.value
        setSelectedCountry(value)
        getStates(value)
    }

    const handleCountryChangeCompany = (e) => {
        const value = e.target.value
        setSelectedCountryCompany(value)
        getStatesCompany(value)
    }

    const checkEndDate = () => {
        if (lifeTimeDeal) return "Yearly Deal"

        const currentDate = new Date();
        if (isYearlyDuration) {
            const nextYearDate = new Date(currentDate.setFullYear(currentDate.getFullYear() + 1));
            return `${nextYearDate.getDate()} ${months[nextYearDate.getMonth()]} ${nextYearDate.getFullYear()}`;
        } else {
            const nextMonthDate = new Date(currentDate.setMonth(currentDate.getMonth() + 1));
            return `${nextMonthDate.getDate()} ${months[nextMonthDate.getMonth()]} ${nextMonthDate.getFullYear()}`;
        }
    }

    const payUsingRazorpay = async () => {
        try {
            setLoading(true)
            const token = localStorage.getItem("token")
            const config = {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            }

            const res = await axios.post(`${constant.url}razorpay/payment`, {
                id: selectedPlan.id,
                national: isIndian,
                code: couponCode?._id,
                duration: isYearlyDuration ? "yearly" : "monthly",

                userDetails: {
                    email: "",
                    firstName: name,
                    lastName: lastName,
                    postalCode: zipCode,
                    country: selectedCountry,
                    state: selectedState,
                    phoneNumber: "",
                    address: formatedAddress
                },

                companyDetails: {
                    name: companyName,
                    postalCode: zipCodeCompany,
                    country: selectedCountryCompany,
                    state: selectedStateCompany,
                    address: companyAddress,
                    gst: GSTNumber,
                }
            }, config)

            const data = res.data

            const options = {
                key: "rzp_live_8maZWSYHZOpj8b",
                order_id: data.id,
                ...data,
                handler: async (response) => {
                    const body = {
                        order_id: response.razorpay_order_id,
                        payment_id: response.razorpay_payment_id,
                        signature: response.razorpay_signature
                    }
                    close()

                    try {
                        await axios.post(`${constant.url}razorpay/verify`, body, config)
                        handlePaymetComplete()
                    } catch (error) {
                        alert("payment failed")
                    }
                }
            };

            const rzp = new window.Razorpay(options);
            rzp.open();
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    const payUsingStripe = async () => {
        try {
            setLoading(true)
            const token = localStorage.getItem("token")
            const config = {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            }

            const res = await axios.post(`${constant.url}stripe/payment`, {
                id: selectedPlan.id,
                national: isIndian,
                code: couponCode?._id,
                duration: isYearlyDuration ? "yearly" : "monthly",

                userDetails: {
                    email: "",
                    firstName: name,
                    lastName: lastName,
                    postalCode: zipCode,
                    country: selectedCountry,
                    state: selectedState,
                    phoneNumber: "",
                    address: formatedAddress
                },

                companyDetails: {
                    name: companyName,
                    postalCode: zipCodeCompany,
                    country: selectedCountryCompany,
                    state: selectedStateCompany,
                    address: companyAddress,
                    gst: GSTNumber,
                }
            }, config)

            if (res.data) {
                window.location.href = res.data;
            }
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    const pay = () => {
        if (name === "" || lastName === "" || zipCode === "" || selectedCountry === "" || selectedState === "") return toast.error("Please fill all fields")
        if (companyDetails) {
            if (isIndian) {
                if (GSTNumber === "" || GSTNumber.length < 15) return toast.error("Please fill correct gst number")
            }

            if (companyName === "" || companyAddress === "" || zipCodeCompany === "" || selectedCountryCompany === "" || selectedStateCompany === "") return toast.error("Please fill all details of company")
        }

        if (loading) return

        if (isIndian) {
            payUsingRazorpay()
        } else {
            payUsingStripe()
        }
    }

    const formatPrice = (number) => {
        number = parseInt(number)
        return number.toLocaleString('en-IN');
    }

    const handleDiscount = () => {
        let price = isYearlyDuration ? selectedPlan.year : selectedPlan.month
        const currency = lifeTimeDeal ? "$" : isIndian ? "₹" : "$"

        if (couponCode) {
            let discountAmount = (price * couponCode.discount) / 100;
            let finalPrice = price - discountAmount;
            finalPrice = formatPrice(finalPrice)
            return currency + finalPrice
        }

        price = formatPrice(price)
        return currency + price
    }

    return (
        <PopupLayout close={close}>
            <div className='relative'>
                <CloseButton close={close} />
                <div className='flex bg-white w-[80vw] max-w-[814px] rounded-[10px] relative overflow-hidden' onClick={(e) => e.stopPropagation()}>
                    <div className='hidden lg:flex w-[407px] min-w-[407px] h-[560px] object-contain'>
                        <Image src={Poster} alt='' />
                    </div>

                    <div className='pt-[15px] sm:pt-[30px] pb-[15px] sm:pb-[25px] px-[15px] sm:px-[30px] w-full'>
                        <div className='flex items-center'>
                            <svg onClick={back} className='cursor-pointer' width="26" height="27" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect y="0.5" width="26" height="26" rx="13" fill="#F8F8F9" />
                                <path d="M14.6956 8.97852L10.1738 13.5003L14.6956 18.022" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>

                            <span className='text-[22px] font-semibold ml-[10px]'>Finalise payment</span>
                        </div>

                        <div className='flex items-center mt-[8px]'>
                            <svg className='mr-[5px]' width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.75 10.25L6.75 13.25L14.25 5.75" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>

                            <span className='text-[14px] text-[#333333]'>No auto renewal. Upgrade anytime.</span>
                        </div>

                        <div className='h-[222px] overflow-y-scroll'>
                            <div className='mt-[20px] grid grid-cols-2 gap-x-[15px]'>
                                <input
                                    type='text'
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    placeholder='first name'
                                    className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px]' />

                                <input
                                    type='text'
                                    value={lastName}
                                    onChange={(e) => setLastName(e.target.value)}
                                    placeholder='last name'
                                    className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px]' />
                            </div>

                            <div className='mt-[15px] flex items-center relative'>
                                <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.29102 9.375L12.4993 13.0208L17.7077 9.375" stroke="#C4C4C4" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M2.08398 17.708V7.29134C2.08398 6.14075 3.01672 5.20801 4.16732 5.20801H20.834C21.9846 5.20801 22.9173 6.14075 22.9173 7.29134V17.708C22.9173 18.8586 21.9846 19.7913 20.834 19.7913H4.16732C3.01672 19.7913 2.08398 18.8586 2.08398 17.708Z" stroke="#C4C4C4" />
                                </svg>

                                <input
                                    type='text'
                                    value={zipCode}
                                    onChange={handleZipCodeChange}
                                    placeholder='Postal code'
                                    className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full' />
                            </div>

                            {
                                zipCode?.length > 0 && <>
                                    <div className='mt-[15px] flex items-center relative'>
                                        <select
                                            className='absolute opacity-0 left-0 right-0 bottom-0 top-0'
                                            value={selectedCountry}
                                            onChange={handleCountryChange}>
                                            {
                                                country.map(data => {
                                                    return <option key={data.country_name} value={data.country_name}>{data.country_name}</option>
                                                })
                                            }
                                        </select>

                                        <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.5007 22.9163C18.2536 22.9163 22.9173 18.2526 22.9173 12.4997C22.9173 6.74671 18.2536 2.08301 12.5007 2.08301C6.74768 2.08301 2.08398 6.74671 2.08398 12.4997C2.08398 18.2526 6.74768 22.9163 12.5007 22.9163Z" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M2.60352 13.0205L8.33268 15.1038L7.29102 18.7497L8.33268 21.8747" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M17.709 21.3538L17.1882 18.7497L14.584 17.708V14.0622L17.709 13.0205L22.3965 13.5413" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M19.7917 5.72949L19.2708 7.29199L15.625 7.81283V10.9378L18.2292 9.89616H20.3125L22.3958 10.9378" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M2.60352 10.9375L5.20768 8.85417L7.81185 8.33333L9.89518 5.20833L8.85352 3.125" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>

                                        <span className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full flex items-center'>{selectedCountry ? selectedCountry : "Please select a country"}</span>

                                        <svg className='absolute right-[10px]' width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.5706 10.2139L11.9992 14.7853L7.42773 10.2139" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>

                                    <div className='mt-[15px] flex items-center relative'>
                                        <select
                                            className='absolute opacity-0 left-0 right-0 bottom-0 top-0'
                                            value={selectedState}
                                            onChange={(e) => setSelectedState(e.target.value)}>
                                            {
                                                states.map(data => {
                                                    return <option key={data.state_name} value={data.state_name}>{data.state_name}</option>
                                                })
                                            }
                                        </select>

                                        <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M20.8327 10.4163C20.8327 15.0187 12.4993 22.9163 12.4993 22.9163C12.4993 22.9163 4.16602 15.0187 4.16602 10.4163C4.16602 5.81397 7.89697 2.08301 12.4993 2.08301C17.1017 2.08301 20.8327 5.81397 20.8327 10.4163Z" stroke="#808080" />
                                            <path d="M12.5007 11.4583C13.076 11.4583 13.5423 10.992 13.5423 10.4167C13.5423 9.84137 13.076 9.375 12.5007 9.375C11.9253 9.375 11.459 9.84137 11.459 10.4167C11.459 10.992 11.9253 11.4583 12.5007 11.4583Z" fill="#808080" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>

                                        <span className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full flex items-center'>{selectedState}</span>

                                        <svg className='absolute right-[10px]' width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.5706 10.2139L11.9992 14.7853L7.42773 10.2139" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>
                                </>
                            }

                            <div className='flex items-center mt-[15px]'>
                                <div onClick={() => setCompanyDetails((prev) => !prev)} className={`size-[16px] rounded-[2px] ${companyDetails ? "bg-[#000000]" : "bg-[#DCDCDC]"} flex justify-center items-center cursor-pointer`}>
                                    <svg width="10" height="8" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.800781 4.6L3.20078 7L9.20078 1" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                                <span className='ml-[10px] mr-[5px] text-[12px] text-[#808080]'>Add company details (optional)</span>
                                <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clipPath="url(#clip0_4077_28152)">
                                        <path d="M5 4.79199V6.87533" stroke="#808080" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M5 3.13013L5.00462 3.125" stroke="#808080" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M4.88737 9.00618C7.18854 9.00618 9.05404 7.14068 9.05404 4.83952C9.05404 2.53833 7.18854 0.672852 4.88737 0.672852C2.58618 0.672852 0.720703 2.53833 0.720703 4.83952C0.720703 7.14068 2.58618 9.00618 4.88737 9.00618Z" stroke="#808080" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_4077_28152">
                                            <rect width="10" height="10" fill="white" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>


                            {
                                companyDetails &&
                                <>
                                    {
                                        isIndian &&
                                        <div className='mt-[15px]'>
                                            <span className='text-[14px] font-medium'>GST number</span>
                                            <input
                                                type='text'
                                                value={GSTNumber}
                                                onChange={(e) => setGSTNumber(e.target.value)}
                                                placeholder='Enter 15 digit GST number'
                                                className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px] w-full' />
                                        </div>
                                    }

                                    <div className='mt-[15px]'>
                                        <span className='text-[14px] font-medium'>Company name</span>
                                        <input
                                            type='text'
                                            value={companyName}
                                            onChange={(e) => setCompanyName(e.target.value)}
                                            placeholder='Enter full company name'
                                            className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px] w-full' />
                                    </div>

                                    <div className='mt-[15px]'>
                                        <span className='text-[14px] font-medium'>Company address</span>
                                        <input
                                            type='text'
                                            value={companyAddress}
                                            onChange={(e) => setCompanyAddress(e.target.value)}
                                            placeholder='Enter company address'
                                            className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px] w-full' />
                                    </div>

                                    <div className='mt-[15px] flex items-center relative'>
                                        <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.29102 9.375L12.4993 13.0208L17.7077 9.375" stroke="#C4C4C4" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M2.08398 17.708V7.29134C2.08398 6.14075 3.01672 5.20801 4.16732 5.20801H20.834C21.9846 5.20801 22.9173 6.14075 22.9173 7.29134V17.708C22.9173 18.8586 21.9846 19.7913 20.834 19.7913H4.16732C3.01672 19.7913 2.08398 18.8586 2.08398 17.708Z" stroke="#C4C4C4" />
                                        </svg>

                                        <input
                                            type='text'
                                            value={zipCodeCompany}
                                            onChange={handleZipCodeChangeCompany}
                                            placeholder='Postal code'
                                            className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full' />
                                    </div>

                                    {
                                        selectedCountryCompany && <>
                                            <div className='mt-[15px] flex items-center relative'>
                                                <select
                                                    className='absolute opacity-0 left-0 right-0 bottom-0 top-0'
                                                    value={selectedCountryCompany}
                                                    onChange={handleCountryChangeCompany}>
                                                    {
                                                        country.map(data => {
                                                            return <option key={data.country_name} value={data.country_name}>{data.country_name}</option>
                                                        })
                                                    }
                                                </select>

                                                <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12.5007 22.9163C18.2536 22.9163 22.9173 18.2526 22.9173 12.4997C22.9173 6.74671 18.2536 2.08301 12.5007 2.08301C6.74768 2.08301 2.08398 6.74671 2.08398 12.4997C2.08398 18.2526 6.74768 22.9163 12.5007 22.9163Z" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M2.60352 13.0205L8.33268 15.1038L7.29102 18.7497L8.33268 21.8747" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M17.709 21.3538L17.1882 18.7497L14.584 17.708V14.0622L17.709 13.0205L22.3965 13.5413" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M19.7917 5.72949L19.2708 7.29199L15.625 7.81283V10.9378L18.2292 9.89616H20.3125L22.3958 10.9378" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M2.60352 10.9375L5.20768 8.85417L7.81185 8.33333L9.89518 5.20833L8.85352 3.125" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>

                                                <span className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full flex items-center'>{selectedCountryCompany}</span>

                                                <svg className='absolute right-[10px]' width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.5706 10.2139L11.9992 14.7853L7.42773 10.2139" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>
                                            </div>

                                            <div className='mt-[15px] flex items-center relative'>
                                                <select
                                                    className='absolute opacity-0 left-0 right-0 bottom-0 top-0'
                                                    value={selectedStateCompany}
                                                    onChange={(e) => setSelectedStateCompany(e.target.value)}>
                                                    {
                                                        companyState.map(data => {
                                                            return <option key={data.state_name} value={data.state_name}>{data.state_name}</option>
                                                        })
                                                    }
                                                </select>

                                                <svg className='absolute left-[10px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M20.8327 10.4163C20.8327 15.0187 12.4993 22.9163 12.4993 22.9163C12.4993 22.9163 4.16602 15.0187 4.16602 10.4163C4.16602 5.81397 7.89697 2.08301 12.4993 2.08301C17.1017 2.08301 20.8327 5.81397 20.8327 10.4163Z" stroke="#808080" />
                                                    <path d="M12.5007 11.4583C13.076 11.4583 13.5423 10.992 13.5423 10.4167C13.5423 9.84137 13.076 9.375 12.5007 9.375C11.9253 9.375 11.459 9.84137 11.459 10.4167C11.459 10.992 11.9253 11.4583 12.5007 11.4583Z" fill="#808080" stroke="#808080" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>

                                                <span className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] pl-[45px] pr-[10px] text-[16px] rounded-[10px] w-full flex items-center'>{selectedStateCompany}</span>

                                                <svg className='absolute right-[10px]' width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.5706 10.2139L11.9992 14.7853L7.42773 10.2139" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>
                                            </div>
                                        </>
                                    }
                                </>
                            }
                        </div>


                        <div className='text-[12px] text-[#707070] flex justify-between items-center mt-[20px]'>
                            <span>Ends on</span>
                            <span>{checkEndDate()}</span>
                        </div>

                        <div className='text-[14px] font-medium text-[#333333] flex justify-between items-center mt-[10px]'>
                            <span>Due Today</span>
                            <span>{handleDiscount()}</span>
                        </div>

                        <button onClick={pay} className='h-[50px] bg-black text-white w-full rounded-[10px] font-semibold mt-[20px] flex justify-center items-center'>
                            {
                                loading ? <div role="status">
                                    <svg aria-hidden="true" className="w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-white" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                    </svg>
                                    <span className="sr-only">Loading...</span>
                                </div> : `Buy ${selectedPlan.planName}`
                            }
                        </button>
                        <p className='text-[12px] mt-[10px] text-[#808080]'>
                            Payments may be processed internationally.
                            You confirm that the location information provided is accurate.
                            By continuing, you agree to the Creatosaurus’s&nbsp;
                            <span className='underline'>
                                <Link href="/terms">Terms of Service.</Link>
                            </span>
                            &nbsp;Read our&nbsp;
                            <span className='underline'>
                                <Link href="privacy">Privacy Policy.</Link>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </PopupLayout>
    )
}

export default CheckoutPage2
import React from 'react'

const CloseButton = ({ close }) => {
    return (
        <svg onClick={close} className='absolute right-[-40px] top-0 z-[102] cursor-pointer group' width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle className='opacity-70 group-hover:opacity-100' cx="15" cy="15" r="15" fill="#333333" />
            <path d="M10.6684 20.1188L9.88086 19.3313L14.2121 15.0001L9.88086 10.6688L10.6684 9.88135L14.9996 14.2126L19.3309 9.88135L20.1184 10.6688L15.7871 15.0001L20.1184 19.3313L19.3309 20.1188L14.9996 15.7876L10.6684 20.1188Z" fill="white" />
        </svg>
    )
}

export default CloseButton
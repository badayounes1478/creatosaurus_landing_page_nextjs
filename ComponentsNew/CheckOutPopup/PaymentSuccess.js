import React from 'react'
import PopupLayout from '../PopupLayout/PopupLayout'
import CloseButton from './CloseButton'
import Link from 'next/link'

const PaymentSuccess = ({ close, selectedPlan }) => {
    return (
        <PopupLayout close={close}>
            <div className='relative'>
                <CloseButton close={close} />
                <div className='flex flex-col bg-white w-[80vw] max-w-[500px] rounded-[10px] relative overflow-hidden p-[15px]' onClick={(e) => e.stopPropagation()}>
                    <span className='text-[14px] font-semibold pb-[10px] border-b-[0.5px] border-[#DCDCDC] w-full'>You’ve successfully upgraded to {selectedPlan.planName} 🎉</span>

                    <p className='text-[12px] text-[#333333] mt-[15px]'>Welcome to the next level of creativity and growth. With your Pro plan, you now have access to powerful AI tools designed to supercharge your content creation, marketing, and social media strategy.</p>

                    <ul className='list-disc mx-[15px] mt-[10px] text-[12px] text-[#333333]'>
                        <li>Start collaborating by
                            <span className='text-[#0078FF] underline'>
                                <Link href="https://www.app.creatosaurus.io/settings/workspace/members"> inviting your team members </Link>
                            </span>
                            to your workspace.</li>
                        <li>Manage different projects efficiently by creating additional
                            <span className='text-[#0078FF] underline'>
                                <Link href="https://www.app.creatosaurus.io/settings/workspace"> new workspaces </Link>
                            </span>
                        </li>
                        <li>
                            <span className='text-[#0078FF] underline'>
                                <Link href="https://www.app.creatosaurus.io/social-accounts"> Connect your social channels </Link>
                            </span>
                            to streamline content creation, scheduling, inbox and analytics.</li>
                        <li>
                            You can view and download your invoice in the
                            <span className='text-[#0078FF] underline'>
                                <Link href="https://www.app.creatosaurus.io/settings/billing"> Billing </Link>
                            </span>
                            section.</li>
                        <li>
                            You can check your subscription details, track AI credits and expiry date in the
                            <span className='text-[#0078FF] underline'>
                                <Link href="https://www.app.creatosaurus.io/settings/plan"> Plan </Link>
                            </span>
                            section.</li>
                    </ul>

                    <p className='text-[12px] text-[#333333] mt-[10px]'>Get ready to create, inspire, and grow like never before!</p>
                    <p className='text-[12px] text-[#333333] mt-[10px]'>
                        If you need any assistance or have questions, feel free to
                        <span className='text-[#0078FF] underline'>
                            <Link href="https://www.creatosaurus.io/contact"> reach out. </Link>
                        </span> We’re here to support your journey!
                    </p>

                    <button className='bg-[#0078FF] rounded-[5px] text-[12px] font-semibold text-white h-[34px] mt-[15px]' onClick={() => window.open("https://www.app.creatosaurus.io")}>Let’s Go {selectedPlan.planName}!</button>
                </div>
            </div>
        </PopupLayout>
    )
}

export default PaymentSuccess
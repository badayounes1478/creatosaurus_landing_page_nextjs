import { useEffect, useRef } from 'react';

const BlogContent = ({ isVisible, content, activeSection }) => {
    const contentRef = useRef(null);

    useEffect(() => {
        if (activeSection && contentRef.current) {
            const escapedSection = CSS.escape(activeSection);
            const activeElement = contentRef.current.querySelector(`#link-${escapedSection}`);

            if (activeElement) {
                activeElement.scrollIntoView({
                    behavior: 'smooth',
                    block: 'center',
                    inline: 'nearest'
                });
            }
        }
    }, [activeSection]);

    return (
        <>
            {
                isVisible &&
                <div ref={contentRef} className='w-[20%] left-[3%] fixed pt-[100px] flex flex-col h-[100vh]' id='content'>
                    <div className='text-[14px] font-semibold px-[15px] py-[5px]'>Table of contents</div>
                    <div className='h-[calc(100vh-131px)] overflow-scroll pb-[20px]'>
                        <ul className='flex flex-col gap-[5px] list-none text-[14px]'>
                            {
                                content &&
                                content.map((item, index) => (
                                    <li key={index}>
                                        <a
                                            id={`link-${item.id}`}
                                            className={`block px-[15px] py-[5px] rounded-[5px] text-[12px] text-[#404040] hover:bg-[rgba(0,120,255,0.10)] ${activeSection === item.id && "bg-[rgba(0,120,255,0.10)]"}`}
                                            href={`#${item.id}`}
                                        >
                                            {item.title}
                                        </a>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            }
        </>
    )
}

export default BlogContent;

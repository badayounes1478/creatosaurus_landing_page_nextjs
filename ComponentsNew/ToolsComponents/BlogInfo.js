import React from 'react'
import Link from 'next/link'
import style from '../../styles/BlogInfo.module.css'

const BlogInfo = ({ post }) => {
    const formatDate = (date) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(date).toLocaleDateString('en-US', options);
    }

    return (
        <div className={style.blogInfo}>
            <span>
                Article By {" "}
                <Link prefetch={false} href="/blog/author/[author]" as={`/blog/author/${post?.authors[0]?.slug}`}>
                    <a>{post?.authors[0]?.name}</a>
                </Link>
            </span>

            <span>
                Reviewed By {" "}
                <Link prefetch={false} href="/blog/author/[author]" as={`/blog/author/${post?.authors[1]?.slug}`}>
                    <a>{post?.authors[1]?.name}</a>
                </Link>
            </span>

            <span>
                Blog Title Generator Tool By {" "}
                <Link prefetch={false} href="https://www.creatosaurus.io/">
                    <a>Creatosaurus.io</a>
                </Link>
            </span>

            <span className={style.lastUpdated}>Last updated on {formatDate(new Date(post?.updated_at))}</span>
        </div>
    )
}

export default BlogInfo
import React from 'react'
import BlogInfo from './BlogInfo';
import style1 from '../../styles/Slug.module.css'

const BlogSection = ({post}) => {
    return (
        <div className={style1.blogContainer} style={{ alignSelf: 'center' }}>
            <div className={style1.postContainer} style={{ margin: 'auto' }}>
                <div className={style1.post}>
                    <div dangerouslySetInnerHTML={{ __html: post?.html }} id='blog' />
                </div>

                <BlogInfo post={post} />
            </div>
        </div>
    )
}

export default BlogSection
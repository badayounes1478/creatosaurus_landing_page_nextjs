import React, { useState } from 'react'
import Compressor from 'compressorjs';

const ImageCompression = () => {
  const [url, setUrl] = useState("")

  const handleFileChange = (e) => {
    const file = e.target.files[0]
    if (!file) return

    new Compressor(file, {
      strict: true,
      checkOrientation: true,
      retainExif: false,
      maxWidth: undefined,
      maxHeight: undefined,
      minWidth: 0,
      minHeight: 0,
      width: undefined,
      height: 1080,
      resize: 'none',
      quality: 0.8,
      mimeType: '',
      convertTypes: 'image/png',
      convertSize: 5000000,

      success: (result) => {
        console.log('Output: ', result);
      }
    })
  }

  return (
    <div>
      <input type='file' onChange={handleFileChange} accept="image/*" />
      <img src={url} alt='' />
    </div>
  )
}

export default ImageCompression
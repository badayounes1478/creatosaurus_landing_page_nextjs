import React from 'react'
import Link from 'next/link'

const Halloween = () => {
    return (
        <div className='flex justify-center items-center'>
            <Link href="/black-friday-cyber-monday">
                <div className='h-[40px] text-[10px]  sm:text-[12px] md:text-[14px] lg:text-[16px] w-full bg-black mt-[70px] fixed top-[0px] flex justify-center items-center text-white font-semibold cursor-pointer z-50 text-center'>
                    🔥⏰🛍️ Black Friday Special Offer (50% off on all paid plans) For first 100 users only. Buy now, use code “BFCM50” at checkout  ⚡🛒🏃
                </div>
            </Link>
        </div>
    )
}

export default Halloween
import React from 'react'
import Image from 'next/image'
import Image1 from '../../public/NewAssets/blackFriday/premium1.png'
import Image2 from '../../public/NewAssets/blackFriday/premium2.png'
import Image3 from '../../public/NewAssets/blackFriday/premium3.png'
import Image4 from '../../public/NewAssets/blackFriday/premium4.png'
import { getAvailableCodes } from '../../utils/CheckAvailableCodes'


const HalloweenTag = () => {
    return (
        <div className='relative flex justify-center items-center'>
            <div className='absolute top-[130px] text-[10px] sm:text-[12px] font-semibold rounded-[50px] bg-[#E6F2FF80] m-auto px-[20px] h-[30px] flex justify-center items-center text-center'>
                Last {getAvailableCodes()} “BFCM50” offer codes remaining, buy now!
            </div>

            <div>
                <div className='absolute size-[50px] left-[10px] bottom-[-200px]'>
                    <Image src={Image1} alt='' />
                </div>

                <div className='absolute size-[50px] left-[100px] bottom-[-400px]'>
                    <Image src={Image2} alt='' />
                </div>

                <div className='absolute size-[50px] right-[10px] bottom-[-400px]'>
                    <Image src={Image3} alt='' />
                </div>

                <div className='absolute size-[50px] right-[100px] bottom-[-300px]'>
                    <Image src={Image4} alt='' />
                </div>
            </div>
        </div>
    )
}

export default HalloweenTag
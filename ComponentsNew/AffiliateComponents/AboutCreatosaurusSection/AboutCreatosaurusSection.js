import React from 'react'
import styles from './AboutCreatosaurusSection.module.css'
import Image from 'next/image'
import banner from '../../../public/Assets/bannerImage.webp';
import PlayButton from '../../../public/Assets/Play.svg';

const AboutCreatosaurusSection = () => {
    return (
        <div className={styles.aboutCreatosaurusContainer}>
            <div className={styles.headerContainer}>
                <span>About Creatosaurus</span>
                <h2>All-In-One Social Media Tool</h2>
                <p>Creatosaurus lets you easily curate ideas, design graphics, write AI content, edit videos, schedule post, search hashtags, craft articles, manage analytics, generate reports, apps & more—in one place.</p>
            </div>

            <div className={styles.video}>
                <Image priority={true} src={banner} alt="hero" objectFit='contain' />
                <div className={styles.youtube}>
                    <Image src={PlayButton} alt="play" objectFit='contain' />
                </div>
            </div>
        </div>
    );
}

export default AboutCreatosaurusSection;
import React, { useState } from 'react'
import styles from './FaqSection.module.css'
import Link from 'next/link';

const FaqSection = () => {
    const [activeIndex, setActiveIndex] = useState([])
    const [questions] = useState([{
        q: "What is Creatosaurus?",
        ans: "Creatosaurus is an all-in-one Social Media Tool. Creatosaurus lets you easily curate ideas, design graphics, write AI content, edit videos, schedule post, search hashtags, craft articles, manage analytics, generate reports, apps & more—in one place."
    }, {
        q: "What shall I do to become the Affiliate?",
        ans: "Signup to the Creatosaurus affiliate program is Free - No initial payments or minimum requirements. Please register here and start earning."
    }, {
        q: "Where can I access your Affiliate Terms & Conditions?",
        ans: "You can find our Affiliate Terms & Conditions by clicking here."
    }, {
        q: "What are your payment terms?",
        ans: "There’s a 30-day pending period after a referral pays for a plan. This is to avoid chargebacks, refunds, and any other fraudulent activity. After 30 days, you’ll be able to withdraw your commission."
    }, {
        q: "What is the eligibility criteria for payout?",
        ans: "To avail payout of your affiliate commission, you must have at least two valid customer referrals, with a minimum approved commission of $50 in your account."
    }, {
        q: "Can I run paid ads? ",
        ans: "No. We have a strict no paid search, or paid social ads policy. PPC bidding is NOT allowed without prior written permission."
    }, {
        q: "How long is your cookie life?",
        ans: "Creatosaurus's affiliate program has a 30-day cookie life."
    }, {
        q: "How much commission will I earn from this program?",
        ans: "You’ll make a 30% recurring commission on all payments with the Creatosaurus Affiliate Program."
    }, {
        q: "How do I track my referrals and link activity?",
        ans: "Sign in to your Creatosaurus dashboard and click on the“Referrals” section on the left side menu to see your referral and link performance."
    }, {
        q: "How will conversions be attributed to my affiliate link?",
        ans: "You can get your unique affiliate link from the“Links” section. Payments within 30 days of signup are eligible for commission. Commissions are paid on a last-click basis meaning that your link must be the last touch point before the user signed up."
    }])

    const toggleQuestion = (index) => {
        if (activeIndex.includes(index)) {
            let filterData = activeIndex.filter(data => data !== index)
            setActiveIndex(filterData)
        } else {
            setActiveIndex((prev) => [...prev, index])
        }
    }

    return (
        <div className={styles.faqSectionContainer}>
            <h2>Frequently Asked Questions</h2>
            <p>We hope these frequently asked questions about Creatosaurus provide clarity. If you have any more questions, please don&apos;t hesitate to reach out to our support team for assistance.</p>
            <div className={styles.questions}>
                {
                    questions.map((data, index) => {
                        return <div key={index} className={styles.card} onClick={() => toggleQuestion(index)}>
                            <div className={styles.row}>
                                <span>{data.q}</span>
                                <div className={styles.svgWrapper}>
                                    {
                                        activeIndex.includes(index) ?
                                            <svg width="44" height="43" viewBox="0 0 44 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22.0011 21.2129L27.3044 26.5162M16.6978 26.5162L22.0011 21.2129L16.6978 26.5162ZM27.3044 15.9096L22.0011 21.2129L27.3044 15.9096ZM22.0011 21.2129L16.6978 15.9096L22.0011 21.2129Z" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                            :
                                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M15 15V22.5M7.5 15H15H7.5ZM22.5 15H15H22.5ZM15 15V7.5V15Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                    }
                                </div>
                            </div>
                            {data.q === "Where can I access your Affiliate Terms & Conditions?" && activeIndex.includes(index) ? (
                                <div className={styles.ans} dangerouslySetInnerHTML={{ __html: data.ans.replace("clicking here", '<a href="/affiliate/terms">clicking here</a>') }} />
                            ) : activeIndex.includes(index) ? (
                                <div className={styles.ans}>{data.ans}</div>
                            ) : null}
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default FaqSection
import React, { useState } from 'react'
import styles from './BlackSection.module.css'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'

const BlackSection = () => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={styles.secondBlackSectionContainer}>
                <p>You focus on telling stories, we do everything else.</p>
                <button onClick={()=> setShowLogin(true)}>Join Affiliate Program</button>
            </div>
        </>
    )
}

export default BlackSection
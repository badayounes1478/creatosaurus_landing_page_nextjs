import React from 'react'
import styles from './PartnerSection.module.css'
import Image from 'next/image'
import reputation from './assets/reputation.svg'
import support from './assets/support.svg'
import bonus from './assets/bonus.svg'
import network from './assets/network.svg'
import commission from './assets/commission.svg'
import cookie from './assets/cookie.svg'
import analytics from './assets/analytics.svg'
import promotion from './assets/promotion.svg'
import payment from './assets/payment.svg'

const PartnerSection = () => {
    const points = [
        {
            name: "Build Your Reputation",
            logo: reputation
        },
        {
            name: "Dedicated Affiliate Support",
            logo: support
        },
        {
            name: "Extra Bonus",
            logo: bonus
        },
        {
            name: "Expand Your Network",
            logo: network
        },
        {
            name: "Industry Leading Commissions",
            logo: commission
        },
        {
            name: "30 Day Cookie Duration",
            logo: cookie
        },
        {
            name: "Live Analytics Dashboard",
            logo: analytics
        },
        {
            name: "Promotion Made Easy",
            logo: promotion
        },
        {
            name: "Hassle Free Monthly Payment",
            logo: payment
        },
    ]


    return (
        <div className={styles.partnerSectionContainer}>
            <div className={styles.headerContainer}>
                <h2>Why Partner with Creatosaurus?</h2>
                <p>As the industry is vast, you can bring many customers on board and earn a 30% recurring commission for up to one year. Here are some top reasons why you should join our partner program.</p>
            </div>

            <div className={styles.pointsContainer}>
                {points.map((data, index) => {
                    return (
                        <div className={styles.point} key={index}>
                            <Image priority={true} src={data.logo} alt="point" />
                            <h2>{data.name}</h2>
                        </div>
                    )
                })}

            </div>
        </div>
    );
}

export default PartnerSection;
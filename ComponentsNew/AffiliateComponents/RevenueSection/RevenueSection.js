import React, { useState } from 'react'
import styles from './RevenueSection.module.css'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth';

const RevenueSection = () => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={styles.revenueSectionContainer}>
                <h2>Receive 30% of revenue generated through your link, forever.</h2>
                <p>You get to enjoy full access to our platform and upcoming features for 1  Year. Your audience gets a special 25% discount on our platform subscription. You get a 30% recurring commission till the subscription is cancelled.</p>
                <button onClick={()=> setShowLogin(true)}>Join Creatosaurus Affiliate Program Now -{'>'}</button>
            </div>
        </>
    );
}

export default RevenueSection;
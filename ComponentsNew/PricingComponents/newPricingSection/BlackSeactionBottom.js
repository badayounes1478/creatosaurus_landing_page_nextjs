import Link from 'next/link'
import React from 'react'

const BlackSeactionBottom = () => {
    return (
        <div className="mt-[100px] max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div className="px-[20px] sm:px-[80px] py-[120px] flex flex-col md:flex-row justify-between items-center bg-black rounded-[10px]">
                <span className="text-white font-bold text-[28px] text-center  sm:text-[36px] md:text-[48px] md:text-left w-full md:max-w-[670px]">You focus on telling stories, we do everything else.</span>
                <Link href="#pricingCards">
                    <button className="h-[50px] w-[230px] bg-[#FF4359] hover:bg-[#e63c50] mt-[30px] md:mt-[0px] rounded-[5px] text-white font-semibold text-[14px] sm:text-[16px] md:text-[20px]">Save 80% cost</button>
                </Link>
            </div>
        </div>
    )
}

export default BlackSeactionBottom
import React from 'react'

const CompareWithOtherAiTools = () => {
    const type = [
        { data: ["Design Editor", "yes", "yes", "no", "no", "no", "no"] },
        { data: ["Video Editor", "yes", "no", "yes", "no", "no", "no"] },
        { data: ["Document Editor", "yes", "no", "no", "yes", "no", "yes"] },
        { data: ["Professional Editing", "yes", "no", "no", "no", "no", "no"] },
        { data: ["Scheduling", "yes", "yes", "no", "no", "yes", "no"] },
        { data: ["Inbox", "yes", "no", "no", "no", "yes", "no"] },
        { data: ["Analytics", "yes", "no", "no", "no", "yes", "no"] },
        { data: ["Brand Kit", "yes", "yes", "yes", "no", "no", "no"] },
        { data: ["Stock Library", "yes", "yes", "yes", "yes", "yes", "no"] },
        { data: ["AI Chat", "yes", "no", "no", "no", "no", "yes"] },
        { data: ["AI image", "yes", "yes", "no", "no", "no", "yes"] },
        { data: ["AI Text", "yes", "yes", "no", "yes", "no", "yes"] },
        { data: ["Basic AI", "yes", "yes", "yes", "yes", "yes", "yes"] },
        { data: ["AI Advance", "yes", "no", "no", "no", "no", "no"] },
        { data: ["App Integrations", "yes", "yes", "no", "yes", "no", "no"] },
    ]

    return (
        <div className="mt-[50px] hidden md:flex flex-col max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <p className="text-center text-[18px]">Here’s how Creatosaurus compares with other AI & Marketing tools</p>

            <div className="grid grid-cols-7 border-[1px] border-[#DCDCDC] mt-[30px] rounded-[10px] overflow-hidden">
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Feature </span>
                    <span>Lists</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Creatosaurus AI</span>
                    <span>All In One</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Design</span>
                    <span>Tool</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Video</span>
                    <span>Tool</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Note Taking</span>
                    <span>Tool</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                    <span>Scheduling Media</span>
                    <span>Tool</span>
                </div>
                <div className="h-[74px] px-[20px] bg-[#F8F8F9] flex flex-col justify-center items-center text-[14px] font-medium">
                    <span>AI</span>
                    <span>Tool</span>
                </div>

                {
                    type.map((data, index) => {
                        return data.data.map((text, index2) => {
                            return <div key={index + index2} className="px-[20px] flex flex-col justify-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                                {
                                    text === "yes" ?
                                        <svg className='m-auto size-[20px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" clipRule="evenodd" d="M12.5007 1.30225C6.31621 1.30225 1.30273 6.31573 1.30273 12.5002C1.30273 18.6846 6.31621 23.6981 12.5007 23.6981C18.6851 23.6981 23.6986 18.6846 23.6986 12.5002C23.6986 6.31573 18.6851 1.30225 12.5007 1.30225ZM7.84486 12.4686C7.53977 12.1635 7.04511 12.1635 6.74001 12.4686C6.43491 12.7737 6.43491 13.2683 6.74001 13.5734L9.865 16.6984C10.1701 17.0035 10.6648 17.0035 10.9698 16.6984L18.2615 9.40676C18.5666 9.10166 18.5666 8.607 18.2615 8.3019C17.9564 7.99681 17.4618 7.99681 17.1567 8.3019L10.4174 15.0411L7.84486 12.4686Z" fill="#009B77" />
                                        </svg>
                                        :
                                        text === "no" ?
                                            <svg className='m-auto size-[22px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.03906 17.9612L12.5001 12.5001M12.5001 12.5001L17.9612 7.03906M12.5001 12.5001L7.03906 7.03906M12.5001 12.5001L17.9612 17.9612" stroke="#FF4359" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                            :
                                            <span className="h-[44px] flex items-center">{text}</span>
                                }
                            </div>
                        })
                    })
                }
            </div>
        </div>
    )
}

export default CompareWithOtherAiTools
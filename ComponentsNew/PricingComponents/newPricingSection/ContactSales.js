import Link from 'next/link'
import React from 'react'

const ContactSales = () => {

    const features = ["Customised limits of AI credits, social accounts, workspace and AI models", "Centrally manage multiple teams, workspace and brands", "API access", "Workspace analytics", "SSO", "Custom apps and integrations", "Design support from our Enterprise creative team", "Dedicated customer success manager"]

    return (
        <div className="mt-[100px] flex justify-center max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div className="border-[1px] border-[#CCE4FF] rounded-[10px] flex flex-col sm:flex-row overflow-hidden max-w-[900px] w-full">
                <div className="px-[45px] py-[50px] bg-[#E6F2FF80] w-full flex flex-col">
                    <button className="w-[108px] h-[25px] bg-white rounded-[50px] text-[10px] font-medium">For organisation</button>
                    <span className="text-[24px] font-semibold mt-[25px]">Creatosaurus Enterprise</span>
                    <p className="max-w-[234px] text-[12px] font-light mt-[10px] text-[#404040]">Empower your organisation with an all in one creative workplace solution.</p>
                    <span className="mt-[25px] text-[20px] font-medium">Let’s talk</span>
                    <p className="text-[12px] font-light text-[#404040]">Get in touch to learn more</p>
                    <Link href="/contact">
                        <button className="rounded-[5px] bg-[#0078FF] hover:bg-[#006ce6] w-full mt-[30px] text-white h-[40px] text-[14px] font-semibold">Contact Sales</button>
                    </Link>
                </div>
                <div className="px-[45px] py-[50px] bg-white w-full">
                    <span className="text-[14px] font-medium mb-[5px]">Everything in Agency, plus:</span>
                    {
                        features.map((data, index) => {
                            return <div key={index} className="mt-[10px] flex items-center text-[12px]">
                                <svg className="min-w-[18px]" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3.75 9.75L6.75 12.75L14.25 5.25" stroke="black" strokeWidth="1.1" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                                <span className="ml-[5px]">{data}</span>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default ContactSales
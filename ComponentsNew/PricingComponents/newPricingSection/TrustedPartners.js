import React from 'react'
import Image from 'next/image'
import motorolaSolutions from "../../BlackFridayComponents/trustedSection/assets/motorolaSolutions.svg"
import decathlon from "../../BlackFridayComponents/trustedSection/assets/decathlon.svg"
import theHealthyBinge from "../../BlackFridayComponents/trustedSection/assets/theHealthyBinge.svg"
import blueBayTravel from "../../BlackFridayComponents/trustedSection/assets/blueBayTravel.svg"
import decim from "../../BlackFridayComponents/trustedSection/assets/decim.svg"
import carCapital from "../../BlackFridayComponents/trustedSection/assets/carCapital.svg"
import billyJ from "../../BlackFridayComponents/trustedSection/assets/billyJ.svg"
import sAssembly from "../../BlackFridayComponents/trustedSection/assets/sAssembly.svg"

const TrustedPartners = () => {
    return (
        <div className="mt-[100px] flex flex-col xl:flex-row items-center xl:items-start justify-between max-[960px]:px-[20px] min-[960px]:px-[80px] max-w-[1350px] m-auto">
            <div className='flex flex-col justify-center items-center'>
                <span className='text-[18px] font-medium text-center w-full'>Get a clear return on investment with Creatosaurus</span>
                <div className='flex gap-x-[20px] md:gap-x-[50px] mt-[20px] max-w-[535px]'>
                    <div className='flex flex-col justify-center items-center text-center'>
                        <span className='text-[26px] md:text-[36px] font-semibold'>50</span>
                        <p className='text-[12px] text-[#333333] text-center'>hours of design saved every month</p>
                    </div>

                    <div className='flex flex-col justify-center items-center text-center'>
                        <span className='text-[26px] md:text-[36px] font-semibold'>3x</span>
                        <p className='text-[12px] text-[#333333] text-center'>content production every month</p>
                    </div>

                    <div className='flex flex-col justify-center items-center text-center'>
                        <span className='text-[26px] md:text-[36px] font-semibold'>60%</span>
                        <p className='text-[12px] text-[#333333] text-center'>reduction in marketing tool costs</p>
                    </div>

                    <div className='flex flex-col justify-center items-center text-center md:w-[290px]'>
                        <span className='text-[26px] md:text-[36px] font-semibold'>55%</span>
                        <p className='text-[12px] text-[#333333]'>Faster collaboration, better communication</p>
                    </div>
                </div>
            </div>

            <div className='flex flex-col max-w-[580px] mt-[50px] xl:mt-[0px]'>
                <span className='text-[18px] font-medium text-center w-full'>Trusted by +30K users from 70+ countries</span>
                <div className='flex flex-col gap-[30px] mt-[30px]'>
                    <div className='flex justify-between items-center gap-x-[30px]'>
                        <Image src={decathlon} alt='' />
                        <Image src={motorolaSolutions} alt='' />
                        <Image src={sAssembly} alt='' />
                    </div>
                    <div className='flex justify-between items-center gap-x-[30px]'>
                        <Image src={billyJ} alt='' />
                        <Image src={carCapital} alt='' />
                        <Image src={theHealthyBinge} alt='' />
                        <Image src={blueBayTravel} alt='' />
                        <Image src={decim} alt='' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TrustedPartners
import React, { useState } from 'react'
import Link from 'next/link'

const PricingCards = ({ isYearlyDuration, isIndian, selectPlan, checkLogin, plans }) => {
    const [showPlan, setShowPlan] = useState([])

    const closeIndex = (index) => {
        let data = showPlan.filter(data => data !== index)
        setShowPlan(data)
    }

    const formatPrice = (number) => {
        number = parseInt(number)
        return number.toLocaleString('en-IN');
    }

    const handleSelectPlan = (type) => {
        if (type === "Creatosaurus Free") return checkLogin()
        const planInfo = plans.find(data => data.planName === type)
        selectPlan(planInfo)
    }

    return (
        <div className="mt-[40px] flex flex-col max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div id='pricingCards' className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-4 gap-[25px] xl:gap-[15px]">
                {
                    plans.map((data, index) => {
                        return <div key={data.planName} style={index === 2 ? { borderColor: data.borderColor, borderWidth: 2 } : { borderColor: data.borderColor }} className="w-full border-[1px] rounded-[10px] overflow-hidden">
                            <div style={{ backgroundColor: data.color }} className="h-[334px] pt-[39px] px-[20px]"> {/* With free trial button height 374 */}
                                <div className="flex justify-between items-center">
                                    <span className="text-[10px] font-medium min-w-[93px] w-[93px] h-[25px] bg-white rounded-[50px] flex justify-center items-center">{data.tag}</span>

                                    {
                                        data.showPremium && <svg className='size-[25px]' width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clipPath="url(#clip0_3052_28109)">
                                                <circle cx="10" cy="10" r="10" fill="#FFBF00" />
                                                <path d="M10.469 6.65052C10.7236 6.49421 10.8933 6.21061 10.8933 5.89128C10.8933 5.39777 10.4936 4.99805 10.0001 4.99805C9.50659 4.99805 9.10687 5.39777 9.10687 5.89128C9.10687 6.21284 9.27658 6.49421 9.53115 6.65052L8.2516 9.20962C8.04839 9.61604 7.52139 9.73216 7.16633 9.44856L5.17666 7.85638C5.28831 7.70676 5.35531 7.52142 5.35531 7.32044C5.35531 6.82693 4.95559 6.42721 4.46208 6.42721C3.96857 6.42721 3.56885 6.82693 3.56885 7.32044C3.56885 7.81395 3.96857 8.21367 4.46208 8.21367H4.47771L5.49822 13.8276C5.62104 14.5065 6.21281 15.0022 6.90506 15.0022H13.0951C13.7852 15.0022 14.3769 14.5087 14.502 13.8276L15.5225 8.21367H15.5381C16.0316 8.21367 16.4313 7.81395 16.4313 7.32044C16.4313 6.82693 16.0316 6.42721 15.5381 6.42721C15.0446 6.42721 14.6449 6.82693 14.6449 7.32044C14.6449 7.52142 14.7119 7.70676 14.8235 7.85638L12.8339 9.44856C12.4788 9.73216 11.9518 9.61604 11.7486 9.20962L10.469 6.65052Z" fill="white" />
                                                <path d="M7.5 13.5H12.5" stroke="#FFBF00" strokeWidth="0.5" strokeLinecap="round" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0_3052_28109">
                                                    <rect width="20" height="20" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                    }
                                </div>

                                <div className="mt-[25px]">
                                    <span className="text-[20px] font-semibold">{data.planName}</span>
                                    <p className="max-w-[220px] text-[12px] font-light text-[#333333] mt-[10px]">{data.info}</p>
                                </div>

                                <div className="mt-[25px] flex flex-col">
                                    <span className="text-[24px] font-semibold">{isIndian ? "₹" : "$"} {isYearlyDuration ? formatPrice(data.year) : formatPrice(data.month)}</span>
                                    <span className="text-[12px] text-[#404040]">/{data.hideMonthYear ? "free" : isYearlyDuration ? "year" : "month"}</span>
                                </div>

                                <button
                                    onClick={() => handleSelectPlan(data.planName)}
                                    style={{ "--button-color": data.buttonColor, '--hover-color': data.buttonHover }}
                                    className="mt-[30px] bg-[--button-color] hover:bg-[--hover-color] text-[14px] font-semibold text-white w-full h-[40px] rounded-[5px]">{data.buttonName}</button>
                                {data.showTrial && <button className="h-[40px] w-full mt-[10px] text-[12px] text-[#808080] rounded-[5px] hover:bg-[#0000000D]">Start 14 days free trial</button>}
                            </div>

                            <div className="py-[20px]">
                                <div className='mb-[5px] relative h-[40px] xl:hidden'>
                                    <div
                                        onClick={() => showPlan.includes(index) ? closeIndex(index) : setShowPlan(prev => [...prev, index])}
                                        className="flex justify-between items-center mb-[5px] cursor-pointer hover:bg-[#f4f4f4] h-[40px] absolute top-0 left-[10px] right-[10px] bottom-0 px-[10px] rounded-[10px]">
                                        <span className="text-[14px] font-medium">Key Features</span>
                                        {
                                            showPlan.includes(index) ?
                                                <svg className="size-[22px]" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.35686 16.0713L13.4997 10.9284L18.6426 16.0713" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg> :
                                                <svg className="size-[22px]" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18.6431 10.9282L13.5003 16.0711L8.35742 10.9282" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>
                                        }
                                    </div>
                                </div>


                                <div className={`${showPlan.includes(index) ? "flex" : "hidden"} xl:flex flex-col px-[20px]`}>
                                    {
                                        data.features.map((text, index) => {
                                            return <div key={text}>
                                                {index === 6 &&
                                                    <>
                                                        <div className='h-[1px] bg-[#DCDCDCB3] mt-[10px]' />
                                                        <span className="text-[14px] font-medium block mt-[10px]">{data.planHead}</span>
                                                    </>
                                                }
                                                <div className="mt-[10px]">
                                                    <div className="flex">
                                                        <svg className="mr-[5px]" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.75 9.75L6.75 12.75L14.25 5.25" stroke="black" strokeWidth="1.1" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                        <span className="text-[12px]">{text}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>

            <Link href="#compare-plan-features">
                <button className="m-auto mt-[40px] flex items-center justify-center w-[260px] hover:bg-[#0000000D] h-[40px] rounded-[5px]">
                    <span className="font-medium mr-[7px]">Compare all features</span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" clipRule="evenodd" d="M13.0297 8.21975C13.1703 8.3604 13.2493 8.55113 13.2493 8.75C13.2493 8.94887 13.1703 9.1396 13.0297 9.28025L8.5297 13.7803C8.38905 13.9209 8.19832 13.9998 7.99945 13.9998C7.80058 13.9998 7.60985 13.9209 7.4692 13.7803L2.9692 9.28025C2.89757 9.21106 2.84043 9.12831 2.80112 9.0368C2.76182 8.9453 2.74113 8.84689 2.74026 8.7473C2.7394 8.64772 2.75837 8.54896 2.79608 8.45678C2.83379 8.36461 2.88948 8.28087 2.9599 8.21045C3.03032 8.14003 3.11406 8.08434 3.20624 8.04663C3.29841 8.00892 3.39717 7.98995 3.49675 7.99081C3.59634 7.99168 3.69475 8.01237 3.78625 8.05167C3.87776 8.09098 3.96052 8.14812 4.0297 8.21975L7.24945 11.4395V2.75C7.24945 2.55109 7.32847 2.36032 7.46912 2.21967C7.60977 2.07902 7.80054 2 7.99945 2C8.19836 2 8.38913 2.07902 8.52978 2.21967C8.67043 2.36032 8.74945 2.55109 8.74945 2.75V11.4395L11.9692 8.21975C12.1098 8.07915 12.3006 8.00016 12.4995 8.00016C12.6983 8.00016 12.8891 8.07915 13.0297 8.21975Z" fill="#333333" />
                    </svg>
                </button>
            </Link>
        </div>
    )
}

export default PricingCards
import Link from 'next/link'
import React from 'react'

const BlackSectionSales = () => {
    return (
        <div className="mt-[100px] bg-black py-[100px] flex justify-center items-center max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div className="max-w-[1087px] flex flex-col">
                <span className="text-white text-[32px] sm:text-[36px] md:text-[44px] font-semibold text-center">Need help finding the right Creatosaurus plan for your needs?</span>
                <p className="mt-[15px] max-w-[704px] text-white m-auto text-center text-[14px] sm:text-[16px] md:text-[18px] font-light">
                    For inquiries regarding selecting the right plan or creating a personalized package for your organization, please contact our Sales team. We would be happy to assist you. Talk to our team 24/7
                </p>
                <Link href="/contact">
                    <button className="w-[362px] bg-[#FF4359] hover:bg-[#e63c50] rounded-[5px] h-[50px] text-white text-[16px] sm:text-[18px] md:text-[20px] font-semibold mt-[40px] m-auto">Contact Sales</button>
                </Link>
            </div>
        </div>
    )
}

export default BlackSectionSales
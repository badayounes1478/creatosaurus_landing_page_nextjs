import React from 'react'

const NewPricingSection = ({ isYearlyDuration, changeDuration }) => {
    return (
        <div className="mt-[160px] max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <h1 className="text-[32px] sm:text-[36px] md:text-[44px] font-semibold text-center">One AI tool for your complete marketing</h1>
            <p className="max-w-[960px] m-auto text-[14px] sm:text-[16px] md:text-[18px] text-[#333333] text-center mt-[10px]">Simple plans for maximum creative impact. Break free from content chaos & supercharge your storytelling. Create, collaborate & transform your marketing strategy effortlessly. Free for teams to try.</p>

            <div className="mt-[30px] flex justify-center items-center">
                <button className="px-[2px] bg-[#EDEDED] h-[33px] text-[12px] font-medium rounded-[50px] flex items-center justify-center">
                    <span onClick={() => changeDuration("monthly")} className={`px-[10px] h-[29px] flex justify-center items-center rounded-[50px] ${isYearlyDuration ? "text-[#404040]" : "bg-white text-black"}`}>Monthly</span>
                    <span onClick={() => changeDuration("yearly")} className={`px-[10px] h-[29px] flex justify-center items-center rounded-[50px] ${isYearlyDuration ? "bg-white text-black" : "text-[#404040]"}`}>Annually <span className="text-[#0078FF]">&nbsp;(2 months free)</span></span>
                </button>
            </div>
        </div>
    )
}

export default NewPricingSection
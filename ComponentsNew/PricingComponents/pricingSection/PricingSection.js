import React, { useState, useEffect } from 'react'
import styles from './PricingSection.module.css'
import Image from 'next/image'
import GreenCheck from '../../../public/NewAssets/Home/greenCheck.svg'
import YellowCheck from '../../../public/NewAssets/Home/yellowCheck.svg'
import Error from '../../../public/NewAssets/Home/error.svg'
import Cancel from '../../../public/NewAssets/Home/cancel.svg'
import Link from 'next/link'

const PricingSection = () => {
    const [indian, setIndian] = useState(true)

    useEffect(() => {
        if (Intl.DateTimeFormat().resolvedOptions().timeZone !== "Asia/Calcutta") {
            setIndian(false)
        }
    }, [])

    return (
        <div className={styles.pricingSectionContainer} id="price">
            {
                /**<Link href="/#price">
                <button
                    style={{
                        width: 300, margin: 'auto', height: 50, borderRadius: 5, border: 'none', cursor: 'pointer',
                        fontSize: 16, fontWeight: 500, color: "#fff", backgroundColor: '#ff3950', marginBottom: 20
                    }}>
                    Claim Cyber Monday Deal 92% off
                </button>
            </Link> */
            }
            <h2>Break Free from Content Chaos & Supercharge Your Storytelling</h2>
            <p>Create, Collaborate & Transform your Marketing Strategy Effortlessly. Select Your Exclusive Package & Simplify Your Workflow with Creatosaurus.</p>
            <div className={styles.grid}>
                <div className={styles.card}>
                    <span className={styles.title}>Agency</span>
                    <span className={styles.subTitle}>For multiple brand storytelling</span>
                    <p className={styles.price}>{indian ? "₹12999" : "$220"}<span>/month</span></p>
                    <Link href="/checkout?plan=agency">
                        <button className={styles.cartButton}>Add to Cart</button>
                    </Link>
                    <div className={styles.planDetails}>
                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>10 User</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>10 Workspace</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Unlimited AI Word Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Unlimited AI Image Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Unlimited Social Accounts</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>20 GB Storage</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Graphic Design Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Premium Stock Asset Library</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Long Form Blog Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Scheduler</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Hashtag Analytics</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Analytics</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Inbox</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Priority Support</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Done for you design</span>
                            <div className={styles.icon} style={{ marginLeft: 5 }}>
                                <Image src={Error} alt="check" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className={styles.card} style={{ border: '3px solid #0078FF' }}>
                    <button className={styles.topButton}>Most Popular</button>
                    <span className={styles.title}>Startup</span>
                    <span className={styles.subTitle}>For teams to scale storytelling</span>
                    <p className={styles.price}>{indian ? "₹4999" : "$80"}<span>/month</span></p>
                    <Link href="/checkout?plan=startup">
                        <button className={styles.cartButton} style={{ backgroundColor: '#0078FF' }}>Add to Cart</button>
                    </Link>
                    <div className={styles.planDetails}>
                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>5 User</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>5 Workspace</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>50,000 AI Word Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>25,000 AI Image Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>30 Social Media Accounts</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>6 GB Storage</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Graphic Design Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Premium Stock Asset Library</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Long Form Blog Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Scheduler</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Hashtag Analytics</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Analytics</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Inbox</span>
                            <div className={styles.icon} style={{ marginLeft: 5 }}>
                                <Image src={Error} alt="check" />
                            </div>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={Cancel} alt="check" />
                            </div>
                            <span style={{ color: '#808080' }}>Priority Support</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={Cancel} alt="check" />
                            </div>
                            <span style={{ color: '#808080' }}>Done for you design</span>
                        </div>
                    </div>
                </div>


                <div className={styles.card}>
                    <span className={styles.title}>Creator</span>
                    <span className={styles.subTitle}>For individuals who want to tell stories</span>
                    <p className={styles.price}>{indian ? "₹1999" : "$30"}<span>/month</span></p>
                    <Link href="/checkout?plan=creator">
                        <button className={styles.cartButton}>Add to Cart</button>
                    </Link>
                    <div className={styles.planDetails}>
                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>1 User</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>1 Workspace</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>10,000 AI Word Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>5,000 AI Image Credits</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>10 Social Media Accounts</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={YellowCheck} alt="check" />
                            </div>
                            <span>2 GB Storage</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Graphic Design Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Premium Stock Asset Library</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Long Form Blog Editor</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Scheduler</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Hashtag Analytics</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={GreenCheck} alt="check" />
                            </div>
                            <span>Social Media Analytics</span>
                            <div className={styles.icon} style={{ marginLeft: 5 }}>
                                <Image src={Error} alt="check" />
                            </div>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={Cancel} alt="check" />
                            </div>
                            <span style={{ color: '#808080' }}>Social Inbox</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={Cancel} alt="check" />
                            </div>
                            <span style={{ color: '#808080' }}>Priority Support</span>
                        </div>

                        <div className={styles.line}>
                            <div className={styles.icon}>
                                <Image src={Cancel} alt="check" />
                            </div>
                            <span style={{ color: '#808080' }}>Done for you design</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PricingSection


const apps = [
    /*{
        title: 'Bitmoji',
        desc: 'Your own personal emoji, cartoon avatar',
        longDesc: 'Bitmoji is your own personal emoji. Create an expressive cartoon avatar, choose from a growing library of moods and stickers - featuring YOU! Put them into any text message, chat or status update.',
        url: "https://www.bitmoji.com/",
        privacy: "https://snap.com/en-US/privacy/privacy-policy"
    },
    {
        title: 'Brand kit',
        desc: 'Coming soon...',
        longDesc: '',
        url: "https://www.brandkit.io/",
        privacy: "https://www.brandkit.io/pages/privacy"
    },*/
    {
        title: 'Cache',
        desc: 'Schedule content and share',
        longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    /*{
        title: 'Captionator',
        desc: 'AI content generator & writing assistant',
        longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
        url: "https://www.captionator.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    {
        title: 'Clearbit',
        desc: 'Find Company Logos by Clearbit',
        longDesc: 'The Clearbit Data Activation Platform helps B2B teams understand customers, identify prospects, & personalize interactions with real-time intelligence.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://dashboard.clearbit.com/privacy"
    },
    {
        title: 'Dropbox',
        desc: 'For your content and workflow',
        longDesc: 'Dropbox helps you simplify your workflow. So you can spend more time in your flow',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.dropbox.com/privacy"
    },
    {
        title: 'Flaticons',
        desc: 'Coming soon...',
        longDesc: 'Download Free Icons and Stickers for your projects. Resources made by and for designers. PNG, SVG, EPS, PSD and CSS formats',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.freepikcompany.com/privacy"
    },
    {
        title: 'Flickr',
        desc: 'Coming soon...',
        longDesc: 'The home for all your photos. Upload, access, organize, edit, and share your photos from any device, from anywhere in the world. Get 1,000GB of photo storage free.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.flickr.com/help/privacy"
    },
    {
        title: 'Google Drive',
        desc: 'Cloud Storage for Work and Home',
        longDesc: 'Learn about Google Drive’s file sharing platform that provides a personal, secure cloud storage option to share content with other users.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://policies.google.com/privacy"
    },
    {
        title: 'Google Photos',
        desc: 'Home for all your photos and videos',
        longDesc: 'Google Photos is the home for all your photos and videos, automatically organized and easy to share.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://policies.google.com/privacy"
    },
    {
        title: 'Giphy',
        desc: 'Best & newest GIFs & Animated Stickers',
        longDesc: 'GIPHY is your top source for the best & newest GIFs & Animated Stickers online. Find everything from funny GIFs, reaction GIFs, unique GIFs and more.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://support.giphy.com/hc/en-us/articles/360032872931-GIPHY-Privacy-Policy"
    },*/
    {
        title: '#Tags',
        desc: 'Organise your hashtag groups in one place',
        longDesc: '',
        url: "https://www.hashtags.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    /*{
        title: 'Iconscout',
        desc: 'Coming soon...',
        longDesc: 'Download 4.8 Million+ high-quality free vector Icons, Illustrations, 3D Assets, and Lottie Animations that are available in static & animated formats at IconScout',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://iconscout.com/legal/privacy-policy"
    },
    {
        title: 'Microsoft Teams',
        desc: 'Coming Soon...',
        longDesc: 'Working together is easier with Microsoft Teams. Tools and files are always available in one place that designed to help you connect naturally, stay organized and bring ideas to life.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://docs.microsoft.com/en-us/microsoftteams/teams-privacy"
    },*/
    {
        title: 'Muse',
        desc: 'An easy to use pro graphic design editor',
        longDesc: 'Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.',
        url: "https://www.muse.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    {
        title: 'Noice',
        desc: 'Free asset library that will make your say noice',
        longDesc: '',
        url: "https://www.noice.creatosaurus.io/home",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    /*{
        title: 'Openmoji',
        desc: 'Coming soon...',
        longDesc: 'Open source emojis for designers, developers and everyone else!',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://unsplash.com/privacy"
    },
    {
        title: 'Pexels',
        desc: 'The best free stock photos and videos',
        longDesc: 'Free stock photos & videos you can use everywhere. Browse millions of high-quality royalty free stock images & copyright free pictures. No attribution required.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.pexels.com/privacy-policy/"
    },
    {
        title: 'Pixabay',
        desc: 'Stunning free images & royalty free stock',
        longDesc: 'Find your perfect free image or video to download and use for anything. ✓ Free for commercial use ✓ No attribution required ✓ High quality images.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://pixabay.com/service/privacy/"
    },*/
    {
        title: 'Quotes',
        desc: 'Massive library of quotes & dialogue',
        longDesc: 'Search & save quotes, authors and more. Quotes by Creatosaurus.',
        url: "https://www.quotes.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    /*{
        title: 'QR Code',
        desc: 'Free customizable QR Code Generator',
        longDesc: '',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
    {
        title: 'Slack Team',
        desc: 'Communicate faster and better.',
        longDesc: 'Slack is a new way to communicate with your team. It’s faster, better organised and more secure than email.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://slack.com/intl/en-in/trust/privacy/privacy-policy"
    },
    {
        title: 'Twemoji',
        desc: 'Coming soon...',
        longDesc: 'Twitter’s open source emoji has you covered for all your projects emoji needs.With support for the latest Unicode emoji specification, featuring 2, 685 emojis, and all for free.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://twitter.com/en/privacyy"
    },
    {
        title: 'Twitter Trends',
        desc: 'Trending topics and hashtags on Twitter',
        longDesc: '',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://twitter.com/en/privacy"
    },
    {
        title: 'Tenor',
        desc: 'Find the perfect Animated GIFs',
        longDesc: 'Say more with Tenor. Find the perfect Animated GIFs and videos to convey exactly what you mean in every conversation.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://tenor.com/legal-terms"
    },
    {
        title: 'Unsplash',
        desc: 'The internet’s source of freely-usable images',
        longDesc: 'Beautiful, free images and photos that you can download and use for any project. Better than any royalty free or stock photos.',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://unsplash.com/privacy"
    },
    {
        title: 'Uploads',
        desc: 'Organise & manage your digital files',
        longDesc: '',
        url: "https://www.cache.creatosaurus.io/",
        privacy: "https://www.creatosaurus.io/privacy"
    },
*/
{
    title: 'Captionator - AI Content Writer',
    desc: 'AI content generator & writing assistant',
    longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
    url: "https://www.creatosaurus.io/apps/captionator/templates/funny-new-year-resolution-idea-generator",
    privacy: "https://www.creatosaurus.io/privacy"
}
];

export default apps;
import GhostContentAPI from '@tryghost/content-api';
import GhostAdminAPI from '@tryghost/admin-api';
import axios from 'axios';

// Create API instance with site credentials
const api = new GhostContentAPI({
  url: 'https://ahijftavqd.creatosaurus.io',
  key: '7f2eea060c7f0bd6d9d87ab0bd',
  version: 'v5.0',
});

const adminAPI = new GhostAdminAPI({
  url: 'https://ahijftavqd.creatosaurus.io',
  key: '64ffeac2df05af7156a691f1:dcdda03a4c7e577cc8947cca9fd72bd6cec8a6f7fb584b3ca689e9d4fba2bd54',
  version: 'v5.0'
});

// get all posts for blog xml
export async function getAllBlogs() {
  let posts = await api.posts
    .browse({
      include: 'title',
      include: 'slug',
      include: 'tags',
      limit: "all",
    })
    .catch((err) => {
      console.error(err);
    });

  return posts;
}


export async function getSinglePost(postSlug) {
  return await api.posts
    .read({
      slug: postSlug,
      include: 'authors,tags',
    })
    .catch((err) => {
      console.error(err);
    });
}

export async function getSinglePostById(id) {
  return await adminAPI.posts
    .read({
      id: id,
      formats: ['html']
    })
    .catch((err) => {
      console.error(err);
    })
}

export async function getPostsByTags(tags) {
  try {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/v4/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=authors,tags&filter=tags:${tags}&limit=3`)
    const data = response.data.posts.map(data => {
      return {
        slug: data.slug,
        primary_tag: data.primary_tag,
        title: data.title,
        excerpt: data.excerpt
      }
    })

    return data;
  } catch (error) {
    console.error(error);
  }
}

export async function getPostsByAuthor(tags) {
  try {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/v4/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=authors,tags&filter=author:${tags}&limit:all`)
    return response.data.posts;
  } catch (error) {
    console.error(error);
  }
}

export async function getAuthorByTag(tags) {
  try {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/v4/content/authors/slug/${tags}?key=7f2eea060c7f0bd6d9d87ab0bd`)
    return response.data.authors[0];
  } catch (error) {
    console.error(error);
  }
}
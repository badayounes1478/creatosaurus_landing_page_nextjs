const getAvailableCodes = () => {
    const dailyReduction = 7; // Number of codes reduced each day
    const initialCodes = 99; // Starting number of codes
    const endDate = new Date("2024-12-06"); // End date
    const currentDate = new Date(); // Today's date

    // Calculate the number of days since today
    const daysElapsed = Math.floor(
        (currentDate - new Date("2024-11-23")) / (1000 * 60 * 60 * 24)
    );

    // Calculate remaining codes
    const remainingCodes = Math.max(initialCodes - dailyReduction * daysElapsed, 0);

    // Check if the end date has passed or no codes are left
    if (currentDate >= endDate || remainingCodes <= 0) {
        return 0; // No codes left or past the end date
    }

    return 0
};

export { getAvailableCodes };

import React from 'react';
import Head from 'next/head';
import museStyle from '../../styles/muse.module.css';
import stylesPricing from '../../styles/PricingPage.module.css';
import { faqData } from '../../Components/faqContent';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import FrequentlyAskedQuestions from '../../Components/FaqSection';
import imgSrc from '../../public/Assets/muse_1.png';
import block_img from '../../public/Assets/image_muse.png';
import Image from 'next/image';
import { AppBlocksDataMuse } from '../../Components/AppBlocksData/AppBlocksDataMuse';
import logo from '../../public/Assets/Logo.svg';
import { useRouter } from 'next/router';
import apps from "../../appsInfo";
import imageOne from "../../public/Assets/howToImgOne.png";
import imageTwo from "../../public/Assets/howToImgTwo.png";
import imageThree from "../../public/Assets/howToImgThree.png";


const svg = {
  leftArrow: (
    <svg
      width='14'
      height='10'
      viewBox='0 0 14 10'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
        stroke='black'
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  ),
};

const openUrl = (url) => {
  window.open(url);
};

const TopBlock = () => {
  const router = useRouter();
  const { app } = router.query;
  const appInfo = apps.find((data) => data.title.toLowerCase() === app);
  return (
    <div className={museStyle.top_block}>
      <div className={museStyle.dataBlocks_dataBox_reverse}>
        <div className={museStyle.blockImage}>
          <Image src={block_img} alt='Failed to load image' />
        </div>
        <div className={museStyle.blockText}>
          <div className={museStyle.blockText_icon}>
            <Image src={logo} alt='Failed to load image' />
          </div>
          <div className={museStyle.blockText_title}>
            <h1>{appInfo?.title}</h1>
          </div>
          <div className={museStyle.blockText_description}>
            <p>
              {appInfo?.longDesc}
            </p>
          </div>
          <div className={museStyle.blockText_button}>
            <button onClick={() => {
              openUrl(appInfo.url);
            }}>Experience it</button>
          </div>
        </div>
      </div>
    </div>
  );
};

const video = () => {
  return (
    <div className={museStyle.videoContainer}>
      <iframe
        className={museStyle.videoBlock}
        src='https://www.youtube.com/embed/TF3p_jpMQ1s'
        title='YouTube video player'
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        allowFullScreen
      ></iframe>
    </div>
  );
};

const dataBlocks = () => {
  return (
    <div className={museStyle.dataBlocks}>
      {AppBlocksDataMuse.map((res, index) => {
        return (
          <div
            className={
              index % 2 == 0
                ? museStyle.dataBlocks_dataBox
                : museStyle.dataBlocks_dataBox_reverse
            }
            key={`blocks-data-muse-${index}`}
          >
            <div className={museStyle.blockImage}>
              <Image src={res.imageSrc} alt='Failed to load image' />
            </div>
            <div className={museStyle.blockText}>
              <div className={museStyle.blockText_title}>
                <h1>{res.title}</h1>
              </div>
              <div className={museStyle.blockText_description}>
                <p>{res.description}</p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

const RedPoster = () => {
  return (
    <div className={museStyle.redPoster}>
      <div className={museStyle.redPosterContainer}>
        <div className={museStyle.redPosterTop}>
          <div className={museStyle.redPosterHeading}>
            <h1>Free online quote post maker and creator</h1>
          </div>
          <div className={museStyle.redPosterPara}>
            <p>
              Generate quote post for Instagram, Facebook at scale with our
              massive 1 million library of quotes and easy to design beautiful
              quote templates.
            </p>
          </div>
          <div className={museStyle.redPosterButton}>
            <button>Get Started for Free {svg.leftArrow}</button>
          </div>
        </div>
        <div className={museStyle.redPosterBottom}>
          <Image src={imgSrc} alt='muse_img loading...' />
        </div>
      </div>
    </div>
  );
};

const FaqContainer = () => {
  return (
    <div className={museStyle.questionsContainer}>
      <div className={museStyle.heading}>
        <span>Frequently Asked Questions</span>
      </div>
      {faqData.map(({ title, content, id }) => (
        <FrequentlyAskedQuestions title={title} content={content} key={id} />
      ))}
    </div>
  );
};

const NeedMoreInformation = () => {
  return (
    <div className={museStyle.moreInfoContainer}>
      <div className={museStyle.moreInfoBox}>
        <span className={museStyle.infoHeading}>
          Need more information?
        </span>
        <span className={museStyle.infoSubHeading}>
          Saving time, making money, and changing lives.
        </span>
        <a
          href='https://calendly.com/malavwarke/creatosaurus'
          target='https://calendly.com/malavwarke/creatosaurus'
        >
          <button
            style={{
              cursor: 'pointer',
            }}
          >
            Let&apos;s Talk
          </button>
        </a>
      </div>
    </div>
  );
};


const howToSection = () => {
  return (
    <div className={museStyle.how_to_container}>
      <h1>How to Edit a Video</h1>
      <div className={museStyle.card_container}>
        <div className={museStyle.card}>
          <div className={museStyle.cardImage}>
            <Image src={imageOne} alt="" />
          </div>
          <h3>Choose a File</h3>
          <p>Select a video to upload, or try one of our sample videos</p>
        </div>
        <div className={museStyle.card}>
          <div className={museStyle.cardImage}>
            <Image src={imageTwo} alt="" />
          </div>
          <h3>Make Edits to your Video, Online</h3>
          <p>In the editor you can add text, add audio, remove audio, add subtitles
            automatically, crop, rotate, add filters & effects, and much, much more!</p>
        </div>
        <div className={museStyle.card}>
          <div className={museStyle.cardImage}>
            <Image src={imageThree} alt="" />
          </div>
          <h3>Choose a File</h3>
          <p>Select a video to upload, or try one of our sample videos</p>
        </div>
      </div>
    </div>
  );
};



const discoverMoreApps = () => {
  return (
    <div className={museStyle.discover_apps_container}>
      <div className={museStyle.more_apps_container}>
        <h1>Discover more:</h1>
        <div className={museStyle.button_container}>
          {apps.map((data, index) => {
            return <button key={index} onClick={() => {
              window.open(data.url);
            }}>{data.title}</button>;
          })}
        </div>
      </div>
    </div>
  );
};

const Apps = () => {
  const router = useRouter();
  const { app } = router.query;
  const appInfo = apps.find((data) => data.title.toLowerCase() === app);
  return (
    <div>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
        <title>{`Creatosaurus | ${appInfo?.title}`}</title>
      </Head>
      <NavigationBar />
      {TopBlock()}
      {video()}
      {dataBlocks()}
      {RedPoster()}
      {howToSection()}
      {discoverMoreApps()}
      {FaqContainer()}
      <FifthBlackSection />
      <Footer />
    </div>
  );
};

export default Apps;

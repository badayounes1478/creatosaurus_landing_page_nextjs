import React from 'react'
import dynamic from 'next/dynamic'
import BlogNavigationBar from '../../../Components/BlogNavigationBar'
import axios from 'axios'
import Link from 'next/link'
import Head from 'next/head'
import ImageShowCase from '../../../ComponentsNew/QuoteComponents/ImageShowCase';
import Share from '../../../ComponentsNew/QuoteComponents/Share';
import TopQuote from '../../../ComponentsNew/QuoteComponents/TopQuote';

const QuotesList = dynamic(() => import('../../../ComponentsNew/QuoteComponents/QuotesList'), {
  loading: () => <p>Loading...</p>,
})

const Footer = dynamic(() => import('../../../Components/LandingPageComponents/Footer'), {
  loading: () => <p>Loading...</p>,
})

export async function getServerSideProps(context) {
  let trimedAuthor = '';
  let quotes;
  let singlequote;

  const id = context.query.slug.substring(context.query.slug.lastIndexOf("-") + 1).replace(/-/g, " ").trim();

  try {
    const response = await axios.get(`https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/get/single/quote?search=${id}`);
    singlequote = response.data;

    if (singlequote.Author.includes(',')) {
      trimedAuthor = singlequote.Author.substring(0, singlequote.Author.indexOf(','));
    } else {
      trimedAuthor = singlequote.Author;
    }

    const author = await axios.get(`https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/search?q=${trimedAuthor}`)
    quotes = author.data

    if (!singlequote) {
      return { notFound: true }
    }

    const trimLengthQuote = 98; // Maximum length for trimmed quote
    const trimLengthTitle = 35; // Maximum length for trimmed title

    // Trim the quote and title to specified lengths
    const trimmedQuote = singlequote.Queote.substring(0, trimLengthQuote);
    const trimmedTitle = singlequote.Queote.substring(0, trimLengthTitle);

    // Construct the description & title
    const description = `${trimmedQuote}... ${trimedAuthor} Quotes - Quotes by Creatosaurus`;
    let title = `${trimedAuthor} - ${trimmedTitle}...`;
    title = title.replace(/\b\w/g, (char) => char.toUpperCase())
    const quoteURL = context.query.slug

    return {
      props: { quotes, singlequote, title, description, quoteURL }
    }
  } catch (error) {
    return { notFound: true }
  }
}

const slugify = (string) => {
  // Convert to string, trim whitespace, and convert to lowercase
  const trimmedString = string.toString().trim().toLowerCase();

  // Replace spaces and special characters with hyphens
  const slug = trimmedString
    .replace(/\s+/g, '-') // Replace spaces with hyphens
    .replace(/[^a-z0-9-]/g, '') // Remove non-alphanumeric characters except hyphens
    .replace(/-{2,}/g, '-') // Replace consecutive hyphens with a single hyphen
    .replace(/^-+/, '') // Remove leading hyphens
    .replace(/-+$/, ''); // Remove trailing hyphens

  return slug;
};

const beautifyAuthor = (data) => {
  let trimmedAuthor = data.Author;
  if (data.Author.includes(',')) {
    trimmedAuthor = data.Author.split(',')[0].trim();
  }

  const authorSlug = slugify(trimmedAuthor);
  const truncatedAuthor = authorSlug.substring(0, 30);
  return truncatedAuthor
}

const QuotePage = ({ quotes, singlequote, title, description, quoteURL }) => {
  return (
    <>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
        <title>{title}</title>
        <meta name='description' content={description} />

        {/* Open Graph (OG) Meta Tags */}
        <meta property='og:title' content={title} />
        <meta property='og:description' content={description} />
        <meta property='og:image' content="https://d1vxvcbndiq6tb.cloudfront.net/37a3a5de-3638-4ae0-b2d4-c7fe3ca26220" />
        <meta property='og:url' content={`https://www.creatosaurus.io/apps/quotes/${quoteURL}`} />
        <meta property='og:card' content='summary_large_image' />
        <meta property='og:type' content='website' />
        <meta property='og:site_name' content='Creatosaurus Quotes' />


        {/* twitter meta tags  */}
        <meta name='twitter:card' content='summary_large_image' />
        <meta name='twitter:site' content='@creatosaurus' />
        <meta name='twitter:title' content={title} />
        <meta name='twitter:description' content={description} />
        <meta name='twitter:image' content="https://d1vxvcbndiq6tb.cloudfront.net/37a3a5de-3638-4ae0-b2d4-c7fe3ca26220" />
      </Head >

      <BlogNavigationBar title="Quotes" slug="/apps/quotes" />
      <div className="mt-[120px] max-[960px]:px-[20px] min-[960px]:px-[80px] w-full flex flex-col">

        <h1 style={{ position: 'absolute', top: -100000 }}>“{singlequote.Queote}”</h1>
        <TopQuote quote={singlequote} hideMargin={true} />

        <div className="w-[80px] border-[0.5px] mt-[30px] self-center" />

        <Share showQuote={true} quote={singlequote.Queote} author={singlequote.Author} imageURL="https://d1vxvcbndiq6tb.cloudfront.net/37a3a5de-3638-4ae0-b2d4-c7fe3ca26220" />
        <ImageShowCase />

        <div className="mt-[50px] sm:mt-[100px] flex flex-col">
          <span className="text-[22px] sm:text-[26px] md:text-[30px] font-semibold text-center">Similar Quotes by {beautifyAuthor(singlequote)}</span>

          <QuotesList quotes={quotes} />

          <div className="mt-[30px] text-[18px] font-semibold text-[#0078FF] hover:underline">
            <Link href={'/apps/quotes/authors/[slug]'} as={`/apps/quotes/authors/${beautifyAuthor(singlequote)}-quotes`}>
              <a className='flex justify-center items-center'>
                <span className='capitalize'>Read all Quotes by {beautifyAuthor(singlequote)}</span>
                <svg className='ml-[10px]' width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.25 7.5H14.75M14.75 7.5L8.375 1.125M14.75 7.5L8.375 13.875" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
              </a>
            </Link>
          </div>
        </div>

        <div className="mt-[100px] flex flex-col">
          <span className="text-[22px] sm:text-[26px] md:text-[30px] font-semibold text-center">Related Topics to <span className='capitalize'>{beautifyAuthor(singlequote)}</span> Quotes</span>

          <div className="flex flex-wrap items-center justify-center gap-[10px] sm:gap-[25px] mt-[30px]">
            {
              singlequote?.Tags[0].split(",").map((data, index) => {
                const quoteSlug = slugify(data);

                return <Link key={index} href={'/apps/quotes/topics/[topic]'} as={`/apps/quotes/topics/${quoteSlug}-quotes`}>
                  <a className='border-[1px] border-[#dcdcdc] p-[10px] rounded-[10px] capitalize font-medium text-[14px] sm:text-[16px] md:text-[18px] hover:underline hover:text-[#0078FF] hover:border-[#0078FF]'>{data}</a>
                </Link>
              })
            }
          </div>
        </div>
      </div>

      <Footer />
    </>
  )
}

export default QuotePage
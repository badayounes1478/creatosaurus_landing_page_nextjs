import React, { useState } from 'react'
import Head from 'next/head';
import NavigationBar from '../../../../Components/LandingPageComponents/NavigationBar';
import museStyle from '../../../../styles/muse.module.css';
import Image from 'next/image';
import FrequentlyAskedQuestions from '../../../../Components/FaqSection';
//import FifthBlackSection from '../../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../../Components/LandingPageComponents/Footer';
import dynamic from 'next/dynamic';

import Image1 from '../../../../public/Assets/Muse/1threads.webp'
import Image3 from '../../../../public/Assets/Muse/2threads.webp'
import Image4 from '../../../../public/Assets/Muse/3threads.webp'
import Image5 from '../../../../public/Assets/Muse/4threads.webp'
import Image6 from '../../../../public/Assets/Muse/5threads.webp'
import Image7 from '../../../../public/Assets/Muse/6threads.webp'
import axios from 'axios';
import ThreadsPosts from '../../../../Components/ThreadsPosts/ThreadsPosts';

const FifthBlackSection = dynamic(() => import('../../../../Components/LandingPageComponents/FifthBlackSection'), {
    ssr: false,
});

const RatingComponent = dynamic(() => import('../../../../Components/RatingComponent'), {
    ssr: false,
});

export async function getServerSideProps({ query }) {
    const page = query.page === undefined ? 1 : query.page
    const res = await axios.get(`https://api.muse.creatosaurus.io/muse/admintemplates/threads/template?page=${page}`)
    const data = res.data.data.slice(0, 20)

    let count = Math.ceil(res.data.count / 20)

    let numbers = []
    for (let i = 0; i < count; i++) {
        numbers.push(i + 1)
    }

    count = numbers

    // Pass data to the page via props
    return { props: { templates: data, count, page } }
}

const ScheduleInstagramStories = ({ templates, count, page }) => {
    const openUrl = (url) => {
        window.open(url)
    }

    const [showVideo, setShowVideo] = useState(false)

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };

    const apps = [
        {
            title: 'Cache',
            desc: 'Schedule content and share',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.creatosaurus.io/apps/cache",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Muse',
            desc: 'An easy to use pro graphic design editor',
            longDesc: 'Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.',
            url: "https://www.creatosaurus.io/apps/muse",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Noice',
            desc: 'Free asset library that will make your say noice',
            longDesc: '',
            url: "https://www.creatosaurus.io/apps/noice",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Quote Post Maker',
            desc: 'Massive library of quotes & dialogue',
            longDesc: 'Search & save quotes, authors and more. Quotes by Creatosaurus.',
            url: "https://www.creatosaurus.io/tools/quote-post-maker",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Captionator - AI Content Writer',
            desc: 'AI content generator & writing assistant',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.creatosaurus.io/apps/captionator",
            privacy: "https://www.creatosaurus.io/privacy"
        }
    ];

    let faq = [
        {
            id: 1,
            title: "Can I log in to Threads with my existing Instagram account?",
            content: "Yes, you can log in to Threads with your existing Instagram account credentials.",
        }, {
            id: 2,
            title: "Is the Threads app available for iOS users?",
            content: "Yes, the Threads app is available for iOS users. You can download it from the <a href='https://apps.apple.com/us/app/threads-an-instagram-app/id6446901002' target='_blank' rel='noopener noreferrer nofollow'>Apple App Store",
        }, {
            id: 3,
            title: "Is the Threads app separate from the Instagram app?",
            content: "Yes, the <a href='https://play.google.com/store/apps/details?id=com.instagram.barcelona&hl=en&gl=US' target='_blank' rel='noopener noreferrer nofollow'>Threads app</a> is a separate application from the Instagram app, specifically designed for messaging and sharing with close friends.",
        }, {
            id: 4,
            title: "Are Threads Instagram posts design templates free to use?",
            content: "Yes, Threads templates are free to use. Get started now and build your audience on the Threads app, no credit card is required to sign up.",
        }, {
            id: 5,
            title: "Does Creatosaurus provide more templates apart from Threads post templates?",
            content: "Yes, Creatosaurus offers a wide range of templates for various platforms including Instagram, Facebook, YouTube, and more, allowing you to create engaging content across different social media channels.",
        }, {
            id: 6,
            title: "What post image aspect ratio or resolution is allowed on Threads from Instagram App?",
            content: "While there hasn't been an official resolution announcement from Threads or Instagram, users on the Threads app, a mobile-first social networking platform, generally prefer slightly vertical images in their Threads feed. Threads Instagram Posts with an aspect ratio of 4:5 or a resolution of 1080*1350 are popular on the Threads Instagram app.",
        }, {
            id: 7,
            title: "Is Threads available on the web & desktop?",
            content: "Yes, the Threads app is now available on the official <a href='https://www.threads.net/' target='_blank' rel='noopener noreferrer nofollow'>Threads website</a>. You start using it through a web browser on your desktop by using your existing Instagram account.",
        }
    ]

    let data = [
        {
            title: "Importance of template design for Threads post for your business",
            p: "Unlock the power of the Threads template for your business and elevate your online presence. With our meticulously designed thread post templates, you can create engaging and cohesive content that tells a compelling story. Maximize audience retention and boost brand awareness with our versatile Threads post templates. Thread post templates have been strategically crafted to cater to a wide range of narratives and visual aesthetics. Open up a world of creative possibilities with Threads app templates that draw readers into your stories like never before.",
            image: Image4
        }
    ]

    let data2 = [
        {
            title: "Take your Threads posts to the Next Level with the Best Threads Template for Content Creators",
            p: "Enhance your Instagram Threads posts with our curated selection of top-notch templates for Threads content creators. Elevate your visual storytelling with the best Threads post design options that cater specifically to your Threads app. Level up your Instagram Threads post game and captivate your audience with stunning and engaging designs. Threads template Instagram helps you not only pique the audience’s interest but also create an immersive and unforgettable reading experience. Level up your Instagram Threads design game with Meta Threads template that resonates, inspires, and evokes your audience.",
            image: Image5
        },
        {
            title: "Captivate your audience with the Threads template",
            p: "Elevate your Instagram Threads posts with eye-catching Instagram Threads design templates. Create captivating content that stands out with our collection of professional Threads Instagram design templates for Threads. Customize and personalize your posts to engage your audience and leave a lasting impression. Boost your visual storytelling with our threads post design templates. Unveil a world of creative possibilities with Threads app templates that draw readers into your stories like never before.",
            image: Image3
        },
        {
            title: "Hundreds of Threads post templates to choose from",
            p: "Experience boundless creativity with an extensive collection of Threads post design templates With hundreds of options to choose from, you'll never run out of inspiring thread post designs to enhance your Threads presence. Whether you're looking to promote a product, share a captivating quote, or showcase stunning visuals, we have the perfect Threads template for every occasion. Infuse your Instagram Threads design with elements like images, stickers, shapes and many more. Elevate your Threads feed with professionally crafted Threads post template designs that effortlessly grab attention and engage your audience. Ensuring that your audience remains captivated and engaged in your narrative journey.",
            image: Image7
        }
    ]

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>286+ Free Threads Post Templates with Customizable Design</title>
                <meta
                    name="title"
                    content="Free Threads Post Templates with 286+ Customizable Design"
                />
                <meta
                    name="description"
                    content="Get Threads template for creating visually appealing Threads post design. Free high quality Threads app template for sharing on Threads Instagram App by Meta."
                />
                <meta
                    property="og:title"
                    content="Free Threads Post Templates with 286+ Customizable Design"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Get Threads template for creating visually appealing Threads post design. Free high quality Threads app template for sharing on Threads Instagram App by Meta."
                    key="description"
                />

                <meta property="og:image" content="https://d1vxvcbndiq6tb.cloudfront.net/fit-in/4f221dec-26aa-4519-a445-f40e09a8667d" />
                <meta property="og:url" content="https://www.creatosaurus.io/apps/muse/templates/threads-instagram-post-design" />

                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="Free Threads Post Templates with 286+ Customizable Design" />
                <meta name="twitter:description" content="Get Threads template for creating visually appealing Threads post design. Free high quality Threads app template for sharing on Threads Instagram App by Meta." />
                <meta name="twitter:image" content="https://d1vxvcbndiq6tb.cloudfront.net/fit-in/4f221dec-26aa-4519-a445-f40e09a8667d" />
            </Head>
            <NavigationBar />


            <div className={museStyle.top_block} style={{ marginTop: 80 }}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{ boxShadow: 'none' }}>
                        <Image priority={true} src={Image1} alt='Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates' />
                    </div>
                    <div className={museStyle.blockText}>
                        <div className={museStyle.blockText_title}>
                            <h1>Free Threads Post Templates with 286+ Customizable Design</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>Enhance your storytelling prowess with our handpicked selection of the best Threads templates. Craft captivating and visually appealing Threads post templates and transport your readers into the heart of each Threads Instagram post.</p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button
                                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                onClick={() => {
                                    openUrl("https://www.muse.creatosaurus.io/editor?app=muse&width=1080&height=1350&type=threads%20from%20instagram");
                                }}>
                                Get Started For Free
                            </button>
                        </div>
                        <span style={{ color: '#828282', fontSize: 10 }}>No credit card required</span>
                    </div>
                </div>
            </div>

            <ThreadsPosts templates={templates} count={count} page={page} />

            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.muse.creatosaurus.io/editor?app=muse&width=1080&height=1350&type=threads%20from%20instagram");
                                        }}>Get Started For Free
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.how_to_container}>
                <h2>How to design a Threads post in 3 easy steps.</h2>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M7.5 12l-2.004 2.672a2 2 0 00.126 2.552l3.784 4.128c.378.413.912.648 1.473.648H15.5c2.4 0 4-2 4-4 0 0 0 0 0 0V9.429M16.5 10v-.571c0-2.286 3-2.286 3 0" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M13.5 10V8.286c0-2.286 3-2.286 3 0V10M10.5 10V7.5c0-2.286 3-2.286 3 0 0 0 0 0 0 0V10M10.5 10V7.5 3.499A1.5 1.5 0 009 2v0a1.5 1.5 0 00-1.5 1.5V15" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <span style={{ fontSize: 18, fontWeight: 600, marginBottom: 10 }}>1. Select a Threads template</span>
                        <p>Choose from a wide selection of Threads templates to create captivating and visually appealing Threads Instagram posts.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M14.363 5.652l1.48-1.48a2 2 0 012.829 0l1.414 1.414a2 2 0 010 2.828l-1.48 1.48m-4.243-4.242l-9.616 9.615a2 2 0 00-.578 1.238l-.242 2.74a1 1 0 001.084 1.085l2.74-.242a2 2 0 001.24-.578l9.615-9.616m-4.243-4.242l4.243 4.242" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <span style={{ fontSize: 18, fontWeight: 600, marginBottom: 10 }}>2. Customise your Threads post template</span>
                        <p>Personalize and customize your to make your Threads Instagram posts unique and tailored to your brand or content.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M12 13v9m0 0l3.5-3.5M12 22l-3.5-3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <span style={{ fontSize: 18, fontWeight: 600, marginBottom: 10 }}>3. Download & share your design</span>
                        <p>Download and share your beautifully designed posts on your Threads app, effortlessly sharing your creations with your audience.</p>
                    </div>
                </div>
            </div>

            <RatingComponent />

            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data2.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 10 }}>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.muse.creatosaurus.io/editor?app=muse&width=1080&height=1350&type=threads%20from%20instagram");
                                        }}>Get Started For Free
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>

            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/IsTSaSCS5eE?origin=https://creatosaurus.io/'
                    title='Threads Post Templates for Free | Best Design Templates for Threads Instagram App'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                    loading="lazy"
                ></iframe>
            </div>


            <div className={museStyle.tipsBlock}>
                <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 30 }}>5 Tips to Design an engaging Threads Post using Threads App Template</h2>
                <p style={{ fontSize: 20, marginBottom: 15 }}>1. Choose visually appealing Threads post templates that align with your brand&apos;s aesthetic and message.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}>2. Use eye-catching Threads Instagram Template graphics and fonts to grab attention and encourage scrolling.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}>3. Incorporate Threads Instagram design elements like vectors or illustrations to increase engagement.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}>4. Create cohesive storylines by using consistent carousel Threads Instagram templates for multiple posts.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}>5. Experiment with different Thread Post Templates to keep your audience engaged and interested.</p>
            </div>


            <div className={museStyle.redPoster} style={{ backgroundColor: '#FF8A25' }}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h2 style={{ fontSize: 34, fontWeight: 700, color: "#FFFFFF" }}>Muse - Online Photo Editing Tool & Free Graphic Design Editor</h2>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Our pro design tool offers a wide range of not only threads app templates but also templates for all your social media designs. Create stunning visuals, customize templates, and make your brand stand out. Elevate your Threads post design game with our user-friendly online graphic design tool today!
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.muse.creatosaurus.io/editor?app=muse&width=1080&height=1350&type=threads%20from%20instagram")}>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <svg onClick={() => setShowVideo(true)} style={showVideo ? { display: 'none' } : null} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg" color="#FFF">
                            <path d="M6.906 4.537A.6.6 0 006 5.053v13.894a.6.6 0 00.906.516l11.723-6.947a.6.6 0 000-1.032L6.906 4.537z" stroke="#FFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        {
                            showVideo ? <iframe
                                className={museStyle.videoBlock}
                                src='https://www.youtube.com/embed/eS5tpAUEuzA?start=492&autoplay=1'
                                title='YouTube video player'
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            >
                            </iframe> : <Image src={Image6} alt='muse_img loading...' />
                        }
                    </div>
                </div>
            </div>

            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <p style={{ fontSize: 28, fontWeight: 600 }}>Discover more:</p>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <a href={data.url} target='_blank' rel="noreferrer" key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</a>;
                        })}
                    </div>
                </div>
            </div>

            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default ScheduleInstagramStories
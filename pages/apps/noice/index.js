import React, { useState } from 'react'
import styles from '../../../styles/Muse1.module.css'
import Image from 'next/image';
import Head from 'next/head';
import RatingComponent from '../../../Components/RatingComponent';
import SupportedBy from '../../../Components/LandingPageComponents/SupportedBy';
import MuseBanner from '../../../public/Assets/Muse/youtube.jpg'
import Play from '../../../public/Assets/Play2.svg';
import museStyle from '../../../styles/muse.module.css';
import YT from '../../../public/Assets/Noice/thumbnail-yt.webp'
import Image1 from '../../../public/Assets/Noice/image1.png'
import Image2 from '../../../public/Assets/Noice/illustration-3d.png'
import Image3 from '../../../public/Assets/Noice/retro.png'
import Image4 from '../../../public/Assets/Noice/Illustration.png'
import Image5 from '../../../public/Assets/Noice/free-300.png'
import FifthBlackSection from '../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../Components/LandingPageComponents/Footer';
import NavigationBar from '../../../Components/LandingPageComponents/NavigationBar';
import FrequentlyAskedQuestions from '../../../Components/FaqSection';

const Noice = () => {
    const [showVideo, setShowVideo] = useState(false)
    const [showVideo2, setShowVideo2] = useState(false)

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };

    let data = [
        {
            title: "Open-source Free Icons",
            p: "With our open-source, always-free icons, you can unlock the world of design possibilities. Designed for creators and innovators, our collection empowers you to craft without limits. Our icons are curated by pro designs to serve as the foundation for your creativity. Our icons are available for free.",
            image: Image1
        }, {
            title: "3D Illustrations",
            p: "3D illustrations are a vital part of today's design and communication, helping to engage people in our increasingly visual digital world. In this growing form of art, designers and artists can express complex ideas, create captivating stories, and share detailed rich tales using 3D illustrations where technology meets art.",
            image: Image2
        }, {
            title: "Retro Icons",
            p: "With our curated retro icons you can navigate to the portals of the past, invoking a sense of nostalgia and a yearning for simpler times. Retro icons serve as a reminder that, as technology advances, the trend of retro symbols continues, making them a treasured and enduring component of our visual culture.",
            image: Image3
        }, {
            title: "Illustrations",
            p: "In today's world of design where visuals convey the story, Illustrations help designers, marketers, or anyone to convey emotions without the language barrier. Our designers have made these curated illustrations to empower you to convey emotions, convey messages, and spark the imagination. Use our illustrations today to add the human touch to your design.",
            image: Image4
        }, {
            title: "300+ Free Icons Categories",
            p: "Apart from Retro, 3D illustrations, and illustrations, Creatosaurus’s designers bring you more than 100+ categories to find the same look and feel icons for your graphic designs, UI, Mobile, and social media. You can request our team to create an icon set for you. So designers and creators why wait anymore start using Creatosaurus’s Amazing Free Icons library available to download in SVG.",
            image: Image5
        },
    ]


    const apps = [
        {
            title: 'Cache',
            desc: 'Schedule content and share',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.creatosaurus.io/apps/cache",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Muse',
            desc: 'An easy to use pro graphic design editor',
            longDesc: 'Instantly create free graphics online using 1000s of free templates. Edit your design & download your graphics with our free graphic design tool.',
            url: "https://www.creatosaurus.io/apps/muse",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: '#Tags',
            desc: 'Organise your hashtag groups in one place',
            longDesc: '',
            url: "https://www.hashtags.creatosaurus.io/",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Quote Post Maker',
            desc: 'Massive library of quotes & dialogue',
            longDesc: 'Search & save quotes, authors and more. Quotes by Creatosaurus.',
            url: "https://www.creatosaurus.io/tools/quote-post-maker",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Captionator - AI Content Writer',
            desc: 'AI content generator & writing assistant',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.creatosaurus.io/apps/captionator/templates/funny-new-year-resolution-idea-generator",
            privacy: "https://www.creatosaurus.io/privacy"
        }
    ];

    let faq = [
        {
            id: 1,
            title: "How do I get free icons?",
            content: "Just create your free account on Noice by Creatosaurus and enjoy the open-source free icons library.",
        }, {
            id: 2,
            title: "Are Creatosaurus icons free?",
            content: "Yes, Creatosaurus offers free icons, illustrations with full access without any copyright issues.",
        }, {
            id: 3,
            title: "How to contribute and submit your work on Noice?",
            content: "You can reach us out through contact or contact@creatosaurus.io to share and showcase your work for the community.",
        }
    ]

    return (
        <div className={styles.museNew}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>11,876+ Free Icons, Illustrations, Clipart, Vector SVG, 3D Assets | Noice</title>
                <meta
                    name="title"
                    content="11,876+ Free Icons, Illustrations, Clipart, Vector SVG, 3D Assets | Noice"
                />
                <meta
                    name="description"
                    content="Free icons, illustrations, Clipart, Vector SVG, 3D assets for your designs, UI, & Social Media are available to download in PNG, SVG for free. With Noice - Where art inspires brilliance!"
                />
                <meta
                    property="og:title"
                    content="11,876+ Free Icons, Illustrations, Clipart, Vector SVG, 3D Assets | Noice"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Free icons, illustrations, Clipart, Vector SVG, 3D assets for your designs, UI, & Social Media are available to download in PNG, SVG for free. With Noice - Where art inspires brilliance!"
                    key="description"
                />

                <meta property="og:image" content="https://d1vxvcbndiq6tb.cloudfront.net/b71ff98a-f530-47e2-a6cd-bd1c92ce179f" />

                <meta property="og:url" content="https://www.creatosaurus.io/apps/noice" />

                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="11,876+ Free Icons, Illustrations, Clipart, Vector SVG, 3D Assets | Noice" />
                <meta name="twitter:description" content="Free icons, illustrations, Clipart, Vector SVG, 3D assets for your designs, UI, & Social Media are available to download in PNG, SVG for free. With Noice - Where art inspires brilliance!" />
                <meta name="twitter:image" content="https://d1vxvcbndiq6tb.cloudfront.net/b71ff98a-f530-47e2-a6cd-bd1c92ce179f" />
            </Head>

            <NavigationBar />

            <div className={styles.heroSection}>
                <h1>Design with 11,876+ Free Icons, Illustrations, and 3D Assets</h1>
                <p>Free SVG icons for your graphic design, UI, social media, and mobile specially curated by pro designers at Noice by Creatosaurus.</p>
                <button onClick={()=> window.open("https://www.noice.creatosaurus.io/")}>
                    Explore Icons
                    <svg style={{ width: 16, height: 16, marginLeft: 5 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                        <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#FFFFFF" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </button>
                <div className={styles.imageWrapper}>
                    {
                        showVideo ? <div className={styles.wrapper}>
                            <iframe src="https://www.youtube.com/embed/GIQRsqXIOmc?si=D63WOmt1IH1E-rNk&autoplay=1" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div> : <div className={styles.img} >
                            <Image priority={true} src={YT} alt="" objectFit='contain' />
                            <div className={styles.youtube} onClick={() => setShowVideo(true)} >
                                <Image src={Play} alt="" objectFit='contain' />
                            </div>
                        </div>
                    }
                </div>
                <SupportedBy />
                <div style={{ marginTop: -50 }}>
                    <RatingComponent />
                </div>
            </div>

            <div className={museStyle.dataBlocks} style={{ marginTop: 80 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage} style={{ width: "40%" }}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => window.open("https://www.noice.creatosaurus.io/")}
                                    >Explore Icons
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>

            <div className={museStyle.redPoster} style={{ backgroundColor: '#FF8A25' }}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h2 style={{ fontSize: 34, fontWeight: 700, color: "#FFFFFF" }}>Muse - Online Photo Editing Tool & Free Graphic Design Editor</h2>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Our pro design tool offers a wide range of templates for your social media designs. Create stunning visuals, customize templates, and make your brand stand out. Elevate your design game with our user-friendly online graphic design tool today!
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram")}>Edit Icons with Muse {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <svg onClick={() => setShowVideo2(true)} style={showVideo2 ? { display: 'none' } : null} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg" color="#FFF">
                            <path d="M6.906 4.537A.6.6 0 006 5.053v13.894a.6.6 0 00.906.516l11.723-6.947a.6.6 0 000-1.032L6.906 4.537z" stroke="#FFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        {
                            showVideo2 ? <iframe
                                className={museStyle.videoBlock}
                                src='https://www.youtube.com/embed/eS5tpAUEuzA?start=492&autoplay=1'
                                title='YouTube video player'
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            >
                            </iframe> :
                                <div style={{ width: '70%', display: 'flex' }}>
                                    <Image src={MuseBanner} alt='muse_img loading...' />
                                </div>
                        }
                    </div>
                </div>
            </div>

            <div className={museStyle.how_to_container}>
                <h2>Noice API for Developers</h2>
                <p style={{ width: '60%', textAlign: 'center', fontSize: 18 }}>Easily add resources to your apps. Get up and running quickly with our diverse assets libraries like icons, vectors, illustrations, photos and more. Looking to integrate? Connect with us.</p>
                <button
                    style={{ marginTop: 30, width: 140, height: 40, borderRadius: 5, border: 'none', backgroundColor: '#000', color: '#fff', cursor: 'pointer', fontWeight: 500 }}
                    onClick={() => window.open("https://www.creatosaurus.io/contact")}>Contact Us</button>
            </div>

            <div className={museStyle.discover_apps_container} style={{ marginTop: 100 }}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>
                        })}
                        <button onClick={() => window.open("https://www.creatosaurus.io/tools/schedule-instagram-stories")}>Instagram Story Scheduler Tool</button>
                    </div>
                </div>
            </div>

            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <div style={{ marginTop: 100 }}>
                <FifthBlackSection text="Chat, generate & design without limit with Muse AI Photo editor" />
            </div>
            <Footer />
        </div>
    )
}

export default Noice
import React, { useState } from 'react'
import Navigation from '../Components/LandingPageComponents/NavigationBar';
import Footer from '../Components/LandingPageComponents/Footer';
import styles from '../styles/Contactus.module.css';
import Head from 'next/head';
import axios from 'axios';
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo';

const Contact = () => {

  const [email, setemail] = useState("")
  const [description, setdescription] = useState("")
  const [loading, setloading] = useState(false)
  const [messageSent, setmessageSent] = useState(false)

  const sendMessage = async () => {
    try {
      if (messageSent) return alert("Message has been sent already, Thanks for connecting.")
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) === false) return alert("Please add valid email address")
      if (email.trim() === "") return alert("Please add your email")
      if (description.trim() === "") return alert("Please add your message")

      setloading(true)
      await axios.post("https://api.app.creatosaurus.io/creatosaurus/contact/info", {
        email: email,
        message: description
      })
      setloading(false)
      setmessageSent(true)
      setemail("")
      setdescription("")
    } catch (error) {
      setloading(false)
    }
  }

  return (
    <div>
      <HeadInfo />
      <Navigation />
      <div className={styles.contactContainer}>
        <div className={styles.head}>
          <h4>Contact us 🙌🏻</h4>
          <h6>Getting product or account support is easy. We are here to help.</h6>

          <input maxLength={50} value={email} type="text" placeholder="Your email" onChange={(e) => setemail(e.target.value)} />
          <textarea className='border-[1px] border-[#808080]' maxLength={1500} value={description} placeholder="Your message" onChange={(e) => setdescription(e.target.value)}></textarea>
          <button onClick={sendMessage}>{messageSent ? "Message Sent Successfully 😊" : loading ? "Sending . . ." : "Send message"}</button>

          <div className={styles.help}>Need some help? <span style={{ cursor: 'pointer' }} onClick={() => window.open("https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht")}>Let&apos;s chat :)</span></div>
        </div>

        <div className={styles.info}>
          <p>We at Creatosaurus are committed to delivering Innovation and Convenience to its users. We build cool products for creators like you to make life better.</p>
          <p>Creatosaurus simplifies the workflow of creators & teams with an all-in-one creator stack to tell stories at scale. Curate ideas, design graphics, edit videos, schedule posts, manage your social media accounts, generate copy, and more to streamline & optimize your workflow across teams in real-time with 15+ integrations. Creatosaurus is one true home for creators.</p>
          <p>Driven by our passion for user happiness, we have a Happiness Equation that&apos;s drilled into our very core. What does this mean for you? Well, we&apos;ll look after you and go to extreme lengths to make sure you&apos;re satisfied. We started in 2018 and it is our mission to only give you the best product.</p>
          <h4>WHERE ARE WE LOCATED?</h4>
          <p>Creatosaurus, 6th Floor, Ideas to Impacts Hub, Sr. N. 284/2d, Behind Vijay Sales, Baner, Pune, Maharashtra, 411045.</p>
          <h4>A GUARANTEE</h4>
          <p>We&apos;re so confident you&apos;ll love our products - we offer a 100% satisfaction guarantee. Don&apos;t like something you bought? Tell us, and we&apos;ll be happy to solve your issue.</p>
          <h4>HAVE ANY QUESTIONS?</h4>
          <p>Contact us now using our contact page or email us at contact@creatosaurus.io or directly call us at +91-9665047289. Happy creating!</p>
        </div>

        <div className={styles.blackBox}>
          <h4>You focus on telling stories, we do everything else.</h4>
          <a href="https://www.creatosaurus.io/signup">
            <button>Signup for Free</button>
          </a>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default Contact
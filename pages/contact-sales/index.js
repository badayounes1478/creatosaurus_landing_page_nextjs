import React, { useState } from 'react'
import Trusted from './assets/trusted.webp'
import Image from 'next/image'
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth';
import RatingComponent from '../../Components/RatingComponent';
import PriceCutSection from '../../ComponentsNew/BlackFridayComponents/priceCutSection/PriceCutSection';
import CompareWithOtherAiTools from '../../ComponentsNew/PricingComponents/newPricingSection/CompareWithOtherAiTools';
import StoryTelling from '../../ComponentsNew/BlackFridayComponents/storyTelling/StoryTelling';
import FAQSection from '../../ComponentsNew/PricingComponents/newPricingSection/FAQSection';
import BlackSeactionBottom from '../../ComponentsNew/PricingComponents/newPricingSection/BlackSeactionBottom';
import Footer from '../../Components/LandingPageComponents/Footer';
import HeadInfo from '../../ComponentsNew/HeadTag/HeadInfo';

const ContactSales = () => {
    const [selectedCountryCode, setSelectedCountryCode] = useState('+1');
    const [showLogin, setshowLogin] = useState(false)

    const countryCodes = [
        { code: '+91', flag: '🇮🇳' },   // India
        { code: '+1', flag: '🇺🇸' },    // United States
        { code: '+44', flag: '🇬🇧' },   // United Kingdom
        { code: '+61', flag: '🇦🇺' },   // Australia
        { code: '+81', flag: '🇯🇵' },   // Japan
        { code: '+7', flag: '🇷🇺' },    // Russia
        { code: '+86', flag: '🇨🇳' },   // China
        { code: '+49', flag: '🇩🇪' },   // Germany
        { code: '+33', flag: '🇫🇷' },   // France
        { code: '+39', flag: '🇮🇹' },   // Italy
        { code: '+34', flag: '🇪🇸' },   // Spain
        { code: '+55', flag: '🇧🇷' },   // Brazil
        { code: '+52', flag: '🇲🇽' },   // Mexico
        { code: '+27', flag: '🇿🇦' },   // South Africa
        { code: '+82', flag: '🇰🇷' },   // South Korea
        { code: '+62', flag: '🇮🇩' },   // Indonesia
        { code: '+60', flag: '🇲🇾' },   // Malaysia
        { code: '+63', flag: '🇵🇭' },   // Philippines
        { code: '+66', flag: '🇹🇭' },   // Thailand
        { code: '+65', flag: '🇸🇬' },   // Singapore
        { code: '+98', flag: '🇮🇷' },   // Iran
        { code: '+92', flag: '🇵🇰' },   // Pakistan
        { code: '+20', flag: '🇪🇬' },   // Egypt
        { code: '+234', flag: '🇳🇬' },  // Nigeria
        { code: '+213', flag: '🇩🇿' },  // Algeria
        { code: '+971', flag: '🇦🇪' },  // United Arab Emirates
        { code: '+353', flag: '🇮🇪' },  // Ireland
        { code: '+47', flag: '🇳🇴' },   // Norway
        { code: '+46', flag: '🇸🇪' },   // Sweden
        { code: '+45', flag: '🇩🇰' },   // Denmark
        { code: '+31', flag: '🇳🇱' },   // Netherlands
        { code: '+41', flag: '🇨🇭' },   // Switzerland
        { code: '+30', flag: '🇬🇷' },   // Greece
        { code: '+48', flag: '🇵🇱' },   // Poland
        { code: '+36', flag: '🇭🇺' },   // Hungary
        { code: '+43', flag: '🇦🇹' },   // Austria
        { code: '+32', flag: '🇧🇪' },   // Belgium
        { code: '+351', flag: '🇵🇹' },  // Portugal
        { code: '+380', flag: '🇺🇦' },  // Ukraine
        { code: '+90', flag: '🇹🇷' },   // Turkey
        { code: '+972', flag: '🇮🇱' },  // Israel
        { code: '+56', flag: '🇨🇱' },   // Chile
        { code: '+54', flag: '🇦🇷' },   // Argentina
        { code: '+58', flag: '🇻🇪' },   // Venezuela
        { code: '+20', flag: '🇪🇬' },   // Egypt
        { code: '+212', flag: '🇲🇦' },  // Morocco
        { code: '+216', flag: '🇹🇳' },  // Tunisia
        { code: '+90', flag: '🇹🇷' },   // Turkey
        { code: '+94', flag: '🇱🇰' },   // Sri Lanka
        { code: '+880', flag: '🇧🇩' },  // Bangladesh
        { code: '+64', flag: '🇳🇿' },   // New Zealand
        { code: '+505', flag: '🇳🇮' },  // Nicaragua
        { code: '+57', flag: '🇨🇴' },   // Colombia
        { code: '+593', flag: '🇪🇨' },  // Ecuador
        { code: '+595', flag: '🇵🇾' },  // Paraguay
        { code: '+51', flag: '🇵🇪' },   // Peru
        { code: '+250', flag: '🇷🇼' },  // Rwanda
        { code: '+263', flag: '🇿🇼' },  // Zimbabwe
        { code: '+254', flag: '🇰🇪' },  // Kenya
        { code: '+256', flag: '🇺🇬' },  // Uganda
        { code: '+255', flag: '🇹🇿' },  // Tanzania
        { code: '+228', flag: '🇹🇬' },  // Togo
        { code: '+221', flag: '🇸🇳' },  // Senegal
        { code: '+241', flag: '🇬🇦' },  // Gabon
        { code: '+267', flag: '🇧🇼' },  // Botswana
        { code: '+27', flag: '🇿🇦' },   // South Africa
        { code: '+213', flag: '🇩🇿' },  // Algeria
        { code: '+216', flag: '🇹🇳' },  // Tunisia
        { code: '+225', flag: '🇨🇮' },  // Ivory Coast (Côte d'Ivoire)
        { code: '+248', flag: '🇸🇨' },  // Seychelles
    ];
    return (
        <div>
            <HeadInfo />
            <NavigationBar />
            {showLogin ? <NavigationAuth close={() => setshowLogin(false)} planName={loading} /> : null}

            <div className="flex flex-col lg:flex-row gap-[40px] lg:gap-[105px] w-full max-[960px]:px-[20px] min-[960px]:px-[80px] mt-[130px]">
                {/* Left Section */}
                <div className="flex flex-col gap-[30px] lg:gap-[40px] w-full lg:w-[50%]">
                    {/* Header */}
                    <div className="flex flex-col gap-[15px] lg:gap-[20px]">
                        <div className="flex flex-col gap-[10px]">
                            <h1 className="text-[24px] md:text-[36px] font-semibold">Contact our sales team</h1>
                            <p className="text-[12px] md:text-[14px] text-[#333] font-normal">
                                We can provide a demo, assist in selecting the best plan, or create a customized package to help your organization maximize the potential of Creatosaurus. For product support, please visit our{" "}
                                <a href="#" className="text-[#0078FF] underline">
                                    Help Center
                                </a>
                            </p>
                        </div>

                        {/* List */}
                        <div className="flex flex-col gap-[8px] lg:gap-[10px]">
                            {[
                                "Quickly create compelling content",
                                "Easily build, manage, and scale your brand",
                                "Simplify your workflows and collaborate effortlessly throughout the content creation process",
                                "Ensure secure access for your expanding global teams with SSO integration",
                            ].map((text, index) => (
                                <div key={index} className="flex gap-[5px] items-center">
                                    <div className='w-[21px] h-[21px]'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21" fill="none">
                                            <path d="M4.375 11.375L7.875 14.875L16.625 6.125" stroke="#333333" strokeWidth="1.1" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>
                                    <span className="text-[12px] md:text-[14px] text-[#333] font-normal">{text}</span>
                                </div>
                            ))}
                        </div>
                    </div>

                    {/* Investment Section */}
                    <div className="flex flex-col gap-[15px] lg:gap-[20px]">
                        <h1 className="text-[16px] md:text-[18px]">Get a clear return on investment with Creatosaurus</h1>

                        {/* Responsive layout: grid on smaller screens, flex on larger screens */}
                        <div className="grid grid-cols-2 gap-[10px] lg:flex lg:justify-between lg:gap-[15px]">
                            {[
                                { stat: "50", description: "hours of design saved every month" },
                                { stat: "3x", description: "content production every month" },
                                { stat: "60%", description: "reduction in marketing tool costs" },
                                { stat: "55%", description: "Faster collaboration, communication" },
                            ].map((item, index) => (
                                <div key={index} className="flex flex-col justify-center items-center w-full">
                                    <h1 className="text-[24px] md:text-[36px] font-semibold">{item.stat}</h1>
                                    <p className="text-[12px] md:text-[12px] text-[#333] font-normal text-center">{item.description}</p>
                                </div>
                            ))}
                        </div>
                    </div>

                    {/* Trusted Companies */}
                    <div className="flex flex-col gap-[15px] lg:gap-[20px] w-full">
                        <h1 className="text-[16px] md:text-[18px] font-medium">Trusted by +30K users from 70+ countries</h1>
                        <div className="flex w-full">
                            <Image src={Trusted} alt="Trusted Companies" />
                        </div>
                    </div>
                </div>

                {/* Right Section */}
                <div className="flex flex-col w-full lg:w-[50%] gap-[10px] lg:gap-[15px] mt-[20px] lg:mt-0">
                    <div className="flex flex-col md:flex-row gap-[10px] lg:gap-[20px]">
                        {/* First Name */}
                        <div className="flex flex-col gap-[5px] w-full">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">First name</span>
                            <input
                                type="text"
                                placeholder='Sam'
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] placeholder:text-[#C4C4C4] text-[14px] md:text-[16px] font-normal"
                            />
                        </div>

                        {/* Last Name */}
                        <div className="flex flex-col gap-[5px] w-full">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Last name</span>
                            <input
                                type="text"
                                placeholder='Parker'
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] placeholder:text-[#C4C4C4] text-[14px] md:text-[16px] font-normal"
                            />
                        </div>
                    </div>

                    {/* Company Email */}
                    <div className="flex flex-col gap-[5px]">
                        <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Company email</span>
                        <input
                            type="email"
                            placeholder='sam.parker@company.com'
                            className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] w-full placeholder:text-[#C4C4C4] text-[14px] md:text-[16px] font-normal"
                        />
                    </div>

                    {/* Company name and size */}
                    <div className="flex flex-col md:flex-row gap-[10px] lg:gap-[20px]">
                        {/* Company Name */}
                        <div className="flex flex-col gap-[5px] w-full">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Company name</span>
                            <input
                                type="text"
                                placeholder="Company name"
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] h-[40px] md:h-[48px] text-[14px] md:text-[16px] font-normal"
                            />
                        </div>

                        {/* Company Size Dropdown */}
                        <div className="flex flex-col gap-[5px] w-full relative">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Company size</span>
                            <select
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none w-full"
                                defaultValue=""
                            >
                                <option value="" disabled hidden className="text-[14px] font-normal">
                                    Please select an option
                                </option>
                                <option value="1-10">1-10 employees</option>
                                <option value="11-50">11-50 employees</option>
                                <option value="51-200">51-200 employees</option>
                                <option value="201-500">201-500 employees</option>
                                <option value="500+">500+ employees</option>
                            </select>

                            {/* Custom dropdown arrow */}
                            <div className="absolute inset-y-0 right-3 flex top-7 items-center pointer-events-none">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                    height="16"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth="2"
                                    className="text-[#333]"
                                >
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                                </svg>
                            </div>
                        </div>
                    </div>

                    {/* Country Dropdown */}
                    <div className="flex flex-col gap-[5px] w-full relative">
                        <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Country or region</span>
                        <select
                            className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none w-full"
                            defaultValue=""
                        >
                            <option value="" disabled hidden className="text-[14px] font-normal">
                                Please select an option
                            </option>
                            <option value="India">India</option>
                        </select>

                        {/* Custom dropdown arrow */}
                        <div className="absolute inset-y-0 right-3 flex top-7 items-center pointer-events-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                strokeWidth="2"
                                className="text-[#333]"
                            >
                                <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                            </svg>
                        </div>
                    </div>

                    {/* Role and Department */}
                    <div className="flex flex-col md:flex-row gap-[10px] lg:gap-[20px]">
                        {/* Role Level */}
                        <div className="flex flex-col gap-[5px] w-full relative">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Role level</span>
                            <select
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none w-full"
                                defaultValue=""
                            >
                                <option value="" disabled hidden className="text-[14px] font-normal">
                                    Please select an option
                                </option>
                            </select>

                            {/* Custom dropdown arrow */}
                            <div className="absolute inset-y-0 right-3 flex top-7 items-center pointer-events-none">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                    height="16"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth="2"
                                    className="text-[#333]"
                                >
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                                </svg>
                            </div>
                        </div>

                        {/* Department */}
                        <div className="flex flex-col gap-[5px] w-full relative">
                            <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Department</span>
                            <select
                                className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none w-full"
                                defaultValue=""
                            >
                                <option value="" disabled hidden className="text-[14px] font-normal">
                                    Please select an option
                                </option>
                            </select>

                            {/* Custom dropdown arrow */}
                            <div className="absolute inset-y-0 right-3 flex top-7 items-center pointer-events-none">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                    height="16"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth="2"
                                    className="text-[#333]"
                                >
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                                </svg>
                            </div>
                        </div>
                    </div>

                    {/* Phone Number */}
                    <div className="flex flex-col gap-[5px] w-full">
                        <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Phone number</span>

                        <div className="flex items-center">
                            {/* Country Code Dropdown */}
                            <div className="relative">
                                <select
                                    value={selectedCountryCode}
                                    onChange={(e) => setSelectedCountryCode(e.target.value)}
                                    className="px-[10px] py-[10px] md:py-[13px] rounded-l-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none pr-[25px]"
                                >
                                    {countryCodes.map((country, index) => (
                                        <option key={index} value={country.code}>
                                            {country.code} {country.flag}
                                        </option>
                                    ))}
                                </select>

                                {/* Custom dropdown arrow */}
                                <div className="absolute inset-y-0 right-3 flex items-center pointer-events-none">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        strokeWidth="2"
                                        className="text-[#333]"
                                    >
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                                    </svg>
                                </div>
                            </div>

                            {/* Phone Number Input */}
                            <input
                                type="tel"
                                placeholder="Enter phone number"
                                className="px-[10px] py-[10px] md:py-[13px] rounded-r-[10px] border-[1px] border-l-0 border-[#DCDCDC] bg-[#FAFAFA] text-[#333] w-full h-[40px] md:h-[48px] text-[14px]"
                            />
                        </div>
                    </div>

                    {/* What’s your enquiry about? */}
                    <div className="flex flex-col gap-[5px] w-full relative">
                        <span className="text-[12px] md:text-[14px] text-[#333] font-medium">What’s your enquiry about?</span>
                        <select
                            className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] text-[#333] placeholder:text-[#C4C4C4] cursor-pointer h-[40px] md:h-[48px] text-[14px] appearance-none w-full"
                            defaultValue=""
                        >
                            <option value="" disabled hidden className="text-[14px] font-normal">
                                Please select an option
                            </option>
                        </select>

                        {/* Custom dropdown arrow */}
                        <div className="absolute inset-y-0 right-3 flex top-7 items-center pointer-events-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                strokeWidth="2"
                                className="text-[#333]"
                            >
                                <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
                            </svg>
                        </div>
                    </div>

                    {/* Additional Information */}
                    <div className="flex flex-col gap-[5px] w-full">
                        <span className="text-[12px] md:text-[14px] text-[#333] font-medium">Is there any other information you would like to share with us?</span>
                        <textarea
                            type="text"
                            placeholder="Please enter details about your enquiry"
                            className="px-[10px] py-[10px] md:py-[13px] rounded-[10px] border-[1px] border-[#DCDCDC] bg-[#FAFAFA] placeholder:text-[#C4C4C4] text-[14px] md:text-[16px] font-normal resize-none"
                        />
                    </div>

                    {/* Agreement */}
                    <div className="flex w-full">
                        <p className="text-[10px] md:text-[11px] text-[#808080] font-normal w-full tracking-[0.20px]">
                            By submitting this form, you agree to receive marketing and updates from Creatosaurus.
                            You can unsubscribe at any time. Creatosaurus handles your information as described in our {" "}
                            <a href="" className="underline">Privacy Policy</a>.
                        </p>
                    </div>

                    {/* Submit Button */}
                    <button className='bg-[#000000] py-[10px] md:py-[13px] text-[14px] md:text-[16px] text-white font-semibold rounded-[10px]'>
                        Submit
                    </button>
                </div>
            </div>

            <RatingComponent />
            <PriceCutSection pricing={true} />
            <CompareWithOtherAiTools />
            <StoryTelling hideTitle={true} />
            <FAQSection />
            <BlackSeactionBottom />
            <Footer />
        </div>
    )
}

export default ContactSales
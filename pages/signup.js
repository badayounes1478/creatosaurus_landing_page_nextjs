import React from 'react'
import Head from 'next/head';
import Auth from '../Components/Auth/Auth'
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo';

const signup = () => {
  return (
    <div className='bg-white h-[100vh] overflow-hidden'>
      <HeadInfo />
      <Auth title="Signup" />
    </div>
  )
}

export default signup
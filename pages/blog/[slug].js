import React, { useState, useEffect, useRef } from 'react'
import dynamic from 'next/dynamic'
import Head from 'next/head';
import BlogNavigationBar from '../../Components/BlogNavigationBar';
import { getSinglePost } from '../../lib/posts';
import { toast } from 'react-toastify'
import axios from 'axios';
import Link from 'next/link';
import Image from 'next/image';
import NoImage from '../../public/Assets/NoImage.jpeg'
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth';
import Ad from '../../ComponentsNew/AdComponents/Ad';

const BlogContent = dynamic(() => import('../../ComponentsNew/BlogComponents/blogContent/BlogContent'), {
  loading: () => <p>Loading...</p>,
})

const NewsLetter = dynamic(() => import('../../ComponentsNew/BlogComponents/newsLetter/NewsLetter'), {
  loading: () => <p>Loading...</p>,
})

const ArticleByCard = dynamic(() => import('../../Components/ArticleByCard'), {
  loading: () => <p>Loading...</p>,
})

const FifthBlackSection = dynamic(() => import('../../Components/LandingPageComponents/FifthBlackSection'), {
  loading: () => <p>Loading...</p>,
})

const Footer = dynamic(() => import('../../Components/LandingPageComponents/Footer'), {
  loading: () => <p>Loading...</p>,
})


export async function getServerSideProps(context) {
  const post = await getSinglePost(context.params.slug);

  if (!post) {
    return {
      notFound: true
    }
  }

  return {
    props: { post }
  }
}


const PostPage = ({ post }) => {
  const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

  const [content, setContent] = useState([]);
  const [loading, setLoading] = useState(false);
  const [activeSection, setActiveSection] = useState('');
  const [progress, setProgress] = useState(0)
  const [email, setEmail] = useState("")
  const [isVisible, setIsVisible] = useState(false);
  const postContainerRef = useRef(null);
  const blogContainerRef = useRef(null)
  const [showLogin, setShowLogin] = useState(false)

  useEffect(() => {
    extractContent(post.html)
  }, [post]);

  useEffect(() => {
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        const id = entry.target.getAttribute('id');
        const element1 = document.getElementById('blogInfoContainer')
        const element2 = document.getElementById('content')
        if (entry.isIntersecting) {
          if (id === "articleBy") {
            if (element1 !== null && element2 !== null) {
              element1.style.opacity = 0
              element2.style.opacity = 0

              element1.style.display = "none"
              element2.style.display = "none"

              element1.style.transition = 'opacity 0.5s ease';
              element2.style.transition = 'opacity 0.5s ease';
            }
          } else {
            if (element1 !== null && element2 !== null) {
              element1.style.opacity = 1
              element2.style.opacity = 1

              element1.style.display = "flex"
              element2.style.display = "flex"

              element1.style.transition = 'opacity 0.5s ease';
              element2.style.transition = 'opacity 0.5s ease';
              setActiveSection(id);
            }
          }
        }
      });
    }, { threshold: 0.5 }); // Adjust threshold as needed

    const idArray = [...content, { title: "Article By", id: "articleBy" }]
    // Observe each section
    idArray.forEach(item => {
      const section = document.getElementById(item.id);
      if (section) {
        observer.observe(section);
      }
    });

    return () => {
      // Disconnect the observer on cleanup
      observer.disconnect();
    };
  }, [content]);

  const onScroll = () => {
    if (blogContainerRef.current) {
      const element = blogContainerRef.current;
      const winScroll = element.scrollTop;
      const height = element.scrollHeight - element.clientHeight;
      const scrolled = (winScroll / height) * 100;
      setProgress(scrolled);
    }
  };

  useEffect(() => {
    const element = blogContainerRef.current;

    if (element) {
      element.addEventListener('scroll', onScroll);
    }

    return () => {
      if (element) {
        element.removeEventListener('scroll', onScroll);
      }
    };
  }, []);


  useEffect(() => {
    if (progress > 6.5) {
      setIsVisible(true)
    } else {
      setIsVisible(false)
    }
  }, [progress])


  // NOTE: Get blog content
  const extractContent = (htmlString) => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(htmlString, 'text/html');
    const h2Elements = doc.querySelectorAll('h2');

    const h2Details = [];
    h2Elements.forEach(element => {
      h2Details.push({
        title: element.textContent,
        id: element.id
      });
    });

    setContent(h2Details)
  }

  // NOTE: Subscribe to newsletter
  const addUserToNewsLetter = async () => {
    try {
      const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (regex.test(email) === false) return toast.error("The email you've provided is invalid. Please enter a valid email address.")

      setLoading(true)
      await axios.post("https://api.app.creatosaurus.io/creatosaurus/newsletter/subscribe", {
        email,
        isSubscribed: true
      })

      setEmail("")
      setLoading(false)
      toast.success("Congratulations! You have successfully subscribed to our newsletter.")
    } catch (error) {
      console.log(error)
      toast.error("Newsletter subscription failed. Please retry or check internet and form accuracy.")
      setLoading(false)
    }
  }


  const shareOnTwitter = () => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const tweetUrl = `https://twitter.com/intent/tweet?url=https://www.creatosaurus.io/blog/${post.slug}&text=${encodeURIComponent(post.meta_title)}&via=creatosaurushq`;
    window.open(tweetUrl, 'test', params);
  }

  const shareOnLinkedin = () => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const postUrl = `https://www.linkedin.com/shareArticle?mini=true&url=https://www.creatosaurus.io/blog/${post.slug}`
    window.open(postUrl, 'test', params)
  }

  const shareOnFacebook = () => {
    const params = 'width=500,height=500,top=200,left=700,_self';
    const postUrl = `https://www.facebook.com/sharer/sharer.php?u=https://www.creatosaurus.io/blog/${post.slug}&text=${encodeURIComponent(post.meta_title)}`
    window.open(postUrl, 'test', params)
  }

  return (
    <>
      <BlogNavigationBar inSideBlog={true} progress={progress} />
      {showLogin && <NavigationAuth close={() => setShowLogin(false)} />}

      <main className="w-[100vw] h-[100vh] overflow-x-hidden overflow-y-scroll scroll-smooth customScrollBar relative" ref={blogContainerRef}>
        <Head>
          <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
          <title>{post.meta_title}</title>
          <meta name='description' content={post.meta_description} />

          {/* Open Graph (OG) Meta Tags */}
          <meta property='og:title' content={post.ogTitle} />
          <meta property='og:image' content={post.ogImg} />
          <meta property='og:description' content={post.ogDis} />
          <meta property='og:url' content={`https://www.creatosaurus.io/blog/${post.slug}`} />
          <meta property='og:card' content='summary_large_image' />
          <meta property='og:type' content='website' />
          <meta property='og:site_name' content='Creatosaurus Blog' />


          {/* twitter meta tags  */}
          <meta name='twitter:card' content='summary_large_image' />
          <meta name='twitter:site' content='@creatosaurus' />
          <meta name='twitter:title' content={post.twitter_title} />
          <meta name='twitter:description' content={post.twitter_description} />
          <meta name='twitter:image' content={post.twitter_image} />
        </Head >

        <div className='flex justify-center px-[12px] sm:px-[50px]'>
          <div className='max-[1000px]:hidden'>
            <BlogContent isVisible={isVisible} content={content} activeSection={activeSection} />

            <NewsLetter
              setShowLogin={setShowLogin}
              isVisible={isVisible}
              setEmail={setEmail}
              email={email}
              loading={loading}
              addUserToNewsLetter={addUserToNewsLetter}
              shareOnLinkedin={shareOnLinkedin}
              shareOnTwitter={shareOnTwitter}
              shareOnFacebook={shareOnFacebook} />
          </div>


          {/* Post Container */}
          <div className="w-[54%] max-[1000px]:w-[100%] pt-[100px] flex flex-col gap-[30px]" ref={postContainerRef}>
            <header className='mt-[20px] flex flex-col justify-center items-center gap-[20px]'>
              <svg className='mb-[-5px]' width="24px" height="24px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#404040">
                <path d="M8 15C12.8747 15 15 12.949 15 8C15 12.949 17.1104 15 22 15C17.1104 15 15 17.1104 15 22C15 17.1104 12.8747 15 8 15Z" stroke="#404040" strokeWidth="1.5" strokeLinejoin="round" />
                <path d="M2 6.5C5.13376 6.5 6.5 5.18153 6.5 2C6.5 5.18153 7.85669 6.5 11 6.5C7.85669 6.5 6.5 7.85669 6.5 11C6.5 7.85669 5.13376 6.5 2 6.5Z" stroke="#404040" strokeWidth="1.5" strokeLinejoin="round" />
              </svg>

              <Link prefetch={false} href='/blog/tag/[tag]' as={`/blog/tag/${post.primary_tag?.slug}`} passHref>
                <a className='text-[14px] sm:text-[16px] uppercase text-[#404040] font-semibold'>{post.primary_tag?.name}</a>
              </Link>

              <h1 className='text-[28px] tracking-normal leading-normal sm:text-[48px] font-semibold text-center sm:leading-[55px] sm:tracking-[1.2px]'>{post.title}</h1>
              <p className='text-[14px] sm:text-[18px] text-[#404040] text-center tracking-[0.200px]'>{post.meta_description}</p>

              <div className="flex flex-col m-auto">
                <div className='flex items-center justify-center text-[12px] sm:text-[14px] text-[#404040] font-light tracking-[0.35px]'>
                  <div>
                    <span>Article By&nbsp;</span>
                    <Link prefetch={false} href='/blog/author/[author]' as={`/blog/author/${post.primary_author.slug}`}>
                      <a className='hover:text-[#0000FF] hover:underline'>{post.authors[0]?.name}</a>
                    </Link>
                  </div>

                  <svg className='size-[16px] sm:size-[18px] ml-[5px] sm:ml-[10px] mr-[5px]' width="24px" height="24px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                    <path d="M1.5 12.5L5.57574 16.5757C5.81005 16.8101 6.18995 16.8101 6.42426 16.5757L9 14" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" />
                    <path d="M16 7L12 11" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" />
                    <path d="M7 12L11.5757 16.5757C11.8101 16.8101 12.1899 16.8101 12.4243 16.5757L22 7" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" />
                  </svg>

                  <div>
                    <span>Reviewed by&nbsp;</span>
                    <Link prefetch={false} href='/blog/author/[author]' as={`/blog/author/${post.authors[1]?.slug}`}>
                      <a className='hover:text-[#0000FF] hover:underline'>{post.authors[1]?.name}</a>
                    </Link>
                  </div>
                </div>


                <div className='flex items-center justify-center mt-[10px] text-center text-[12px] sm:text-[14px] text-[#404040] font-light tracking-[0.35px]'>
                  <span>
                    Last Updated On - {" "}
                    {month[new Date(post.updated_at).getMonth()]}{' '}
                    {new Date(post.updated_at).getDate()},{' '}
                    {new Date(post.updated_at).getFullYear()}
                  </span>

                  <span className='block size-[6px] bg-black mx-[10px]' />

                  <span>{post.reading_time} min read</span>
                </div>
              </div>

              <div className='absolute top-[-1000000px]'>
                <Image
                  width={100}
                  height={55}
                  layout='responsive'
                  src={post.feature_image || NoImage}
                  alt='blogpic'
                />
              </div>
            </header>

            <div className="prose prose-sm sm:prose-base m-auto text-black xl:prose-lg">
              <div dangerouslySetInnerHTML={{ __html: post.html }} id='blog' />
            </div>
          </div>
        </div>

        <div className='hidden max-[1000px]:flex justify-center items-center pl-[12px] pr-[8px] sm:pl-[50px] sm:pr-[46px]'>
          <Ad setShowLogin={setShowLogin} refresh={true} />
        </div>


        <div className='px-[30px] w-full flex justify-center' id='articleBy'>
          <ArticleByCard post={post} />
        </div>

        <div className='mt-[100px] mb-[0px]'>
          <FifthBlackSection />
        </div>

        <Footer />
      </main>
    </>
  );
};

export default PostPage;


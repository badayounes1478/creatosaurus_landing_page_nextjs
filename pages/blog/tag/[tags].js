import React from 'react'
import BlogCard2 from '../../../Components/BlogCard2';
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import { getPostsByTags } from '../../../lib/posts';
import Head from 'next/head';
import BlogStyle from '../../../styles/Blog.module.css';
import FifthBlackSection from '../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../Components/LandingPageComponents/Footer';

export async function getServerSideProps(context) {
    const blogs = await getPostsByTags(context.params.tags);
    if (!blogs) {
        return {
            notFound: true,
        };
    }
    return {
        props: {
            blogs,
        },
    };
}
const BlogsByTags = ({ blogs }) => {
    const month = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sept',
        'Oct',
        'Nov',
        'Dec',
    ];


    return <div className={BlogStyle.BlogSection}>
        <Head>
            <title>
                Creatosaurus - All in One Creator Stack | Storytelling at Scale
            </title>
            <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
        </Head>
        <BlogNavigationBar />
        <div className={BlogStyle.tagContainer}>
            <div className={BlogStyle.tagHeading}>
                <h1>{blogs[0].primary_tag.name}</h1>
                <p>{blogs[0].primary_tag?.description}</p>
            </div>
            <div className={BlogStyle.Grid}>
                {blogs.map((post) => {
                    return <BlogCard2 key={post.slug} post={post} month={month} />;
                })}
            </div>
        </div>
        <FifthBlackSection />
        <Footer />
    </div>
}

export default BlogsByTags;
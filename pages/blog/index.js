import React, { useState } from 'react'
import blogStyle from '../../styles/NewBlogHome.module.css';
import Head from 'next/head';
import axios from 'axios';
import BlogNavigationBar from '../../Components/BlogNavigationBar';
import Link from 'next/link';
import Image from 'next/image';
import SubscribeNewsLetter from '../../Components/SubscribeNewsLetter';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../Components/LandingPageComponents/Footer';
import NoImage from '../../public/Assets/NoImage.jpeg'
import NewsLetterCard from '../../Components/NewsLetterCard';

export const getServerSideProps = async () => {
  const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=tags,title,slug,authors&limit=15&page=1`);
  const blogs = response.data.posts.map(data => {
    delete data.html
    return data
  })

  if (!blogs) return { notFound: true }

  return {
    props: { blogs }
  }
}

const Blog = ({ blogs }) => {
  const [posts, setPosts] = useState(blogs)
  const [pageNum, setPageNum] = useState(2);
  const [hasMore, setHasMore] = useState(true);
  const [blogPage, setBlogPage] = useState(["page"]);
  const [loading, setLoading] = useState(false)

  const dateFormater = (value) => {
    const date = new Date(value)
    const formattedDate = date.toLocaleDateString("en-US", { month: "short", day: "numeric", year: "numeric", })
    return formattedDate
  }

  const getMoreBlogs = async () => {
    try {
      setLoading(true)
      const res = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=tags,title,slug,authors&page=${pageNum}`);

      if (res.data.meta.pagination.pages === pageNum) {
        setHasMore(false);
      }

      setPosts([...posts, ...res.data.posts]);
      setLoading(false)
    } catch (error) {
      setLoading(false)
    }
  };

  const handleClick = () => {
    if (!hasMore) return

    setBlogPage((prev) => [...prev, "page"])
    setPageNum((no) => no + 1)
    getMoreBlogs()
  }

  return (
    <div className='mt-[120px]'>
      <Head>
        <title>
          Creatosaurus - All in One Creator Stack | Storytelling at Scale
        </title>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
      </Head>

      <BlogNavigationBar />
      <main className='max-[960px]:px-[20px] min-[960px]:px-[50px]'>
        <div>
          {/* First Post */}
          <Link href='/blog/[slug]' as={`/blog/${posts[0].slug}`} passHref>
            <div className='grid grid-cols-1 lg:grid-cols-2 items-center gap-[20px]'>
              <div className={blogStyle.imageWrapper}>
                <Image src={posts[0].feature_image || NoImage} width={100} height={70} layout='responsive' alt='' loading='lazy' />
              </div>

              <div className='cursor-pointer mt-[-20px] lg:mt-0'>
                <Link href='/blog/tag/[tags]' as={`/blog/tag/${posts[0].primary_tag?.slug}`}>
                  <div className='px-[16px] py-[6px] lg:py-[8px] text-[10px]  lg:text-[12px] text-[#ff43ca] tracking-[0.025em] font-medium bg-[rgba(255,67,202,0.1)] inline rounded-[15px]'>
                    {posts[0].primary_tag && posts[0].primary_tag.name}
                  </div>
                </Link>

                <h2 className='mt-[10px] md:mt-[15px] text-[20px] sm:text-[24px] md:text-[28px] lg:text-[32px] font-semibold hover:underline tracking-[0.025em]'>{posts[0].title}</h2>
                <p className='mt-[10px] md:mt-[15px] text-[12px] sm:text-[14px] mg:text-[16px] tracking-[0.025em]'>{posts[0].excerpt}</p>

                <div className='mt-[15px] md:mt-[25px] text-[10px] sm:text-[12px] md:text-[14px] text-[#404040] tracking-[0.025em]'>
                  <span>{dateFormater(posts[0].updated_at)}</span>
                  <span className='mx-[15px] sm:mx-[30px]'>{posts[0].reading_time} min read</span>
                  <Link href='/blog/author/[author]' as={`/blog/author/${posts[0].primary_author.slug}`}>
                    <span className='hover:underline hover:text-black'>{posts[0].primary_author.name}</span>
                  </Link>
                </div>
              </div>
            </div>
          </Link>
        </div>

        {/* Category */}
        <div className='hidden sm:flex text-[12px] text-[#333333] font-medium gap-x-[15px] mt-[15px] mb-[30px]'>
          <Link href='/blog' as='/blog'>
            <button className='border-b-[1px] pb-[5px] border-[#ff4359] text-[#ff4359]'>All Categories</button>
          </Link>

          <Link href='/blog/tag/[tags]' as='/blog/tag/creator-digest' passHref>
            <button className='border-b-[1px] pb-[5px] border-black hover:text-[#ff4359] hover:border-[#ff4359]'>Creator Digest</button>
          </Link>

          <Link href='/blog/tag/[tags]' as='/blog/tag/getting-started' passHref>
            <button className='border-b-[1px] pb-[5px] border-black hover:text-[#ff4359] hover:border-[#ff4359]'>Getting Started</button>
          </Link>

          <Link href='/blog/tag/[tags]' as='/blog/tag/social-media-marketing' passHref>
            <button className='border-b-[1px] pb-[5px] border-black hover:text-[#ff4359] hover:border-[#ff4359]'>Social Media Marketing</button>
          </Link>
        </div>

        {/* List Of Blogs */}
        <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-[20px] sm:gap-[40px] mt-[40px] sm:mt-[0px]'>
          {
            posts.map((data, index) => {
              if (index === 0) return null
              if (index === 3) return <>
                <NewsLetterCard />

                <Link key={index} href='/blog/[slug]' as={`/blog/${data.slug}`} passHref >
                  <div className='flex flex-col'>
                    <div className={blogStyle.imageWrapper}>
                      <Image src={data.feature_image || NoImage} width={100} height={70} layout='responsive' alt='' loading='lazy' />
                    </div>

                    <div>
                      <Link href='/blog/tag/[tags]' as={`/blog/tag/${data.primary_tag?.slug}`}>
                        <div className='px-[16px] py-[6px] lg:py-[8px] text-[10px] text-[#ff43ca] tracking-[0.025em] font-medium bg-[rgba(255,67,202,0.1)] inline rounded-[15px]'>
                          {data.primary_tag && data.primary_tag.name}
                        </div>
                      </Link>

                      <h2 className='mt-[10px] md:mt-[15px] text-[20px] md:text-[24px] font-semibold hover:underline tracking-[0.025em] cursor-pointer'>{data.title}</h2>
                      <p className='mt-[10px]  text-[12px] tracking-[0.025em] line-clamp-3'>{data.excerpt}</p>

                      <div className='mt-[15px] text-[10px] md:text-[12px] text-[#404040] tracking-[0.025em]'>
                        <span>{dateFormater(data.updated_at)}</span>
                        <span className='mx-[15px] sm:mx-[30px]'>{data.reading_time} min read</span>
                        <Link href='/blog/author/[author]' as={`/blog/author/${data.primary_author.slug}`}>
                          <span className='hover:underline hover:text-black cursor-pointer'>{data.primary_author.name}</span>
                        </Link>
                      </div>
                    </div>
                  </div>
                </Link>
              </>

              return <Link key={index} href='/blog/[slug]' as={`/blog/${data.slug}`} passHref >
                <div className='flex flex-col'>
                  <div className={blogStyle.imageWrapper}>
                    <Image src={data.feature_image || NoImage} width={100} height={70} layout='responsive' alt='' loading='lazy' />
                  </div>

                  <div>
                    <Link href='/blog/tag/[tags]' as={`/blog/tag/${data.primary_tag?.slug}`}>
                      <div className='px-[16px] py-[6px] lg:py-[8px] text-[10px] text-[#ff43ca] tracking-[0.025em] font-medium bg-[rgba(255,67,202,0.1)] inline rounded-[15px]'>
                        {data.primary_tag && data.primary_tag.name}
                      </div>
                    </Link>

                    <h2 className='mt-[10px] md:mt-[15px] text-[20px] md:text-[24px] font-semibold hover:underline tracking-[0.025em] cursor-pointer'>{data.title}</h2>
                    <p className='mt-[10px]  text-[12px] tracking-[0.025em] line-clamp-3'>{data.excerpt}</p>

                    <div className='mt-[15px] text-[10px] md:text-[12px] text-[#404040] tracking-[0.025em]'>
                      <span>{dateFormater(data.updated_at)}</span>
                      <span className='mx-[15px] sm:mx-[30px]'>{data.reading_time} min read</span>
                      <Link href='/blog/author/[author]' as={`/blog/author/${data.primary_author.slug}`}>
                        <span className='hover:underline hover:text-black cursor-pointer'>{data.primary_author.name}</span>
                      </Link>
                    </div>
                  </div>
                </div>
              </Link>
            })
          }
        </div>

        <div className='mt-[50px] flex justify-center items-center'>
          {
            loading ?
              <div role="status">
                <svg aria-hidden="true" className="w-8 h-8 text-gray-200 animate-spin fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                  <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                </svg>
                <span className="sr-only">Loading...</span>
              </div>
              :
              <>
                {
                  blogPage.map((page, index) => {
                    return <button className='bg-[#ff4359] text-white px-[14px] py-[6px] rounded-[5px] text-[14px] mr-[10px]' key={page + index}>{index + 1}</button>
                  })
                }

                {hasMore && <button onClick={handleClick} className='px-[14px] py-[6px]'>&raquo;</button>}
              </>
          }
        </div>
      </main>

      <SubscribeNewsLetter />
      <FifthBlackSection />
      <Footer />
    </div>
  )
}

export default Blog
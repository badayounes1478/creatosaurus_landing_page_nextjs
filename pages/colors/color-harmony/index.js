import React, { useRef, useEffect, useState } from 'react'
import { colord, extend, random } from "colord";
import { useRouter } from 'next/router'
import harmonies from "colord/plugins/harmonies";
import iro from '@jaames/iro';
import tinycolor from "tinycolor2"
import style from './colorHarmony.module.css'
import Link from 'next/link';
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import Head from 'next/head';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import { getSinglePostById } from '../../../lib/posts';
let colorPicker

export async function getServerSideProps() {
    const id = "66805889da8d0c062a1db039"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ColorHarmony = ({ post }) => {
    const router = useRouter()
    const harmoniesType = [{
        title: 'Complementary Harmony',
        type: "complementary"
    }, {
        title: 'Analogous Harmony',
        type: "analogous"
    }, {
        title: 'Triadic Harmony',
        type: "triadic"
    }, {
        title: 'Tetradic Harmony',
        type: "tetradic"
    }, {
        title: 'Rectangle Harmony',
        type: "rectangle"
    }, {
        title: 'Split Complementary Harmony',
        type: "split-complementary"
    }, {
        title: 'Double Complementary Harmony',
        type: "double-split-complementary"
    }]

    extend([harmonies]);
    const colorPickerRef = useRef(null);
    const [activeColor, setActiveColor] = useState('#ff0000')
    const [colorToChange, setColorToChange] = useState('#ff0000')
    const [showMessage, setShowMessage] = useState(null)
    const [copyColorName, setCopyColorName] = useState(null)

    useEffect(() => {
        colorPicker = new iro.ColorPicker(colorPickerRef.current, {
            color: activeColor,
            width: 260,
            sliderSize: 10,
            layout: [
                {
                    component: iro.ui.Box,
                    options: {
                        boxHeight: 140,
                    }
                },
                {
                    component: iro.ui.Slider,
                    options: {
                        sliderType: 'hue'
                    }
                }
            ]
        });

        colorPicker.on('color:change', (color) => {
            setActiveColor(color.hexString)
            setColorToChange(color.hexString)
        });
    }, []);

    function getContrastingColor(color) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return luminance > 0.5 ? '#000' : '#FFF';
    }

    const getAnalogousColors = (type, colorData) => {
        let colorCode = colorData ? colorData : activeColor
        const color = colord(colorCode);
        return color.harmonies(type).map((c) => c.toHex());
    }

    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return activeColor
        }
    }

    const checkValidColor = () => {
        let data = getColorOrDefault(colorToChange)
        setActiveColor(data)
        setColorToChange(data)
        colorPicker.setColors([data])
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            checkValidColor();
        }
    }

    const copy = (color, type) => {
        setCopyColorName(color + type)
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`${color} copied to the clipboard!`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1200);
    }

    const generateRandomColor = () => {
        const data = random().toHex();
        setActiveColor(data)
        setColorToChange(data)
        colorPicker.setColors([data])
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    return (
        <div className={style.harmonyContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head >

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.harmonyContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.harmonyGenerator}>
                    <div className={style.harmonyWrapper}>
                        <div className={style.leftSide}>
                            <h2>Color Harmony
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="color-harmony" disabled selected>Color Harmony</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div className={style.row}>
                                <label>Pick a Color</label>
                                <div className={style.wrapper}>
                                    <input type='text'
                                        placeholder='#000000'
                                        value={colorToChange}
                                        onFocus={(e) => e.target.select()}
                                        onChange={(e) => setColorToChange(e.target.value)}
                                        onBlur={checkValidColor}
                                        onKeyDown={handleKeyPress} />
                                    <span style={{ backgroundColor: activeColor }} />
                                </div>

                                <div ref={colorPickerRef} style={{ marginTop: 30, width: '260px', height: '162px', alignSelf: 'center' }}></div>
                                <div className={style.buttons}>
                                    <button onClick={generateRandomColor}>
                                        Random
                                        <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M18.3327 6.33398C15.8327 6.33398 11.2493 6.33398 9.58268 10.9173C7.91602 15.5007 4.16602 15.5007 1.66602 15.5007" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M16.666 4.66602C16.666 4.66602 17.6818 5.68181 18.3327 6.33268C17.6818 6.98356 16.666 7.99935 16.666 7.99935" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M18.3327 15.5007C15.8327 15.5007 11.2493 15.5007 9.58268 10.9173C7.91602 6.33398 4.16602 6.33399 1.66602 6.33398" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M16.666 17.1673C16.666 17.1673 17.6818 16.1515 18.3327 15.5007C17.6818 14.8498 16.666 13.834 16.666 13.834" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </button>
                                    <button style={{ display: 'none' }}>Export All Harmony</button>
                                    <button>Powered by {" "}
                                        <Link href="https://www.creatosaurus.io/">
                                            <a target="_blank">Creatosaurus.io</a>
                                        </Link>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className={style.rightSide}>
                            {
                                harmoniesType.map((category, index) => {
                                    return <div key={category.type} className={style.row} style={{ marginTop: index === 0 ? 0 : 30 }}>
                                        <label>{category.title}</label>
                                        <div className={style.wrapper}>
                                            {
                                                getAnalogousColors(category.type).map((data, index) => {
                                                    return <div
                                                        onClick={() => copy(data, category.type)}
                                                        key={data + index}
                                                        className={style.card}
                                                        onMouseLeave={() => setCopyColorName(null)}
                                                        style={{ backgroundColor: data }}>
                                                        {
                                                            data + category.type === copyColorName ?
                                                                <svg width="24px" height="24px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color={getContrastingColor(data)}>
                                                                    <path d="M5 13L9 17L19 7" stroke={getContrastingColor(data)} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                                                </svg> : <>
                                                                    <span style={{ color: getContrastingColor(data) }}>{data}</span>
                                                                    <div style={data === activeColor ? { backgroundColor: getContrastingColor(data) } : { display: 'none' }} className={style.dot} />
                                                                </>
                                                        }
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </div>
                                })
                            }

                            <Link href="https://www.creatosaurus.io/apps/muse">
                                <a target="_blank">
                                    Design using this color harmony on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
            {showMessage ? <Toast message={showMessage} /> : null}
        </div>
    )
}

export default ColorHarmony
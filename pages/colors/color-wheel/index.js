import React, { useRef, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import iro from '@jaames/iro';
import style from './colorWheel.module.css'
import colornamer from 'color-namer';
import Link from 'next/link';
import Head from 'next/head';
import tinycolor from "tinycolor2"
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import { colord, extend } from "colord";
import harmoniesPlugin from "colord/plugins/harmonies";
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import { getSinglePostById } from '../../../lib/posts';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';

let colorPickerData = null
let colorType = "Complementary"

export async function getServerSideProps() {
    const id = "667ff948da8d0c062a1daf65"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ColorWheel = ({ post }) => {
    extend([harmoniesPlugin]);
    const router = useRouter()
    const colorPickerRef = useRef(null);
    const [color, setColor] = useState(["#ff0000"]);
    const [activeColor, setActiveColor] = useState("#ff0000")
    const [colorToChange, setColorToChange] = useState('#ff0000')
    const [colorName, setColorName] = useState([])
    const [type, setType] = useState("Complementary")
    const [showMessage, setShowMessage] = useState(null)
    const [activeHoverIndex, setActiveHoverIndex] = useState(null)

    useEffect(() => {
        const colorPicker = new iro.ColorPicker(colorPickerRef.current, {
            width: 260,
            colors: color,
            wheelAngle: 270,
            wheelDirection: 'clockwise'
        });

        colorPickerData = colorPicker
        colorPicker.on('color:change', (color) => {
            const hex = color.hexString;

            const value = colorType
            if (value === "Complementary") {
                getComplementaryColors(hex)
            } else if (value === "Analogous") {
                getAnalogousColors(hex)
            } else if (value === "Triadic") {
                getTriadicColors(hex)
            } else if (value === "Tetradic") {
                getTetradColors(hex)
            } else if (value === "Double Split Complementary") {
                getDoubleSplitComplementary(hex)
            } else if (value === "Rectangle") {
                getRectangleColors(hex)
            } else if (value === "Split Complementary") {
                getSplitComplementaryColors(hex)
            } else if (value === "Monochromatic") {
                getMonochromaticColors(hex)
            }
        });

        getComplementaryColors(color[0])

        // Cleanup function to remove event listener
        return () => {
            colorPicker.off('color:change');
        };
    }, []); // Include color as a dependency


    function getComplementaryColors(baseColor) {
        const color = colord(baseColor);
        let colorsData = color.harmonies("complementary").map((c) => c.toHex())

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    function getAnalogousColors(baseColor) {
        const color = colord(baseColor);
        let colorsData = color.harmonies("analogous").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    function getTriadicColors(baseColor) {
        const color = colord(baseColor);
        let colorsData = color.harmonies("triadic").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    function getTetradColors(baseColor) {
        const color = colord(baseColor);
        let colorsData = color.harmonies("tetradic").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    const getDoubleSplitComplementary = (baseColor) => {
        const color = colord(baseColor);
        let colorsData = color.harmonies("double-split-complementary").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    const getRectangleColors = (baseColor) => {
        const color = colord(baseColor);
        let colorsData = color.harmonies("rectangle").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    const getSplitComplementaryColors = (baseColor) => {
        const color = colord(baseColor);
        let colorsData = color.harmonies("split-complementary").map((c) => c.toHex());

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }

    const getMonochromaticColors = (baseColor) => {
        const colors = tinycolor(baseColor).monochromatic();
        let colorsData = colors.map(function (t) { return t.toHexString(); })
        colorsData = colorsData.slice(1)
        colorsData = [...colorsData, baseColor].reverse()

        setColor(colorsData);
        setActiveColor(baseColor)
        setColorToChange(baseColor)
        colorPickerData.setColors(colorsData)

        const colorNames = colorsData.map(data => {
            return colornamer(data, { pick: ['ntc'] }).ntc[0].name
        })
        setColorName(colorNames)
    }


    const changeType = (e) => {
        const value = e.target.value

        if (value === "Complementary") {
            getComplementaryColors(activeColor)
        } else if (value === "Analogous") {
            getAnalogousColors(activeColor)
        } else if (value === "Triadic") {
            getTriadicColors(activeColor)
        } else if (value === "Tetradic") {
            getTetradColors(activeColor)
        } else if (value === "Double Split Complementary") {
            getDoubleSplitComplementary(activeColor)
        } else if (value === "Rectangle") {
            getRectangleColors(activeColor)
        } else if (value === "Split Complementary") {
            getSplitComplementaryColors(activeColor)
        } else if (value === "Monochromatic") {
            getMonochromaticColors(activeColor)
        }

        setType(value)
        colorType = value
    }

    function getContrastingColor(color) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return luminance > 0.5 ? '#000' : '#FFF';
    }

    const exportColorPalate = () => {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext('2d');

        // Increase the canvas resolution for higher quality
        const scale = 2; // Adjust the scale factor as needed
        canvas.width = 1600 * scale;
        canvas.height = 1200 * scale;
        const blockWidth = canvas.width / color.length;

        // Set text properties with scaling
        ctx.font = `600 48px Arial`;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.letterSpacing = `${1.3 * scale}px`;

        // Disable image smoothing for crisper output
        ctx.imageSmoothingEnabled = false;

        color.forEach((color, index) => {
            ctx.fillStyle = color;
            ctx.fillRect(index * blockWidth, 0, blockWidth, canvas.height);

            // Draw color code
            ctx.fillStyle = getContrastingColor(color);

            const textX = index * blockWidth + blockWidth / 2;
            const textY = canvas.height / 1.5;

            // Save the current context
            ctx.save();
            ctx.translate(textX, textY);
            ctx.fillText(color.toUpperCase(), 0, 0);
            ctx.restore();

            ctx.save();
            ctx.translate(textX, textY + 100);
            ctx.fillText(colorName[index], 0, 0);
            ctx.restore();
        });

        // Scale back down for the output image
        const outputCanvas = document.createElement("canvas");
        outputCanvas.width = 1600;
        outputCanvas.height = 1200;
        const outputCtx = outputCanvas.getContext('2d');
        outputCtx.drawImage(canvas, 0, 0, outputCanvas.width, outputCanvas.height);

        // Export the image as a PNG
        const dataURL = outputCanvas.toDataURL('image/png');

        // Create a temporary link element
        const link = document.createElement('a');
        link.href = dataURL;
        link.download = 'creatosaurus_palate.png'; // Set the filename
        document.body.appendChild(link);

        // Trigger the click event to download the image
        link.click();

        // Clean up: remove the temporary link element
        document.body.removeChild(link);
    }


    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return "#000000"; // Return black if the color is not valid
        }
    }

    const checkValidColor = () => {
        let data = getColorOrDefault(colorToChange)

        const value = type
        if (value === "Complementary") {
            getComplementaryColors(data)
        } else if (value === "Analogous") {
            getAnalogousColors(data)
        } else if (value === "Triadic") {
            getTriadicColors(data)
        } else if (value === "Tetradic") {
            getTetradColors(data)
        } else if (value === "Double Split Complementary") {
            getDoubleSplitComplementary(data)
        } else if (value === "Rectangle") {
            getRectangleColors(data)
        } else if (value === "Split Complementary") {
            getSplitComplementaryColors(data)
        } else if (value === "Monochromatic") {
            getMonochromaticColors(data)
        }

        setColorToChange(data)
        setActiveColor(data)
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            checkValidColor();
        }
    }

    const copy = (color) => {
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`${color} copied to the clipboard!`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1200);
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    const getStyle = (data, index) => {
        if (index === activeHoverIndex) {
            return getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.3)" : "rgba(255,255,255,0.3)"
        } else {
            return getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.1)" : "rgba(255,255,255,0.1)"
        }
    }

    return (
        <div className={style.wheelContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head >

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.wheelContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.wheelGenerator}>
                    <div className={style.contrastWrapper}>
                        <div className={style.leftSide}>
                            <h2>Color Wheel
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="color-wheel" disabled selected>Color Wheel</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div className={style.row}>
                                <label>Text Color</label>
                                <div className={style.wrapper}>
                                    <input type='text'
                                        placeholder='#000000'
                                        value={colorToChange}
                                        onFocus={(e) => e.target.select()}
                                        onChange={(e) => setColorToChange(e.target.value)}
                                        onBlur={checkValidColor}
                                        onKeyDown={handleKeyPress} />
                                    <span style={{ backgroundColor: activeColor }} />
                                </div>
                            </div>

                            <div ref={colorPickerRef} style={{ marginTop: 30, width: '260px', height: '300px', alignSelf: 'center' }}></div>

                            <div className={style.row}>
                                <label>Select Color Combination</label>
                                <select value={type} onChange={changeType}>
                                    <option value="Complementary">Complementary</option>
                                    <option value="Monochromatic">Monochromatic</option>
                                    <option value="Analogous">Analogous</option>
                                    <option value="Triadic">Triadic</option>
                                    <option value="Tetradic">Tetradic</option>
                                    <option value="Rectangle">Rectangle</option>
                                    <option value="Split Complementary">Split Complementary</option>
                                    <option value="Double Split Complementary">Double Split Complementary</option>
                                </select>
                            </div>

                            <span className={style.poweredBy}>
                                Powered by {" "}
                                <Link href="https://creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </span>
                        </div>

                        <div className={style.rightSide}>
                            <div className={style.colorBox}>
                                {
                                    color.map((data, index) => {
                                        return <div key={index} className={style.box} style={{ backgroundColor: data }}>
                                            <div
                                                onMouseEnter={() => setActiveHoverIndex(index)}
                                                onMouseLeave={() => setActiveHoverIndex(null)}
                                                className={style.copy}
                                                style={{ backgroundColor: getStyle(data, index), cursor: 'pointer' }} onClick={() => copy(data)}>
                                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12.125 12.5H6C5.79289 12.5 5.625 12.3321 5.625 12.125V6C5.625 5.79289 5.79289 5.625 6 5.625H12.125C12.3321 5.625 12.5 5.79289 12.5 6V12.125C12.5 12.3321 12.3321 12.5 12.125 12.5Z" stroke={getContrastingColor(data)} strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M9.375 5.625V2.875C9.375 2.66789 9.20712 2.5 9 2.5H2.875C2.66789 2.5 2.5 2.66789 2.5 2.875V9C2.5 9.20712 2.66789 9.375 2.875 9.375H5.625" stroke={getContrastingColor(data)} strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>
                                            </div>
                                            <span onClick={() => copy(data)} style={{ color: getContrastingColor(data), cursor: 'pointer' }}>{data}</span>
                                            <span style={{ color: getContrastingColor(data) }}>{colorName[index]}</span>
                                            <span style={{ width: 5, height: 5, backgroundColor: getContrastingColor(data), borderRadius: 10, marginBottom: 70, marginTop: 10, opacity: activeColor === data ? 1 : 0 }} />
                                        </div>
                                    })
                                }
                            </div>
                            <button onClick={exportColorPalate}>
                                Export palette
                                <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.75 7.50033H12.25M12.25 7.50033L7.29167 2.54199M12.25 7.50033L7.29167 12.4587" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </button>

                            <Link href="https://www.creatosaurus.io/apps/muse">
                                <a target="_blank">
                                    Design using this color combination on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.50033H12.25M12.25 7.50033L7.29167 2.54199M12.25 7.50033L7.29167 12.4587" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
            {showMessage ? <Toast message={showMessage} /> : null}
        </div>
    )
}

export default ColorWheel
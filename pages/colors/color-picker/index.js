import React, { useRef, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import iro from '@jaames/iro';
import style from './ColorPicker.module.css'
import Link from 'next/link';
import colornamer from 'color-namer';
import tinycolor from 'tinycolor2';
import { colord, extend } from "colord";
import mixPlugin from "colord/plugins/mix";
import harmonies from "colord/plugins/harmonies";
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import convert from 'color-convert';
import Head from 'next/head';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import { getSinglePostById } from '../../../lib/posts';
let colorPicker

export async function getServerSideProps() {
    const id = "667ffe08da8d0c062a1dafb5"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ColorPicker = ({ post }) => {
    extend([mixPlugin, harmonies]);
    const router = useRouter()
    const { query } = router

    const colorPickerRef = useRef(null);
    const [activeColor, setActiveColor] = useState('#ff0000')
    const [activeColorToChange, setActiveColorToChange] = useState('#ff0000')
    const [activeHoverIndex, setActiveHoverIndex] = useState(null)
    const [showMessage, setShowMessage] = useState(null)
    const [copyColorName, setCopyColorName] = useState(null)
    const [colorConversions, setColorConversions] = useState({
        "HEX": "ff0000",
        "LAB": "53, 80, 67",
        "RGB": "255, 0, 0",
        "XYZ": "41, 21, 2",
        "CMYK": "0, 100, 100, 0",
        "LCH": "53, 104, 40",
        "HSB": "0, 100, 100",
        "HSL": "0, 100, 50",
        "HWB": "0, 0, 0",
        "HCG": "0, 100, 0"
    })

    const variationTypes = [{
        title: 'Tint',
        type: "tint"
    }, {
        title: 'Shades',
        type: "shades"
    }, {
        title: 'Tones',
        type: "tones"
    }, {
        title: 'Hues',
        type: "hues"
    }]

    const harmoniesType = [{
        title: 'Complementary Harmony',
        type: "complementary"
    }, {
        title: 'Analogous Harmony',
        type: "analogous"
    }, {
        title: 'Triadic Harmony',
        type: "triadic"
    }, {
        title: 'Tetradic Harmony',
        type: "tetradic"
    }, {
        title: 'Rectangle Harmony',
        type: "rectangle"
    }, {
        title: 'Split Complementary Harmony',
        type: "split-complementary"
    }, {
        title: 'Double Complementary Harmony',
        type: "double-split-complementary"
    }]

    useEffect(() => {
        let colorData = "#" + query.color
        const color = tinycolor(colorData);
        if (color.isValid()) {
            colorData = color.toHexString()
            setActiveColor(colorData)
            setActiveColorToChange(colorData)

            if (colorPicker) {
                colorPicker.setColors([colorData])
            }
        }
    }, [query])


    useEffect(() => {
        colorPicker = new iro.ColorPicker(colorPickerRef.current, {
            color: activeColor,
            width: 260,
            sliderSize: 10,
            layout: [
                {
                    component: iro.ui.Box,
                    options: {
                        boxHeight: 140,
                    }
                },
                {
                    component: iro.ui.Slider,
                    options: {
                        sliderType: 'hue'
                    }
                }
            ]
        });

        colorPicker.on('color:change', (color) => {
            const rgb = color.rgb;
            const hex = color.hexString;
            const labColor = convert.rgb.lab(rgb.r, rgb.g, rgb.b)

            let obj = {
                HEX: convert.rgb.hex(rgb.r, rgb.g, rgb.b),
                LAB: labColor.join(', '),
                RGB: `${rgb.r}, ${rgb.g}, ${rgb.b}`,
                XYZ: convert.rgb.xyz(rgb.r, rgb.g, rgb.b).join(', '),
                CMYK: convert.rgb.cmyk(rgb.r, rgb.g, rgb.b).join(', '),
                LCH: convert.lab.lch(labColor).join(', '),
                HSB: convert.rgb.hsv(rgb.r, rgb.g, rgb.b).join(', '),
                HSL: convert.rgb.hsl(rgb.r, rgb.g, rgb.b).join(', '),
                HWB: convert.rgb.hwb(rgb.r, rgb.g, rgb.b).join(', '),
                HCG: convert.rgb.hcg(rgb.r, rgb.g, rgb.b).join(', ')
            }

            setActiveColor(hex)
            setActiveColorToChange(hex)
            setColorConversions(obj)
        });
    }, []);

    function getContrastingColor(color) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return luminance > 0.5 ? '#000' : '#FFF';
    }

    const copy = () => {
        const color = activeColor
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`${color} copied to the clipboard!`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1200);
    }

    const copyClickColor = (color, type) => {
        setCopyColorName(color + type)
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`${color} copied to the clipboard!`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1200);
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    const getStyle = (data) => {
        if (activeHoverIndex === 1) {
            return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.3)" : "rgba(255,255,255,0.3)" }
        } else {
            return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.1)" : "rgba(255,255,255,0.1)" }
        }
    }

    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return activeColor // Return black if the color is not valid
        }
    }

    const validateColor = () => {
        let color = getColorOrDefault(activeColorToChange)

        const labColor = convert.hex.lab(color)

        let obj = {
            HEX: convert.rgb.hex(convert.hex.rgb(color)),
            LAB: labColor.join(', '),
            RGB: convert.hex.rgb(color),
            XYZ: convert.hex.xyz(color).join(', '),
            CMYK: convert.hex.cmyk(color).join(', '),
            LCH: convert.hex.lch(labColor).join(', '),
            HSB: convert.hex.hsv(color).join(', '),
            HSL: convert.hex.hsl(color).join(', '),
            HWB: convert.hex.hwb(color).join(', '),
            HCG: convert.hex.hcg(color).join(', ')
        }

        setActiveColor(color)
        setActiveColorToChange(color)
        setColorConversions(obj)
        colorPicker.setColors([color])
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            validateColor()
        }
    }

    const getColorVariations = (type) => {
        const color = colord(activeColor);
        if (type === "tint") {
            return color.tints(11).map((c) => c.toHex())
        } else if (type === "shades") {
            return color.shades(11).map((c) => c.toHex())
        } else if (type === "tones") {
            return color.tones(11).map((c) => c.toHex())
        } else if (type === "hues") {
            let nextHues = [];
            let prevHues = [];
            let color = colord(activeColor).toHsl();
            let steps = 5
            let baseHue = color.h

            for (let i = 1; i <= steps; i++) {
                // Calculate next hue
                let nextHue = (baseHue + i * 10) % 360;
                nextHues.push({ h: nextHue, s: color.s, l: color.l });

                // Calculate previous hue
                let prevHue = (baseHue - i * 10 + 360) % 360;
                prevHues.push({ h: prevHue, s: color.s, l: color.l });
            }

            prevHues = prevHues.reverse()

            let colorData = [...prevHues, { h: color.h, s: color.s, l: color.l }, ...nextHues]
            colorData = colorData.map(hue => colord(hue).toHex());
            return colorData
        }
    }

    const getAnalogousColors = (type, colorData) => {
        let colorCode = colorData ? colorData : activeColor
        const color = colord(colorCode);
        return color.harmonies(type).map((c) => c.toHex());
    }

    const hexToRgb = (hex) => {
        let bigint = parseInt(hex.slice(1), 16);
        let r = (bigint >> 16) & 255;
        let g = (bigint >> 8) & 255;
        let b = bigint & 255;
        return [r, g, b];
    }

    const relativeLuminance = (rgb) => {
        let [r, g, b] = rgb.map(value => {
            value /= 255;
            return (value <= 0.03928) ? value / 12.92 : Math.pow((value + 0.055) / 1.055, 2.4);
        });

        return 0.2126 * r + 0.7152 * g + 0.0722 * b;
    }

    const contrastRatio = (luminance1, luminance2) => {
        let brightest = Math.max(luminance1, luminance2);
        let darkest = Math.min(luminance1, luminance2);
        return (brightest + 0.05) / (darkest + 0.05);
    }

    const meetsWCAG = (ratio, isLargeText = false) => {
        return isLargeText ? ratio >= 3.0 : ratio >= 4.5;
    };

    const calculateContrast = (textColor, backgroundColor) => {
        let textColorData = textColor
        let backgroundColorData = backgroundColor

        let rgb1 = hexToRgb(textColorData);
        let rgb2 = hexToRgb(backgroundColorData);
        let luminance1 = relativeLuminance(rgb1);
        let luminance2 = relativeLuminance(rgb2);
        let ratio = contrastRatio(luminance1, luminance2);
        let passOrFailed = meetsWCAG(ratio)

        let contrast = ratio
        let grade = "Very Good"
        if (ratio < 3) {
            grade = "Very Poor"
        } else if (contrast < 4.5) {
            grade = "Poor"
        } else if (contrast < 7) {
            grade = "Good"
        } else {
            grade = "Very Good"
        }

        let score = 1
        if (contrast > 12) {
            score = 5
        } else if (contrast > 7) {
            score = 4
        } else if (contrast > 4.5) {
            score = 3
        } else if (contrast > 3) {
            score = 2
        } else {
            score = 1
        }

        return {
            passOrFailed,
            grade,
            score
        }
    };


    return (
        <div className={style.ColorPickerContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head >

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.ColorPickerContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.ColorPickerGenerator}>
                    <div className={style.ColorPickerWrapper}>

                        <div className={style.leftSide}>
                            <h2>Color Picker
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="color-picker" disabled selected>Color Picker</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div ref={colorPickerRef} style={{ marginTop: 30, width: '260px', height: '162px' }}></div>
                            <div className={style.row}>
                                <label>Color</label>
                                <input type='text'
                                    placeholder='#000000'
                                    value={activeColorToChange}
                                    onFocus={(e) => e.target.select()}
                                    onChange={(e) => setActiveColorToChange(e.target.value)}
                                    onBlur={validateColor}
                                    onKeyDown={handleKeyDown} />
                            </div>
                            <button>Powered by {" "}
                                <Link href="https://www.creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </button>
                        </div>

                        <div className={style.rightSide}>
                            <div className={style.box} style={{ backgroundColor: activeColor }}>
                                <span onClick={copy} style={{ color: getContrastingColor(activeColor) }}>{activeColor}</span>
                                <h3 style={{ color: getContrastingColor(activeColor) }}>{colornamer(activeColor, { pick: ['ntc'] }).ntc[0].name}</h3>
                                <div
                                    onMouseEnter={() => setActiveHoverIndex(1)}
                                    onMouseLeave={() => setActiveHoverIndex(null)}
                                    onClick={copy}
                                    style={getStyle(activeColor)}>
                                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.125 12.5H6C5.79289 12.5 5.625 12.3321 5.625 12.125V6C5.625 5.79289 5.79289 5.625 6 5.625H12.125C12.3321 5.625 12.5 5.79289 12.5 6V12.125C12.5 12.3321 12.3321 12.5 12.125 12.5Z" stroke={getContrastingColor(activeColor)} strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M9.375 5.625V2.875C9.375 2.66789 9.20712 2.5 9 2.5H2.875C2.66789 2.5 2.5 2.66789 2.5 2.875V9C2.5 9.20712 2.66789 9.375 2.875 9.375H5.625" stroke={getContrastingColor(activeColor)} strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                            </div>

                            <div className={style.conversions}>
                                <h3>Conversions</h3>
                                <div className={style.row}>
                                    {Object.entries(colorConversions)?.map(([key, value]) => (
                                        <div key={key}>
                                            <span>{key}</span>
                                            <span style={{ textTransform: 'uppercase' }}>{value}</span>
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className={style.variations}>
                                <h3>Variations</h3>
                                <div>
                                    {
                                        variationTypes.map((category, index) => {
                                            return <div key={category.type} className={style.row} style={{ marginTop: index === 0 ? 10 : 20 }}>
                                                <label>{category.title}</label>
                                                <div className={style.wrapper}>
                                                    {
                                                        getColorVariations(category.type).map((data, i) => {
                                                            return <div onMouseLeave={() => setCopyColorName(null)} key={data + i} className={style.card} style={{ backgroundColor: data }} onClick={() => copyClickColor(data, category.type)}>
                                                                {
                                                                    data + category.type === copyColorName ?
                                                                        <svg width="24px" height="24px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color={getContrastingColor(data)}>
                                                                            <path d="M5 13L9 17L19 7" stroke={getContrastingColor(data)} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                                                        </svg> : <>
                                                                            <span style={{ color: getContrastingColor(data) }}>{data}</span>
                                                                            <div style={data === activeColor ? { backgroundColor: getContrastingColor(data) } : { display: 'none' }} className={style.dot} />
                                                                        </>
                                                                }
                                                            </div>
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>

                            <div className={style.variations}>
                                <h3>Harmonies</h3>
                                <div>
                                    {
                                        harmoniesType.map((category, index) => {
                                            return <div key={category.type} className={style.row} style={{ marginTop: index === 0 ? 10 : 20 }}>
                                                <label>{category.title}</label>
                                                <div className={style.wrapper}>
                                                    {
                                                        getAnalogousColors(category.type).map((data, i) => {
                                                            return <div onMouseLeave={() => setCopyColorName(null)} key={data + i} className={style.card} style={{ backgroundColor: data }} onClick={() => copyClickColor(data, category.type)}>
                                                                {
                                                                    data + category.type === copyColorName ?
                                                                        <svg width="24px" height="24px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color={getContrastingColor(data)}>
                                                                            <path d="M5 13L9 17L19 7" stroke={getContrastingColor(data)} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                                                        </svg> : <>
                                                                            <span style={{ color: getContrastingColor(data) }}>{data}</span>
                                                                            <div style={data === activeColor ? { backgroundColor: getContrastingColor(data) } : { display: 'none' }} className={style.dot} />
                                                                        </>
                                                                }
                                                            </div>
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>

                            <div className={style.contrast}>
                                <h3>Contrast Checker</h3>
                                <div className={style.section}>
                                    <div className={style.wrapper}>
                                        <div className={style.head1}>
                                            <span>White Background</span>
                                            <span style={{ backgroundColor: calculateContrast(activeColor, "#ffffff").passOrFailed ? "#FBF5D0" : "#FBD0DA" }}>{calculateContrast(activeColor, "#ffffff").grade} {calculateContrast(activeColor, "#ffffff").score}/5</span>
                                        </div>
                                        <div className={style.cBox} style={{ backgroundColor: '#fff' }}>
                                            <span style={{ color: activeColor }}>Quote</span>
                                            <p style={{ color: activeColor }}>A clever quote opens new paths in the minds of the clever!</p>
                                            <span style={{ color: activeColor }}>Mehmet Murat ildan</span>
                                        </div>
                                    </div>

                                    <div className={style.wrapper} style={{ marginTop: 30 }}>
                                        <div className={style.head1}>
                                            <span>Black Background</span>
                                            <span style={{ backgroundColor: calculateContrast(activeColor, "#000000").passOrFailed ? "#FBF5D0" : "#FBD0DA" }}>{calculateContrast(activeColor, "#000000").grade} {calculateContrast(activeColor, "#000000").score}/5</span>
                                        </div>
                                        <div className={style.cBox} style={{ backgroundColor: '#000' }}>
                                            <span style={{ color: activeColor }}>Quote</span>
                                            <p style={{ color: activeColor }}>A clever quote opens new paths in the minds of the clever!</p>
                                            <span style={{ color: activeColor }}>Mehmet Murat ildan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <Link href="https://www.creatosaurus.io/apps/muse">
                                <a target="_blank">
                                    Design with this<span style={{ textTransform: 'uppercase' }}>&nbsp;{activeColor}&nbsp;</span>color on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
            {showMessage ? <Toast message={showMessage} /> : null}
        </div>
    )
}

export default ColorPicker
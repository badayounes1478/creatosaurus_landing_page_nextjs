import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import style from './ContrastChecker.module.css'
import { ChromePicker } from 'react-color';
import tinycolor from "tinycolor2"
import Link from 'next/link'
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import { getSinglePostById } from '../../../lib/posts';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import Head from 'next/head';

export async function getServerSideProps() {
    const id = "667fd6dfda8d0c062a1dae53"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ContrastChecker = ({ post }) => {
    const router = useRouter()
    const [textColor, setTextColor] = useState("#000000")
    const [textColorToChange, setTextColorToChange] = useState("#000000")

    const [backgroundColor, setBackgroundColor] = useState("#6AFFED")
    const [backgroundColorToChange, setBackgroundColorToChange] = useState("#6AFFED")

    const [showColorPicker1, setShowColorPicker1] = useState(false)
    const [showColorPicker2, setShowColorPicker2] = useState(false)
    const [contrast, setContrast] = useState(0)

    useEffect(() => {
        calculateContrast()
    }, [])


    const hexToRgb = (hex) => {
        let bigint = parseInt(hex.slice(1), 16);
        let r = (bigint >> 16) & 255;
        let g = (bigint >> 8) & 255;
        let b = bigint & 255;
        return [r, g, b];
    }

    const relativeLuminance = (rgb) => {
        let [r, g, b] = rgb.map(value => {
            value /= 255;
            return (value <= 0.03928) ? value / 12.92 : Math.pow((value + 0.055) / 1.055, 2.4);
        });

        return 0.2126 * r + 0.7152 * g + 0.0722 * b;
    }

    const contrastRatio = (luminance1, luminance2) => {
        let brightest = Math.max(luminance1, luminance2);
        let darkest = Math.min(luminance1, luminance2);
        return (brightest + 0.05) / (darkest + 0.05);
    }

    const calculateContrast = (color1, color2) => {
        let textColorData = color1 ? color1 : textColor
        let backgroundColorData = color2 ? color2 : backgroundColor

        let rgb1 = hexToRgb(textColorData);
        let rgb2 = hexToRgb(backgroundColorData);
        let luminance1 = relativeLuminance(rgb1);
        let luminance2 = relativeLuminance(rgb2);
        let ratio = contrastRatio(luminance1, luminance2);
        setContrast(ratio);
    };

    const handleColorChange = (color) => {
        setTextColor(color.hex);
        setTextColorToChange(color.hex)
        calculateContrast(color.hex)
    };

    const handleColorChange2 = (color) => {
        setBackgroundColor(color.hex);
        setBackgroundColorToChange(color.hex)
        calculateContrast(textColor, color.hex)
    };

    const openColorPicker1 = (event) => {
        event.stopPropagation();
        setShowColorPicker1((prev) => !prev);
        setShowColorPicker2(false)
    };

    const openColorPicker2 = (event) => {
        event.stopPropagation();
        setShowColorPicker2((prev) => !prev);
        setShowColorPicker1(false)
    };

    const closeColorPicker = () => {
        setShowColorPicker1(false)
        setShowColorPicker2(false)
    }

    const meetsWCAG = (ratio, isLargeText = false) => {
        return isLargeText ? ratio >= 3.0 : ratio >= 4.5;
    };

    const checkGrade = () => {
        if (contrast < 3) {
            return "(Very Poor)"
        } else if (contrast < 4.5) {
            return "(Poor)"
        } else if (contrast < 7) {
            return "(Good)"
        } else {
            return "(Very Good)"
        }
    }

    const starSvg = (data, opacity = 1, color = "#5F071C") => {
        return <svg key={data} width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6.5 9.79375L10.3625 12.125L9.3375 7.73125L12.75 4.775L8.25625 4.39375L6.5 0.25L4.74375 4.39375L0.25 4.775L3.6625 7.73125L2.6375 12.125L6.5 9.79375Z" fill={color} fillOpacity={opacity} />
        </svg>
    }

    const getStars = () => {
        let stars = null
        const color = "#5F5207"

        if (contrast > 12) {
            stars = [1, 1, 1, 1, 1].map((data, index) => {
                return starSvg(index, data, color)
            })
        } else if (contrast > 7) {
            stars = [1, 1, 1, 1, 0.25].map((data, index) => {
                return starSvg(index, data, color)
            })
        } else if (contrast > 4.5) {
            stars = [1, 1, 1, 0.25, 0.25].map((data, index) => {
                return starSvg(index, data, color)
            })
        } else if (contrast > 3) {
            stars = [1, 1, 0.25, 0.25, 0.25].map((data, index) => {
                return starSvg(index, data)
            })
        } else {
            stars = [1, 0.25, 0.25, 0.25, 0.25].map((data, index) => {
                return starSvg(index, data)
            })
        }

        return stars
    }


    const getStarRating = (ratio, isLargeText = false) => {
        let stars = null
        const color = "#5F5207"

        if (isLargeText) {
            if (ratio >= 7.0) {
                stars = [1, 1, 1, 1, 1].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 4.5) {
                stars = [1, 1, 1, 1, 0.25].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 3.0) {
                stars = [1, 1, 1, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 2.0) {
                stars = [1, 1, 0.25, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data)
                })
            } else {
                stars = [1, 0.25, 0.25, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data)
                })
            }
        } else {
            if (ratio >= 10.0) {
                stars = [1, 1, 1, 1, 1].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 7.0) {
                stars = [1, 1, 1, 1, 0.25].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 4.5) {
                stars = [1, 1, 1, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data, color)
                })
            } else if (ratio >= 3.0) {
                stars = [1, 1, 0.25, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data)
                })
            } else {
                stars = [1, 0.25, 0.25, 0.25, 0.25].map((data, index) => {
                    return starSvg(index, data)
                })
            }
        }

        return stars
    };

    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return textColor; // Return black if the color is not valid
        }
    }

    function getColorOrDefault2(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return backgroundColor; // Return black if the color is not valid
        }
    }

    const checkColor = () => {
        let data = getColorOrDefault(textColorToChange)
        setTextColor(data)
        setTextColorToChange(data)
        calculateContrast(data, null)
    }

    const checkColor2 = () => {
        let data = getColorOrDefault2(backgroundColorToChange)
        setBackgroundColor(data)
        setBackgroundColorToChange(data)
        calculateContrast(null, backgroundColor)
    }


    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            checkColor()
        }
    }

    const handleKeyPress2 = (e) => {
        if (e.key === 'Enter') {
            checkColor2()
        }
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }


    return (
        <div className={style.contrastCheckerContainerScroll} onClick={closeColorPicker}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head>

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.contrastCheckerContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.contrastGenerator}>
                    <div className={style.contrastWrapper}>
                        <div className={style.leftSide}>
                            <h2>Color Contrast Checker
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="contrast-checker" disabled selected>Contrast Checker</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div className={style.row}>
                                <label>Text Color</label>
                                <div className={style.wrapper}>
                                    <input type='text'
                                        placeholder='#000000'
                                        value={textColorToChange}
                                        onFocus={(e) => e.target.select()}
                                        onChange={(e) => setTextColorToChange(e.target.value)}
                                        onBlur={checkColor}
                                        onKeyDown={handleKeyPress} />
                                    <span style={{ backgroundColor: textColor }} onClick={openColorPicker1} />
                                </div>

                                <>
                                    {showColorPicker1 ? (
                                        <div className={style.colorPicker} onClick={(e) => e.stopPropagation()}>
                                            <ChromePicker color={textColor} onChange={handleColorChange} />
                                        </div>
                                    ) : null}
                                </>
                            </div>

                            <div className={style.row} style={{ marginTop: 15 }}>
                                <label>Background Color</label>
                                <div className={style.wrapper}>
                                    <input type='text'
                                        placeholder='#000000'
                                        value={backgroundColorToChange}
                                        onFocus={(e) => e.target.select()}
                                        onChange={(e) => setBackgroundColorToChange(e.target.value)}
                                        onBlur={checkColor2}
                                        onKeyDown={handleKeyPress2} />
                                    <span style={{ backgroundColor: backgroundColor }} onClick={openColorPicker2} />
                                </div>

                                <>
                                    {showColorPicker2 ? (
                                        <div className={style.colorPicker} onClick={(e) => e.stopPropagation()}>
                                            <ChromePicker color={backgroundColor} onChange={handleColorChange2} />
                                        </div>
                                    ) : null}
                                </>
                            </div>

                            <div className={style.ratioRow}>
                                <span className={style.title}>Contrast Ratio</span>
                                <div className={style.card} style={{ backgroundColor: meetsWCAG(contrast) ? "#D2FBD0" : "#FBD0DA" }}>
                                    <div>{contrast.toFixed(2)}  <span>{checkGrade()}</span></div>
                                    <div>{getStars()}</div>
                                </div>

                                <div className={style.card} style={{ backgroundColor: meetsWCAG(contrast) ? "#D2FBD0" : "#FBD0DA" }}>
                                    <div>Small text</div>
                                    <div>{getStarRating(contrast)}</div>
                                </div>

                                <div className={style.card} style={{ backgroundColor: meetsWCAG(contrast, true) ? "#D2FBD0" : "#FBD0DA" }}>
                                    <div>Large text</div>
                                    <div>{getStarRating(contrast, true)}</div>
                                </div>
                            </div>

                            <span className={style.poweredBy}>
                                Powered by {" "}
                                <Link href="https://creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </span>
                        </div>
                        <div className={style.rightSide}>
                            <div className={style.quote} style={{ backgroundColor: backgroundColor }}>
                                <span style={{ color: textColor }}>Quote</span>
                                <p style={{ color: textColor }}>A clever quote opens new paths in the minds of the clever!</p>
                                <span style={{ color: textColor }}>Mehmet Murat ildan</span>
                            </div>

                            <Link href="https://www.creatosaurus.io/apps/muse">
                                <a target="_blank">
                                    Design using this color contrast on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.50033H12.25M12.25 7.50033L7.29167 2.54199M12.25 7.50033L7.29167 12.4587" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default ContrastChecker
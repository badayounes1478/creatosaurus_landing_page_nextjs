import React, { useEffect, useState, useRef } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import style from './ColorImagePicker.module.css'
import ColorThief from 'colorthief';
import ColorNamer from 'color-namer';
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import { getSinglePostById } from '../../../lib/posts';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import Head from 'next/head';

let canvas = null
let ctx = null
let img = null

export async function getServerSideProps() {
    const id = "66806f23da8d0c062a1db0c2"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ColorImagePicker = ({ post }) => {
    const router = useRouter()
    const fileInputRef = useRef(null);
    const [palette, setPalette] = useState([]);
    const [colorCount, setColorCount] = useState(4)
    const [activeColor, setActiveColor] = useState("#000000")
    const [image, setImage] = useState("https://images.unsplash.com/photo-1716872491089-a43035b4db58?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw3fHx8ZW58MHx8fHx8")
    const [showMessage, setShowMessage] = useState(null)

    const rgbToHex = (r, g, b) => {
        const componentToHex = c => {
            const hex = c.toString(16);
            return hex.length === 1 ? '0' + hex : hex;
        };
        return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
    };

    useEffect(() => {
        canvas = document.getElementById("canvas")
        ctx = canvas.getContext("2d");

        img = new Image();
        img.onload = () => {
            let width = img.naturalWidth
            let height = img.naturalHeight
            drawImageOnCanvas(width, height)
            getColorPalate(colorCount)
        }

        img.src = "https://images.unsplash.com/photo-1716872491089-a43035b4db58?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw3fHx8ZW58MHx8fHx8"
        img.crossOrigin = 'Anonymous';
    }, [])

    const drawImageOnCanvas = (width, height) => {
        // Define maximum dimensions for the canvas
        const maxWidth = window.innerWidth > 1100 ? 507 : window.innerWidth;
        const maxHeight = 50000;

        // Calculate scaled dimensions while maintaining aspect ratio
        if (width > maxWidth || height > maxHeight) {
            const ratio = Math.min(maxWidth / width, maxHeight / height);
            width *= ratio;
            height *= ratio;

            canvas.width = width
            canvas.height = height

            // Clear canvas before drawing
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            // Adjust canvas context for high-density displays
            const devicePixelRatio = window.devicePixelRatio || 1;
            canvas.style.width = `${canvas.width}px`;
            canvas.style.height = `${canvas.height}px`;
            canvas.width *= devicePixelRatio;
            canvas.height *= devicePixelRatio;

            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        }
    }

    const checkStyle = (data, index) => {
        if (index === 0) {
            return { backgroundColor: data.hex, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }
        } else if (palette.length - 1 === index) {
            return { backgroundColor: data.hex, borderTopRightRadius: 5, borderBottomRightRadius: 5 }
        } else {
            return { backgroundColor: data.hex }
        }
    }

    const getColorPalate = (count) => {
        const colorThief = new ColorThief();
        const palette = colorThief.getPalette(img, count).map((color) => {
            const hex = rgbToHex(color[0], color[1], color[2]);
            const name = ColorNamer(hex, { pick: ['ntc'] }).ntc[0].name
            return { rgb: color, hex, name };
        });

        setPalette(palette);
        if (palette.length <= 0) return
        setActiveColor(palette[0].hex)

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        /*palette.forEach(color => {
            const position = findColorPosition(color.rgb);
            if (position) {
                drawCircle(position.x, position.y, color.hex);
            }
        });*/

        setImage(canvas.toDataURL())
    }

    const add = () => {
        let maxCount = 10
        let count = colorCount + 1
        if (count > maxCount) return

        setColorCount(count)
        getColorPalate(count)
    }

    const subtract = () => {
        let minCount = 1
        let count = colorCount - 1
        if (count <= minCount) return

        setColorCount(count)
        getColorPalate(count)
    }

    const handleButtonClick = () => {
        fileInputRef.current.click();
    };

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                img = new Image();
                img.onload = () => {
                    let width = img.naturalWidth
                    let height = img.naturalHeight

                    drawImageOnCanvas(width, height)
                    getColorPalate(colorCount)
                };
                img.src = e.target.result;
                img.crossOrigin = 'Anonymous';
            }

            reader.readAsDataURL(file);
        }
    }


    const copy = () => {
        const color = activeColor
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`${color} copied to the clipboard!`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1200);
    }

    function isColorMatch(r, g, b, r1, g1, b1, tolerance = 10) {
        return Math.abs(r - r1) <= tolerance &&
            Math.abs(g - g1) <= tolerance &&
            Math.abs(b - b1) <= tolerance;
    }

    function drawCircle(x, y, color) {
        const radius = 18;
        ctx.beginPath();
        ctx.lineWidth = 6;
        ctx.arc(x, y, radius, 0, Math.PI * 2, false);
        ctx.fillStyle = color;
        ctx.strokeStyle = "#fff";
        ctx.fill();
        ctx.stroke();

        const radius1 = 22;
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.arc(x, y, radius1, 0, Math.PI * 2, false);
        ctx.strokeStyle = "#828282";
        ctx.stroke();
    }

    const findColorPosition = (rgb) => {
        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const data = imageData.data;

        for (let y = 0; y < canvas.height; y++) {
            for (let x = 0; x < canvas.width; x++) {
                const index = (y * canvas.width + x) * 4;
                const r = data[index];
                const g = data[index + 1];
                const b = data[index + 2];

                if (isColorMatch(r, g, b, rgb[0], rgb[1], rgb[2])) {
                    return { x, y }; // Return the position immediately
                }
            }
        }
        return null; // Return null if no match is found
    };

    const exportColorPalate = () => {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext('2d');

        // Increase the canvas resolution for higher quality
        const scale = 2; // Adjust the scale factor as needed
        canvas.width = 1600 * scale;
        canvas.height = 1200 * scale;
        const blockWidth = canvas.width / palette.length;

        // Set text properties with scaling
        ctx.font = `500 48px Arial`;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.letterSpacing = `${1.3 * scale}px`;

        // Disable image smoothing for crisper output
        ctx.imageSmoothingEnabled = false;

        palette.forEach((color, index) => {
            ctx.fillStyle = color.hex;
            ctx.fillRect(index * blockWidth, 0, blockWidth, canvas.height);

            // Draw color code
            ctx.fillStyle = getContrastingColor(color.hex);
            const textX = index * blockWidth + blockWidth / 2;
            const textY = canvas.height / 2;

            // Save the current context
            ctx.save();
            ctx.translate(textX, textY);
            ctx.rotate(-Math.PI / 2);
            ctx.fillText(color.hex.toUpperCase().slice(1), 0, 0);
            ctx.restore();
        });

        // Scale back down for the output image
        const outputCanvas = document.createElement("canvas");
        outputCanvas.width = 1600;
        outputCanvas.height = 1200;
        const outputCtx = outputCanvas.getContext('2d');
        outputCtx.drawImage(canvas, 0, 0, outputCanvas.width, outputCanvas.height);

        // Export the image as a PNG
        const dataURL = outputCanvas.toDataURL('image/png');

        // Create a temporary link element
        const link = document.createElement('a');
        link.href = dataURL;
        link.download = 'creatosaurus_palate.png'; // Set the filename
        document.body.appendChild(link);

        // Trigger the click event to download the image
        link.click();

        // Clean up: remove the temporary link element
        document.body.removeChild(link);
    }

    function getContrastingColor(color) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return luminance > 0.5 ? '#000' : '#FFF';
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    return (
        <div className={style.colorPickerContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head>

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.colorPickerContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.colorGenerator}>
                    <div className={style.colorWrapper}>
                        <div className={style.leftSide}>
                            <h2>Image Color Palette
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="color-image-picker" disabled selected>Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div className={style.colorInfo}>
                                <span>Selected Color</span>
                                <p>Hex code of the color from the photo</p>
                                <div className={style.colorDetails}>
                                    <div>
                                        <div className={style.box} style={{ backgroundColor: activeColor }} />
                                        <span onClick={copy}>{activeColor}</span>
                                    </div>
                                    <svg onClick={copy} style={{ cursor: 'pointer' }} width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M20.2083 20.8333H10C9.65482 20.8333 9.375 20.5535 9.375 20.2083V10C9.375 9.65482 9.65482 9.375 10 9.375H20.2083C20.5535 9.375 20.8333 9.65482 20.8333 10V20.2083C20.8333 20.5535 20.5535 20.8333 20.2083 20.8333Z" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M15.6243 9.37533V4.79199C15.6243 4.44681 15.3446 4.16699 14.9993 4.16699H4.79102C4.44584 4.16699 4.16602 4.44681 4.16602 4.79199V15.0003C4.16602 15.3455 4.44584 15.6253 4.79102 15.6253H9.37435" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                                <Link href={`/colors/color-picker?color=${activeColor.slice(1)}`}>
                                    <a target="_blank">
                                        Visit {activeColor} complete color profile
                                        <svg style={{ marginLeft: 5 }} width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1.25 5.49967H8.75M8.75 5.49967L5.20833 1.95801M8.75 5.49967L5.20833 9.04134" stroke="#0078FF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </a>
                                </Link>
                            </div>
                            <div className={style.buttons}>
                                <button onClick={handleButtonClick}>Upload your image
                                    <input
                                        type="file"
                                        ref={fileInputRef}
                                        style={{ display: 'none' }}
                                        onChange={handleFileChange}
                                    />
                                </button>
                                <button onClick={exportColorPalate}>Export Palette</button>
                            </div>
                            <span className={style.info}>Powered by{" "}
                                <Link href="https://www.creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </span>
                        </div>

                        <div className={style.rightSide}>
                            <canvas id='canvas' style={{ position: 'absolute', top: -1000000 }}></canvas>
                            <img draggable={false} src={image} alt='' />
                            <div className={style.colorSection}>
                                <p>Extracted palette from the photo</p>
                                <div className={style.colors}>
                                    {
                                        palette.map((data, index) => {
                                            return <div key={data.hex + index} className={style.card} onClick={() => setActiveColor(data.hex)}>
                                                <div className={style.colorBox} style={checkStyle(data, index)}>
                                                    <div style={data.hex === activeColor ? { display: 'flex' } : { display: 'none' }} className={style.dot} />
                                                    <span style={{ color: getContrastingColor(data.hex) }}>{data.hex}</span>
                                                </div>
                                            </div>
                                        })
                                    }

                                    <div className={style.button}>
                                        <button onClick={add}>+</button>
                                        <button onClick={subtract}>-</button>
                                    </div>
                                </div>

                                <Link href="https://www.creatosaurus.io/apps/muse">
                                    <a target="_blank">
                                        Design with this color palette on Muse
                                        <svg style={{ marginLeft: 5 }} width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1.25 5.49967H8.75M8.75 5.49967L5.20833 1.95801M8.75 5.49967L5.20833 9.04134" stroke="#0078FF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
            {showMessage ? <Toast message={showMessage} /> : null}
        </div>
    )
}

export default ColorImagePicker
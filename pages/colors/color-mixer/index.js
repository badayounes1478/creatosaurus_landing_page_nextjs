import React, { useRef, useEffect, useState } from 'react'
import iro from '@jaames/iro';
import { useRouter } from 'next/router'
import { random } from "colord";
import tinycolor from 'tinycolor2';
import colornamer from 'color-namer';
import Link from 'next/link';
import style from './ColorMixer.module.css'
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import { getSinglePostById } from '../../../lib/posts';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import Head from 'next/head';
let colorPicker
let colorsData = []
let colorDataIndex = 0
let dragging = false
let isMouseOnHandle = false

export async function getServerSideProps() {
    const id = "66928a3fda8d0c062a1db99f"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const ColorMixer = ({ post }) => {
    const router = useRouter()
    const colorPickerRef = useRef(null);
    const [activeColor, setActiveColor] = useState('#413EB7')
    const [activeColorToChange, setActiveColorToChange] = useState('#413EB7')
    const [mixedColor, setMixedColor] = useState("#4346A2")

    const [activePercentageToChange, setActivePercentageToChange] = useState(50)
    const [activeIndex, setActiveIndex] = useState(0)
    const [activeHoverIndex, setActiveHoverIndex] = useState(null)
    const [refresh, setRefresh] = useState(false)
    const [showMessage, setShowMessage] = useState(null)
    const [copyHover, setCopyHover] = useState(false)

    const [colors, setColors] = useState([
        { color: '#413EB7', percentage: 25 },
        { color: '#00ECC2', percentage: 25 },
        { color: '#0000ff', percentage: 25 },
        { color: '#9B2828', percentage: 25 },
    ]);

    useEffect(() => {
        colorsData = colors

        colorPicker = new iro.ColorPicker(colorPickerRef.current, {
            color: mixedColor,
            width: 260,
            sliderSize: 10,
            layout: [
                {
                    component: iro.ui.Box,
                    options: {
                        boxHeight: 140,
                    }
                },
                {
                    component: iro.ui.Slider,
                    options: {
                        sliderType: 'hue'
                    }
                }
            ]
        });

        mixTheColors(colors)

        colorPicker.on('color:change', (color) => {
            let updatedColor = colorsData.map((data, index) => {
                if (index === colorDataIndex) {
                    data.color = color.hexString
                }

                return data
            })

            setActiveColor(color.hexString)
            setActiveColorToChange(color.hexString)
            setColors(updatedColor)
            mixTheColors(updatedColor)
            colorsData = updatedColor
        });
    }, []);

    const mixTheColors = (updatedColors) => {
        let mixedColor = tinycolor(updatedColors[0].color);
        for (let i = 1; i < updatedColors.length; i++) {
            mixedColor = tinycolor.mix(mixedColor, tinycolor(updatedColors[i].color), updatedColors[i].percentage);
        }

        setMixedColor(tinycolor(mixedColor).toHexString())
    }


    const handleDrag = (index, e) => {
        const initialX = e.clientX;
        const initialWidth = e.target.parentElement.offsetWidth;
        dragging = true
        setRefresh((prev) => !prev)

        const mouseMoveHandler = (moveEvent) => {
            const currentX = moveEvent.clientX;
            const diff = currentX - initialX;

            const newWidth = initialWidth + diff;
            const totalWidth = e.target.parentElement.parentElement.offsetWidth;
            const newPercentage = Math.trunc((newWidth / totalWidth) * 100)

            if (newPercentage >= 0 && newPercentage <= 100) {
                const updatedColors = [...colors];

                let newDiff = updatedColors[index].percentage - newPercentage

                if (newPercentage < 1 || (updatedColors[index + 1].percentage + newDiff) < 1) return

                updatedColors[index].percentage = newPercentage;
                updatedColors[index + 1].percentage += newDiff

                setColors(updatedColors);
                mixTheColors(updatedColors)
                setActivePercentageToChange(updatedColors[activeIndex].percentage)
                colorsData = updatedColors
            }
        };

        const mouseUpHandler = () => {
            dragging = false
            setRefresh((prev) => !prev)
            document.removeEventListener('mousemove', mouseMoveHandler);
            document.removeEventListener('mouseup', mouseUpHandler);
        };

        document.addEventListener('mousemove', mouseMoveHandler);
        document.addEventListener('mouseup', mouseUpHandler);
    };


    function getContrastingColor(color) {
        const r = parseInt(color.slice(1, 3), 16);
        const g = parseInt(color.slice(3, 5), 16);
        const b = parseInt(color.slice(5, 7), 16);
        const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return luminance > 0.5 ? '#000' : '#FFF';
    }


    const copy = () => {
        const color = activeColor
        navigator.clipboard.writeText(color);
        color = color.toUpperCase()
        setShowMessage(`Copied ${color} color`)
        setTimeout(() => {
            setShowMessage(null)
        }, 1000);
    }

    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return "#000000"; // Return black if the color is not valid
        }
    }

    const validateColor = () => {
        const color = getColorOrDefault(activeColorToChange)
        setActiveColorToChange(color)
        setActiveColor(color)

        let updatedColor = colors.map((data, index) => {
            if (index === activeIndex) {
                data.color = color
            }

            return data
        })

        setColors(updatedColor)
        mixTheColors(updatedColor)
        colorsData = updatedColor
        colorPicker.setColors([color])
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            validateColor()
        }
    }

    const changeActiveColor = (value, index) => {
        setActiveColorToChange(value)
        setActiveColor(value)
        setActiveIndex(index)
        colorDataIndex = index
        colorPicker.setColors([value])
    }

    const addColor = () => {
        if (colors.length >= 5) return

        const newColor = random().toHex();
        const defaultPercentage = 25; // Default percentage for new color
        const newColors = [...colors, { color: newColor, percentage: defaultPercentage }];
        const totalPercentage = newColors.reduce((sum, color) => sum + color.percentage, 0);
        const scale = 100 / totalPercentage;

        const scaledColors = newColors.map(color => ({
            ...color,
            percentage: Math.trunc(color.percentage * scale)
        }));

        setColors(scaledColors);
        colorsData = scaledColors
        mixTheColors(scaledColors)
    };

    const removeColor = (index) => {
        if (colors.length <= 2) return

        const remainingColors = colors.filter((color, i) => i !== index);
        const totalPercentage = remainingColors.reduce((sum, color) => sum + color.percentage, 0);
        const scale = 100 / totalPercentage;

        const scaledColors = remainingColors.map(color => ({
            ...color,
            percentage: Math.trunc(color.percentage * scale)
        }));

        colorsData = scaledColors

        if (index === activeIndex) {
            setActiveColorToChange(scaledColors[0].color)
            setActiveColor(scaledColors[0].color)
            setActiveIndex(0)
            colorDataIndex = index
        }

        setColors(scaledColors);
        mixTheColors(scaledColors)
    };

    const getStyleOfBox = (data, index) => {
        if (index === 0) {
            return { borderTopLeftRadius: 5, borderBottomLeftRadius: 5, width: `${data.percentage}%`, backgroundColor: data.color }
        } if (index === colors.length - 1) {
            return { borderTopRightRadius: 5, borderBottomRightRadius: 5, width: `${data.percentage}%`, backgroundColor: data.color }
        } else {
            return { width: `${data.percentage}%`, backgroundColor: data.color }
        }
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    const mouseOnHandle = (e) => {
        e.stopPropagation()
        isMouseOnHandle = true
        setRefresh((prev) => !prev)
    }

    const mouseLeaveHandle = (e) => {
        e.stopPropagation()
        isMouseOnHandle = false
        setRefresh((prev) => !prev)
    }

    const handleActiveIndex = (e, index) => {
        setActiveHoverIndex(index)
    }

    const handleDeactiveIndex = (e) => {
        setActiveHoverIndex(null)
    }

    const getStyle = (data) => {
        if (copyHover) {
            return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.3)" : "rgba(255,255,255,0.3)" }
        } else {
            return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.1)" : "rgba(255,255,255,0.1)" }
        }
    }

    return (
        <div className={style.mixerContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head >

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.mixerContainer}>

                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>

                <div className={style.mixerGenerator}>
                    <div className={style.mixerWrapper}>

                        <div className={style.leftSide}>
                            <h2>Color Mixer
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="color-mixer" disabled selected>Color Mixer</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="gradient-generator">Gradient Generator</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div ref={colorPickerRef} style={{ marginTop: 30, width: '260px', height: '162px' }}></div>
                            <div className={style.row}>
                                <div className={style.inputWrapper}>
                                    <label>Color</label>
                                    <input type='text'
                                        placeholder='#000000'
                                        value={activeColorToChange}
                                        onFocus={(e) => e.target.select()}
                                        onChange={(e) => setActiveColorToChange(e.target.value)}
                                        onBlur={validateColor}
                                        onKeyDown={handleKeyDown} />
                                </div>
                                <div className={style.inputWrapper} style={{ display: 'none' }}>
                                    <label>Percentage</label>
                                    <input type='text'
                                        placeholder='20%'
                                        value={activePercentageToChange}
                                        onChange={(e) => setActivePercentageToChange(e.target.value)} />
                                </div>
                            </div>

                            <span className={style.info}>Powered by{" "}
                                <Link href="https://www.creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </span>
                        </div>

                        <div className={style.rightSide}>
                            <div className={style.wrapper}>
                                <div className={style.box} style={{ backgroundColor: mixedColor }}>
                                    <span onClick={copy} style={{ color: getContrastingColor(mixedColor) }}>{mixedColor}</span>
                                    <h3 style={{ color: getContrastingColor(mixedColor) }}>{colornamer(mixedColor, { pick: ['ntc'] }).ntc[0].name}</h3>
                                    <div onMouseEnter={() => setCopyHover(true)} onMouseLeave={() => setCopyHover(false)} onClick={copy} style={getStyle(mixedColor)}>
                                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.125 12.5H6C5.79289 12.5 5.625 12.3321 5.625 12.125V6C5.625 5.79289 5.79289 5.625 6 5.625H12.125C12.3321 5.625 12.5 5.79289 12.5 6V12.125C12.5 12.3321 12.3321 12.5 12.125 12.5Z" stroke={getContrastingColor(mixedColor)} strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M9.375 5.625V2.875C9.375 2.66789 9.20712 2.5 9 2.5H2.875C2.66789 2.5 2.5 2.66789 2.5 2.875V9C2.5 9.20712 2.66789 9.375 2.875 9.375H5.625" stroke={getContrastingColor(mixedColor)} strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>
                                </div>
                                <div className={style.colors}>
                                    <div className={style.cardWrapper} id='mixerWrapper'>
                                        {
                                            colors.map((data, index) => {
                                                return <div
                                                    key={data.color + index}
                                                    className={style.card}
                                                    style={getStyleOfBox(data, index)}
                                                    onMouseEnter={(e) => handleActiveIndex(e, index)}
                                                    onMouseLeave={(e) => handleDeactiveIndex(e)}
                                                    onClick={() => changeActiveColor(data.color, index)}>
                                                    {index < colors.length - 1 && (
                                                        <div onMouseEnter={mouseOnHandle} onMouseLeave={mouseLeaveHandle} className={style.handle} onMouseDown={(e) => handleDrag(index, e)} />
                                                    )}
                                                    <svg style={activeHoverIndex === index && !dragging && !isMouseOnHandle ? { display: 'flex' } : null} onClick={() => removeColor(index)} xmlns="http://www.w3.org/2000/svg" width="25" height="31" viewBox="0 0 25 31" fill="none">
                                                        <rect y="6" width="25" height="25" rx="3" fill="black" />
                                                        <path d="M12.2223 0.962171C12.6225 0.466996 13.3775 0.466996 13.7777 0.962171L17.7458 5.87138C18.2744 6.52532 17.8089 7.5 16.9681 7.5H9.03193C8.19108 7.5 7.72565 6.52532 8.25422 5.87138L12.2223 0.962171Z" fill="black" />
                                                        <path d="M9.375 24.125C9.03125 24.125 8.73698 24.0026 8.49219 23.7578C8.2474 23.513 8.125 23.2188 8.125 22.875V14.75H7.5V13.5H10.625V12.875H14.375V13.5H17.5V14.75H16.875V22.875C16.875 23.2188 16.7526 23.513 16.5078 23.7578C16.263 24.0026 15.9687 24.125 15.625 24.125H9.375ZM15.625 14.75H9.375V22.875H15.625V14.75ZM10.625 21.625H11.875V16H10.625V21.625ZM13.125 21.625H14.375V16H13.125V21.625Z" fill="white" />
                                                    </svg>

                                                    <span style={{ width: 5, height: 5, borderRadius: 10, backgroundColor: getContrastingColor(data.color), display: activeColor === data.color ? "flex" : "none" }} />
                                                    <div className={style.colorInfo}>
                                                        <span>{data.percentage}%</span>
                                                        <span>{data.color}</span>
                                                    </div>
                                                </div>
                                            })
                                        }

                                        <button style={colors.length === 5 ? { display: 'none' } : null} onClick={() => addColor()}>
                                            <svg width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.75 4.5H4.5M4.5 4.5H8.25M4.5 4.5V0.75M4.5 4.5V8.25" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <Link href="https://www.creatosaurus.io/apps/muse">
                                <a target="_blank">
                                    Design with this<span style={{ textTransform: 'uppercase' }}>&nbsp;{mixedColor}&nbsp;</span>color on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
            {showMessage ? <Toast message={showMessage} /> : null}
        </div>
    )
}

export default ColorMixer
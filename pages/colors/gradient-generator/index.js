import React, { useState, useEffect, useCallback } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router'
import { random } from "colord";
import style from './GradientGenerator.module.css';
import { ChromePicker } from 'react-color';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import tinycolor from "tinycolor2"
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';
import Head from 'next/head';
import { getSinglePostById } from '../../../lib/posts';

export async function getServerSideProps() {
    const id = "667ff549da8d0c062a1daf14"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const GradientGenerator = ({ post }) => {
    const router = useRouter()
    const [activeHandle, setActiveHandle] = useState(null);
    const [showColorPicker, setShowColorPicker] = useState(false)

    const [linearGradientAngle, setLinearGradientAngle] = useState(90)
    const [editLinearGradientAngle, setEditLinearGradientAngle] = useState(90)

    const [type, setType] = useState("linear")

    const [position, setPosition] = useState(0)

    const [activeFocus, setActiveFocus] = useState(null)
    const [activeIndex, setActiveIndex] = useState(0)
    const [activeEditColor, setActiveEditColor] = useState('#6AFFED')
    const [editColor, setEditColor] = useState('#6AFFED')

    const [handles, setHandles] = useState([
        { id: 1, left: 0, color: '#6AFFED' },
        { id: 2, left: 100, color: '#7F52FF' }
    ]);

    const onMouseMove = useCallback((e) => {
        if (activeHandle !== null) {
            const rect = document.getElementById('slider').getBoundingClientRect();
            let offsetX = e.clientX - rect.left;
            offsetX = Math.max(0, Math.min(offsetX, rect.width)); // Confine within slider

            const percent = parseInt((offsetX / rect.width) * 100);
            setHandles((prevHandles) =>
                prevHandles.map((handle) =>
                    handle.id === activeHandle.id ? { ...handle, left: percent } : handle
                )
            );

            setPosition(percent)
        }
    }, [activeHandle]);

    const onMouseUp = useCallback(() => {
        setActiveHandle(null);
    }, []);

    useEffect(() => {
        if (activeHandle !== null) {
            document.addEventListener('mousemove', onMouseMove);
            document.addEventListener('mouseup', onMouseUp);
        } else {
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup', onMouseUp);
        }

        return () => {
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup', onMouseUp);
        };
    }, [activeHandle, onMouseMove, onMouseUp]);

    const onMouseDown = (e, handle, index) => {
        e.stopPropagation();

        setActiveHandle(handle);
        setActiveIndex(index)
        setEditColor(handle.color)
        setActiveEditColor(handle.color)
        setPosition(handle.left)
    };

    const onSliderClick = (e) => {
        const rect = document.getElementById('slider').getBoundingClientRect();
        let offsetX = e.clientX - rect.left;
        offsetX = Math.max(0, Math.min(offsetX, rect.width)); // Confine within slider

        const percent = parseInt((offsetX / rect.width) * 100);
        const color = random().toHex()
        const newHandle = { id: Date.now(), left: percent, color: color };
        let handleData = [...handles, newHandle]
        setHandles(handleData);

        setActiveHandle(newHandle);
        setActiveIndex(handleData.length - 1)
        setEditColor(newHandle.color)
        setActiveEditColor(newHandle.color)
        setPosition(newHandle.left)
    };

    const createLinearGradient = () => {
        let gradientString

        // Ensure handles are sorted by left position for correct gradient order
        const sortedHandles = [...handles].sort((a, b) => {
            const leftA = parseInt(a.left);
            const leftB = parseInt(b.left);
            return leftA - leftB;
        });

        if (type === "linear") {
            // Construct the gradient string dynamically
            gradientString = `linear-gradient(${linearGradientAngle}deg`;
            for (const handle of sortedHandles) {
                gradientString += `, ${handle.color} ${handle.left}%`;
            }
            gradientString += ')';
        } else if (type === "radial") {
            // Construct the radial gradient string dynamically
            gradientString = `radial-gradient(circle`;
            for (const handle of sortedHandles) {
                gradientString += `, ${handle.color} ${handle.left}%`;
            }
            gradientString += ')';
        } else {
            // Assuming handles is an array of objects with color and left properties
            gradientString = 'conic-gradient(';
            for (const handle of sortedHandles) {
                const degree = Math.floor(handle.left * 3.6); // Convert percentage to degrees
                gradientString += `${handle.color} ${degree}deg, `;
            }
            gradientString = gradientString.slice(0, -2) + ')';
        }

        return gradientString;
    }

    const createLinearGradientSlider = () => {
        let gradientString

        // Ensure handles are sorted by left position for correct gradient order
        const sortedHandles = [...handles].sort((a, b) => {
            const leftA = parseInt(a.left);
            const leftB = parseInt(b.left);
            return leftA - leftB;
        });

        // Construct the gradient string dynamically
        gradientString = `linear-gradient(${linearGradientAngle}deg`;
        for (const handle of sortedHandles) {
            gradientString += `, ${handle.color} ${handle.left}%`;
        }

        return gradientString += ')';
    }


    const handleColorChange = (color) => {
        setActiveEditColor(color.hex)
        setEditColor(color.hex)
        let colorsData = handles.map((data, index) => {
            if (index === activeIndex) {
                data.color = color.hex
            }

            return data
        })

        setHandles(colorsData)
    }

    const removehandle = () => {
        let colorData = handles.filter((data, index) => activeIndex !== index)
        setHandles(colorData)
        setActiveEditColor(colorData[0].color)
        setEditColor(colorData[0].color)
    }

    const closeColorPicker = () => {
        setShowColorPicker(false)
    }

    const openColorPicker = (e) => {
        e.stopPropagation();
        setShowColorPicker(true)
    }

    const handlePositionChange = (e) => {
        const value = e.target.value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
        setPosition(value);
    };

    const updatePosition = () => {
        let number = parseInt(position)

        if (isNaN(number)) {
            setPosition(handles[activeIndex].left)
        } else {
            number = number > 100 ? 100 : number
            number = number < 0 ? 0 : number

            setPosition(number)
            const updatedPosition = handles.map((data, index) => {
                if (index === activeIndex) {
                    data.left = number
                }

                return data
            })

            setHandles(updatedPosition)
        }

        setActiveFocus(null)
    }

    const handlePositionKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.target.blur()
            updatePosition()
        }
    }

    const handleAngleChange = (e) => {
        const value = e.target.value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
        setEditLinearGradientAngle(value);
    }

    const updateRotate = () => {
        let number = parseInt(editLinearGradientAngle)

        if (isNaN(number)) {
            setEditLinearGradientAngle(linearGradientAngle)
        } else {
            number = number > 360 ? 360 : number
            number = number < -360 ? -360 : number
            setEditLinearGradientAngle(number)
            setLinearGradientAngle(number)
        }

        setActiveFocus(null)
    }

    const handleRotateKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.target.blur()
            updateRotate()
        }
    }

    function getColorOrDefault(inputColor) {
        const color = tinycolor(inputColor);
        if (color.isValid()) {
            return color.toHexString();
        } else {
            return editColor
        }
    }

    const handleBlur = () => {
        let data = getColorOrDefault(activeEditColor)
        setActiveEditColor(data)
        setEditColor(data)
    }

    const handleColorKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.target.blur()
            handleBlur()
        }
    }

    const goToPage = (e) => {
        router.push('/colors/' + e.target.value)
    }

    return (
        <div className={style.gradientContainerScroll} onClick={closeColorPicker}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head>

            <BlogNavigationBar title="Colors" slug="/" />
            <div className={style.gradientContainer}>

                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share />
                </div>


                <div className={style.gradientGenerator}>
                    <div className={style.gradientWrapper}>
                        <div className={style.leftSide}>
                            <h2>Gradient Generator
                                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>

                                <select
                                    style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                                    onChange={goToPage}>
                                    <option value="gradient-generator" disabled selected>Gradient Generator</option>
                                    <option value="contrast-checker">Contrast Checker</option>
                                    <option value="color-wheel">Color Wheel</option>
                                    <option value="color-mixer">Color Mixer</option>
                                    <option value="color-image-picker">Color Image Picker</option>
                                    <option value="color-blender">Color Blender</option>
                                    <option value="color-harmony">Color Harmony</option>
                                    <option value="color-picker">Color Picker</option>
                                    <option value="tint-shades">Tint Shades</option>
                                </select>
                            </h2>
                            <div id='slider' className={style.slider} onMouseDown={onSliderClick} style={{ background: createLinearGradientSlider() }}>
                                {handles.map((handle, index) => (
                                    <div
                                        key={handle.id}
                                        className={style.handle}
                                        style={index === activeIndex ? { left: handle.left + "%", border: '1.5px solid #0078FF' } : { left: handle.left + "%" }}
                                        onMouseDown={(e) => onMouseDown(e, handle, index)}>
                                        <div className={style.circle} style={{ backgroundColor: handle.color }} />
                                    </div>
                                ))}
                            </div>

                            <div className={style.row}>
                                <div className={style.wrapper}>
                                    <label>Color</label>
                                    <input style={{ textTransform: 'uppercase' }} type='text'
                                        placeholder='#DCDCDC'
                                        value={activeEditColor}
                                        onFocus={(e) => e.target.select()}
                                        onBlur={handleBlur}
                                        onKeyDown={handleColorKeyDown}
                                        onChange={(e) => setActiveEditColor(e.target.value)} />
                                    <div className={style.box} style={{ backgroundColor: editColor }} onClick={openColorPicker} />
                                    <div
                                        className={style.remove}
                                        onClick={removehandle}
                                        style={handles.length === 2 ? { display: 'none' } : null}>Remove</div>
                                </div>
                                <>
                                    {showColorPicker ? (
                                        <div className={style.colorPicker} onClick={(e) => e.stopPropagation()}>
                                            <ChromePicker color={editColor} onChange={handleColorChange} />
                                        </div>
                                    ) : null}
                                </>
                                <div className={style.wrapper}>
                                    <label>Position</label>
                                    <input
                                        placeholder='10%'
                                        type='text'
                                        onFocus={(e) => {
                                            setActiveFocus("position")
                                            setTimeout(() => {
                                                e.target.select()
                                            }, 30);
                                        }}
                                        onChange={handlePositionChange}
                                        onBlur={updatePosition}
                                        onKeyDown={handlePositionKeyDown}
                                        value={activeFocus === "position" ? position : position + "%"} />
                                </div>
                            </div>

                            <div className={style.row}>
                                <div className={style.wrapper}>
                                    <label>Rotation</label>
                                    <input
                                        type='text'
                                        placeholder='90°'
                                        onFocus={(e) => {
                                            setActiveFocus("rotate")
                                            setTimeout(() => {
                                                e.target.select()
                                            }, 30);
                                        }}
                                        onChange={handleAngleChange}
                                        onBlur={updateRotate}
                                        onKeyDown={handleRotateKeyDown}
                                        value={activeFocus === "rotate" ? editLinearGradientAngle : editLinearGradientAngle + "°"} />
                                </div>
                                <div className={style.wrapper}>
                                    <label>Type</label>
                                    <select value={type} onChange={(e) => setType(e.target.value)}>
                                        <option value="linear">Linear</option>
                                        <option value="radial">Radial</option>
                                        <option value="conic">Conic</option>
                                    </select>
                                </div>
                            </div>

                            <div className={style.buttons}>
                                <button>
                                    Random
                                    <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M18.3327 6.33301C15.8327 6.33301 11.2493 6.33301 9.58268 10.9163C7.91602 15.4997 4.16602 15.4997 1.66602 15.4997" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M16.666 4.66699C16.666 4.66699 17.6818 5.68278 18.3327 6.33366C17.6818 6.98453 16.666 8.00033 16.666 8.00033" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M18.3327 15.4997C15.8327 15.4997 11.2493 15.4997 9.58268 10.9163C7.91602 6.333 4.16602 6.33302 1.66602 6.33301" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M16.666 17.1663C16.666 17.1663 17.6818 16.1505 18.3327 15.4997C17.6818 14.8488 16.666 13.833 16.666 13.833" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </button>
                                <button style={{ display: 'none' }}>Export Gradient</button>
                                <button>Powered by {" "}
                                    <Link href="https://www.creatosaurus.io/">
                                        <a target="_blank">Creatosaurus.io</a>
                                    </Link>
                                </button>
                            </div>
                        </div>

                        <div className={style.rightSide}>
                            <div className={style.box} style={{ background: createLinearGradient() }} />
                            <Link href="https://muse.creatosaurus.io/">
                                <a target="_blank">
                                    Design with this gradient on Muse
                                    <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div style={{ marginTop: -50 }}>
                    <BlogSection post={post} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default GradientGenerator;

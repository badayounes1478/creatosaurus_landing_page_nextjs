import React from 'react';
import termsStyle from '../../styles/Tos.module.css';
import Navigation from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import Image from 'next/image';

const Tos = () => {
    const termsPage = () => {
        return (
            <div className={termsStyle.box}>
                <div className={termsStyle.container}>
                    <h1>Affiliate Terms</h1>
                    <h2>January 31, 2023</h2>
                    <p>
                        Welcome, and thank you for your interest in Creatosaurus
                        (“Creatosaurus,” “we,” or “us”) and our website at Creatosaurus.io,
                        along with our related websites, networks, applications, mobile
                        applications, and other services provided by us (collectively, the
                        “Service”). These Terms of Service are a legally binding contract
                        between you and Creatosaurus regarding your use of the Service.
                    </p>
                    <div className={termsStyle.points}>
                        <h1>Please read the following terms carefully</h1>
                        <p>
                            As an authorized affiliate (Affiliate) of Creatosaurus Pvt. Ltd. (Creatosaurus), you agree to abide by the terms and conditions contained in this Agreement (Agreement). Please read the entire Agreement carefully before registering and promoting Creatosaurus as an Affiliate.
                            When you join the Creatosaurus Affiliate Program, your role is to lawfully promote our website.
                            In return, you earn a commission on memberships and products bought by individuals who visit Creatosaurus through your website or personal referrals.
                            By registering for the Program, you confirm your agreement with the terms and conditions outlined in this Agreement.

                        </p>
                        <h1>1. Approval or Rejection of the Application</h1>
                        <p>
                            We retain the authority to either accept or decline ANY Affiliate Program Application at our sole and absolute discretion. You will not have any legal recourse against us in case your Affiliate Program Application is rejected.
                        </p>
                        <h1>2. Commissions</h1>
                        <p>
                            Commissions are issued on a monthly basis. In order for an Affiliate to receive a commission, the referred account must stay active for a minimum of 45 days.

                            Self-referrals are not permitted, and commissions will not be awarded for one&apos;s own accounts. Additionally, there is a restriction of one commission per referral. If a visitor clicks on your site&apos;s link and places orders for multiple accounts, the commission will apply solely to the initial order.

                            Payments will only be disbursed for transactions that have been successfully finalized. Transactions resulting in chargebacks or refunds will not be eligible for payment.

                        </p>
                        <h1>3. Termination</h1>
                        <p>
                            Your participation in the Program as an affiliate, as well as your current status, could face suspension or termination for any of the reasons listed below:
                        </p>
                        <p>1. Displaying inappropriate advertisements, such as false claims or misleading hyperlinks.</p>
                        <p>2. Engaging in spamming activities, including mass email or mass newsgroup posting.</p>
                        <p>3. Advertising on websites that promote or contain illegal activities.</p>
                        <p>4. Bidding on Creatosaurus&quot;s branded keywords, including but not limited to &quot;Creatosaurus hashtags&quot; &quot;Creatosaurus tech,&quot; &quot;Creatosaurus,&quot; and misspellings of these terms.</p>
                        <p>5. Conducting any Pay-Per-Click (PPC) activity that hasn&apos;t received prior approval via email from malav@creatosaurus.io.</p>
                        <p>6. Failing to disclose your affiliate relationship for promotions that qualify as endorsements under existing Commission guidelines, regulations, or relevant state laws.</p>
                        <p>7. Violating intellectual property rights; Creatosaurus reserves the right to request license agreements from those using Creatosaurus’s trademarks to safeguard our intellectual property rights.</p>
                        <p>8. Offering rebates, or other promised kickbacks from your affiliate commission as an incentive.</p>
                        <p>9. Engaging in self-referrals, fraudulent transactions, or suspected affiliate fraud.</p>
                        <p>In addition to the foregoing, Creatosaurus reserves the right to terminate any Affiliate account at any time, for any violations of this Agreement or no reason.</p>

                        <h1>4. Affiliate Links</h1>
                        <p>Feel free to utilize both graphic and text links on your website, social channels, and in your email messages.

                            You can employ the graphics and text we provide, or you have the option to create your own, ensuring they comply with the conditions and do not violate the guidelines outlined in Condition 3.
                        </p>

                        <h1>5. Coupon and Deal Sites</h1>
                        <p>Creatosaurus sometimes extends exclusive coupons to specific affiliates and our newsletter subscribers. If you haven&apos;t been pre-approved or assigned a branded coupon, you are not permitted to promote it. The following terms apply to any affiliate contemplating the promotion of our products in connection with a deal or coupon:</p>
                        <p>1. Affiliates are prohibited from using deceptive text on affiliate links, buttons, or images to suggest anything other than currently authorized deals for the specific affiliate.</p>
                        <p>2. Affiliates must refrain from generating pop-ups, pop-unders, iframes, frames, or any other visible or invisible actions that set affiliate cookies unless the user has explicitly shown interest in activating a specific saving by clicking on a clearly marked link, button, or image for that particular coupon or deal. The link should direct the visitor to the merchant site</p>
                        <p>3. The user must have access to coupon/deal/savings information and details before an affiliate cookie is set. For instance, using phrases like &quot;click here to see coupons and open a window to the merchant site&quot; is not permitted.</p>
                        <p>4. Affiliate sites are not allowed to display &quot;Click for (or to see) Deal/Coupon&quot; or any variation when no coupons or deals are available, and the click opens the merchant site or sets a cookie. Affiliates employing such text on the merchant landing page will be promptly removed from the program.</p>
                        <p>5. Promotion of Creatosaurus on coupon and loyalty websites is not allowed. When endorsing Creatosaurus, publishers should exclusively utilize coupons and promotional codes provided through the affiliate program. Affiliates can access all available coupon codes through the platform database. Any unauthorized use of exclusive coupon codes by affiliates will lead to invalidated transactions.</p>

                        <h1>6. Pay Per Click (PPC) Policy</h1>
                        <p>PPC bidding is NOT allowed without prior written permission.</p>

                        <h1>7. Liability</h1>
                        <p>
                            Creatosaurus holds no responsibility for indirect or incidental damages, such as the loss of revenue or commissions, arising from affiliate tracking failures, loss of database files, or any malicious actions directed towards the Program and/or our website(s).

                            We do not provide any expressed or implied warranties regarding the Program and/or the memberships or products offered by Creatosaurus. We do not guarantee that the operation of the Program and/or our website(s) will be free of errors, and we will not be held liable for any interruptions or errors that may occur.
                        </p>

                        <h1>8. Term of the Agreement</h1>
                        <p>This Agreement takes effect upon your acceptance into the Program and remains in force until your Affiliate account is terminated.

                            We reserve the right to modify the terms and conditions of this agreement at any time. If any changes are unacceptable to you, your sole option is to terminate your Affiliate account. Your ongoing participation in the Program will be considered as your acceptance of any such changes.
                        </p>

                        <h1>9. Indemnification</h1>
                        <p>The Affiliate agrees to indemnify and hold Creatosaurus, its affiliate and subsidiary companies, officers, directors, employees, licensees, successors, and assigns—those either licensed or authorized by Creatosaurus to transmit and distribute materials—harmless from all liabilities, damages, fines, judgments, claims, costs, losses, and expenses (including reasonable legal fees and costs). This indemnification is in connection with any and all claims sustained due to negligence, misrepresentation, failure to disclose, or intentional misconduct on the part of the Affiliate in relation to this Agreement.</p>

                        <h1>10. Governing Law, Jurisdiction, and Attorney Fees</h1>
                        <p>This Agreement will be governed by and interpreted in accordance with the laws of India. Any dispute arising under or related to this Agreement will be exclusively settled in the courts located in India.

                            If litigation is necessary to enforce any provision of this Agreement, the prevailing party will have the right to recover its costs and fees, including reasonable legal fees, from the other party.
                        </p>

                        <h1>11. Electronic Signatures Effective</h1>
                        <p>This Agreement constitutes an electronic contract that establishes the legally binding terms for your involvement in the Creatosaurus affiliate program. By completing the application process, you signify your acceptance of this Agreement and all the terms and conditions outlined or referred to within. This act generates an electronic signature, which holds the same legal validity and impact as a handwritten signature.</p>

                        <h1>12. Link Cloaking</h1>
                        <p>All links must be clearly visible and not concealed. We require visibility of the referring URL from the exact page of the last click. Cloaking links, utilizing redirects, or presenting one page to us and a different one to end-users is strictly prohibited.

                            If it is discovered that links are being cloaked, we reserve the right to reverse all commissions from cloaked links or where the referrer is unavailable, without prior notice. Violation of this policy may also result in removal from the program, and this action cannot be overridden by any prior, current, or future agreements.
                        </p>

                        <h1>13. No Automonetization Tools or Scripts</h1>
                        <p>We consider any interference with the ongoing shopping process akin to adware and strictly prohibit such actions for any reason. The conversion of direct links into affiliate links using JavaScript, toolbars, contextual software, etc., is not permitted, as it amounts to redirecting traffic that Creatosaurus would have naturally received.

                            There is one exception for new traffic and content generated directly by the efforts of tool or script providers. However, for this content to be deemed commissionable, it must receive written approval from the current head of marketing or e-commerce, not the affiliate manager. Verbal permissions will not be considered valid for approval.
                        </p>

                        <h1>14. Advertising Disclosures</h1>
                        <p>
                            Advertising disclosures must be consistently visible and appear on every webpage utilizing affiliate links. They must precede the visibility of any affiliate links and should not be placed in pop-up windows, drop-down menus, footers, the bottom of blog posts, or any location appearing after an affiliate link. Additionally, they should not require users to click to another page to view them.

                            Furthermore, it is mandatory to include advertising disclosures when promoting affiliate links on social media sites, email campaigns, or any other marketing materials. Failure to adhere to proper advertising disclosure practices may result in immediate removal from the SocialPilot affiliate program and potential reversal of commissions, irrespective of their origin.

                            In certain cases, commissions may be set to 0% with a warning issued until proper disclosures are implemented. As the affiliate, you also accept responsibility for any financial and legal consequences arising for SocialPilot or the affiliate management agency due to your failure to use or properly implement disclosures.
                        </p>

                        <h1>15. Trademark Usage Policy</h1>
                        <p>The utilization of our trademarks, URLs, misspellings, extensions, or any variations to optimize for them is strictly prohibited. This includes the restriction of incorporating the Creatosaurus brand name, including any misspellings, in the domain name of your website.</p>
                        <p>Guidelines to adhere to:</p>
                        <p>1. Search Engine Marketing (SEM): Running any form of advertisement on any Ad network to attract customers is not permitted. Any rewards obtained through such advertisements will be promptly declined, and our partnership may be terminated.</p>
                        <p>2. Social Media: The use of the Creatosaurus name or any brand variation as hashtags, pretending to be us, representing us, or falsely claiming endorsement or sponsorship by Creatosaurus is prohibited. Creating accounts using our name, posting messages, including our materials or links, or tagging, calling out, or linking to our social profiles is also not allowed.</p>
                        <p>3. SEO: The inclusion of the Creatosaurus brand name, including any misspellings, in your website&apos;s domain name is prohibited. Upon identification of such domains, immediate takedown is required. Failure to comply may result in removal from the program, and all commissions, regardless of origin, will be reversed.</p>

                        <h1>16. Additional Guidelines:</h1>
                        <p>1. Self Referral: Shopping through your own affiliate links to purchase an account, even for the purpose of reviewing Creatosaurus, is not allowed. Referring employers, employees, or co-workers is also prohibited. Commissions earned on self-referrals, including those for employers, employees, and co-workers, will be invalidated.</p>
                        <p>2.
                            Content Restrictions: Creatosaurus promotion is not permitted on gambling websites, adult/hate/violent/defamatory content platforms, or any site featuring offensive or inappropriate material. Additionally, promoting Creatosaurus on websites violating third-party rights or applicable laws is prohibited.
                        </p>
                        <p>3. Sanctioned Countries: Participants in the program must confirm that they are not residents of countries currently under sanction by the Office of Foreign Assets Control (OFAC), including Crimea – Region of Ukraine, Iraq, North Korea, Iran, Syria, and Cuba. Please note that the list of sanctioned countries can change at any time; however, a current list can always be located at https://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx. Compliance with applicable OFAC restrictions is mandatory, and engagement with businesses or persons in sanctioned countries is not allowed.</p>
                        <p>4. Age Verification: Individual participants must confirm that they were at least 18 years old on the effective date of this agreement.</p>
                        <p>5. Commissions Reversal and Program Acceptance: We retain the right to reverse commissions without warning and remove any partner at any time for any reason. This agreement supersedes all previous, current, and future agreements, and there are no guaranteed commissions, payouts, or commission levels. Custom offers may be revoked with or without warning.</p>
                        <p>6. When endorsing regulated products such as nicotine, alcohol, or tobacco, which are subject to age restrictions, you must adhere to all legal requirements and include appropriate disclaimers. Failure to comply will lead to instant expulsion from the program without prior notice, and all commissions will be revoked. The disclaimers should align with the regulations of the United States of America, the European Union, and any other country where your audience may encounter the affiliate links and advertisements.</p>

                        <h1>17. TOS Changes</h1>
                        <p>It is your responsibility to check our TOS as we will update them on occasion, and we may not provide the dates when they will change or the details about the changes.</p>
                        <h1>18. Contact</h1>
                        <p>
                            Please contact us at contact@creatosaurus.io with any questions
                            regarding this Agreement.
                        </p>
                    </div>
                </div>
            </div>
        );
    };

    const blackBox = () => {
        return (
            <div className={termsStyle.blackBoxContainer}>
                <div className={termsStyle.blackBox}>
                    <div className={termsStyle.taglines}>
                        <h1>You focus on telling stories,</h1>
                        <h1> we do everything else.</h1>
                        <button>Signup for Free</button>
                    </div>
                    <div className={termsStyle.blackBoxImage}>
                        <Image
                            src='/Assets/Girl4.png'
                            width={448}
                            height={399}
                            alt='Failed to load image'
                        />
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div>
            <Navigation />
            {termsPage()}
            {blackBox()}
            <Footer />
        </div>
    );
};

export default Tos;

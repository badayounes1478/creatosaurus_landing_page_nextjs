import React from 'react'
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar'
import HeroSection from '../../ComponentsNew/AffiliateComponents/HeroSection/HeroSection'
import ReviewSection from '../../ComponentsNew/AffiliateComponents/ReviewSection/ReviewSection'
import PartnerSection from '../../ComponentsNew/AffiliateComponents/PartnerSection/PartnerSection'
import AboutCreatosaurusSection from '../../ComponentsNew/AffiliateComponents/AboutCreatosaurusSection/AboutCreatosaurusSection'
import RevenueSection from '../../ComponentsNew/AffiliateComponents/RevenueSection/RevenueSection'
import FaqSection from '../../ComponentsNew/AffiliateComponents/FAQSection/FaqSection'
import BlackSection from '../../ComponentsNew/AffiliateComponents/BlackSection/BlackSection'
import Footer from '../../Components/LandingPageComponents/Footer'
import WorkSection from '../../ComponentsNew/AffiliateComponents/WorkSection/WorkSection'
import AffiliateStatisticsSection from '../../ComponentsNew/AffiliateComponents/AffiliateStatisticsSection/AffiliateStatisticsSection'

const index = () => {
    return (
        <div style={{ maxWidth: 1600, margin: 'auto' }}>
            <NavigationBar />
            <HeroSection />
            <ReviewSection />
            <PartnerSection />
            <AffiliateStatisticsSection />
            <WorkSection />
            <AboutCreatosaurusSection />
            <RevenueSection />
            <FaqSection />
            <BlackSection />
            <Footer />
        </div>
    )
}

export default index
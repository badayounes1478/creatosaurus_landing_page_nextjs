import React from 'react'
import Script from 'next/script';

const Plausible = () => {
    return <Script
        id='p1'
        strategy='lazyOnload'
        src={`https://plausible.creatosaurus.io/js/script.js`}
        data-domain="creatosaurus.io"
    />
}
export default Plausible
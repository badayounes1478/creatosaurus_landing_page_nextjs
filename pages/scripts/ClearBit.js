import React from 'react'
import Script from 'next/script';

const ClearBit = () => {
    return <Script id='cb' strategy='lazyOnload'
        src={`https://tag.clearbitscripts.com/v1/pk_23f6cb20ad240729cbee56db0778b1f7/tags.js`}
        referrerPolicy='strict-origin-when-cross-origin'
    />
}
export default ClearBit
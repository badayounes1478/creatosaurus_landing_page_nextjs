import React from 'react'
import Script from 'next/script';

const Tawkto = () => {
    return (
        <Script
            id='tw'
            strategy='lazyOnload'>
            {`
          var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
          (function(){
          var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
          s1.async=true;
          s1.src='https://embed.tawk.to/615fde58157d100a41ab5e5d/1fhf7p2ht';
          s1.charset='UTF-8';
          s1.setAttribute('crossorigin','*');
          s0.parentNode.insertBefore(s1,s0);
          })();
    `}
        </Script>
    );
}

export default Tawkto
import Script from 'next/script';

export default function TrackingComponent() {
  return (
    <>
      {/* Initialize faitracker */}
      <Script id="faitracker-init" strategy="lazyOnload">
        {`
            window.faitracker = window.faitracker || (function () {
              this.q = [];
              const event = new CustomEvent("FAITRACKER_QUEUED_EVENT");
              
              // Initialization function
              this.init = function(token, params, callback) {
                this.TOKEN = token;
                this.INIT_PARAMS = params;
                this.INIT_CALLBACK = callback;
                window.dispatchEvent(new CustomEvent("FAITRACKER_INIT_EVENT"));
              };
              
              // Call function to queue events
              this.call = function () {
                const payload = { k: "", a: [] };
                if (arguments && arguments.length >= 1) {
                  for (let i = 1; i < arguments.length; i++) {
                    payload.a.push(arguments[i]);
                  }
                  payload.k = arguments[0];
                }
                this.q.push(payload);
                window.dispatchEvent(event);
              };
  
              // Message function to handle postMessage events
              this.message = function () {
                window.addEventListener("message", function (messageEvent) {
                  if (messageEvent.data.origin === "faitracker") {
                    this.call("message", messageEvent.data.type, messageEvent.data.message);
                  }
                });
              };
              
              this.message();
              this.init(
                "ey2jp87drmeo67raimigyp1bij3ogzax",
                { host: "https://api.factors.ai" }
              );
              
              return this;
            })();
          `}
      </Script>

      {/* Load external script */}
      <Script
        src="https://app.factors.ai/assets/factors.js"
        strategy="lazyOnload"
      />
    </>
  );
}

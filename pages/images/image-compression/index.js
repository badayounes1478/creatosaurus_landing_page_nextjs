import React, { useCallback, useState } from 'react'
import style from './ImageCompression.module.css'
import Link from 'next/link';
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import { useDropzone } from 'react-dropzone'

const ColorWheel = () => {
  const [url, setUrl] = useState("")
  const [mimeTypeToConvert, setMimeTypeToConvert] = useState("image/jpeg")
  const [loading, setLoading] = useState(false)

  const onDrop = useCallback(acceptedFiles => {
    const imageFile = acceptedFiles[0]
    const reader = new FileReader();
    setLoading(true)

    reader.onload = function () {
      const fileName = imageFile.name.split(".")[0]
      convertImage(reader.result, fileName);
    };

    reader.readAsDataURL(imageFile);
  }, [])

  const convertImage = (dataUrl, fileName, mimeType) => {
    mimeType = mimeType ? mimeType : mimeTypeToConvert
    const img = new Image();
    img.src = dataUrl;

    img.onload = () => {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);

      const convertedDataUrl = canvas.toDataURL(mimeType);
      const size = calculateImageSize(convertedDataUrl);
      setLoading(false)

      setUrl({
        url: convertedDataUrl,
        size: size,
        fileName
      })
    };
  };


  const calculateImageSize = (dataUrl) => {
    const base64String = dataUrl.split(',')[1];
    const decodedString = atob(base64String);
    return decodedString.length;
  };

  const formatSize = (sizeInBytes) => {
    if (sizeInBytes >= 1048576) {
      return (sizeInBytes / 1048576).toFixed(2) + ' MB';
    } else if (sizeInBytes >= 1024) {
      return (sizeInBytes / 1024).toFixed(2) + ' KB';
    } else {
      return sizeInBytes + ' bytes';
    }
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop, noKeyboard: true })

  const convertToTheFollowingFormat = (e) => {
    let value = e.target.value
    setLoading(true)
    setUrl("")
    setMimeTypeToConvert(value)
    convertImage(url.url, url.fileName, value)
  }

  const getType = () => {
    return mimeTypeToConvert === "image/jpeg" ? "jpeg" : mimeTypeToConvert === "image/png" ? "png" : "webp"
  }

  const downloadImage = () => {
    let type = getType()
    const a = document.createElement("a");
    a.href = url.url
    a.download = `${url.fileName}.${type}`
    a.click()
  }

  const clear = () => {
    setUrl("")
  }

  return (
    <div className={style.wheelContainerScroll}>
      <BlogNavigationBar title="Images" slug="/" />
      <div className={style.wheelContainer}>
        <div className={style.head}>
          <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
          </svg>

          <h1>Image Color Picker</h1>
          <p>Use the online image color picker above to select a color and get the HTML Color Code of this pixel. Also you get the HEX color code value, RGB value and HSV value.</p>
          <div className={style.divider} />

          <Share />
        </div>

        <div className={style.wheelGenerator}>
          <div className={style.contrastWrapper}>
            <div className={style.leftSide}>
              <h2>PNG to JPG Converter</h2>

              <div className={style.row}>
                <label>Convert from</label>
                <select>
                  <option value="Complementary">PNG</option>
                  <option value="Monochromatic">JPEG</option>
                  <option value="Analogous">JPG</option>
                  <option value="Analogous">SVG</option>
                  <option value="Analogous">WEBP</option>
                </select>
              </div>


              <div className={style.row}>
                <label>Convert to</label>
                <select value={mimeTypeToConvert} onChange={convertToTheFollowingFormat}>
                  <option value="image/jpeg">JPEG</option>
                  <option value="image/png">PNG</option>
                  <option value="image/webp">WEBP</option>
                </select>
              </div>

              <span className={style.poweredBy}>
                Powered by {" "}
                <Link href="https://creatosaurus.io/">
                  <a target="_blank">Creatosaurus.io</a>
                </Link>
              </span>
            </div>

            <div className={style.rightSide}>
              {
                url === "" ? <div className={style.colorBox} {...getRootProps()}>
                  <input {...getInputProps()} />
                  {
                    loading ? <span className={style.loader} />
                      :
                      <>
                        <svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <g clipPath="url(#clip0_13662_1262)">
                            <path d="M15.5 16.25L11.125 20.625M15.5 27.5V16.25V27.5ZM15.5 16.25L19.875 20.625L15.5 16.25Z" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M25.5 22.0091C27.3671 21.2776 29.25 19.6111 29.25 16.25C29.25 11.25 25.0834 10 23 10C23 7.5 23 2.5 15.5 2.5C8 2.5 8 7.5 8 10C5.91666 10 1.75 11.25 1.75 16.25C1.75 19.6111 3.63285 21.2776 5.5 22.0091" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                          </g>
                          <defs>
                            <clipPath id="clip0_13662_1262">
                              <rect width="30" height="30" fill="white" transform="translate(0.5)" />
                            </clipPath>
                          </defs>
                        </svg>
                        <p>Drag & drop your file anywhere on this page</p>
                        <button>Upload your image</button>
                      </>
                  }
                </div> : <div className={style.imageBox}>
                  <div className={style.wrapper}>
                    <img src={url?.url} alt='' />
                    <div>
                      <span>
                        <svg style={{ marginRight: 5 }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="8.10742" cy="8" r="7.5" fill="#216F5C" />
                          <path d="M5.04492 8.4375L6.79492 10.1875L11.1699 5.8125" stroke="#00ECC2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        Converted to {mimeTypeToConvert === "image/jpeg" ? "JPEG" : mimeTypeToConvert === "image/png" ? "PNG" : "WEBP"}
                      </span>
                      <span>{url?.fileName}.{getType()}</span>
                      <span>{formatSize(url?.size)}</span>
                    </div>
                  </div>

                  <div className={style.links}>
                    <span onClick={downloadImage}>
                      Download
                      <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                      </svg>
                    </span>

                    <span onClick={clear}>
                      Reset, upload another file
                      <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                      </svg>
                    </span>

                    <Link href="https://creatosaurus.io/">
                      <a target="_blank">
                        Design using converted file on Creatosaurus
                        <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M1.75 7.49935H12.25M12.25 7.49935L7.29167 2.54102M12.25 7.49935L7.29167 12.4577" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                      </a>
                    </Link>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>

      </div>
      <Footer />
    </div>
  )
}

export default ColorWheel
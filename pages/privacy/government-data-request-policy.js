import React from 'react';
import privacyStyle from '../..//styles/privacy.module.css';
import Navigation from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import Image from 'next/image';

const GovernmentDataRequestPolicy = () => {
    const privacyPage = () => {
        return (
            <div className={privacyStyle.box}>
                <div className={privacyStyle.container}>
                    <h1>Government Data Request Policy</h1>
                    <h2>January 1, 2024</h2>
                    <p>
                        This Government Data Request Policy describes how we will respond to requests from law enforcement agencies or government authorities (collectively, “Government Authority”).

                        Creatosaurus is committed to protecting the privacy of personal data under its custody and control. We will not disclose our customers’ personal data to any Government Authority unless we are instructed by the customer to do so, or as described in this Policy.

                        If we do receive an order by a Government Authority to disclose personal data relating to a particular customer, we will take the following steps:
                    </p>
                    <div className={privacyStyle.points}>
                        <h1>1. Redirection of the request</h1>
                        <p>
                            We will first inform the Government Authority that we are not the original source of social media data. The original source of the social media data is managed and stored by the social networks. Creatosaurus does not permanently store social media data received from social networks, except in very limited cases, as Creatosaurus relies on real time APIs to retrieve the data which enables this information to be displayed in the services, and therefore, we are not the appropriate entity to receive these requests.

                            We will then attempt to redirect the request to the relevant customer so the customer may directly respond to the request. We may need to provide the customer’s basic contact information to the Government Authority for these purposes.
                        </p>
                        <h1>2. Notification</h1>
                        <p>In the event that redirection is not possible, we would attempt to notify the customer unless we are legally prohibited from doing so. This would provide the customer with an opportunity to manage the request and seek a protective order or other appropriate remedy.</p>

                        <h1>3. Reviewing the legality and enforceability of the request</h1>
                        <p>Where we are compelled to respond, we would review the request to assess whether it is lawful and enforceable in the jurisdiction of the Creatosaurus entity or Affiliate that controls the records that are the subject of the request, and determine if the request may need to be challenged. If the request is not lawful and enforceable, we will notify the Government Authority that we are unable to disclose any data.</p>

                        <h1>4. Incompatibility with GDPR requirements</h1>
                        <p>If Creatosaurus is legally prohibited from notifying the customer of the request, and we have reason to believe that the request is incompatible with European data protection requirements and conflicts with our existing contractual commitments to our customers, we will notify the customer of our inability to comply with European Data Protection Laws, and the customer will have the right to immediately suspend any further processing of personal data and/or terminate its agreement with Creatosaurus.</p>

                        <h1>5. Minimizing the data provided</h1>
                        <p>If it is determined that Creatosaurus is required to provide information in response to a valid and legally binding request and/or order from a Government Authority, only the minimum information will be provided to the extent legally permissible.</p>

                        <h1>6. Information Request - Statistics</h1>
                        <br />
                        <span>Government Authority Statistics</span>
                        <p>The table below sets out the number of Government Authority information requests that Creatosaurus receives annually.</p>
                        <div className={privacyStyle.tableContainer}>
                            <table>
                                <thead>
                                    <tr>
                                        <th className={privacyStyle.headingData}>Year</th>
                                        <th className={privacyStyle.headingData}>Number of Requests Received</th>
                                        <th className={privacyStyle.headingData}>Number of Times Data Disclosed</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className={privacyStyle.headingData}>2023</td>
                                        <td className={privacyStyle.headingData}>0</td>
                                        <td className={privacyStyle.headingData}>0</td>
                                    </tr>
                                    <tr>
                                        <td className={privacyStyle.headingData}>2022</td>
                                        <td className={privacyStyle.headingData}>0</td>
                                        <td className={privacyStyle.headingData}>0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>United States National Security Requests -
                            Creatosaurus has not received any United States national security requests, which include U.S. National Security Letters (“NSLs”) and requests issued under the Foreign Intelligence Surveillance Act (“FISA”).</p>
                    </div>
                </div>
            </div>
        );
    };

    const blackBox = () => {
        return (
            <div className={privacyStyle.blackBoxContainer}>
                <div className={privacyStyle.blackBox}>
                    <div className={privacyStyle.taglines}>
                        <h1>You focus on telling stories,</h1>
                        <h1> we do everything else.</h1>
                        <button>Signup for Free</button>
                    </div>
                    <div className={privacyStyle.blackBoxImage}>
                        <Image
                            src='/Assets/Girl4.png'
                            width={448}
                            height={399}
                            alt='Failed to load image'
                        />
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div>
            <Navigation />
            {privacyPage()}
            {blackBox()}
            <Footer />
        </div>
    );
};

export default GovernmentDataRequestPolicy;

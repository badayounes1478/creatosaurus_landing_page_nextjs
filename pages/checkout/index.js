import React, { useState, useEffect } from 'react'
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar'
import PlanPeriod from '../../ComponentsNew/CheckoutComponents/planPeriod/PlanPeriod'
import PlanDetails from '../../ComponentsNew/CheckoutComponents/planDetails/PlanDetails'
import SelectPayment from '../../ComponentsNew/CheckoutComponents/selectPayment/SelectPayment'
import { useRouter } from 'next/router'
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth'
import jwtDecode from 'jwt-decode'

const Checkout = () => {
  const router = useRouter()
  const { query } = router

  const [planName, setPlanName] = useState("creator")
  const [planDuration, setPlanDuration] = useState("yearly")
  const [showIndianPrice, setShowIndianPrice] = useState(false)
  const [showUserLoggedIn, setShowUserLoggedIn] = useState(true)
  const [invitationCode, setInvitationCode] = useState(null)
  const [isBlakFridayDeal, setIsBlakFridayDeal] = useState(false)
  const [userDetails, setUserDetails] = useState(null)

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }


  useEffect(() => {
    if (query.plan !== undefined) {
      setPlanName(query.plan)
    }

    if (query.deal === "blackFriday") {
      setIsBlakFridayDeal(true)
    }

    if (query.ref !== undefined) {
      setInvitationCode(query.ref)
    } else {
      const value = getCookie('ref');
      if (value !== undefined && value !== null) {
        setInvitationCode(value)
      }
    }

    if (Intl.DateTimeFormat().resolvedOptions().timeZone === "Asia/Calcutta") {
      setShowIndianPrice(true)
    }

    const value = getCookie('Xh7ERL0G');
    if (value) {
      const decoded = jwtDecode(value);
      setUserDetails(decoded)
      const expirationTime = (decoded.exp * 1000) - 60000
      if (Date.now() >= expirationTime) {
        setShowUserLoggedIn(false)
      }
    } else {
      setShowUserLoggedIn(false)
    }
  }, [query.plan, query.ref])

  const changePlanDuration = (value) => {
    setPlanDuration(value)
  }

  const changeUserLoggedIn = () => {
    setShowUserLoggedIn(true)
  }

  return (
    <div style={{ backgroundColor: '#F8F8F9' }}>
      {showUserLoggedIn ? null : <NavigationAuth changeUserLoggedIn={changeUserLoggedIn} />}
      <NavigationBar userLoggedIn={showUserLoggedIn} />
      <PlanDetails planName={planName} showIndianPrice={showIndianPrice} />
      <PlanPeriod planDuration={planDuration} changePlanDuration={changePlanDuration} planName={planName} showIndianPrice={showIndianPrice} isBlakFridayDeal={isBlakFridayDeal} />
      <SelectPayment userDetails={userDetails} invitationCode={invitationCode} planDuration={planDuration} planName={planName} showIndianPrice={showIndianPrice} isBlakFridayDeal={isBlakFridayDeal} />
    </div>
  )
}

export default Checkout
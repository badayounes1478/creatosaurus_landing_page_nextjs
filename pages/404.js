import React from 'react'
import NotFoundNavigationBar from '../Components/NotFoundNavigationBar'
import Link from 'next/link'

const Index = () => {
    return (
        <div>
            <NotFoundNavigationBar />

            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 'calc(100vh - 60px)' }}>
                <span style={{ color: '#000', fontSize: 32, fontWeight: 600 }}>Oops, page not found!</span>
                <Link href="/">
                    <button style={{ marginTop: 20, width: 180, height: 40, border: 'none', fontSize: 18, borderRadius: 5, backgroundColor: '#FF4359', color: '#fff', cursor: 'pointer' }}>Go To Home</button>
                </Link>
            </div>
        </div>
    )
}

export default Index
import React, { useState } from 'react'
import Footer from '../Components/LandingPageComponents/Footer';
import NavigationBar from '../Components/LandingPageComponents/NavigationBar';
import styles from '../styles/About.module.css';
import Head from 'next/head';
import NavigationAuth from '../Components/NavigationAuth/NavigationAuth';
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo';

const About = () => {
  const [showLogin, setshowLogin] = useState(false)

  return (
    <React.Fragment>
      {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
      <HeadInfo />
      <NavigationBar />
      <div className={styles.aboutContainer}>
        <div className={styles.headContainer}>
          <h1>Empowering creators. <br />  Democratizing storytelling.</h1>
          <h6>Creatosaurus is one true home for creators.</h6>
          <button onClick={() => setshowLogin(true)}>Get Started-It&apos;s Free</button>
        </div>

        <div className={styles.info}>
          {/*
          <p>We at Creatosaurus are committed to delivering Innovation and Convenience to its users. We build cool products for creators like you to make life better.</p>
          <p>Creatosaurus simplifies the workflow of creators & teams with an all-in-one creator stack to tell stories at scale. Curate ideas, design graphics, edit videos, schedule posts, manage your social media accounts, generate copy, and more to streamline & optimize your workflow across teams in real-time with 15+ integrations. Creatosaurus is one true home for creators.</p>
          <p>Driven by our passion for user happiness, we have a Happiness Equation that&apos;s drilled into our very core. What does this mean for you? Well, we&apos;ll look after you and go to extreme lengths to make sure you&apos;re satisfied. We started in 2018 and it is our mission to only give you the best product.</p>
          <h5>WHERE ARE WE LOCATED?</h5>
          <p>Creatosaurus, 12 - Gangai Hills Building C Survey No 8/4 Dabhadi Road, Mansoba Mandir, Ambegaon Bk, Ambegaon Pune, Maharashtra, 411046</p>
          <h5>A GUARANTEE</h5>
          <p>We&apos;re so confident you&apos;ll love our products - we offer a 100% satisfaction guarantee. Don&apos;t like something you bought? Tell us, and we&apos;ll be happy to solve your issue.</p>
          <h5>HAVE ANY QUESTIONS?</h5>
          <p>Contact us now using our contact page or email us at contact@creatosaurus.io or directly call us at +91-9665047289. Happy creating!</p>
         */}
          <p>Hello Creators 🙌🏻</p>
          <p>We are thrilled to introduce you to Creatosaurus - the only social media marketing tool you&apos;ll ever need.</p>
          <p>We are proud to say that we have created an all-in-one platform that will revolutionize the way creators, marketers, and businesses tell their stories.</p>
          <h5>Creatosaurus - The Story 🦖</h5>
          <p>As creative individuals, we were frustrated with juggling multiple marketing tools, stressed with the process of team collaboration and let’s not even talk about the overwhelming chaos of scaling marketing strategies.</p>
          <p>We are in the midst of a new creator age, where chaos can be replaced with free time, creative work, and stories.</p>
          <p>That&apos;s why we&apos;ve created Creatosaurus - a platform that lets you easily curate ideas, design graphics, write AI content, schedule posts, search hashtags, craft articles, apps and more - all in one place.</p>
          <p>As makers, creators, movers, and shakers, we are in this together. Our goal is to streamline & optimize your entire social media marketing process, so you can focus on telling your stories.</p>
          <h5>With Creatosaurus, you can 🤘🏻</h5>
          <div style={{ display: "flex", flexDirection: 'column' }}>
            <span style={{ marginTop: 5 }}>💡 &nbsp;Curate ideas and inspiration from a variety of quotes and phrases</span>
            <span style={{ marginTop: 5 }}>🎨 &nbsp;Design stunning graphics with our drag-and-drop Pro design editor & templates</span>
            <span style={{ marginTop: 5 }}>🔍 &nbsp;Perfect hashtags for your content with our hashtag search & analytics tool</span>
            <span style={{ marginTop: 5 }}>✍🏻 &nbsp;Generate AI content for your social media and content marketing</span>
            <span style={{ marginTop: 5 }}>📅 &nbsp;Schedule and manage your social media posts all in one place</span>
            <span style={{ marginTop: 5 }}>📝 &nbsp;Write articles & documents on the go with an AI-powered, fast text editor</span>
            <span style={{ marginTop: 5 }}>🎥 &nbsp;Edit high-quality videos with our powerful online video editor</span>
            <span style={{ marginTop: 5 }}>📊 &nbsp;Generate analytics report for your social media and grow your brand</span>
            <span style={{ marginTop: 5 }}>🧩 &nbsp;Add media, like shapes, images, and vector art with our free stock asset library</span>
            <span style={{ marginTop: 5 }}>🫂 &nbsp;Collaborate with your team members across many workspaces</span>
            <span style={{ marginTop: 5 }}>🎁 &nbsp;Seamlessly work with 20+ 3rd party apps like Unsplash, Slack, Drive and more</span>
            <span style={{ marginTop: 5 }}>🎯 &nbsp;And save yourself endless amounts of time, resources, money, and frustration</span>
          </div>
          <p style={{ marginTop: 10 }}>Plus we have an exciting roadmap ahead.</p>
          <h5>Quick Links 🔗</h5>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://creatosaurus.tawk.help/'>Knowledge Base</a>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://creatosaurus.canny.io/'>Roadmap</a>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://youtu.be/eS5tpAUEuzA'>Video</a>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://www.facebook.com/groups/creatosaurus/'>Facebook Community Group</a>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://www.creatosaurus.io/'>Get started for Free</a>
            <a style={{ color: '#ff3950', marginTop: 5 }} href='https://www.creatosaurus.io/contact'>Contact Us</a>
          </div>
          <h5 style={{ marginTop: 10 }}>Live Q&A 🙋🏻</h5>
          <p>We meet bi-weekly to cover new updates & community questions. Join the Facebook group to learn more. ;)</p>
          <h5>New Era for Creators of Tomorrow! </h5>
          <p>Our platform is built for companies with distributed teams, global creative individuals and creators of tomorrow - those focused on content, community, culture, and commerce.</p>
          <p>At Creatosaurus, we believe that the future of content creation is all about storytelling. That&apos;s why we&apos;ve created a platform that does everything, so you can focus on what you do the best.</p>
          <p>This is a place where imaginative minds come to thrive. We believe in the power of creators, the ones who dare to dream big and relentlessly pursue their goals. In your pursuit, we don&apos;t just see dreamers, but pioneers. We are dedicated to empowering creators like you to bring your stories to life and push the boundaries of what&apos;s possible.</p>
          <p>So, join us on our mission to bring more stories to life. Creatosaurus is the only tool you&apos;ll ever need to create cutting-edge, high-quality stories that will engage and delight your audience.</p>
          <p>Don&apos;t blend in, tell your story.</p>
          <div className={styles.footer}>
            <span>Best,</span>
            <span>The Creatosaurus Team</span>
            <span>Made with ❤️ for Creators</span>
            <span>#StoriesMatter</span>
          </div>
        </div>

        {
          /**
           * <div className={styles.decorationContainer}>
          <h4 className={styles.decoration}>We are in the midst  of a new creator age.</h4>
          <h4>Imagine a world where chaos is replaced with free time, creative work and stories.</h4>
          <h4 className={styles.decoration}>Creatosaurus is a platform for storytelling.</h4>
          <h4>Built for companies with distributed teams  and global creative individuals.</h4>
          <h4 className={styles.decoration}>As makers, creators, movers and shakers,  we are in this together.</h4>
          <h4>We have been building  something new for creators like us.</h4>
          <h4 className={styles.decoration}>All in one creator stack.  That&apos;s right, we are making it.</h4>
          <h4>While it&apos;s a crazy idea and  has never been tried before,  we are up for the challenge.</h4>
        </div>
           */
        }

        <div className={styles.blackBox}>
          <h4>You focus on telling stories, we do everything else.</h4>
          <a href="https://www.app.creatosaurus.io/signup">
            <button>Signup for Free</button>
          </a>
        </div>

        <Footer />
      </div>
    </React.Fragment>
  )
}

export default About
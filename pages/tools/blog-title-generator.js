import React, { useState } from 'react'
import dynamic from 'next/dynamic'
import style from '../../styles/TitleGenerator.module.css'
import BlogNavigationBar from '../../Components/BlogNavigationBar'
import Link from 'next/link'
import axios from 'axios'
import { toast } from 'react-toastify'
import { getSinglePostById } from '../../lib/posts'
import Head from 'next/head';
import Share from '../../ComponentsNew/QuoteComponents/Share'
import Image from 'next/image';
import constant from '../../constant'

const Footer = dynamic(() => import('../../Components/LandingPageComponents/Footer'), {
    loading: () => <p>Loading...</p>
})

const ArticleByCard = dynamic(() => import('../../Components/ArticleByCard'), {
    loading: () => <p>Loading...</p>
})

const BlogSection = dynamic(() => import('../../ComponentsNew/ToolsComponents/BlogSection'), {
    loading: () => <p>Loading...</p>
});

export async function getServerSideProps() {
    const id = "65c114a8bdb7c7bc705c7029"
    const post = await getSinglePostById(id)

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: { post }
    }
}

const Titlegenerator = ({ post }) => {
    const [prompt, setPrompt] = useState("")
    const [language, setLanguage] = useState("english")
    const [blogTitle, setBlogTitle] = useState("")
    const [loading, setLoading] = useState(false)

    const generate = async () => {
        try {
            if (loading) return
            setLoading(true)

            const res = await axios.post("https://api.captionator.creatosaurus.io/captionator/groq", {
                prompt: prompt,
                language: language
            })

            setBlogTitle(res.data.data)
            setLoading(false)
        } catch (error) {
            if (error.response.status === 429) toast.error("You have reached your daily limit for content generation. To continue, please sign up.")
            setLoading(false)
        }
    }

    return (
        <div className={style.titleGeneratorContainerScroll}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>{post.title}</title>
                <meta name='description' content={post.custom_excerpt} />

                {/* Open Graph (OG) Meta Tags */}
                <meta property='og:title' content={post.ogTitle} />
                <meta property='og:image' content={post.ogImg} />
                <meta property='og:description' content={post.ogDis} />
                <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
                <meta property='og:card' content='summary_large_image' />
                <meta property='og:type' content='website' />
                <meta property='og:site_name' content='Creatosaurus' />


                {/* twitter meta tags  */}
                <meta name='twitter:card' content='summary_large_image' />
                <meta name='twitter:site' content='@creatosaurus' />
                <meta name='twitter:title' content={post.twitter_title} />
                <meta name='twitter:description' content={post.twitter_description} />
                <meta name='twitter:image' content={post.twitter_image} />
            </Head >

            <BlogNavigationBar title="All Tools" slug="/" />
            <div className={style.titleGeneratorContainer}>
                <div className={style.head}>
                    <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.8669 23.5748L7.50977 24.0391L7.97405 18.6819L19.1169 7.61051C19.2833 7.44044 19.4819 7.3053 19.7012 7.21303C19.9203 7.12076 20.1558 7.07324 20.3937 7.07324C20.6315 7.07324 20.8671 7.12076 21.0863 7.21303C21.3056 7.3053 21.5042 7.44044 21.6704 7.61051L23.9383 9.89622C24.1056 10.0622 24.2385 10.2597 24.3292 10.4773C24.4199 10.695 24.4665 10.9283 24.4665 11.1641C24.4665 11.3998 24.4199 11.6332 24.3292 11.8508C24.2385 12.0684 24.1056 12.2659 23.9383 12.4319L12.8669 23.5748Z" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M2.0032 7.093C1.37654 6.98396 1.37654 6.08437 2.0032 5.97536C4.27349 5.58037 6.07916 3.85116 6.57195 1.60008L6.60972 1.42753C6.74529 0.808186 7.62716 0.804331 7.76815 1.42247L7.814 1.62356C8.32502 3.86402 10.1312 5.57862 12.3952 5.9725C13.025 6.08207 13.025 6.98627 12.3952 7.09584C10.1312 7.48971 8.32502 9.20432 7.814 11.4448L7.76815 11.6459C7.62716 12.264 6.74529 12.2602 6.60972 11.6408L6.57195 11.4683C6.07916 9.21718 4.27349 7.48796 2.0032 7.093Z" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <h1>{post.title}</h1>
                    <p>{post.custom_excerpt}</p>
                    <div className={style.divider} />

                    <Share quote={post.title} imageURL={post?.ogImg} />
                </div>

                <div className={style.contentGenerator}>
                    <div className={style.imageWrapper}>
                        <Image priority={true} src="https://d1vxvcbndiq6tb.cloudfront.net/73fc75be-b949-4b67-9c4e-aa6bf846777c" alt="effect" width={100} height={100} layout="responsive" />
                    </div>
                    <div className={style.contentWrapper}>
                        <div className={style.leftSide}>
                            <h2>Blog Title Generator</h2>
                            <div className={style.inputWrapper}>
                                <label>Brief *</label>
                                <span>Write 1-2 sentences describing your blog post</span>
                                <textarea
                                    maxLength={400}
                                    value={prompt}
                                    onChange={(e) => setPrompt(e.target.value)}
                                    placeholder='It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem'></textarea>
                            </div>

                            <div className={style.inputWrapper}>
                                <label>Language</label>
                                <span>Output in more than 100+ languages</span>
                                <select
                                    value={language}
                                    onChange={(e) => setLanguage(e.target.value)}>
                                    {
                                        constant.languages.map((data, index) => {
                                            return <option key={index} value={data.toLowerCase()}>{data}</option>
                                        })
                                    }
                                </select>
                            </div>

                            <button onClick={generate}>{loading ? <span className={style.loader} /> : "Generate"}</button>
                            <span className={style.info}>Powered by{" "}
                                <Link href="https://www.creatosaurus.io/">
                                    <a target="_blank">Creatosaurus.io</a>
                                </Link>
                            </span>
                        </div>

                        <div className={style.rightSide} style={blogTitle.trim() === "" ? { display: 'none' } : null}>
                            <div className={style.card}>
                                <p>{blogTitle.split("\n").map((data, index) => {
                                    return <span key={index}>
                                        {data}
                                        <br />
                                    </span>
                                })}</p>
                                <div className={style.circle}>
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.1667 16.6667H8C7.72386 16.6667 7.5 16.4428 7.5 16.1667V8C7.5 7.72386 7.72386 7.5 8 7.5H16.1667C16.4428 7.5 16.6667 7.72386 16.6667 8V16.1667C16.6667 16.4428 16.4428 16.6667 16.1667 16.6667Z" stroke="#404040" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M12.5007 7.49967V3.83301C12.5007 3.55687 12.2768 3.33301 12.0006 3.33301H3.83398C3.55784 3.33301 3.33398 3.55687 3.33398 3.83301V11.9997C3.33398 12.2758 3.55784 12.4997 3.83398 12.4997H7.50065" stroke="#404040" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                            </div>
                            <Link href='https://captionator.creatosaurus.io/'>
                                <a target="_blank">
                                    Try our largest powerful AI model results for Free on Creatosaurus.io
                                    <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.75 7.50033H12.25M12.25 7.50033L7.29167 2.54199M12.25 7.50033L7.29167 12.4587" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <BlogSection post={post} />

                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                    <ArticleByCard post={post} />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Titlegenerator
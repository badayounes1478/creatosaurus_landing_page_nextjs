/** @type {import('next-sitemap').IConfig} */

module.exports = {
    siteUrl: 'https://www.creatosaurus.io/',
    changefreq: 'daily',
    priority: 0.7,
    sitemapSize: 5000,
    generateRobotsTxt: true,
    generateIndexSitemap: false,
    sitemapBaseFileName: 'main-sitemap',
    exclude: ['/exclusive-deal'],
    transform: async (config, path) => {
        if (path.startsWith('/scripts/')) {
            return null;
        }

        if (path.startsWith('/Cookie')) {
            return null;
        }

        if (path.startsWith('/alternatives')) {
            return null;
        }

        if (path.startsWith('/blog-sitemap')) {
            return null
        }


        return {
            loc: path,
            changefreq: config.changefreq,
            priority: config.priority,
            lastmod: config.autoLastmod ? new Date().toISOString() : undefined,
            alternateRefs: config.alternateRefs ?? [],
        }
    },
    robotsTxtOptions: {
        policies: [
            {
                userAgent: '*',
                allow: '/',
            }
        ],
        additionalSitemaps: [
            'https://www.creatosaurus.io/blog-sitemap.xml',
            'https://sitemap.creatosaurus.io/quote.xml',
            'https://sitemap.creatosaurus.io/author.xml',
            'https://sitemap.creatosaurus.io/topic.xml'
        ],
    },
}
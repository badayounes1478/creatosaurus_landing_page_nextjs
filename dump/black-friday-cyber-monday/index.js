import React, { useState, useEffect } from 'react'
import axios from 'axios'
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar'
import HeroSectionNew from '../../ComponentsNew/BlackFridayComponents/heroSection/HeroSectionNew'
import Trusted from '../../ComponentsNew/BlackFridayComponents/trustedSection/Trusted'
import PriceCutSection from '../../ComponentsNew/BlackFridayComponents/priceCutSection/PriceCutSection'
import TeamsSection from '../../ComponentsNew/BlackFridayComponents/teamsSection/TeamsSection'
import RatingComponent from '../../Components/RatingComponent'
import StoryTelling from '../../ComponentsNew/BlackFridayComponents/storyTelling/StoryTelling'
import ToolsInfo from '../../ComponentsNew/BlackFridayComponents/toolsInfo/ToolsInfo'
import BlackSection from '../../ComponentsNew/BlackFridayComponents/blackSection/BlackSection'
import AppIntegrations from '../../ComponentsNew/BlackFridayComponents/AppIntegration/AppIntegrations'
import ReviewSection from '../../ComponentsNew/BlackFridayComponents/reviewSection/ReviewSection'
import TemplateSection from '../../ComponentsNew/BlackFridayComponents/templateSection/TemplateSection'
import RoadMap from '../../ComponentsNew/BlackFridayComponents/roadMap/RoadMap'
import LimitedUserSection from '../../ComponentsNew/BlackFridayComponents/limitedUserSection/LimitedUserSection'
import SupportedBy from '../../Components/LandingPageComponents/SupportedBy'
import FaqSection from '../../ComponentsNew/BlackFridayComponents/FAQSection/FaqSection'
import SecondBlackSection from '../../ComponentsNew/BlackFridayComponents/secondBlackSection/SecondBlackSection'
import Footer from '../../Components/LandingPageComponents/Footer'
import HeroSection from '../../ComponentsNew/BlackFridayComponents/heroSection/HeroSection'
import PricingCards from '../../ComponentsNew/BlackFridayComponents/pricingCards/PricingCards'
import PaymentSuccess from '../../ComponentsNew/CheckOutPopup/PaymentSuccess'
import CheckoutPage1 from '../../ComponentsNew/CheckOutPopup/CheckoutPage1'
import CheckoutPage2 from '../../ComponentsNew/CheckOutPopup/CheckoutPage2'
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth'
import HeadInfo from '../../ComponentsNew/HeadTag/HeadInfo'

export async function getServerSideProps() {
    const res = await axios.get(`https://api.muse.creatosaurus.io/muse/admintemplates/best`)
    const midpoint = Math.floor(res.data.length / 2);

    let templateArray1 = res.data.slice(0, midpoint);
    let templateArray2 = res.data.slice(midpoint);

    return { props: { templateArray1, templateArray2 } }
}


const Index = ({ templateArray1, templateArray2 }) => {
    const [showLogin, setshowLogin] = useState(false)
    const [isYearlyDuration, setIsYearlyDuration] = useState(true)
    const [isIndian, setIsIndian] = useState(false)
    const [selectedPlan, setSelectedPlan] = useState("")
    const [showCheckOut, setShowCheckOut] = useState(false)
    const [showSecondCheckout, setShowSecondCheckout] = useState(false)
    const [paymentComplete, setPaymentComplete] = useState(false)
    const [couponCode, setCouponCode] = useState(null)

    const plans = [{
        color: "#F8F8F9",
        borderColor: "#E8E8E8",
        tag: "For individual",
        showPremium: false,
        planName: "Creatosaurus Free",
        info: "Create anything & turn your ideas into reality at no cost",
        month: 0,
        year: 0,
        buttonName: "Get Started",
        buttonColor: "#808080",
        buttonHover: "#676767",
        planHead: "Features you'll love:",
        showTrial: false,
        hideMonthYear: true,
        features: ["1  Workspace", "2 Brand Kits", "3 Social accounts", "5k/month AI words credits", "20/month AI image credits", "1 GB Cloud storage", "Easy drag-and-drop editor", "Plan and schedule social content", "AI-generated writing and images", "Pro design, video & document editor", "1000+ content types (social posts, videos, docs and more)", "Professionally designed templates", "3M+ stock photos and graphics"]
    }, {
        id: "6247085137b553074fb6b25a",
        color: "#E6F2FF80",
        borderColor: "#CCE4FF",
        tag: "For creator",
        showPremium: false,
        planName: "Creatosaurus Pro",
        info: "Access premium templates, robust content tools and AI features",
        month: isIndian ? "1000" : "25",
        year: isIndian ? "10000" : "250",
        discount: isIndian ? "5000" : "125",
        buttonName: "Get Creatosaurus Pro",
        buttonColor: "#0078FF",
        buttonHover: "#006ce6",
        planHead: "Everything in Free, plus:",
        showTrial: false,
        features: ["2  Workspace", "5 Brand Kits", "5 Social accounts", "30k/month AI words credits", "100/month AI image credits", "10 GB Cloud storage", "Unlimited team members", "All in one social inbox", "Social media analytics", "Unlimited schedule social content", "Quickly resize and translate designs", "Remove backgrounds in a click", "Boost creativity with 20+ AI tools", "Unlimited premium templates", "100M+ photos, videos, graphics, audio", "Online customer support"]
    }, {
        id: "6247081137b553074fb6b258",
        color: "#E6F2FF80",
        borderColor: "#0078FF",
        tag: "For startup",
        showPremium: true,
        planName: "Creatosaurus Teams",
        info: "Enhance collaboration, grow your brand & streamline your workflows.",
        month: isIndian ? "2000" : "50",
        year: isIndian ? "20000" : "500",
        discount: isIndian ? "10000" : "250",
        buttonName: "Get Creatosaurus Teams",
        buttonColor: "#0078FF",
        buttonHover: "#006ce6",
        planHead: "Everything in Pro, plus:",
        showTrial: false,
        features: ["3  Workspace", "10 Brand Kits", "15 Social accounts", "100k/month AI words credits", "250/month AI image credits", "50 GB Cloud storage", "Reports and insights", "Competitor analysis", "Hashtag analysis & management", "Ensure brand consistency with approvals", "Edit, comment, and collaborate in real time", "Generate on brand copy with AI", "Online customer support"]
    }, {
        id: "625fd4c1c7770407d20177fc",
        color: "#E6F2FF80",
        borderColor: "#CCE4FF",
        tag: "For agency",
        showPremium: false,
        planName: "Creatosaurus Agency",
        info: "Empower your clients with an all in one creative workplace solution",
        month: isIndian ? "4000" : "100",
        year: isIndian ? "40000" : "1000",
        discount: isIndian ? "20000" : "500",
        buttonName: "Get Creatosaurus Agency",
        buttonColor: "#0078FF",
        buttonHover: "#006ce6",
        planHead: "Everything in Teams, plus:",
        showTrial: false,
        features: ["10  Workspace", "100 Brand Kits", "50 Social accounts", "300k/month AI words credits", "750/month AI image credits", "250 GB Cloud storage", "Branded Reports", "Brand templates", "Individual approvals and permission by use case", "Org wide Brand Kits and Templates", "Admin Controls", "Scale your brand and centralise assets", "New app integrations", "Priority customer support"]
    }]


    useEffect(() => {
        if (Intl.DateTimeFormat().resolvedOptions().timeZone === "Asia/Calcutta") {
            setIsIndian(true)
        }

        const script = document.createElement('script');
        script.src = 'https://checkout.razorpay.com/v1/checkout.js';
        script.async = true;
        document.body.appendChild(script);
    }, [])


    const changeDuration = (type) => {
        if (type === "yearly") {
            setIsYearlyDuration(true)
        } else {
            setIsYearlyDuration(false)
        }
    }

    const selectPlan = (planDetails) => {
        setSelectedPlan(planDetails)
        checkLogin()
    }

    const closeCheckoutPage1 = () => {
        setShowCheckOut(false)
    }

    const openSecondCheckoutPage = () => {
        setShowCheckOut(false)
        setShowSecondCheckout(true)
    }

    const handlePaymetComplete = () => {
        setPaymentComplete(true)
    }

    const handleSecondCheckoutBack = () => {
        setShowCheckOut(true)
        setShowSecondCheckout(false)
    }

    const getCookie = (name) => {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const checkLogin = () => {
        const token = getCookie("Xh7ERL0G")
        if (token) {
            setShowCheckOut(true)
        } else {
            setshowLogin(true)
        }
    }

    const changeUserLoggedIn = () => {
        setShowCheckOut(true)
        setshowLogin(false)
    }


    const handleCode = (value) => {
        setCouponCode(value)
    }


    return (
        <div style={{ maxWidth: 1600, margin: 'auto' }}>
            <HeadInfo />
            {
                paymentComplete && <PaymentSuccess
                    selectedPlan={selectedPlan}
                    close={() => setPaymentComplete(false)} />
            }

            {
                showCheckOut &&
                <CheckoutPage1
                    isYearlyDuration={isYearlyDuration}
                    isIndian={isIndian}
                    exclusiveDeal={true}
                    selectedPlan={selectedPlan}
                    handleCode={handleCode}
                    couponCode={couponCode}
                    changeDuration={changeDuration}
                    next={openSecondCheckoutPage}
                    close={closeCheckoutPage1} />
            }

            {
                showSecondCheckout &&
                <CheckoutPage2
                    isYearlyDuration={isYearlyDuration}
                    isIndian={isIndian}
                    exclusiveDeal={true}
                    back={handleSecondCheckoutBack}
                    selectedPlan={selectedPlan}
                    couponCode={couponCode}
                    handlePaymetComplete={handlePaymetComplete}
                    close={() => setShowSecondCheckout(false)} />
            }

            {showLogin ? <NavigationAuth changeUserLoggedIn={changeUserLoggedIn} close={() => setshowLogin(false)} /> : null}
            <NavigationBar />
            <HeroSection checkLogin={checkLogin} plans={plans} selectPlan={selectPlan} />
            <HeroSectionNew hideTitles={true} />
            <Trusted />
            <PriceCutSection />
            <TeamsSection />
            <RatingComponent />
            <PricingCards plans={plans} isYearlyDuration={isYearlyDuration} isIndian={isIndian} selectPlan={selectPlan} />
            <StoryTelling />
            <ToolsInfo />
            <BlackSection />
            <AppIntegrations />
            <ReviewSection />
            <TemplateSection templateArray1={templateArray1} templateArray2={templateArray2} />
            <RoadMap />
            <LimitedUserSection />
            <SupportedBy />
            <FaqSection />
            <SecondBlackSection />
            <Footer />
        </div>
    )
}

export default Index
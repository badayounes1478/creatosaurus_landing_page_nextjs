import Head from 'next/head';
import Link from 'next/link';
import Footer from '../../Components/LandingPageComponents/Footer';
import BlogStyle from '../../styles/Blog.module.css';
import { useState } from 'react';
import BlogCard2 from '../../Components/BlogCard2';
import NewsLetterCard from '../../Components/NewsLetterCard';
import BlogNavigationBar from '../../Components/BlogNavigationBar';
import Image from 'next/image';
import axios from 'axios';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import SubscribeNewsLetter from '../../Components/SubscribeNewsLetter';
import NoImage from '../../public/Assets/NoImage.jpeg'

export const getServerSideProps = async () => {
    const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=tags,title,slug,authors&limit=15&page=1`);
    const blogs = response.data.posts.map(data => {
        delete data.html
        return data
    })

    if (!blogs) {
        return {
            notFound: true,
        };
    }
    return {
        props: {
            blogs,
        },
    };
};

export default function Blog({ blogs }) {
    const [button, setbutton] = useState(1);
    const [pageNum, setPageNum] = useState(2);
    const [hasMore, setHasMore] = useState(true);
    const [posts, setPosts] = useState(blogs);
    const [blogPage, setBlogPage] = useState({
        pageArray: ["page"]
    });
    const [search, setSearch] = useState("")
    const [searchBlogs, setSearchBlogs] = useState([])


    const handleOnChange = (e) => {
        let value = e.target.value
        let filteredBlogs = blogs.filter((blog) => blog.title.toLowerCase().includes(value?.trim().toLowerCase()));
        setSearchBlogs(filteredBlogs)
        setSearch(value)
    }

    const month = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sept',
        'Oct',
        'Nov',
        'Dec',
    ];


    const getMoreBlogs = async () => {
        const response = await axios.get(`https://ahijftavqd.creatosaurus.io/ghost/api/content/posts/?key=7f2eea060c7f0bd6d9d87ab0bd&include=tags,title,slug,authors&page=${pageNum}`);
        if (response.data.meta.pagination.pages === pageNum) {
            setHasMore(false);
        }
        setPosts([...posts, ...response.data.posts]);
    };

    // adding undefined to array at position 3 to display newsletter component
    const insert = (arr, index, ...newItems) => [
        // part of the array before the specified index
        ...arr.slice(0, index),
        // inserted items
        ...newItems,
        // part of the array after the specified index
        ...arr.slice(index)
    ];


    const handleClick = () => {
        if (hasMore) {
            setBlogPage(prevState => ({
                pageArray: [...prevState.pageArray, "page"]
            }));
            setPageNum((no) => no + 1);
            getMoreBlogs();
        }
    };

    const filterBlogByCategories = (value) => {
        if (value === "All Categories") {
            setPosts(blogs)
        } else if (value === "Social Media marketing") {
            const filteredBlogs = blogs.filter(({ tags }) => tags?.[0]?.name === "Social Media Marketing");
            if (filteredBlogs.length === 0) {
                setPosts(blogs)
            } else {
                setPosts(filteredBlogs)
            }
        } else if (value === "Content marketing") {
            const filteredBlogs = blogs.filter(({ tags }) => tags?.[0]?.name === "Content marketing");
            if (filteredBlogs.length === 0) {
                setPosts(blogs)
            } else {
                setPosts(filteredBlogs)
            }
        } else if (value === "Content Creators") {
            const filteredBlogs = blogs.filter(({ tags }) => tags?.[0]?.name === "Content Creators");
            if (filteredBlogs.length === 0) {
                setPosts(blogs)
            } else {
                setPosts(filteredBlogs)
            }
        } else if (value === "Getting Started") {
            const filteredBlogs = blogs.filter(({ tags }) => tags?.[0]?.name === "Getting Started");
            if (filteredBlogs.length === 0) {
                setPosts(blogs)
            } else {
                setPosts(filteredBlogs)
            }
        }
    }



    return (
        <div className={BlogStyle.BlogSection}>
            <div>
                <Head>
                    <title>
                        Creatosaurus - All in One Creator Stack | Storytelling at Scale
                    </title>
                    <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                </Head>
                <main>
                    <BlogNavigationBar handleOnChange={handleOnChange} setSearch={setSearch} />

                    {search !== "" ? <div>
                        <br /> <br />
                    </div> : <Link href='/blog/[slug]' as={`/blog/${posts[0].slug}`} passHref>

                        <div className={BlogStyle.FirstPost}>
                            <div className={BlogStyle.DisplayImage}>
                                <Image
                                    src={posts[0].feature_image || NoImage}
                                    width={100}
                                    height={70}
                                    layout='responsive'
                                    alt=''
                                    loading='lazy'
                                />
                            </div>
                            <div>
                                <Link href='/blog/tag/[tags]' as={`/blog/tag/${posts[0].primary_tag?.slug}`}>
                                    <div className={BlogStyle.Tag}>
                                        {posts[0].primary_tag === null
                                            ? null
                                            : posts[0].primary_tag.name}
                                    </div>
                                </Link>
                                <h2>
                                    {posts[0].title}
                                </h2>
                                <p>{posts[0].excerpt}</p>
                                <div className={BlogStyle.PublishInfo}>
                                    <span>
                                        {month[new Date(posts[0].updated_at).getMonth()]}{' '}
                                        {new Date(posts[0].updated_at).getDate()},{' '}
                                        {new Date(posts[0].updated_at).getFullYear()}
                                    </span>
                                    <span>{posts[0].reading_time} min read</span>
                                    <Link href='/blog/author/[author]' as={`/blog/author/${posts[0].primary_author.slug}`}>
                                        <span style={{
                                            marginLeft: "20px"
                                        }} className={BlogStyle.author}>{posts[0].primary_author.name}</span>
                                    </Link>
                                </div>
                            </div>
                        </div>

                    </Link>}

                    {search !== "" ? null : <div className={BlogStyle.ButtonContainer}>
                        <Link href='/blog' as='/blog'>
                            <button
                                className={button === 1 ? BlogStyle.Active : null}
                            >
                                All Categories
                            </button>
                        </Link>
                        <Link href='/blog/tag/[tags]' as={`/blog/tag/creator-digest`} passHref>
                            <button>
                                Creator Digest
                            </button>
                        </Link>

                        <Link href='/blog/tag/[tags]' as={`/blog/tag/getting-started`} passHref>
                            <button>
                                Getting Started
                            </button>
                        </Link>

                        <Link href='/blog/tag/[tags]' as={`/blog/tag/social-media-marketing`} passHref>
                            <button >
                                Social Media Marketing
                            </button>
                        </Link>
                    </div>}

                    {search !== "" ? <div className={BlogStyle.Grid}>
                        {searchBlogs.map((post, index) => {
                            return <BlogCard2 key={post.slug} post={post} month={month} />;
                        })}</div> : <div className={BlogStyle.Grid}>

                        {posts.length < 3 ? null : insert(posts, 3, undefined).map((post, index) => {
                            if (index === 0) return null;
                            if (index === 3) return search !== "" ? null : <NewsLetterCard key={index} />;

                            return <BlogCard2 key={post.slug} post={post} month={month} />;
                        })}
                    </div>}

                    {search !== "" ? null : <div className={BlogStyle.paginationDiv}>
                        {blogPage.pageArray.map((page, index) => {
                            return (
                                <button style={{ backgroundColor: "#ff4359", color: "#ffffff" }} key={index}>{index + 1}</button>
                            );
                        })}
                        {hasMore ? <button onClick={handleClick}>&raquo;</button> : null}
                    </div>}
                </main>``
            </div >
            <SubscribeNewsLetter />
            <FifthBlackSection />
            <Footer />
        </div >
    );
}

